<?php

    // configuration
    require("includes/config.php");

    // if form was submitted
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {	
		if ($_POST['tid'] == 0)
			apologize("Tem de escolher um trabalhador da lista!");
		
		// verifica se o formulário não foi adulterado com SQL injections
		$sql_tid = "SELECT * FROM trabalhador WHERE tid = ? AND eid = ?";
		$sql_raid = "SELECT * FROM registo_acidente WHERE ra_id = ? AND eid = ?";
				
		$rows_tid = query($sql_tid, $_POST['tid'], $_SESSION['cur_eid']);
		$rows_raid = query($sql_raid, $_POST['ra_id'], $_SESSION['cur_eid']);
				
		if ((($rows_tid === false) || empty($rows_tid)) || (($rows_raid === false) || empty($rows_raid)))
			apologize("Os dados para associar o trabalhador ao registo foram corrompidos.");
		else
		{
			$sql = "INSERT INTO `trabalhador-to-reg_acidente`
			(tid, ra_id) 
			VALUES (?, ?)";
			
			if (query($sql, $_POST['tid'], $_POST['ra_id']) === false)
				apologize("Algo correu mal ao associar o trabalhador ao registo de acidente na base de dados.");
			else
				redirect("regacidente.php?id=" . $_POST['ra_id']);
		}
	}
	else
		apologize("Algo correu mal ao associar o trabalhador ao registo de acidente");
?>