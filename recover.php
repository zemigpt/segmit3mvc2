
<?php

    // configuration
    require("includes/config.php");

    // email
    require_once("PHPMailer/class.phpmailer.php");

    // if form was submitted
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        if  (empty($_POST["email"]))
        {
            apologize("You must provide your email.");
        }
        else
        {
            // if none of the above occurred, odds are everything is ok
            // so insert new user in database
            if (query("INSERT INTO users (username, password, real_name, email, birthday, gender) VALUES(?, ?, ?, ?, ?)", $_POST["username"], crypt($_POST["password"]), $_POST["real_name"], $_POST["email"], $_POST["birthday"], $_POST["gender"] ) === false)
            {
                apologize("Something went wrong... Perhaps that username already exists.");
            }
            else
            {
				// if query returns true then new user registration was successull. 
				// login user 
				
				$rows = query("SELECT LAST_INSERT_ID() AS id");
				$id = $rows[0]["id"];
				
				// remember that user's now logged in by storing user's ID in session
				$_SESSION["id"] = $id;

                // redirect to homepage
                redirect("/");
				
			}
            
            
            
        }
    }
    else
    {
        // else render form
        render("register_form.php", ["title" => "Register"]);
    }

?>

