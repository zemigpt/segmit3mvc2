<?php

    // configuration
    require_once("includes/config.php");

	
	
	/************************************************************** 
	 *
	 * Trata dos casos em que o ficheiro é usado com $_POST
	 *
	 *************************************************************/
	 
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        
		// verifica se estamos a editar um trabalhador existente ou a adicionar um novo
		if ($_POST['action']=="edit")
		{
			if (isset($_POST['tm_id']))
			{
				
					
				$sql = "UPDATE tipo_maquina 
						SET nome = ? 
						WHERE tm_id = ? AND eid = ?";
				
					
				if (query($sql, $_POST['tmaquina_nome'], $_POST['tm_id'], $_SESSION['cur_eid'] ) === false)
				{
					apologize("Algo correu mal ao editar o tipo de máquina na base de dados.");
				}
				else
				{

									
					// redirect to homepage
					redirect("tmaquina.php?id=" . $_POST['tm_id']);
					
				}
			}
			// não temos for_id para identificar o trabalhador - aborta
			else
				apologize("Tipo de máquina não identificado - impossível editar");
			
		
		}
		elseif 	($_POST['action']=="add")
		{
			  
            $sql = "INSERT INTO tipo_maquina 
					(nome, eid) 
					VALUES(?, ?)";           
            
            if (query($sql, $_POST['tmaquina_nome'], $_SESSION['cur_eid'] ) === false)
            {
                apologize("Algo correu mal ao inserir o novo tipo de máquina na base de dados.");
            }
            else
            {

				
				$rows = query("SELECT LAST_INSERT_ID() AS tm_id");
				
				$tm_id = $rows[0]["tm_id"];
								
				// redirect to homepage
                redirect("tmaquina.php?id=" . $tm_id);
				
			}
        }  
        elseif 	($_POST['action']=="delete")
		{
			$sql ="DELETE FROM tipo_maquina WHERE tm_id = ? AND eid = ?";
			query($sql, $_POST['tm_id'], $_SESSION['cur_eid']);
			// redirect to lista de registo de acidentes
            redirect("tmaquina.php");
		}
		// não foi especificada acção no formulário - aborta
		else
			apologize("Não foi especificada acção no formulário...");
    }
	
	
	
	
	/************************************************************** 
	 *
	 * Trata dos casos em que o ficheiro é usado com $_GET
	 *
	 *************************************************************/
	 
    else
    {
		//se for especificada uma acção no URL
		if (isset($_GET['action']))
		{
			// caso o utilizador queira editar um trabalhador
			if ($_GET['action'] == 'edit')
			{
				if (isset($_GET['id']))
				{
					$tm_id = $_GET['id']; 
					
					$tmaquinas  = query("SELECT * FROM tipo_maquina WHERE tm_id = ? AND eid = ?", $tm_id, $_SESSION['cur_eid']);
					
					// se existe trabalhador com este tid
					if (!empty($tmaquinas[0])) 
					{
						$tmaquina = $tmaquinas[0];
						//constroi as várias queries necessárias
						
					
					}
					
					// a query devolveu uma lista vazia
					else 
						apologize("O tipo de máquina especificado não existe...");
				
					render("tmaquina_form_editar.php", array("title" => "Editar Tipo Máquina", "tmaquina" => $tmaquina));
				}
				
				// o utilizador quer editar, mas falta o tid no URL, logo não sabemos qual é o trabalhador a editar
				else 
					apologize("Não foi especificado qual o tipo de máquina a editar.");
			}
			
			// caso o utilizador queira adicionar um trabalhador
			elseif ($_GET['action'] == 'add')
			{
				 render("tmaquina_form_adicionar.php", array("title" => "Adicionar Tipo de Máquina"));
			}
			
			// caso o utilizador queira apagar um trabalhador
			elseif ($_GET['action'] == 'delete')
			{
				if (isset($_GET['id']))
				{
					$tm_id = $_GET['id']; 
					
					$tmaquinas  = query("SELECT * FROM tipo_maquina WHERE tm_id = ? AND eid = ?", $tm_id, $_SESSION['cur_eid']);
					
					// se existe trabalhador com este tid
					if (!empty($tmaquinas[0])) 
					{
						$tmaquina = $tmaquinas[0];
					}
					
					// a query devolveu uma lista vazia
					else 
						apologize("Remover tipo de máquina - registo especificado não existe...");
				
					render("tmaquina_form_remover.php", array("title" => "Remover Tipo de Máquina", "tmaquina" => $tmaquina));
				}
				
				// o utilizador quer editar, mas falta o tid no URL, logo não sabemos qual é o trabalhador a editar
				else 
					apologize("Não foi especificado qual o tipo de máquian a remover.");
			}
			
			else
				apologize("Operação não suportada.");
		
		}		
		
		
		//se não houver acção especificada no URL, mas houver um tid
		elseif (isset($_GET['id'])) 
		{
			$tm_id = $_GET['id']; 
			
			//constroi as várias queries necessárias
			$tmaquinas  = query("SELECT * FROM tipo_maquina WHERE tm_id = ? AND eid = ?", $tm_id, $_SESSION['cur_eid']);
			
			$sql_formacoes = "Select
			  tipo_maquina.tm_id As tm_id,
			  tipo_maquina.eid As eid,
			  tipo_maquina.nome As nome,
			  formacao.for_id As for_id,
			  formacao.tema As tema,
			  formacao.duracao As duracao
			From
			  tipo_maquina Inner Join
			  `formacao-to-tipomaquina`
				On tipo_maquina.tm_id = `formacao-to-tipomaquina`.tm_id Left Join
			  formacao On `formacao-to-tipomaquina`.for_id = formacao.for_id
			Where
			  tipo_maquina.tm_id = ? AND tipo_maquina.eid = ?";
			  
			$sql_todas_formacoes ="Select
			  formacao.for_id As for_id,
			  formacao.eid As eid,
			  formacao.tema As tema,
			  tipo_maquina.tm_id As tm_id
			From
			  formacao Left Join
			  (Select * From `formacao-to-tipomaquina` Where `formacao-to-tipomaquina`.tm_id = ?) tipo_maquina
				On formacao.for_id = tipo_maquina.for_id 
			Where
			  (tipo_maquina.tm_id IS NULL) 
			   And formacao.eid = ?
			Order By formacao.for_id ASC";
									
			if (!empty($tmaquinas[0])) // se há trabalhadores com este tid
			{
				// o tmaquina que queremos está na linha 0
				$tmaquina = $tmaquinas[0];
				
				// dado que o tmaquina existe podemos executar as outras queries
				$formacoes = query($sql_formacoes, $tm_id, $_SESSION['cur_eid']);
				$todas_formacoes = query($sql_todas_formacoes, $tm_id, $_SESSION['cur_eid']);
				
				// mostra o tmaquina
				render("tmaquina_visualizar.php", array("title" => "Tipo de Máquina", "tmaquina" => $tmaquina, "tm_id" => $tm_id, "formacoes" => $formacoes, "todas_formacoes" => $todas_formacoes));
			}
			else // a query devolveu uma lista vazia
				apologize("O tipo de máquina especificado não existe...");	
		}
		
		
		
		// se não for especificado o id do tmaquina no URL, mostra lista de trabalhadores
		else
		{
			$tmaquinas = query("SELECT * FROM tipo_maquina WHERE eid = ?", $_SESSION['cur_eid']);
			render("tmaquina_lista.php", array("title" => "Lista de Tipo de Máquina", "tmaquinas" => $tmaquinas));
		}
		
    }

?>

