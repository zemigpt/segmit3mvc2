<?php

    // configuration
    require("includes/config.php");

    // if form was submitted
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {	
		if ($_POST['ptid'] == 0)
			apologize("Tem de escolher um posto de trabalho da lista!");
		
		// verifica se o formulário não foi adulterado com SQL injections
		$sql_ptrab = "SELECT * FROM `posto-trabalho` WHERE ptid = ? AND eid = ?";
		$sql_tid = "SELECT * FROM trabalhador WHERE tid = ? AND eid = ?";
		
		$rows_ptrab = query($sql_ptrab, $_POST['ptid'], $_SESSION['cur_eid']);
		$rows_tid = query($sql_tid, $_POST['tid'], $_SESSION['cur_eid']);
		
		if ((($rows_ptrab === false) || empty($rows_ptrab)) || (($rows_tid === false) || empty($rows_tid)))
			apologize("Os dados para associar o posto de trabalho ao trabalhador foram corrompidos.");
		else
		{
			$sql = "INSERT INTO `posto-to-trabalhador`
			(ptid, tid) 
			VALUES (?, ?)";
			
			if (query($sql, $_POST['ptid'], $_POST['tid']) === false)
				apologize("Algo correu mal ao associar o posto de trabalho ao trabalhador na base de dados.");
			else
				redirect("trabalhador.php?id=" . $_POST['tid']);
		}
	}
	else
		apologize("Algo correu mal ao associar o posto de trabalho ao trabalhador");
?>