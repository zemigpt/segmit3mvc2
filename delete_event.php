<?php

    // configuration
    require("includes/config.php");

    if (isset($_POST["event_id"]))
        $event_id = $_POST["event_id"];

    // if form was submitted
    if ($_SERVER["REQUEST_METHOD"] == "POST" && !isset($_POST["formname"]))
    {
        
        if (empty($_POST["delete_confirmation"]))
        {
            apologize("Delete confirmation not received. Something went wrong...");
            exit();
        } 
        elseif (empty($_POST["event_id"]))
        {
            apologize("Post id not received. Something went wrong...");
            exit();
        }
        
        else
        {
            // if none of the above occurred, odds are everything is ok
            // so try to insert new event in database
            
            $query_string = "DELETE FROM event WHERE event_id = ?";
            //dump($query_string);
            if (query($query_string, $_POST["event_id"] ) === false)
            {
                apologize("Failed to delete from Database. Something went wrong...");
            }
            else
            {
				// if query returns true then delete event was successull. 
								            
                // let's now delete the pictures from that event, if any exist
                
                $query_string = "SELECT * FROM event_images WHERE event_id = ?";
                if (query($query_string, $_POST["event_id"] ) === false)
                {
                    apologize("Failed to delete event images from Database. Something went wrong...");
                }
                         
                
                
                // redirect to homepage
                redirect("/");
				
			}
            
        }
    }
    else
    {
                        
        $rows = query("SELECT * FROM event LEFT JOIN event_types ON event.event_type = event_types.event_type WHERE event_id = ?", $event_id);
        $row = $rows[0];
        
        
        // get images 
        $event_images = query("SELECT * FROM event_images WHERE event_id = ?", $row["event_id"]);
           
        // construct variable to pass
        $event = array([
            "event_id" => $row["event_id"],
            "event_date" => $row["event_date"],
            "event_title" => $row["event_title"],
            "event_description" => $row["event_description"],
            "event_type_value" => $row["event_type_value"],
            "event_type_description" => $row["event_type_description"],
            "images" => $event_images
            ]);
                    
               
        // render form
        render("delete_event_form.php", array("title" => "Delete post", "event" => $event));
    }

?>

