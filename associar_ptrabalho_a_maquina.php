<?php

    // configuration
    require("includes/config.php");

    // if form was submitted
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {	
		if ($_POST['ptid'] == 0)
			apologize("Tem de escolher um posto de trabalho da lista!");
		
		// verifica se o formulário não foi adulterado com SQL injections
		$sql_ptrab = "SELECT * FROM `posto-trabalho` WHERE ptid = ? AND eid = ?";
		$sql_mid = "SELECT * FROM maquina WHERE mid = ? AND eid = ?";
		
		$rows_ptrab = query($sql_ptrab, $_POST['ptid'], $_SESSION['cur_eid']);
		$rows_mid = query($sql_mid, $_POST['mid'], $_SESSION['cur_eid']);
		
		if ((($rows_ptrab === false) || empty($rows_ptrab)) || (($rows_mid === false) || empty($rows_mid)))
			apologize("Os dados para associar o posto de trabalho à maquina foram corrompidos.");
		else
		{
			$sql = "INSERT INTO `maquina-to-posto_trabalho`
			(mid, ptid) 
			VALUES (?, ?)";
			
			if (query($sql, $_POST['mid'], $_POST['ptid']) === false)
				apologize("Algo correu mal ao associar o posto de trabalho à maquina na base de dados.");
			else
				redirect("maquina.php?id=" . $_POST['mid']);
		}
	}
	else
		apologize("Algo correu mal ao associar o posto de trabalho à maquina");
?>