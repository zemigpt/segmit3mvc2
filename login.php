<?php

    // configuration
    require_once("includes/config.php"); 

    // if form was submitted
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        // validate submission
        if (empty($_POST["username"]))
        {
            apologize("You must provide your username.");
        }
        else if (empty($_POST["password"]))
        {
            apologize("You must provide your password.");
        }

        // query database for user
        $rows = query("SELECT * FROM users WHERE username = ?", $_POST["username"]);

        // if we found user, check password
        if (count($rows) == 1)
        {
            // first (and only) row
            $row = $rows[0];

            $userhash = $row["password"];
            $hash = crypt($_POST["password"], $row["password"]);
            $userid = $row["uid"];
			            
            // compare hash of user's input against hash that's in database
            if ($hash == $userhash)
            {
                // remember that user's now logged in by storing user's ID in session
                $_SESSION["id"] = $userid ;
				$_SESSION["username"] = $row["username"];
				
				// update table 'users' with the last_login timestamp
				query("UPDATE users SET last_login=now() WHERE uid = $userid");
				
				// get list of empresa the user belongs to and save it in a session variable
				$rows = query("SELECT empresa.eid, empresa.nome FROM `empresa-to-users` LEFT JOIN users On users.uid = `empresa-to-users`.uid LEFT JOIN empresa On empresa.eid = `empresa-to-users`.eid WHERE users.uid = ?", $_SESSION["id"]);
				
				$empresas = array();
				
				// guarda as tabs das empresas deste user nesta vari�vel global
				global $user_tabs;
				
				foreach ($rows as $row)
				{
					
					$empresas[] = array(
									'eid' => $row['eid'],
									'nome' => $row['nome'],
								);
					
					// acrescenta esta empresa �s tabs de empresas deste user
					$user_tabs = $user_tabs . "<li><a href='dashboard_empresa.php?eid=" . $row['eid'] . "'>" . $row['nome'] . "</a></li>\n";
				}
				
				$_SESSION["eid"] = $empresas;

				// se o user s� tiver uma empresa n�o vale a pena chatear e perguntar qual � a empresa que ele quer. Redireccionar logo para a empresa dele.
				if (count($empresas) == 1)
				{
					$_SESSION['cur_eid'] = $empresas[0]['eid'];
					redirect("index.php?eid=" . $_SESSION['cur_eid']);
				}
				//dump($user_tabs);
				
                // redirect to homepage
                redirect("index.php");
            }
        }

        // else apologize
        
        apologize("Invalid username and/or password.");
    }
    else
    {
        // else render form
        render("login_form.php", array("title" => "Log In"));
    }

?>
