<?php

    // configuration
    require("includes/config.php");

    // if form was submitted
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {	
		if ($_POST['for_id'] == 0)
			apologize("Tem de escolher uma formação da lista!");
		
		// verifica se o formulário não foi adulterado com SQL injections
		$sql_form = "SELECT * FROM formacao WHERE for_id = ? AND eid = ?";
		$sql_tid = "SELECT * FROM trabalhador WHERE tid = ? AND eid = ?";
		
		$rows_form = query($sql_form, $_POST['for_id'], $_SESSION['cur_eid']);
		$rows_tid = query($sql_tid, $_POST['tid'], $_SESSION['cur_eid']);
		
		if ((($rows_form === false) || empty($rows_form)) || (($rows_tid === false) || empty($rows_tid)))
			apologize("Os dados para associar a formação ao trabalhador foram corrompidos.");
		else
		{
			$sql = "INSERT INTO `formacao-to-trabalhador`
			(for_id, tid) 
			VALUES (?, ?)";
			
			if (query($sql, $_POST['for_id'], $_POST['tid']) === false)
				apologize("Algo correu mal ao associar a formação ao trabalhador na base de dados.");
			else
				redirect("trabalhador.php?id=" . $_POST['tid']);
		}
	}
	else
		apologize("Algo correu mal ao associar a formação ao trabalhador");
?>