<?php

    // configuration
    require("includes/config.php");

    // if form was submitted
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        /**************
		*
		* Validation
		*
		*****************/
		
		/*if (empty($_POST["field"]))
        {
            apologize("O campo 'field' � obrigat�rio!");
        }
        
        else
        {*/
            // if none of the above occurred, odds are everything is ok
            // so insert new record in database
                   
           
		    // grava o ficheiro
            // code adapted from http://www.w3schools.com/php/php_file_upload.asp
			
			if (!empty($_FILES["posto_foto"])){
				$upload_result = UploadFile($_FILES["posto_foto"], "image");
				if ($upload_result != "ERR")
				{
					$foto_url = $upload_result;
				}
				else
				{
					$foto_url = "";
				}
			}			
			else
			{
				$foto_url = "";
			}			
            
            
            $sql = "INSERT INTO `posto-trabalho` 
					(nome, foto, eid) 
					VALUES (?, ?, ?)";
            
            if (query($sql, $_POST['posto_nome'], $foto_url, $_SESSION['cur_eid'] ) === false)
            {
                apologize("Algo correu mal ao inserir o novo posto de trabalho na base de dados.");
            }
            else
            {
				$rows = query("SELECT LAST_INSERT_ID() AS ptid");
				
				$ptid = $rows[0]["ptid"];

				// insere os v�rios agentes
				
				// Ruido
				if (!empty($_POST["afar_data_execucao"])) 
				{
					
					$n=count($_POST["afar_data_execucao"]);
					for ($i=0; $i<$n; $i++)
					{
						$sql = "INSERT INTO ag_fisico_aval_ruido 
								(valor_registado, data_execucao, data_validade, entidade_executante, relatorio_avaliacao, eid)
								VALUES (?, ?, ?, ?, ?, ?)";
						
						// Determina URL do relat�rio carregado
						if (!empty($_FILES["afar_relatorio"])){
							$files_afar = reorderFILES($_FILES["afar_relatorio"]);
							
							$upload_result = UploadFile($files_afar[$i], "pdf");
							if ($upload_result != "ERR")
							{
								$url_relatorio = $upload_result;
							}
							else
							{
								$url_relatorio = "";
							}
						}			
						else
						{
							$url_relatorio = "";
						}			
						
						// faz a query e verifica se corre bem
						if (query($sql, $_POST["afar_valor_registado"][$i], $_POST["afar_data_execucao"][$i], $_POST["afar_data_validade"][$i], $_POST["afar_entidade_executante"][$i], $url_relatorio, $_SESSION['cur_eid'] ) === false)
						{
							apologize("Algo correu mal ao inserir o novo posto de trabalho na base de dados. - APT:77");
						}
						else
						{
							// se correu bem, insere na tabela de rela��o
							$rows = query("SELECT LAST_INSERT_ID() AS afar_id");
							$afar_id = $rows[0]["afar_id"];
							
							$sql = "INSERT INTO `posto-to-ag_fisico_aval_ruido`  
									(ptid, afar_id) 
									VALUES (?, ?)";
								
							if (query($sql, $ptid, $afar_id ) === false)
							{
								apologize("Algo correu mal ao inserir o novo posto de trabalho na base de dados. - APT:90");
							}
						
						}
					}
				
				}
				
				
				// Termico
				if (!empty($_POST["afat_data_execucao"])) 
				{
					
					$n=count($_POST["afat_data_execucao"]);
					for ($i=0; $i<$n; $i++)
					{
						$sql = "INSERT INTO ag_fisico_aval_termico 
								(valor_registado, data_execucao, data_validade, entidade_executante, relatorio_avaliacao, eid)
								VALUES (?, ?, ?, ?, ?, ?)";
						
						// Determina URL do relat�rio carregado
						if (!empty($_FILES["afat_relatorio"])){
							$files_afar = reorderFILES($_FILES["afat_relatorio"]);
							
							$upload_result = UploadFile($files_afar[$i], "pdf");
							if ($upload_result != "ERR")
							{
								$url_relatorio = $upload_result;
							}
							else
							{
								$url_relatorio = "";
							}
						}			
						else
						{
							$url_relatorio = "";
						}			
						
						// faz a query e verifica se corre bem
						if (query($sql, $_POST["afat_valor_registado"][$i], $_POST["afat_data_execucao"][$i], $_POST["afat_data_validade"][$i], $_POST["afat_entidade_executante"][$i], $url_relatorio, $_SESSION['cur_eid'] ) === false)
						{
							apologize("Algo correu mal ao inserir o novo posto de trabalho na base de dados. - APT:151");
						}
						else
						{
							// se correu bem, insere na tabela de rela��o
							$rows = query("SELECT LAST_INSERT_ID() AS afat_id");
							$afar_id = $rows[0]["afat_id"];
							
							$sql = "INSERT INTO `posto-to-ag_fisico_aval_termico`  
									(ptid, afat_id) 
									VALUES (?, ?)";
								
							if (query($sql, $ptid, $afat_id ) === false)
							{
								apologize("Algo correu mal ao inserir o novo posto de trabalho na base de dados. - APT:165");
							}
						
						}
					}
				
				}
				
				
				// Iluminancia
				if (!empty($_POST["afai_data_execucao"])) 
				{
					
					$n=count($_POST["afai_data_execucao"]);
					for ($i=0; $i<$n; $i++)
					{
						$sql = "INSERT INTO ag_fisico_aval_ilum 
								(valor_registado, data_execucao, data_validade, entidade_executante, relatorio_avaliacao, eid)
								VALUES (?, ?, ?, ?, ?, ?)";
						
						// Determina URL do relat�rio carregado
						if (!empty($_FILES["afai_relatorio"])){
							$files_afar = reorderFILES($_FILES["afai_relatorio"]);
							
							$upload_result = UploadFile($files_afar[$i], "pdf");
							if ($upload_result != "ERR")
							{
								$url_relatorio = $upload_result;
							}
							else
							{
								$url_relatorio = "";
							}
						}			
						else
						{
							$url_relatorio = "";
						}			
						
						// faz a query e verifica se corre bem
						if (query($sql, $_POST["afai_valor_registado"][$i], $_POST["afai_data_execucao"][$i], $_POST["afai_data_validade"][$i], $_POST["afai_entidade_executante"][$i], $url_relatorio, $_SESSION['cur_eid'] ) === false)
						{
							apologize("Algo correu mal ao inserir o novo posto de trabalho na base de dados. - APT:207");
						}
						else
						{
							// se correu bem, insere na tabela de rela��o
							$rows = query("SELECT LAST_INSERT_ID() AS afai_id");
							$afar_id = $rows[0]["afai_id"];
							
							$sql = "INSERT INTO `posto-to-ag_fisico_aval_ilum`  
									(ptid, afai_id) 
									VALUES (?, ?)";
								
							if (query($sql, $ptid, $afai_id ) === false)
							{
								apologize("Algo correu mal ao inserir o novo posto de trabalho na base de dados. - APT:221");
							}
						
						}
					}
				
				}
				
				
				// Vibra��es
				if (!empty($_POST["afav_data_execucao"])) 
				{
					
					$n=count($_POST["afav_data_execucao"]);
					for ($i=0; $i<$n; $i++)
					{
						$sql = "INSERT INTO ag_fisico_aval_vibracoes 
								(valor_registado, data_execucao, data_validade, entidade_executante, relatorio_avaliacao, eid)
								VALUES (?, ?, ?, ?, ?, ?)";
						
						// Determina URL do relat�rio carregado
						if (!empty($_FILES["afav_relatorio"])){
							$files_afar = reorderFILES($_FILES["afav_relatorio"]);
							
							$upload_result = UploadFile($files_afar[$i], "pdf");
							if ($upload_result != "ERR")
							{
								$url_relatorio = $upload_result;
							}
							else
							{
								$url_relatorio = "";
							}
						}			
						else
						{
							$url_relatorio = "";
						}			
						
						// faz a query e verifica se corre bem
						if (query($sql, $_POST["afav_valor_registado"][$i], $_POST["afav_data_execucao"][$i], $_POST["afav_data_validade"][$i], $_POST["afav_entidade_executante"][$i], $url_relatorio, $_SESSION['cur_eid'] ) === false)
						{
							apologize("Algo correu mal ao inserir o novo posto de trabalho na base de dados. - APT:207");
						}
						else
						{
							// se correu bem, insere na tabela de rela��o
							$rows = query("SELECT LAST_INSERT_ID() AS afav_id");
							$afar_id = $rows[0]["afav_id"];
							
							$sql = "INSERT INTO `posto-to-ag_fisico_aval_vibracoes`  
									(ptid, afav_id) 
									VALUES (?, ?)";
								
							if (query($sql, $ptid, $afav_id ) === false)
							{
								apologize("Algo correu mal ao inserir o novo posto de trabalho na base de dados. - APT:221");
							}
						
						}
					}
				
				}
				
				
				// redirect to homepage
                redirect("index.php?ptid=" . $ptid);
				
			}
            
            
            
        /*}
		
		**********************/
    }
    else
    {
        // else render form
		
		
		// verifica se esta m�quina est� a ser adicionada atrav�s do trabalhador
		if (isset($_GET['tid']))
			$tid=$_GET['tid'];
		else
			$tid="";
			
		// verifica se esta m�quina est� a ser adicionada atrav�s de uma m�quina
		if (isset($_GET['mid']))
			$mid=$_GET['mid'];
		else
			$mid="";
				
		// faz lista de aquinas
		$sql="SELECT * FROM maquina WHERE eid = ?";
		$maquinas = query($sql, $_SESSION['cur_eid']);
		
		
		// render form
        render("adicionar_posto_trabalho_form.php", array("title" => "Adicionar Posto de Trabalho", "tid" => $tid, "mid" => $mid, "maquinas" => $maquinas, "eid" => $_SESSION['cur_eid']));
    }

?>

