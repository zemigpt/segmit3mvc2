<?php

    /***********************************************************************
     * functions.php
     *
     * SEGMIT3MVC
     * 
     *
     * Helper functions.
     **********************************************************************/

    require_once("constants.php");
	require_once("mimetypes.php");

	
	/**
     * Apologizes to user with message.
     */
    function apologize($message)
    {
        render("apology.php", array("message" => $message));
        exit;
    }

    /**
     * Facilitates debugging by dumping contents of variable
     * to browser.
     */
    function dump($variable)
    {
        $variables = func_get_args();
		require("templates/dump.php");
        exit;
    }

	
	/**
     * Facilitates debugging by dumping contents of variable
     * to a file.
     */
    function logdump($variable)
    {
		$file = "debug_log.txt";
		$variables = func_get_args();
		file_put_contents($file, date('m/d/y h:i a',strtotime("now")). "\n" , FILE_APPEND );
		foreach ($variables as $var) {
			$log = print_r($var, true);
			file_put_contents($file, $log . "\n" , FILE_APPEND );
		}

    }
	
	
    /**
     * Logs out current user, if any.  Based on Example #1 at
     * http://us.php.net/manual/en/function.session-destroy.php.
     */
    function logout()
    {
        // unset any session variables
        $_SESSION = array();

        // expire cookie
        if (!empty($_COOKIE[session_name()]))
        {
            setcookie(session_name(), "", time() - 42000);
        }

        // destroy session
        session_destroy();
    }

    /**
     * Executes SQL statement, possibly with parameters, returning
     * an array of all rows in result set or false on (non-fatal) error.
     */
    function query(/* $sql [, ... ] */)
    {
        // SQL statement
        $sql = func_get_arg(0);

        // parameters, if any
        $parameters = array_slice(func_get_args(), 1);

        // try to connect to database
        static $handle;
        if (!isset($handle))
        {
            try
            {
                // connect to database
                $handle = new PDO("mysql:dbname=" . DATABASE . ";host=" . SERVER, USERNAME, PASSWORD);

                // ensure that PDO::prepare returns false when passed invalid SQL
                $handle->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
				
				// ensures that the charset is UTF-8
				$handle->exec("set names utf8");
            }
            catch (Exception $e)
            {
                // trigger (big, orange) error
                trigger_error($e->getMessage(), E_USER_ERROR);
                exit;
            }
        }

        // prepare SQL statement
        $statement = $handle->prepare($sql);
        if ($statement === false)
        {
            // trigger (big, orange) error
			$err = $handle->errorInfo();
            trigger_error($err[2], E_USER_ERROR);
            exit;
        }

        // execute SQL statement
        $results = $statement->execute($parameters);

        // return result set's rows, if any
        if ($results !== false)
        {
            return $statement->fetchAll(PDO::FETCH_ASSOC);
        }
        else
        {
            return false;
        }
    }

    /**
     * Redirects user to destination, which can be
     * a URL or a relative path on the local host.
     *
     * Because this function outputs an HTTP header, it
     * must be called before caller outputs any HTML.
     */
    function redirect($destination)
    {
        // handle URL
        if (preg_match("/^https?:\/\//", $destination))
        {
            header("Location: " . $destination);
        }

        // handle absolute path
        else if (preg_match("/^\//", $destination))
        {
            $protocol = (isset($_SERVER["HTTPS"])) ? "https" : "http";
            $host = $_SERVER["HTTP_HOST"];
            header("Location: $protocol://$host$destination");
        }

        // handle relative path
        else
        {
            // adapted from http://www.php.net/header
            $protocol = (isset($_SERVER["HTTPS"])) ? "https" : "http";
            $host = $_SERVER["HTTP_HOST"];
            $path = rtrim(dirname($_SERVER["PHP_SELF"]), "/\\");
            header("Location: $protocol://$host$path/$destination");
        }

        // exit immediately since we're redirecting anyway
        exit;
    }

    /**
     * Renders template, passing in values.
     */
    function render($template, $values = array())
    {
        // if template exists, render it
        if (file_exists("templates/$template"))
        {
            // extract variables into local scope
            extract($values);

            // render header
            require("templates/header.php");

            // render menu if user logged in
            if (!empty($_SESSION["id"]))
                require("templates/menu.php");
            
            // render template
            require("templates/$template");

            // render footer
            require("templates/footer.php");
        }

        // else err
        else
        {
            trigger_error("Invalid template: $template", E_USER_ERROR);
        }
    }

    /**
     * Gets the current page and compares it with the passed argument
     * Returns 'class="active"' if they match, empty string if not.
     *
     * some code addapted from http://webcheatsheet.com/PHP/get_current_page_url.php
     */

    function matchCurPageName($page) {
        $curPage = substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1);
        if ($curPage == $page)
            return 'active';
        else
            return '';
    }
        
    /**
     * Calculate the number of days passed since a date
     *
     */
    function calcDays($date1)  {
        $ts1 = strtotime($date1);
        $ts2 = time();

        $seconds_diff = $ts2 - $ts1;
        $days = intval($seconds_diff/(60*60*24));
        return $days;
    }  




	/**
	 * Faz uma lista de tabs com as várias empresas administradas pelo user actual
	 * 
	 */
	 
	function MakeUserTabs() {
	
		$groups = $_SESSION["eid"];
		$tabs = "";
		foreach ($groups as $empresa){
					// acrescenta esta empresa às tabs de empresas deste user
					$tabs = $tabs . "<li><a href='index.php?eid=" . $empresa['eid'] . "'>" . $empresa['nome'] . "</a></li>\n";
		}
				
		return $tabs;
	 }
	 
	 
	 
	 /**
	 * Trata dos uploads de ficheiros
	 * Devolve o URL do ficheiro como string
	 * 
	 * Call method: UploadFile($_FILE[field_name], $file_type);
	 *
	 * $file_type can "image" or "pdf"
	 *							 
	 * Return: (string) $file_url
	 *         (string) "ERR" on error
	 * 
	 */
	 function UploadFile($uploaded_file, $file_type) {
		
		$file_url="ERR"; // inicializa a variável partindo do principio que pode dar erro.
		
		if ($uploaded_file["error"] == UPLOAD_ERR_OK) 
		{  
			$files_dir = "files/eid-" . $_SESSION['cur_eid'] . "/";
			
			if (!is_dir($files_dir))
				mkdir($files_dir);
			

									
			if (
					($file_type == "image" &&				
						(
						($uploaded_file["type"] == "image/gif")
							|| ($uploaded_file["type"] == "image/jpeg")
							|| ($uploaded_file["type"] == "image/jpg")
							|| ($uploaded_file["type"] == "image/png")
						)
					)
					||
					($file_type == "pdf" &&				
						(
						($uploaded_file["type"] == "application/pdf")
							|| ($uploaded_file["type"] == "application/x-pdf")
						)
					)
					&& ($uploaded_file["size"] < 20000000)
				) 
			{
				
				if ($uploaded_file["error"] > 0)  //error
				{
					apologize("Ocorreu um erro ao gravar o ficheiro.");
					$file_url = "ERR";
				}
				
				else //success
				{
					$uploaded_file_name = $uploaded_file["name"];
					$new_file_name = transliterate($uploaded_file_name);
					$new_file_path = $files_dir . $new_file_name;

					if (file_exists($new_file_path))
					{
						$filedetails = explode(".",$new_file_name);
						$fileextension = $filedetails[count($filedetails)-1];
						$filename=$filedetails[0];
						for($j=1; $j < count($filedetails)-1; $j++)
							$filename = $filename . "." . $filedetails[$j];                           
						
						$iteration = 0;
						do
						{
							$iteration++;
							$finalfile = $filename . "_" . $iteration . "." . $fileextension;
							$finalfilelocation = $files_dir . $finalfile;
						} while (file_exists($finalfilelocation));
						
						move_uploaded_file($uploaded_file["tmp_name"], $finalfilelocation);
						chmod($finalfilelocation, 0644);
					}
					
					
					else
					{
						$finalfile = $new_file_name; 
						$finalfilelocation = $new_file_path;
						move_uploaded_file($uploaded_file["tmp_name"], $finalfilelocation);
						chmod($finalfilelocation, 0644);
					}
					
					// insert the image in the database
					$file_url = $finalfilelocation;
												
						
				}
			}
			else
			{
				if ($file_type=="image")
					$files_ext = "*.jpg, *.jpeg, *.png, *.gif";
				elseif ($file_type=="pdf")	
					$files_ext = "*.pdf";
				apologize("O ficheiro enviado não é válido! Deverá fazer upload de um ficheiro com extensão " . $files_ext);
				$file_url = "ERR";
			}
			   
		 }
		 return $file_url;
	 }
	 
	 
	function eid_autorizado($eid) {
		$rows = query("SELECT empresa.eid FROM `empresa-to-users` INNER JOIN users On users.uid = `empresa-to-users`.uid INNER JOIN empresa On empresa.eid = `empresa-to-users`.eid WHERE users.uid = ?", $_SESSION["id"]);
		
		foreach ($rows as $row)
		{
			if ($eid == $row['eid'])
				return true;
		}
		return false;
	}
	
	
	function transliterate($str) {
	  $a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ');
	  $b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o');
	  return str_replace($a, $b, $str);
	}
	
	function downloadFile($file, $download_filename) {
		// array of support file types for download script and there mimetype
		$mimeTypes = defineMimeTypes();

		// gets the extension of the file to be loaded for searching array above
		$ext = explode('.', $file);
		$ext = end($ext);

		// gets the file name to send to the browser to force download of file
		$fileName = explode("/", $file);
		$fileName = end($fileName);

		// opens the file for reading and sends headers to browser
		$fp = fopen($file,"r") ;
		header("Content-Type: ".$mimeTypes[$ext]);
		header('Content-Disposition: attachment; filename="'.$fileName.'"');

		// reads file and send the raw code to browser
		while (! feof($fp)) {
			$buff = fread($fp,4096);
			echo $buff;
		}
		// closes file after whe have finished reading it
		fclose($fp);
	}
?>
