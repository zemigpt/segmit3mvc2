<?php

    // configuration
    require_once("includes/config.php");

    // if form was submitted
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        /**************
		*
		* Validation
		*
		*****************/
		
		/*if (empty($_POST["field"]))
        {
            apologize("O campo 'field' � obrigat�rio!");
        }
        
        else
        {*/
            // if none of the above occurred, odds are everything is ok
            // so insert new record in database
                   
           
		    // grava o ficheiro
            // code adapted from http://www.w3schools.com/php/php_file_upload.asp
			
			
			$upload_result = UploadFile($_FILES["trab_foto"], "image");
			if ($upload_result != "ERR")
			{
				$foto_url = $upload_result;
			}
			else
			{
				$foto_url = "";
			}			
            
            
            $sql = "INSERT INTO trabalhador 
					(nome, foto, num_mecanografico, morada, codpostal, localidade, telefone, email, contacto_sos_nome, contacto_sos_telefone, bi, bi_validade, niss, nif, apolice_seguro, eid) 
					VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            
            
            
            
            if (query($sql, $_POST['trab_nome'], $foto_url, $_POST['num_mecanografico'], $_POST['trab_morada'], $_POST['trab_codpostal'], $_POST['trab_localidade'], $_POST['trab_telefone'], $_POST['trab_email'], $_POST['trab_contacto_sos_nome'], $_POST['trab_contacto_sos_telefone'], $_POST['trab_bi'], $_POST['trab_bi_validade'], $_POST['trab_niss'], $_POST['trab_nif'], $_POST['trab_apolice_seguro'], $_SESSION['cur_eid'] ) === false)
            {
                apologize("Algo correu mal ao inserir o novo trabalhador na base de dados.");
            }
            else
            {

				
				$rows = query("SELECT LAST_INSERT_ID() AS tid");
				
				$tid = $rows[0]["tid"];
								
				// redirect to homepage
                redirect("index.php?eid=" . $_SESSION['cur_eid']);
				
			}
            
            
            
        /*}
		
		**********************/
    }
    else
    {
        // else render form
        render("adicionar_trabalhador_form.php", array("title" => "Adicionar Trabalhador", "eid" => $_SESSION['cur_eid']));
    }

?>

