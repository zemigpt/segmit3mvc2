<?php

    // configuration
    require("includes/config.php");

    // if form was submitted
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {	
		if ($_POST['tid'] == 0)
			apologize("Tem de escolher um trabalhador da lista!");
		
		// verifica se o formulário não foi adulterado com SQL injections
		$sql_trab = "SELECT * FROM trabalhador WHERE tid = ? AND eid = ?";
		$sql_mid = "SELECT * FROM maquina WHERE mid = ? AND eid = ?";
		
		$rows_trab = query($sql_trab, $_POST['tid'], $_SESSION['cur_eid']);
		$rows_mid = query($sql_mid, $_POST['mid'], $_SESSION['cur_eid']);
		
		if ((($rows_trab === false) || empty($rows_trab)) || (($rows_mid === false) || empty($rows_mid)))
			apologize("Os dados para associar o trabalhador à maquina foram corrompidos.");
		else
		{
			$sql = "INSERT INTO `maquina-to-trabalhador`
			(mid, tid) 
			VALUES (?, ?)";
			
			if (query($sql, $_POST['mid'], $_POST['tid']) === false)
				apologize("Algo correu mal ao associar o trabalhador à maquina na base de dados.");
			else
				redirect("maquina.php?id=" . $_POST['mid']);
		}
	}
	else
		apologize("Algo correu mal ao associar o trabalhador à maquina");
?>