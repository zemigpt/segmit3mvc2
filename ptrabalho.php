<?php

    // configuration
    require_once("includes/config.php");

	
	
	/************************************************************** 
	 *
	 * Trata dos casos em que o ficheiro é usado com $_POST
	 *
	 *************************************************************/
	 
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        
		// verifica se estamos a editar um ptrabalho existente ou a adicionar um novo
		if ($_POST['action']=="edit")
		{
			if (isset($_POST['posto_ptid']))
			{
				$upload_result = UploadFile($_FILES["posto_foto"], "image");
				if ($upload_result != "ERR")
				{
					$foto_url = $upload_result;
				}
				else
				{
					$foto_url = "";
				}			
				
				// se o upload de foto nova falhou, continua a usar o existente
				if  (!empty($_FILES["posto_foto"]) && $foto_url=="" && !empty($_POST['posto_foto_existente']))
					$foto_url = $_POST['posto_foto_existente'];
				
				$sql = "UPDATE `posto-trabalho` 
						SET nome = ?, foto = ?, descricao = ?
						WHERE ptid = ? AND eid = ?";
				
					
				if (query($sql, $_POST['posto_nome'], $foto_url, $_POST['posto_descricao'], $_POST['posto_ptid'], $_SESSION['cur_eid'] ) === false)
				{
					apologize("Algo correu mal ao editar o posto de trabalho na base de dados.");
				}
				else
				{

									
					// redirect to homepage
					redirect("ptrabalho.php?id=" . $_POST['posto_ptid']);
					
				}
			}
			// não temos tid para identificar o ptrabalhador - aborta
			else
				apologize("Posto de trabalho não identificado - impossível editar");
			
		
		}
		elseif 	($_POST['action']=="add")
		{
			$upload_result = UploadFile($_FILES["posto_foto"], "image");
			if ($upload_result != "ERR")
			{
				$foto_url = $upload_result;
			}
			else
			{
				$foto_url = "";
			}			
            
            
            $sql = "INSERT INTO `posto-trabalho` 
					(nome, foto, descricao, eid) 
					VALUES(?, ?, ?, ?)";           
            
            if (query($sql, $_POST['posto_nome'], $foto_url, $_POST['posto_descricao'], $_SESSION['cur_eid'] ) === false)
            {
                apologize("Algo correu mal ao inserir o novo posto de trabalho na base de dados.");
            }
            else
            {

				
				$rows = query("SELECT LAST_INSERT_ID() AS ptid");
				
				$ptid = $rows[0]["ptid"];
								
				// redirect to homepage
                redirect("ptrabalho.php?id=" . $ptid);
				
			}
        }  
        elseif 	($_POST['action']=="delete")
		{
			$sql ="DELETE FROM `posto-trabalho` WHERE ptid = ? AND eid = ?";
			query($sql, $_POST['posto_ptid'], $_SESSION['cur_eid']);
			// redirect to lista de trabalhadores
            redirect("ptrabalho.php");
		}
		// não foi especificada acção no formulário - aborta
		else
			apologize("Não foi especificada acção no formulário...");
    }
	
	
	
	
	/************************************************************** 
	 *
	 * Trata dos casos em que o ficheiro é usado com $_GET
	 *
	 *************************************************************/
	 
    else
    {
		//se for especificada uma acção no URL
		if (isset($_GET['action']))
		{
			// caso o utilizador queira editar um trabalhador
			if ($_GET['action'] == 'edit')
			{
				if (isset($_GET['id']))
				{
					$ptid = $_GET['id']; 
					
					$ptrabalhos  = query("SELECT * FROM `posto-trabalho` WHERE ptid = ? AND eid = ?", $ptid, $_SESSION['cur_eid']);
					
					// se existe trabalhador com este ptid
					if (!empty($ptrabalhos[0])) 
					{
						$ptrabalho = $ptrabalhos[0];
						//constroi as várias queries necessárias
						
					
					}
					
					// a query devolveu uma lista vazia
					else 
						apologize("O posto de trabalho especificado não existe...");
				
					render("ptrabalho_form_editar.php", array("title" => "Editar Posto de Trabalho", "ptrabalho" => $ptrabalho));
				}
				
				// o utilizador quer editar, mas falta o id no URL, logo não sabemos qual é o ptrabalhador a editar
				else 
					apologize("Não foi especificado qual o posto de trabalho a editar.");
			}
			
			// caso o utilizador queira adicionar um trabalhador
			elseif ($_GET['action'] == 'add')
			{
				 render("ptrabalho_form_adicionar.php", array("title" => "Adicionar Posto Trabalho"));
			}
			
			// caso o utilizador queira apagar um trabalhador
			elseif ($_GET['action'] == 'delete')
			{
				if (isset($_GET['id']))
				{
					$ptid = $_GET['id']; 
					
					$ptrabalhos  = query("SELECT * FROM `posto-trabalho` WHERE ptid = ? AND eid = ?", $ptid, $_SESSION['cur_eid']);
					
					// se existe ptrabalhador com este ptid
					if (!empty($ptrabalhos[0])) 
					{
						$ptrabalho = $ptrabalhos[0];
					}
					
					// a query devolveu uma lista vazia
					else 
						apologize("Remover Posto Trabalho - O posto de trabalho especificado não existe...");
				
					render("ptrabalho_form_remover.php", array("title" => "Remover Posto Trabalho", "ptrabalho" => $ptrabalho));
				}
				
				// o utilizador quer editar, mas falta o ptid no URL, logo não sabemos qual é o trabalhador a editar
				else 
					apologize("Não foi especificado qual o posto de trabalho a remover.");
			}
			
			else
				apologize("Operação não suportada.");
		
		}		
		
		
		//se não houver acção especificada no URL, mas houver um ptid
		elseif (isset($_GET['id'])) 
		{
			$ptid = $_GET['id']; 
			
			//constroi as várias queries necessárias
			$ptrabalhos  = query("SELECT * FROM `posto-trabalho` WHERE ptid = ? AND eid = ?", $ptid, $_SESSION['cur_eid']);
			
			
			$sql_ptrabalhos = "Select
			  `posto-trabalho`.ptid,
			  `posto-trabalho`.nome As nome,
			  `posto-trabalho`.foto As foto
			From
			  `posto-trabalho`
			Where
			  `posto-trabalho`.ptid = ? AND `posto-trabalho`.eid = ?";
			
			$sql_todos_trabalhadores  = "Select
				trabalhador.tid As tid,
				trabalhador.nome As nome,
				trabalhador.eid as eid,
				trabalhador.num_mecanografico As num_mecanografico,
				`posto`.ptid as ptid
			From
				trabalhador LEFT JOIN
				  (SELECT * FROM `posto-to-trabalhador` WHERE `posto-to-trabalhador`.ptid = ?) `posto`
				ON trabalhador.tid = `posto`.tid
			WHERE `posto`.`ptid` IS NULL
				AND eid = ?
			ORDER BY nome ASC";
			 
			$sql_trabalhadores = "Select
			  trabalhador.tid As tid,
			  trabalhador.nome As nome,
			  trabalhador.num_mecanografico As num_mecanografico,
			  `posto-trabalho`.ptid
			From
			  trabalhador Inner Join
			  `posto-to-trabalhador` On `posto-to-trabalhador`.tid = trabalhador.tid
			  Inner Join
			  `posto-trabalho` On `posto-trabalho`.ptid = `posto-to-trabalhador`.ptid
			Where
			  `posto-trabalho`.ptid = ? AND `posto-trabalho`.eid = ?";

			$sql_todas_maquinas = "Select
				maquina.mid As mid,
				maquina.nome As nome,
				maquina.eid as eid,
				posto.ptid as ptid
			From
				maquina left Join
				(Select * from `maquina-to-posto_trabalho` WHERE  `maquina-to-posto_trabalho`.ptid = ?) posto
			   On posto.mid = maquina.mid
			WHERE maquina.eid = ? And   posto.ptid Is Null
				ORDER BY nome ASC";

			
			$sql_maquinas = "Select
			  `posto-trabalho`.ptid,
			  maquina.nome As nome,
			   maquina.mid As mid,
			  maquina.num_serie As num_serie
			From
			  `posto-trabalho` Inner Join
			  `maquina-to-posto_trabalho` On `posto-trabalho`.ptid =
				`maquina-to-posto_trabalho`.ptid Inner Join
			  maquina On `maquina-to-posto_trabalho`.mid = maquina.mid
			Where
			  `posto-trabalho`.ptid = ? AND `posto-trabalho`.eid = ?";
			  
			$sql_afruidos = "Select
			  `posto-trabalho`.ptid,
			  ag_fisico_aval_ruido.eid,
			  ag_fisico_aval_ruido.afar_id,
			  ag_fisico_aval_ruido.valor_registado As valor,
			  ag_fisico_aval_ruido.data_execucao As data,
			  ag_fisico_aval_ruido.data_validade As validade
			From
			  `posto-to-ag_fisico_aval_ruido` Left Join
			  `posto-trabalho`
				On `posto-trabalho`.ptid = `posto-to-ag_fisico_aval_ruido`.ptid Left Join
			  ag_fisico_aval_ruido On `posto-to-ag_fisico_aval_ruido`.afar_id =
				ag_fisico_aval_ruido.afar_id
			Where
			  `posto-trabalho`.ptid = ? AND ag_fisico_aval_ruido.eid = ? ";
			  
			$sql_afiluminancias = "Select
			  `posto-trabalho`.ptid,
			  ag_fisico_aval_ilum.eid,
			  ag_fisico_aval_ilum.afai_id,
			  ag_fisico_aval_ilum.valor_registado As valor,
			  ag_fisico_aval_ilum.data_execucao As data,
			  ag_fisico_aval_ilum.data_validade As validade
			From
			  `posto-trabalho` Inner Join
			  `posto-to-ag_fisico_aval_ilum` On `posto-trabalho`.ptid =
				`posto-to-ag_fisico_aval_ilum`.ptid Inner Join
			  ag_fisico_aval_ilum On `posto-to-ag_fisico_aval_ilum`.afai_id =
				ag_fisico_aval_ilum.afai_id
			Where
			  `posto-trabalho`.ptid = ? AND ag_fisico_aval_ilum.eid = ?";
			 
			$sql_aftermicos= "Select
			  `posto-trabalho`.ptid,
			  ag_fisico_aval_termico.eid,
			  ag_fisico_aval_termico.afat_id,
			  ag_fisico_aval_termico.valor_registado As valor,
			  ag_fisico_aval_termico.data_execucao As data,
			  ag_fisico_aval_termico.data_validade As validade
			From
			  `posto-trabalho` Inner Join
			  `posto-to-ag_fisico_aval_termico` On `posto-trabalho`.ptid =
				`posto-to-ag_fisico_aval_termico`.ptid Inner Join
			  ag_fisico_aval_termico On `posto-to-ag_fisico_aval_termico`.afat_id =
				ag_fisico_aval_termico.afat_id
			Where
			  `posto-trabalho`.ptid = ? AND ag_fisico_aval_termico.eid = ?"; 
			 
			
			$sql_afvibracoes = "Select
			  `posto-trabalho`.ptid,
			  ag_fisico_aval_vibracoes.eid,
			  ag_fisico_aval_vibracoes.afav_id,
			  ag_fisico_aval_vibracoes.valor_registado As valor,
			  ag_fisico_aval_vibracoes.data_execucao As data,
			  ag_fisico_aval_vibracoes.data_validade As validade,
			  ag_fisico_aval_vibracoes.entidade_executante As entidade
			From
			  `posto-trabalho` Inner Join
			  `posto-to-ag_fisico_aval_vibracoes` On `posto-trabalho`.ptid =
				`posto-to-ag_fisico_aval_vibracoes`.ptid Inner Join
			  ag_fisico_aval_vibracoes On `posto-to-ag_fisico_aval_vibracoes`.afav_id =
				ag_fisico_aval_vibracoes.afav_id
			Where
			  `posto-trabalho`.ptid = ? AND ag_fisico_aval_vibracoes.eid = ?";
			  
			$sql_agbiologicos = "Select
			  `posto-trabalho`.ptid,
			  ag_biologico.eid,
			  ag_biologico.ab_id,
			  ag_biologico.data_execucao As data,
			  ag_biologico.valor_registado As valor
			From
			  `posto-trabalho` Inner Join
			  `posto-to-ag_biologico`
				On `posto-trabalho`.ptid = `posto-to-ag_biologico`.ptid Inner Join
			  ag_biologico On ag_biologico.ab_id = `posto-to-ag_biologico`.ab_id
			Where
			  `posto-trabalho`.ptid = ? AND ag_biologico.eid = ?";
			  
			$sql_agquimicos = "Select
			  `posto-trabalho`.ptid,
			  ag_quimico.eid,
			  ag_quimico.data_execucao As data,
			  ag_quimico.agente As agente,
			  ag_quimico.dose As dose
			From
			  `posto-trabalho` Inner Join
			  `posto-to-ag_quimico` On `posto-trabalho`.ptid = `posto-to-ag_quimico`.ptid
			  Inner Join
			  ag_quimico On `posto-to-ag_quimico`.aq_id = ag_quimico.aq_id
			Where
			  `posto-trabalho`.ptid = ? AND ag_quimico.eid = ?";
			 
			$sql_lista_agquimicos = "Select * From ag_quimico_lista";
			
			
			$sql_avalriscos = "Select
			  `posto-trabalho`.ptid,
			  posto_avaliacao.pta_id,
			  posto_avaliacao.eid,
			  posto_avaliacao.data As data,
			  posto_avaliacao.tarefa As tarefa,
			  posto_avaliacao.perigo As perigo,
			  posto_avaliacao.grau_perigosidade As grau_perigosidade,
			  posto_avaliacao.indice_justificacao As indice_justificacao,
			  posto_avaliacao.responsavel As responsavel,
			  posto_avaliacao.medidas As medidas,
			  posto_avaliacao.risco As risco
			From
			  `posto-trabalho` Inner Join
			  posto_avaliacao On `posto-trabalho`.ptid = posto_avaliacao.ptid
			Where
			  `posto-trabalho`.ptid = ? AND posto_avaliacao.eid = ?";
			  
			$sql_forrequeridas = "Select
			  formacao.tema As tema,
			  formacao.duracao As duracao,
			  formacao.for_id As for_id,
			  maquina.nome As maq_nome,
			  maquina.mid As mid,
			  `posto-trabalho`.eid As eid,
			  `posto-trabalho`.ptid As ptid
			From
			  formacao Inner Join
			  `formacao-to-tipomaquina`
				On formacao.for_id = `formacao-to-tipomaquina`.for_id Inner Join
			  tipo_maquina On `formacao-to-tipomaquina`.tm_id = tipo_maquina.tm_id
			  Inner Join
			  maquina On tipo_maquina.tm_id = maquina.tm_id Inner Join
			  `maquina-to-posto_trabalho` On maquina.mid = `maquina-to-posto_trabalho`.mid
			  Inner Join
			  `posto-trabalho` On `maquina-to-posto_trabalho`.ptid = `posto-trabalho`.ptid
			Where
			  `posto-trabalho`.ptid = ? And
			  `posto-trabalho`.eid = ? ";
			  
			$sql_regacidentes = "Select
			  registo_acidente.ra_id,
			  registo_acidente.eid,
			  registo_acidente.estado As estado,
			  registo_acidente.data As data,
			  `posto-trabalho`.ptid,
			  `posto-trabalho`.eid As eid1
			From
			  registo_acidente Inner Join
			  `posto-to-reg_acidente` On registo_acidente.ra_id =
				`posto-to-reg_acidente`.ra_id Inner Join
			  `posto-trabalho` On `posto-trabalho`.ptid = `posto-to-reg_acidente`.ptid
			Where
			  `posto-trabalho`.ptid = ? And
			  `posto-trabalho`.eid = ?";
			
			$sql_epis = "Select
			  epi.nome As nome,
			  `posto-trabalho`.ptid As ptid,
			  `posto-trabalho`.eid As eid,
			  epi.epi_id As epi_id
			From
			  epi Inner Join
			  `posto-to-epi` On `posto-to-epi`.epi_id = epi.epi_id Inner Join
			  `posto-trabalho` On `posto-trabalho`.ptid = `posto-to-epi`.ptid
			Where
			  `posto-trabalho`.ptid = ? And `posto-trabalho`.eid = ?";
			  
			$sql_todos_epi = "Select
				epi.epi_id As epi_id,
				epi.nome As nome,
				posto.ptid as ptid
			From
				epi left Join
				(Select * from `posto-to-epi` WHERE  `posto-to-epi`.ptid = ?) posto
			   On posto.epi_id = epi.epi_id
			WHERE posto.ptid Is Null
			ORDER BY nome ASC";

											
			if (!empty($ptrabalhos[0])) // se há postos trabalho com este ptid
			{
				// o posto trabalho que queremos está na linha 0
				$ptrabalho = $ptrabalhos[0];
				
				// dado que o trabalhador existe podemos executar as outras queries
				$ptrabalhos = query($sql_ptrabalhos, $ptid, $_SESSION['cur_eid']);
				$trabalhadores = query($sql_trabalhadores, $ptid, $_SESSION['cur_eid']);
				$todos_trabalhadores = query($sql_todos_trabalhadores, $ptid, $_SESSION['cur_eid']);
				$maquinas = query($sql_maquinas, $ptid, $_SESSION['cur_eid']);
				$todas_maquinas = query($sql_todas_maquinas, $ptid, $_SESSION['cur_eid']);
				$afruidos = query($sql_afruidos, $ptid, $_SESSION['cur_eid']);
				$afiluminancias = query($sql_afiluminancias, $ptid, $_SESSION['cur_eid']);
				$afambterms = query($sql_aftermicos, $ptid, $_SESSION['cur_eid']);
				$afvibracoes = query($sql_afvibracoes, $ptid, $_SESSION['cur_eid']);
				$agbiologicos = query($sql_agbiologicos, $ptid, $_SESSION['cur_eid']);
				$agquimicos = query($sql_agquimicos, $ptid, $_SESSION['cur_eid']);
				$lista_agquimicos = query($sql_lista_agquimicos);
				$avalriscos = query($sql_avalriscos, $ptid, $_SESSION['cur_eid']);
				$forrequeridas = query($sql_forrequeridas, $ptid, $_SESSION['cur_eid']);
				$regacidentes = query($sql_regacidentes, $ptid, $_SESSION['cur_eid']);
				$epis = query($sql_epis, $ptid, $_SESSION['cur_eid']);
				$todos_epi = query($sql_todos_epi, $ptid);
				
				
				// mostra o posto de trabalho
				render("ptrabalho_visualizar.php", array("title" => "Posto Trabalho", "ptid" => $ptid, "ptrabalho" => $ptrabalho, "trabalhadores" => $trabalhadores, "maquinas" => $maquinas, "afruidos" => $afruidos, "afiluminancias" => $afiluminancias, "afambterms" => $afambterms, "afvibracoes" => $afvibracoes, "agbiologicos" => $agbiologicos, "agquimicos" => $agquimicos, "lista_agquimicos" => $lista_agquimicos, "avalriscos" => $avalriscos, "forrequeridas" => $forrequeridas, "regacidentes" => $regacidentes, "todos_trabalhadores" => $todos_trabalhadores, "todas_maquinas" => $todas_maquinas, "epis" => $epis, "todos_epi" => $todos_epi));
				
				
			}
			else // a query devolveu uma lista vazia
				apologize("O posto de trabalho especificado não existe...");

		
		}
		
		
		
		// se não for especificado o ptid do ptrabalho no URL, mostra lista de ptrabalhadores
		else
		{
			$ptrabalhos = query("SELECT * FROM `posto-trabalho` WHERE eid = ?", $_SESSION['cur_eid']);
			render("ptrabalho_lista.php", array("title" => "Lista de Postos de Trabalho", "ptrabalhos" => $ptrabalhos));
		}
		
    }

?>

