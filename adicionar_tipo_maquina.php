<?php

    // configuration
    require_once("includes/config.php");

    // if form was submitted
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        /**************
		*
		* Validation
		*
		*****************/
		
		/*if (empty($_POST["field"]))
        {
            apologize("O campo 'field' é obrigatório!");
        }
        
        else
        {*/
            // if none of the above occurred, odds are everything is ok
            // so insert new record in database
                   
           
		    // grava o ficheiro
            // code adapted from http://www.w3schools.com/php/php_file_upload.asp            
            
            $sql = "INSERT INTO tipo_maquina
					(nome, eid) 
					VALUES(?,?)";
            
            
            
       
            if (query($sql, $_POST['tipo_maq_nome'], $_SESSION['cur_eid'] ) === false)
            {
                apologize("Algo correu mal ao inserir o novo tipo de máquina na base de dados.");
            }
            else
            {

				
				$rows = query("SELECT LAST_INSERT_ID() AS tm_id");
				
				$tm_id = $rows[0]["tm_id"];
				
				
				if (!isset($_POST["ajax"]) || $_POST["ajax"] != "true") {
					// redirect to homepage
					redirect("index.php?tm_id=0");
				}
				else
				{
					echo json_encode(array("tm_id" => $tm_id, "tipo_maq_nome" => $_POST['tipo_maq_nome']));
				}
			}
            
            
            
        /*}
		
		**********************/
    }
    else
    {
        // else render form
        render("adicionar_tipo_maquina_form.php", array("title" => "Adicionar Tipo Máquina", "eid" => $_SESSION['cur_eid']));
    }

?>

