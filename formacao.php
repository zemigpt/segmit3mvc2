<?php

    // configuration
    require_once("includes/config.php");

	
	
	/************************************************************** 
	 *
	 * Trata dos casos em que o ficheiro é usado com $_POST
	 *
	 *************************************************************/
	 
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        
		// verifica se estamos a editar um trabalhador existente ou a adicionar um novo
		if ($_POST['action']=="edit")
		{
			if (isset($_POST['for_id']))
			{
				
				$upload_result = UploadFile($_FILES["formacao_relatorio"], "pdf");
				if ($upload_result != "ERR")
				{
					$relatorio_url = $upload_result;
				}
				else
				{
					$relatorio_url = "";
				}			
				
				// se o upload de foto nova falhou, continua a usar o existente
				if  (!empty($_FILES["formacao_relatorio"]) && $relatorio_url=="" && !empty($_POST['formacao_relatorio']))
					$relatorio_url = $_POST['formacao_relatorio'];
				
				$sql = "UPDATE formacao 
						SET tema = ?, duracao = ?, entidade_formadora = ?, relatorio = ? 
						WHERE for_id = ? AND eid = ?";
				
					
				if (query($sql, $_POST['formacao_tema'], $_POST['formacao_duracao'], $_POST['formacao_entidade_formadora'], $relatorio_url, $_POST['for_id'], $_SESSION['cur_eid'] ) === false)
				{
					apologize("Algo correu mal ao editar a formação na base de dados.");
				}
				else
				{

									
					// redirect to homepage
					redirect("formacao.php?id=" . $_POST['for_id']);
					
				}
			}
			// não temos for_id para identificar o trabalhador - aborta
			else
				apologize("Formação não identificada - impossível editar");
			
		
		}
		elseif 	($_POST['action']=="add")
		{
			$upload_result = UploadFile($_FILES["formacao_relatorio"], "pdf");
			if ($upload_result != "ERR")
			{
				$relatorio_url = $upload_result;
			}
			else
			{
				$relatorio_url = "";
			}				
            
            
            $sql = "INSERT INTO formacao 
					(tema, duracao, entidade_formadora, relatorio, eid) 
					VALUES(?, ?, ?, ?, ?)";           
            
            if (query($sql, $_POST['formacao_tema'], $_POST['formacao_duracao'], $_POST['formacao_entidade_formadora'], $relatorio_url, $_SESSION['cur_eid'] ) === false)
            {
                apologize("Algo correu mal ao inserir a nova formação na base de dados.");
            }
            else
            {

				
				$rows = query("SELECT LAST_INSERT_ID() AS for_id");
				
				$for_id = $rows[0]["for_id"];
								
				// redirect to homepage
                redirect("formacao.php?id=" . $for_id);
				
			}
        }  
        elseif 	($_POST['action']=="delete")
		{
			$sql ="DELETE FROM formacao WHERE for_id = ? AND eid = ?";
			query($sql, $_POST['for_id'], $_SESSION['cur_eid']);
			// redirect to lista de trabalhadores
            redirect("formacao.php");
		}
		// não foi especificada acção no formulário - aborta
		else
			apologize("Não foi especificada acção no formulário...");
    }
	
	
	
	
	/************************************************************** 
	 *
	 * Trata dos casos em que o ficheiro é usado com $_GET
	 *
	 *************************************************************/
	 
    else
    {
		//se for especificada uma acção no URL
		if (isset($_GET['action']))
		{
			// caso o utilizador queira editar um trabalhador
			if ($_GET['action'] == 'edit')
			{
				if (isset($_GET['id']))
				{
					$for_id = $_GET['id']; 
					
					$formacoes  = query("SELECT * FROM formacao WHERE for_id = ? AND eid = ?", $for_id, $_SESSION['cur_eid']);
					
					// se existe trabalhador com este tid
					if (!empty($formacoes[0])) 
					{
						$formacao = $formacoes[0];
						//constroi as várias queries necessárias
						
					
					}
					
					// a query devolveu uma lista vazia
					else 
						apologize("A formação especificada não existe...");
				
					render("formacao_form_editar.php", array("title" => "Editar Formação", "formacao" => $formacao));
				}
				
				// o utilizador quer editar, mas falta o tid no URL, logo não sabemos qual é o trabalhador a editar
				else 
					apologize("Não foi especificado qual a formação a editar.");
			}
			
			// caso o utilizador queira adicionar um trabalhador
			elseif ($_GET['action'] == 'add')
			{
				 render("formacao_form_adicionar.php", array("title" => "Adicionar Formação"));
			}
			
			// caso o utilizador queira apagar um trabalhador
			elseif ($_GET['action'] == 'delete')
			{
				if (isset($_GET['id']))
				{
					$for_id = $_GET['id']; 
					
					$formacoes  = query("SELECT * FROM formacao WHERE for_id = ? AND eid = ?", $for_id, $_SESSION['cur_eid']);
					
					// se existe trabalhador com este tid
					if (!empty($formacoes[0])) 
					{
						$formacao = $formacoes[0];
					}
					
					// a query devolveu uma lista vazia
					else 
						apologize("Remover formação - a formação especificada não existe...");
				
					render("formacao_form_remover.php", array("title" => "Remover Formação", "formacao" => $formacao));
				}
				
				// o utilizador quer editar, mas falta o tid no URL, logo não sabemos qual é o trabalhador a editar
				else 
					apologize("Não foi especificado qual a formação a remover.");
			}
			
			else
				apologize("Operação não suportada.");
		
		}		
		
		
		//se não houver acção especificada no URL, mas houver um tid
		elseif (isset($_GET['id'])) 
		{
			$for_id = $_GET['id']; 
			
			//constroi as várias queries necessárias
			$formacoes  = query("SELECT * FROM formacao WHERE for_id = ? AND eid = ?", $for_id, $_SESSION['cur_eid']);
									
			if (!empty($formacoes[0])) // se há trabalhadores com este tid
			{
				// o trabalhador que queremos está na linha 0
				$formacao = $formacoes[0];
				
				// dado que o trabalhador existe podemos executar as outras queries
				//$maquinas = query($sql_maquinas, $tid);
				
				// mostra o trabalhador
				render("formacao_visualizar.php", array("title" => "Formação - " . $formacao['tema'], "formacao" => $formacao, "for_id" => $for_id));
			}
			else // a query devolveu uma lista vazia
				apologize("A formação especificada não existe...");

		
		}
		
		
		
		// se não for especificado o tid do trabalhador no URL, mostra lista de trabalhadores
		else
		{
			$formacoes = query("SELECT * FROM formacao WHERE eid = ?", $_SESSION['cur_eid']);
			render("formacao_lista.php", array("title" => "Lista de Formações", "formacoes" => $formacoes));
		}
		
    }

?>

