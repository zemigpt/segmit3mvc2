<?php

    // configuration
    require_once("includes/config.php");

	
	
	/************************************************************** 
	 *
	 * Trata dos casos em que o ficheiro é usado com $_POST
	 *
	 *************************************************************/
	 
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        
		// verifica se estamos a editar um trabalhador existente ou a adicionar um novo
		if ($_POST['action']=="edit")
		{
			if (isset($_POST['ra_id']))
			{
				
				$upload_result = UploadFile($_FILES["relatorio"], "pdf");
				if ($upload_result != "ERR")
				{
					$relatorio_url = $upload_result;
				}
				else
				{
					$relatorio_url = "";
				}			
				
				// se o upload de relatorio nova falhou, continua a usar o existente
				if  (!empty($_FILES["relatorio"]) && $relatorio_url=="" && !empty($_POST['relatorio_existente']))
					$relatorio_url = $_POST['relatorio_existente'];
				
				$sql = "UPDATE registo_acidente 
						SET data = ?, hora = ?, local = ?, como = ?, regiao = ?, lesao = ?, outras_lesoes = ?, testemunhas = ?, assistencia = ?, tipo_lesao = ?, participacao = ?, incapacidade = ?, alta_provisoria = ?, alta_definitiva = ?, causa = ?, agente = ?, acto = ?, descricao = ?, condicao = ?, relatorio = ?, estado = ?
						WHERE ra_id = ? AND eid = ?";
				
					
				if (query($sql, $_POST['registo_acidente_data'], $_POST['registo_acidente_hora'] , $_POST['registo_acidente_local'] , $_POST['registo_acidente_como'] , $_POST['registo_acidente_regiao'] , $_POST['registo_acidente_lesao'] , $_POST['registo_acidente_outras_lesoes'] , $_POST['registo_acidente_testemunhas'] , $_POST['registo_acidente_assistencia'] , $_POST['registo_acidente_tipo_lesao'] , $_POST['registo_acidente_participacao'] , $_POST['registo_acidente_incapacidade'] , $_POST['registo_acidente_alta_provisoria'] , $_POST['registo_acidente_alta_definitiva'] , $_POST['registo_acidente_causa'], $_POST['registo_acidente_agente'] , $_POST['registo_acidente_acto'] , $_POST['registo_acidente_descricao'] , $_POST['registo_acidente_condicao'] , $relatorio_url, $_POST['registo_acidente_estado'], $_POST['ra_id'], $_SESSION['cur_eid'] ) === false)
				{
					apologize("Algo correu mal ao editar o registo do acidente na base de dados.");
				}
				else
				{
					
					if (isset($_POST['registo_acidente_localizacao_lesao']))
					{
						// apaga todas as lesões, para a seguir as inserir
						query("DELETE FROM `registo_acidente_localizacao_lesao` WHERE ra_id = ?", $_POST['ra_id']);
						
						
						// insere as várias lesões na respectiva tabela
						$lesoes = $_POST['registo_acidente_localizacao_lesao'];
						$sql = "INSERT INTO `registo_acidente_localizacao_lesao` (ra_id, lesao_id) VALUES ";
						
						foreach ($lesoes as $lesao)
						{
							$sql .= "(" . $_POST['ra_id'] . ", " . $lesao . "), ";
						
						}
						
						$sql = rtrim($sql,", ");
						query($sql);
					}			
									
					// redirect to homepage
					redirect("regacidente.php?id=" . $_POST['ra_id']);
					
				}
			}
			// não temos for_id para identificar o registo de acidente - aborta
			else
				apologize("Registo de acidente não identificado - impossível editar");
			
		
		}
		elseif 	($_POST['action']=="add")
		{
			$upload_result = UploadFile($_FILES["relatorio"], "pdf");
			if ($upload_result != "ERR")
			{
				$relatorio_url = $upload_result;
			}
			else
			{
				$relatorio_url = "";
			}				
            
            
            $sql = "INSERT INTO registo_acidente 
					(data, hora, local, como, regiao, lesao, outras_lesoes, testemunhas, assistencia, tipo_lesao, participacao, incapacidade, alta_provisoria, alta_definitiva, causa, agente, acto, descricao, condicao, relatorio, estado, eid)
					VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";        
			
			
            if (query($sql, $_POST['registo_acidente_data'], $_POST['registo_acidente_hora'], $_POST['registo_acidente_local'], $_POST['registo_acidente_como'], $_POST['registo_acidente_regiao'], $_POST['registo_acidente_lesao'], $_POST['registo_acidente_outras_lesoes'], $_POST['registo_acidente_testemunhas'], $_POST['registo_acidente_assistencia'], $_POST['registo_acidente_tipo_lesao'], $_POST['registo_acidente_participacao'], $_POST['registo_acidente_incapacidade'], $_POST['registo_acidente_alta_provisoria'], $_POST['registo_acidente_alta_definitiva'], $_POST['registo_acidente_causa'], $_POST['registo_acidente_agente'], $_POST['registo_acidente_acto'], $_POST['registo_acidente_descricao'], $_POST['registo_acidente_condicao'], $relatorio_url, $_POST['registo_acidente_estado'], $_SESSION['cur_eid'] ) === false)
            {
                apologize("Algo correu mal ao inserir o novo registo de acidente na base de dados.");
            }
            else
            {

				
				$rows = query("SELECT LAST_INSERT_ID() AS ra_id");
				
				$ra_id = $rows[0]["ra_id"];
				
				// insere as várias lesões na respectiva tabela
				$lesoes = $_POST['registo_acidente_localizacao_lesao'];
				$sql = "INSERT INTO `registo_acidente_localizacao_lesao` (ra_id, lesao_id) VALUES ";
				
				foreach ($lesoes as $lesao)
				{
					$sql .= "(" . $ra_id . ", " . $lesao . "), ";
				
				}
				
				$sql = rtrim($sql,", ");
				query($sql);
				
				// redirect to homepage
                redirect("regacidente.php?id=" . $ra_id);
				
			}
        }  
        elseif 	($_POST['action']=="delete")
		{
			$sql ="DELETE FROM registo_acidente WHERE ra_id = ? AND eid = ?";
			query($sql, $_POST['ra_id'], $_SESSION['cur_eid']);
			// redirect to lista de registo de acidentes
            redirect("regacidente.php");
		}
		// não foi especificada acção no formulário - aborta
		else
			apologize("Não foi especificada acção no formulário...");
    }
	
	
	
	
	/************************************************************** 
	 *
	 * Trata dos casos em que o ficheiro é usado com $_GET
	 *
	 *************************************************************/
	 
    else
    {
		//se for especificada uma acção no URL
		if (isset($_GET['action']))
		{
			// caso o utilizador queira editar um trabalhador
			if ($_GET['action'] == 'edit')
			{
				if (isset($_GET['id']))
				{
					$ra_id = $_GET['id']; 
					
					$regacidentes  = query("SELECT * FROM registo_acidente WHERE ra_id = ? AND eid = ?", $ra_id, $_SESSION['cur_eid']);
					
					// se existe trabalhador com este tid
					if (!empty($regacidentes[0])) 
					{
						$regacidente = $regacidentes[0];
						
						//constroi as várias queries necessárias
						
						$sql_lista_lesoes = "SELECT * FROM `registo_acidente_lista_lesoes`";
						$lista_lesoes = query($sql_lista_lesoes);
				 
				 
						$sql_localizacoes_lesoes = "Select
												  registo_acidente_localizacao_lesao.ra_id,
												  registo_acidente_lista_lesoes.lesao_id,
												  registo_acidente_lista_lesoes.lesao
												From
												  registo_acidente_localizacao_lesao Inner Join
												  registo_acidente_lista_lesoes On registo_acidente_lista_lesoes.lesao_id =
													registo_acidente_localizacao_lesao.lesao_id
												Where
												  registo_acidente_localizacao_lesao.ra_id = ? 
												 Order by  registo_acidente_localizacao_lesao.lesao_id";
						
						$localizacoes_lesoes = query($sql_localizacoes_lesoes, $regacidente['ra_id']);
						
						// faz lista simples de indices de lesões, para definir se o select está ou não escolhido
						$localizacoes_lesoes_lista =array();
						foreach ($localizacoes_lesoes as $lesao)
						{
							$localizacoes_lesoes_lista[] = $lesao['lesao_id'];
						}
						
					}
					
					// a query devolveu uma lista vazia
					else 
						apologize("O registo de acidente especificado não existe...");
				
					render("regacidente_form_editar.php", array("title" => "Editar Registo Acidente", "regacidente" => $regacidente, "localizacoes_lesoes" => $localizacoes_lesoes, "localizacoes_lesoes_lista" => $localizacoes_lesoes_lista, "lista_lesoes" => $lista_lesoes));
				}
				
				// o utilizador quer editar, mas falta o tid no URL, logo não sabemos qual é o trabalhador a editar
				else 
					apologize("Não foi especificado qual o registo de acidente a editar.");
			}
			
			// caso o utilizador queira adicionar um trabalhador
			elseif ($_GET['action'] == 'add')
			{
				 $sql_lista_lesoes = "SELECT * FROM `registo_acidente_lista_lesoes`";
				 $lista_lesoes = query($sql_lista_lesoes);
				 
				 render("regacidente_form_adicionar.php", array("title" => "Adicionar Registo Acidente", "lista_lesoes" => $lista_lesoes));
			}
			
			// caso o utilizador queira apagar um trabalhador
			elseif ($_GET['action'] == 'delete')
			{
				if (isset($_GET['id']))
				{
					$ra_id = $_GET['id']; 
					
					$regacidentes  = query("SELECT * FROM registo_acidente WHERE ra_id = ? AND eid = ?", $ra_id, $_SESSION['cur_eid']);
					
					// se existe trabalhador com este tid
					if (!empty($regacidentes[0])) 
					{
						$regacidente = $regacidentes[0];
					}
					
					// a query devolveu uma lista vazia
					else 
						apologize("Remover registo acidente - registo especificado não existe...");
				
					render("regacidente_form_remover.php", array("title" => "Remover Registo Acidente", "regacidente" => $regacidente));
				}
				
				// o utilizador quer editar, mas falta o tid no URL, logo não sabemos qual é o trabalhador a editar
				else 
					apologize("Não foi especificado qual o registo acidente a remover.");
			}
			
			else
				apologize("Operação não suportada.");
		
		}		
		
		
		//se não houver acção especificada no URL, mas houver um tid
		elseif (isset($_GET['id'])) 
		{
			$ra_id = $_GET['id']; 
			
			//constroi as várias queries necessárias
			$regacidentes  = query("SELECT * FROM registo_acidente WHERE ra_id = ? AND eid = ?", $ra_id, $_SESSION['cur_eid']);
			
			$sql_postos_trabalho = "Select
			  registo_acidente.ra_id,
			  `posto-trabalho`.ptid As ptid,
			  `posto-trabalho`.eid As eid,
			  `posto-trabalho`.nome As nome,
			  `posto-trabalho`.foto As foto
			From
			  registo_acidente Inner Join
			  `posto-to-reg_acidente` On registo_acidente.ra_id =
				`posto-to-reg_acidente`.ra_id Inner Join
			  `posto-trabalho` On `posto-trabalho`.ptid = `posto-to-reg_acidente`.ptid
			Where
			  registo_acidente.ra_id = ? AND registo_acidente.eid = ? ";
			  
			$sql_trabalhadores = "Select
			  registo_acidente.ra_id,
			  trabalhador.tid,
			  registo_acidente.eid,
			  trabalhador.nome,
			  trabalhador.num_mecanografico
			From
			  registo_acidente Inner Join
			  `trabalhador-to-reg_acidente` On registo_acidente.ra_id =
				`trabalhador-to-reg_acidente`.ra_id Inner Join
			  trabalhador On `trabalhador-to-reg_acidente`.tid = trabalhador.tid
			Where
			  registo_acidente.ra_id = ? And
			  registo_acidente.eid = ?";
			  
			$sql_maquinas = "Select
			  registo_acidente.ra_id,
			  registo_acidente.eid,
			  maquina.nome As nome,
			  maquina.num_serie As num_serie
			From
			  registo_acidente Inner Join
			  `maquina-to-reg_acidente` On registo_acidente.ra_id =
				`maquina-to-reg_acidente`.ra_id Inner Join
			  maquina On `maquina-to-reg_acidente`.mid = maquina.mid
			Where
			  registo_acidente.ra_id = ? And
			  registo_acidente.eid = ? ";
			  
			$sql_todos_trabalhadores = "Select
			  registo_acidente.ra_id As ra_id,
			  trabalhador.tid As tid,
			  trabalhador.nome As nome,
			  trabalhador.num_mecanografico As num_mecanografico,
			  trabalhador.foto As foto
			FROM
				trabalhador LEFT JOIN (SELECT * FROM `trabalhador-to-reg_acidente` WHERE `trabalhador-to-reg_acidente`.ra_id = ?) registo_acidente ON trabalhador.tid = registo_acidente.tid
			WHERE
				registo_acidente.tid IS NULL
			And eid = ?
			ORDER BY nome ASC";
			
			
			$sql_todos_ptrabalhos = "
			SELECT 
				registo_acidente.ra_id AS ra_id,  
				`posto-trabalho`.ptid AS ptid,  
				`posto-trabalho`.nome AS nome,  
				`posto-trabalho`.foto AS foto
			FROM  
				`posto-trabalho` 
				LEFT JOIN (
				SELECT * 
				FROM  `posto-to-reg_acidente` 
				WHERE  `posto-to-reg_acidente`.ra_id = ?
				)registo_acidente ON  `posto-trabalho`.ptid = registo_acidente.ptid
			WHERE registo_acidente.ptid IS NULL 
			AND eid = ?
			ORDER BY nome ASC 
			";
			
			$sql_todas_maquinas = "
			SELECT 
				registo_acidente.ra_id AS ra_id,  
				maquina.mid AS mid,  
				maquina.nome AS nome,  
				maquina.foto AS foto
			FROM  
				maquina LEFT JOIN (SELECT * FROM  `maquina-to-reg_acidente` 
				WHERE  `maquina-to-reg_acidente`.ra_id = ?
				)registo_acidente ON  maquina.mid = registo_acidente.mid
			WHERE registo_acidente.mid IS NULL 
			AND eid = ?
			ORDER BY nome ASC 
			";
			
			$sql_localizacoes_lesoes = "Select
												  registo_acidente_localizacao_lesao.ra_id,
												  registo_acidente_lista_lesoes.lesao_id,
												  registo_acidente_lista_lesoes.lesao
												From
												  registo_acidente_localizacao_lesao Inner Join
												  registo_acidente_lista_lesoes On registo_acidente_lista_lesoes.lesao_id =
													registo_acidente_localizacao_lesao.lesao_id
												Where
												  registo_acidente_localizacao_lesao.ra_id = ? 
												 Order by  registo_acidente_localizacao_lesao.lesao_id";		
									
			if (!empty($regacidentes[0])) // se há registos de acidente com este id
			{
				// o regacidente que queremos está na linha 0
				$regacidente = $regacidentes[0];
				
				// dado que o regacidente existe podemos executar as outras queries
				$postos_trabalho = query($sql_postos_trabalho, $ra_id, $_SESSION['cur_eid']);
				$trabalhadores = query($sql_trabalhadores, $ra_id, $_SESSION['cur_eid']);
				$maquinas = query($sql_maquinas, $ra_id, $_SESSION['cur_eid']);
				$todos_trabalhadores = query($sql_todos_trabalhadores, $ra_id, $_SESSION['cur_eid']);
				$todos_ptrabalhos = query($sql_todos_ptrabalhos, $ra_id, $_SESSION['cur_eid']);
				$todas_maquinas = query($sql_todas_maquinas, $ra_id, $_SESSION['cur_eid']);
				$localizacoes_lesoes = query($sql_localizacoes_lesoes, $regacidente['ra_id']);
						
				// faz lista simples de indices de lesões, para definir se o select está ou não escolhido
				$localizacoes_lesoes_lista =array();
				foreach ($localizacoes_lesoes as $lesao)
				{
					$localizacoes_lesoes_lista[] = $lesao['lesao'];
				}
						
				// mostra o regacidente
				render("regacidente_visualizar.php", array("title" => "Registo Acidente", "regacidente" => $regacidente, "ra_id" => $ra_id, "postos_trabalho" => $postos_trabalho, "trabalhadores" => $trabalhadores, "maquinas" => $maquinas, "todos_trabalhadores" => $todos_trabalhadores, "todos_ptrabalhos" => $todos_ptrabalhos, "todas_maquinas" => $todas_maquinas, "localizacoes_lesoes_lista" => $localizacoes_lesoes_lista));
			}
			else // a query devolveu uma lista vazia
				apologize("O registo de acidente especificado não existe...");	
		}
		
		
		
		// se não for especificado o id do regacidente no URL, mostra o report de acidentes e a lista de acidentes
		else
		{
			$sql_acidentesporano = "SELECT Year( `registo_acidente`.data ) AS ano, Count( * ) AS acidentes
									FROM registo_acidente
									where registo_acidente.eid = ?
									GROUP BY Ano";
			$acidentesporano = query($sql_acidentesporano, $_SESSION['cur_eid'] );
			
			$sql_acidentesporpostotrabalho = "SELECT `posto-trabalho`.nome as posto, Count( * ) AS acidentes
									FROM `posto-trabalho` 
									LEFT JOIN `posto-to-reg_acidente` ON `posto-trabalho`.ptid = `posto-to-reg_acidente`.ptid
									INNER JOIN registo_acidente ON `posto-to-reg_acidente`.ra_id = registo_acidente.ra_id
									where registo_acidente.eid = ?
									GROUP BY `posto-trabalho`.nome";
			$acidentesporpostotrabalho = query($sql_acidentesporpostotrabalho, $_SESSION['cur_eid'] );
			
			$sql_acidentesportrabalhador = "SELECT trabalhador.nome as trabalhador, Count( * ) AS acidentes
									FROM trabalhador
									INNER JOIN `trabalhador-to-reg_acidente` ON trabalhador.tid = `trabalhador-to-reg_acidente`.tid
									INNER JOIN registo_acidente ON `trabalhador-to-reg_acidente`.ra_id = registo_acidente.ra_id
									where registo_acidente.eid = ?
									GROUP BY trabalhador.nome";
			$acidentesportrabalhador = query($sql_acidentesportrabalhador, $_SESSION['cur_eid'] );
			
			$regacidentes = query("SELECT * FROM registo_acidente WHERE eid = ?", $_SESSION['cur_eid']);
			render("regacidente_lista.php", array("title" => "Lista de Registo de Acidentes", "regacidentes" => $regacidentes, "acidentesporano" => $acidentesporano, "acidentesporpostotrabalho" => $acidentesporpostotrabalho, "acidentesportrabalhador" => $acidentesportrabalhador));
		}
		
    }

?>

