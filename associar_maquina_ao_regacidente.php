<?php

    // configuration
    require("includes/config.php");

    // if form was submitted
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {	
		if ($_POST['mid'] == 0)
			apologize("Tem de escolher uma máquina da lista!");
		
		// verifica se o formulário não foi adulterado com SQL injections
		$sql_mid = "SELECT * FROM maquina WHERE mid = ? AND eid = ?";
		$sql_raid = "SELECT * FROM registo_acidente WHERE ra_id = ? AND eid = ?";
				
		$rows_mid = query($sql_mid, $_POST['mid'], $_SESSION['cur_eid']);
		$rows_raid = query($sql_raid, $_POST['ra_id'], $_SESSION['cur_eid']);
				
		if ((($rows_mid === false) || empty($rows_mid)) || (($rows_raid === false) || empty($rows_raid)))
			apologize("Os dados para associar a máquina ao registo foram corrompidos.");
		else
		{
			$sql = "INSERT INTO `maquina-to-reg_acidente`
			(mid, ra_id) 
			VALUES (?, ?)";
			
			if (query($sql, $_POST['mid'], $_POST['ra_id']) === false)
				apologize("Algo correu mal ao associar a máquina ao registo de acidente na base de dados.");
			else
				redirect("regacidente.php?id=" . $_POST['ra_id']);
		}
	}
	else
		apologize("Algo correu mal ao associar a máquina ao registo de acidente");
?>