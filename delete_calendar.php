<?php

    // configuration
    require("includes/config.php");

    if (isset($_POST["calendar_id"]))
        $calendar_id = $_POST["calendar_id"];
        

    // if form was submitted
    if ($_SERVER["REQUEST_METHOD"] == "POST" && !isset($_POST["formname"]))
    {
        
        if (empty($_POST["delete_confirmation"]))
        {
            apologize("Delete confirmation not received. Something went wrong...");
            exit();
        } 
        elseif (empty($_POST["calendar_id"]))
        {
            apologize("Calendar id not received. Something went wrong...");
            exit();
        }
        
        else
        {
            // if none of the above occurred, odds are everything is ok
            // so try to insert new event in database
            
            $query_string = "DELETE FROM calendar WHERE calendar_id = ?";
            //dump($query_string);
            if (query($query_string, $_POST["calendar_id"] ) === false)
            {
                apologize("Failed to delete from Database. Something went wrong...");
            }
            else
            {
                // redirect to previous page if set
                if (isset($_POST["referrer"]))
                    redirect($_POST["referrer"]);
                else
                    redirect("/");
			}
            
        }
    }
    else
    {
                        
        $rows = query("SELECT * FROM calendar WHERE calendar_id = ?", $calendar_id);
        $calendar = $rows[0];
               
        // render form
        render("delete_calendar_form.php", array("title" => "Delete calendar event", "calendar" => $calendar, "referrer" => $_POST["referrer"]));
    }

?>

