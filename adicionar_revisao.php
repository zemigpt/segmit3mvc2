<?php

    // configuration
    require_once("includes/config.php");

    // if form was submitted
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        /**************
		*
		* Validation
		*
		*****************/
		
		/*if (empty($_POST["field"]))
        {
            apologize("O campo 'field' � obrigat�rio!");
        }
        
        else
        {*/
            // if none of the above occurred, odds are everything is ok
            // so insert new record in database
                   
           
		    // grava o ficheiro
            // code adapted from http://www.w3schools.com/php/php_file_upload.asp
			
			
			$upload_result = UploadFile($_FILES["trab_foto"]);
			if ($upload_result != "ERR")
			{
				$foto_url = $upload_result;
			}
			else
			{
				$foto_url = "";
			}			
            
            
            $sql = "INSERT INTO maquina_manutencao 
					(mid, data_revisao, relatorio_revisao, data_prox_revisao, eid) 
					VALUES(?, ?, ?, ?, ?)";
            
            
            
            
            if (query($sql, $_POST['maq_revisao_data_revisao'], $_POST['maq_revisao_relatorio_revisao'], $_POST['maq_revisao_data_proxima_revisao'], $_SESSION['cur_eid'] ) === false)
            {
                apologize("Algo correu mal ao inserir o novo trabalhador na base de dados.");
            }
            else
            {

				
				$rows = query("SELECT LAST_INSERT_ID() AS mr_id");
				
				$mr_id = $rows[0]["mr_id"];
								
				// redirect to homepage
                redirect("index.php?eid=" . $_SESSION['cur_eid']);
				
			}
            
            
            
        /*}
		
		**********************/
    }
    else
    {
        // else render form
        render("adicionar_revisao_form.php", array("title" => "Adicionar Revis�o", "eid" => $_SESSION['cur_eid']));
    }

?>

