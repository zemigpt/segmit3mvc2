<?php

    // configuration
    require_once("includes/config.php");

    // if form was submitted
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        /**************
		*
		* Validation
		*
		*****************/
		
		/*if (empty($_POST["field"]))
        {
            apologize("O campo 'field' � obrigat�rio!");
        }
        
        else
        {*/
            // if none of the above occurred, odds are everything is ok
            // so insert new record in database
                   
           
		    // grava o ficheiro
            // code adapted from http://www.w3schools.com/php/php_file_upload.asp
			
			$upload_result = UploadFile($_FILES["relatorio"], "pdf");
			if ($upload_result != "ERR")
			{
				$relatorio_url = $upload_result;
			}
			else
			{
				$relatorio_url = "";
			}			
			
			$sql = "INSERT INTO registo_acidente 
					(data, relatorio, estado, eid) 
					VALUES(?, ?, ?, ?)";
            
            
            
            
            if (query($sql, $_POST['registo_acidente_data'], $relatorio_url, $_POST['registo_acidente_estado'], $_SESSION['cur_eid'] ) === false)
            {
                apologize("Algo correu mal ao inserir o registo de acidente na base de dados.");
            }
            else
            {

				
				$rows = query("SELECT LAST_INSERT_ID() AS ra_id");
				
				$ra_id = $rows[0]["ra_id"];
								
				// redirect to homepage
                redirect("index.php?ra_id=0");
				
			}
            
            
            
        /*}
		
		**********************/
    }
    else
    {
        // else render form
        render("adicionar_regacidente_form.php", array("title" => "Adicionar Registo Acidente", "eid" => $_SESSION['cur_eid']));
    }

?>

