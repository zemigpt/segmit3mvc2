<?php

    // configuration
    require_once("includes/config.php");

    // if form was submitted
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        /**************
		*
		* Validation
		*
		*****************/
		
		/*if (empty($_POST["field"]))
        {
            apologize("O campo 'field' � obrigat�rio!");
        }
        
        else
        {*/
            // if none of the above occurred, odds are everything is ok
            // so insert new record in database
                   
           
		    // grava o ficheiro
            // code adapted from http://www.w3schools.com/php/php_file_upload.asp
			
			
			
			$upload_result = UploadFile($_FILES["maq_foto"], "image");
			if ($upload_result != "ERR")
			{
				$foto_maq = $upload_result;
			}
			else
			{
				$foto_maq = "";
			}			
           
   		    $upload_result = UploadFile($_FILES["maq_manual_instrucao"], "pdf");
			if ($upload_result != "ERR")
			{
				$manual_instrucao_url = $upload_result;
			}
			else
			{
				$manual_instrucao_url = "";
			}
            
			$upload_result = UploadFile($_FILES["maq_instrucao_trabalho"], "pdf");
			if ($upload_result != "ERR")
			{
				$instrucao_trabalho_url = $upload_result;
			}
			else
			{
				$instrucao_trabalho_url = "";
			}
			
			//constroi a query sql
            $sql = "INSERT INTO maquina 
					(nome, tm_id, fid, num_serie, foto, data_compra, data_aprov_seguranca, manual_instrucao, instrucao_trabalho, eid) 
					VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

			// corre a query sql e verifica se deu erro
            if (query($sql, $_POST['maq_nome'], $_POST['maq_tipo_maquina'],$_POST['maq_fabricante'], $_POST['maq_num_serie'], $foto_maq, $_POST['maq_data_compra'], $_POST['maq_data_aprov_seguranca'], $manual_instrucao_url, $instrucao_trabalho_url, $_SESSION['cur_eid'] ) === false)
            {
                apologize("Algo correu mal a inserir a m�quina nova na base de dados.");
            }
			
			// se n�o tiver dado erro
            else 
            {
				$rows = query("SELECT LAST_INSERT_ID() AS mid");
				
				$mid = $rows[0]["mid"];
				
			}
            
			// verifica se h� campo $tid. Se houver, insere na tabela maquina-to-trabalhador
			if (isset($_POST['trab_id']) && !empty($_POST['trab_id'])) 
			{
				//constroi a query sql
				$sql = "INSERT INTO `maquina-to-trabalhador`
						(mid, tid) 
						VALUES(?, ?)";

				// corre a query sql e verifica se deu erro
				if (query($sql, $mid, $_POST['trab_id'] ) === false)
				{
					apologize("Algo correu mal a inserir a m�quina nova na base de dados.");
				}
				
				// se n�o tiver dado erro
				else 
				{								
					// redirect to homepage
					redirect("index.php?tid=" . $_POST['trab_id']);	
				}		
			
			}
			
			redirect("index.php?tid=" . $_POST['trab_id']);	
            
            
        /*}
		
		**********************/
    }
    else
    {
        // else render form
		
		// faz lista de tipos de maquina
		$sql="SELECT * FROM tipo_maquina WHERE eid = ?";
		$tipo_maquinas = query($sql, $_SESSION['cur_eid']);
		
		/// faz lista de fabricantes
		$sql="SELECT * FROM fabricante WHERE eid = ?";
		$fabricantes = query($sql, $_SESSION['cur_eid']);
		
		// verifica se esta m�quina est� a ser adicionada atrav�s do trabalhador
		if (isset($_GET['tid']))
			$tid=$_GET['tid'];
		else
			$tid="";
			
		// verifica se esta m�quina est� a ser adicionada atrav�s do posto de trabalho
		if (isset($_GET['ptid']))
			$ptid=$_GET['ptid'];
		else
			$ptid="";
			
        render("adicionar_maquina_form.php", array("title" => "Adicionar Maquina", "fabricantes" => $fabricantes, "tipo_maquinas" => $tipo_maquinas, "eid" => $_SESSION['cur_eid'], "tid" => $tid, "ptid" => $ptid ));
    }

?>

