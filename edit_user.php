<?php

    // configuration
    require("includes/config.php");

    // if form was submitted
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        if (empty($_POST["username"]))
        {
            apologize("You must provide your username.");
        }
        else if  (empty($_POST["password"]))
        {
            apologize("You must provide a password.");
        }
        
        else if ($_POST["password"] != $_POST["confirmation"])
        {
            apologize("Passwords do not match!");
        }
        else if  (empty($_POST["gender"]))
        {
            apologize("You must provide your gender.");
        }
        else if  (empty($_POST["email"]))
        {
            apologize("You must provide an email.");
        }
        else
        {
            // if none of the above occurred, odds are everything is ok
            // so insert new user in database
            if (query("UPDATE users SET username = ?, password = ?, email = ?, gender = ? WHERE user_id = ?", $_POST["username"], crypt($_POST["password"]), $_POST["email"], $_POST["gender"], $_SESSION["id"] ) === false)
            {
                apologize("Something went wrong with the update... ");
            }
            else
            {
				

                // redirect to homepage
                redirect("user.php");
				
			}
            
            
            
        }
    }
    else
    {
        
        $sql = "SELECT * FROM users WHERE user_id = ?";
        $rows = query($sql, $_SESSION["id"]);
        $user = $rows[0];
        
        // else render form
        render("edit_user_form.php", array("title" => "Edit User", "user" => $user));
    }

?>

