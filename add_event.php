<?php

    // configuration
    require("includes/config.php");

    // if form was submitted
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        
        $user_id = $_SESSION["id"];
        $baby_id = $_SESSION["baby_id"];

        if (empty($_POST["event_date"]))
        {
            apologize("The event to log must have a date. None was specified.");
            exit();
        }
        elseif (empty($_POST["event_type"]))
        {
            apologize("Event type not specified. You must choose an event type.");
            exit();
        }
        elseif (empty($_POST["event_title"]) && empty($_POST["event_description"]))
        {
            apologize("Event title and Event description can't be both blank!");
            exit();       
        } 
        else
        {
            // if none of the above occurred, odds are everything is ok
            // so try to insert new event in database
            
            $query_string = "INSERT INTO event (baby_id, user_id, event_date, event_title, event_description, event_type) VALUES(?, ?, ?, ?, ?, ?)";
           
            if (query($query_string, $baby_id, $user_id, $_POST["event_date"], $_POST["event_title"], $_POST["event_description"], $_POST["event_type"]) === false)
            {
                apologize("Something went wrong...");
            }
            else
            {
				// if query returns true then new event insert was successull. 
				
				$rows = query("SELECT LAST_INSERT_ID() AS id");
				$event_id = $rows[0]["id"];
            
                // let's now save the pictures if any has been posted
                // code adapted from http://www.w3schools.com/php/php_file_upload.asp
                
                foreach ($_FILES["event_image"]["error"] as $i => $error) 
                {
                    if ($error == UPLOAD_ERR_OK) 
                    {  
              
                        $files_dir = "files/";
                                                
                        if ((($_FILES["event_image"]["type"][$i] == "image/gif")
                            || ($_FILES["event_image"]["type"][$i] == "image/jpeg")
                            || ($_FILES["event_image"]["type"][$i] == "image/jpg")
                            || ($_FILES["event_image"]["type"][$i] == "image/png"))
                            && ($_FILES["event_image"]["size"][$i] < 20000000))
                        {
                            if ($_FILES["event_image"]["error"][$i] > 0)  //error
                            {
                                apologize("An error has occurred while saving your uploaded file");
                            }
                            
                            else //success
                            {
                                if (file_exists($files_dir . $_FILES["event_image"]["name"][$i]))
                                {
                                    $filedetails = explode(".",$_FILES["event_image"]["name"][$i]);
                                    $fileextension = $filedetails[count($filedetails)-1];
                                    $filename=$filedetails[0];
                                    for($j=1;$j< count($filedetails)-1;$j++)
                                        $filename = $filename . "." . $filedetails[$j];                           
                                    
                                    $iteration = 0;
                                    do
                                    {
                                        $iteration++;
                                        $finalfile = $filename . "_" . $iteration . "." . $fileextension;
                                    
                                    } while (file_exists($files_dir . $finalfile));
                                    
                                    move_uploaded_file($_FILES["event_image"]["tmp_name"][$i], $files_dir . $finalfile);
                                    chmod($files_dir . $finalfile, 0644);
                                }
                                
                                
                                else
                                {
                                    $finalfile = $_FILES["event_image"]["name"][$i]; 
                                    move_uploaded_file($_FILES["event_image"]["tmp_name"][$i], $files_dir . $finalfile);
                                    chmod($files_dir . $finalfile, 0644);
                                }
                                
                                // insert the image in the database
                                $imageurl = $files_dir . $finalfile;
                                if (query("INSERT INTO event_images (event_id, image_url) VALUES (?,?)", $event_id, $imageurl) === false)
                                {
                                    apologize("Tried to insert picture in database, but something went wrong...");
                                }
                                    
                                    
                                }
                            }
                            else
                            {
                                apologize("You have uploaded an invalid file");
                            }
                    }    
                }
               
                
                
                // redirect to homepage
                redirect("/");
				
			}
            
        }
    }
    else
    {
        $eventtypes = query("SELECT * FROM event_types ORDER BY event_type_description ASC");
        
        // else render form
        render("add_event_form.php", array("title" => "Add post", "eventtypes" => $eventtypes));
    }

?>

