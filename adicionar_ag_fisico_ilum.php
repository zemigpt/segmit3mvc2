<?php

    // configuration
    require("includes/config.php");

    // if form was submitted
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        /**************
		*
		* Validation
		*
		*****************/
		
		/*if (empty($_POST["field"]))
        {
            apologize("O campo 'field' � obrigat�rio!");
        }
        
        else
        {*/
            // if none of the above occurred, odds are everything is ok
            // so insert new record in database
                   
           
		    // grava o ficheiro
            // code adapted from http://www.w3schools.com/php/php_file_upload.asp            
            
            $sql = "INSERT INTO ag_fisico_aval_ilum
					(registo, data, intervalo, eid) 
					VALUES(?,?,?,?)";
            
            
            
       
            if (query($sql, $_POST['ag_fisico_aval_ilum_registo'], $_POST['ag_fisico_aval_ilum_data'], $_POST['ag_fisico_aval_ilum_intervalo'], $_SESSION['cur_eid'] ) === false)
            {
                apologize("Algo correu mal ao inserir o registo de avalia��o de agentes f�sicos ilum�ncia na base de dados.");
            }
            else
            {

				
				$rows = query("SELECT LAST_INSERT_ID() AS afai_id");
				
				$afai_id = $rows[0]["afai_id"];
				
				
				if (!isset($_POST["ajax"]) || $_POST["ajax"] != "true") {
					// redirect to homepage
					redirect("index.php?eid=" . $_SESSION['cur_eid']);
				}
				else
				{
					echo json_encode(array("afai_id" => $afai_id, "ag_fisico_aval_ilum_registo" => $_POST['ag_fisico_aval_ilum_registo']));
				}
			}
            
            
            
        /*}
		
		**********************/
    }
    else
    {
        // else render form 
        render("adicionar_ag_fisico_aval_ilum_form.php", array("title" => "Adicionar Avalia��o Agente F�sico: Ilumin�ncia", "eid" => $_SESSION['cur_eid']));
    }

?>

