﻿<div class="row-fluid">
	<div class="span6 offset3">
		<form action="trabalhador.php" method="post" enctype="multipart/form-data" id="editar-trabalhador" class="form-horizontal well" >
			<fieldset>
				<legend><?=$title?></legend>
				<div class="control-group">
					<label class="control-label" for="trab_name">Nome do Trabalhador</label>
					<div class="controls">
						<input autofocus name="trab_nome" type="text" value="<?=$trabalhador['nome']?>"/>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="trab_foto">Fotografia</label>
					<div class="controls">
						<img src="<?=$trabalhador['foto']?>" height="100px"><br/>
						<input type="hidden" name="trab_foto_existente" value="<?=$trabalhador['foto']?>">
						<input type="file" name="trab_foto"/>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="trab_morada">Número Mecanográfico</label>
					<div class="controls">
						<input type="text" name="trab_num_mecanografico" value="<?=$trabalhador['num_mecanografico']?>"/>
					</div>
				</div>
				
				
				<div class="control-group">
					<label class="control-label" for="trab_morada">Morada</label>
					<div class="controls">
						<input type="text" name="trab_morada" value="<?=$trabalhador['morada']?>"/>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="trab_codpostal">Cod. Postal</label>
					<div class="controls">
						<input type="text" name="trab_codpostal" value="<?=$trabalhador['codpostal']?>"/>
				   </div>
				</div>
				<div class="control-group">
					<label class="control-label" for="trab_localidade">Localidade</label>
					<div class="controls">
						<input type="text" name="trab_localidade" value="<?=$trabalhador['localidade']?>"/>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="trab_telefone">Telefone</label>
					<div class="controls">
						<input type="text" name="trab_telefone" value="<?=$trabalhador['telefone']?>"/>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="trab_email">Email</label>
					<div class="controls">
						<input type="text" name="trab_email" value="<?=$trabalhador['email']?>" required/>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="trab_contacto_sos_nome">Contacto Emergência</label>
					<div class="controls">
						<input type="text" name="trab_contacto_sos_nome" value="<?=$trabalhador['contacto_sos_nome']?>"/>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label" for="trab_contacto_sos_telefone">Telefone Emergência</label>
					<div class="controls">
						<input type="text" name="trab_contacto_sos_telefone" value="<?=$trabalhador['contacto_sos_telefone']?>"/>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label" for="trab_bi">Doc. Identificação</label>
					<div class="controls">
						<input type="text" name="trab_bi" value="<?=$trabalhador['bi']?>" required/>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="datepicker">Validade Doc. Ident.</label>
					<div class="controls">
						<input type="text" class="datepicker" name="trab_bi_validade" value="<?=$trabalhador['bi_validade']?>"/>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="trab_niss">Núm. Seg. Social</label>
					<div class="controls">
						<input type="text" name="trab_niss" value="<?=$trabalhador['niss']?>" required/>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="trab_nif">Número Contribuinte</label>
					<div class="controls">
						<input type="text" name="trab_nif" value="<?=$trabalhador['nif']?>" required/>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="trab_apolice_seguro">Apólice Seguro</label>
					<div class="controls">
						<input type="text" name="trab_apolice_seguro" value="<?=$trabalhador['apolice_seguro']?>"/>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label" for="trab_tamanho_sapato">Tamanho Sapato</label> 
					<div class="controls"> 
						<select name="trab_tamanho_sapato">
							<option value="" disabled selected>Escolher Tamanho</option>
							<option value="34" <?php if ($trabalhador['tamanho_sapato'] == 34) echo "selected"?> >34</option>
							<option value="35" <?php if ($trabalhador['tamanho_sapato'] == 35) echo "selected"?> >35</option>
							<option value="36" <?php if ($trabalhador['tamanho_sapato'] == 36) echo "selected"?> >36</option>
							<option value="37" <?php if ($trabalhador['tamanho_sapato'] == 37) echo "selected"?> >37</option>
							<option value="38" <?php if ($trabalhador['tamanho_sapato'] == 38) echo "selected"?> >38</option>
							<option value="39" <?php if ($trabalhador['tamanho_sapato'] == 39) echo "selected"?> >39</option>
							<option value="40" <?php if ($trabalhador['tamanho_sapato'] == 40) echo "selected"?> >40</option>
							<option value="41" <?php if ($trabalhador['tamanho_sapato'] == 41) echo "selected"?> >41</option>
							<option value="42" <?php if ($trabalhador['tamanho_sapato'] == 42) echo "selected"?> >42</option>
							<option value="43" <?php if ($trabalhador['tamanho_sapato'] == 43) echo "selected"?> >43</option>
							<option value="44" <?php if ($trabalhador['tamanho_sapato'] == 44) echo "selected"?> >44</option>
							<option value="45" <?php if ($trabalhador['tamanho_sapato'] == 45) echo "selected"?> >45</option>
							<option value="46" <?php if ($trabalhador['tamanho_sapato'] == 46) echo "selected"?> >46</option>
							<option value="47" <?php if ($trabalhador['tamanho_sapato'] == 47) echo "selected"?> >47</option>
						</select>
					</div>
				</div>
				
				<div class="control-group"> 
					<label class="control-label" for="trab_tamanho_roupa">Tamanho Roupa</label> 
					<div class="controls"> 
						<select name="trab_tamanho_roupa">
							<option value="" disabled selected>Escolher Tamanho</option>
							<option value="XS" <?php if ($trabalhador['tamanho_roupa'] == "XS") echo "selected"?> >XS</option>
							<option value="S" <?php if ($trabalhador['tamanho_roupa'] == "S") echo "selected"?> >S</option>
							<option value="M" <?php if ($trabalhador['tamanho_roupa'] == "M") echo "selected"?> >M</option>
							<option value="L" <?php if ($trabalhador['tamanho_roupa'] == "L") echo "selected"?> >L</option>
							<option value="XL" <?php if ($trabalhador['tamanho_roupa'] == "XL") echo "selected"?> >XL</option>
							<option value="XXL" <?php if ($trabalhador['tamanho_roupa'] == "XXL") echo "selected"?> >XXL</option>
						</select>
					</div> 
				</div>


				
				<input type="hidden" name="trab_eid" value="<?=$_SESSION['cur_eid'] ?>">
				<input type="hidden" name="trab_id" value="<?=$trabalhador['tid']?>">
				<input type="hidden" name="action" value="edit">
				<div class="form-actions">
					<button type="submit" class="btn btn-primary">Gravar alterações</button>
					<a href="#" class="btn" onclick="history.go(-1);">Cancelar</a>
				</div>
								
			</fieldset>
		</form>
	</div>
</div>
