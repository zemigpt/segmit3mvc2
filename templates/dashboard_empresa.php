﻿<div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><a href="#" class="btn-minimize"><i class="icon-chevron-up"></i></a> <i class="icon-user"></i> Lista de Trabalhadores</h2>
			<div class="box-icon">
				<a href="adicionar_trabalhador.php?eid=<?=$eid?>" title="Adicionar Trabalhador" class="btn btn-round"><i class="icon-plus"></i></a>
			 
			</div>
		</div>
		<div class="box-content">
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>Foto</th>
						<th>Nome Trabalhador</th>
						<th>Acções</th>
					</tr>
				</thead>

				<tbody>
					<?php if (empty($trabalhadores)) echo '<tr><td colspan="8">Esta empresa não tem trabalhadores associados</td></tr>'; ?>
						<?php foreach ($trabalhadores as $trabalhador): ?>				
						<tr>
							<td><img src="<?= $trabalhador["foto"]?>" width="75px"/></td>
							<td><?= $trabalhador["nome"]?></td>
							<td><a class="btn" href="trabalhador.php?id=<?= $trabalhador["tid"]?>"><i class="icon-eye-open" title="Ver detalhes do trabalhador"></i></a> <a class="btn" href="#"  title="Editar trabalhador"><i class="icon-pencil"></i></a></td>
						</tr>
						<? endforeach ?>
				</tbody>
			</table>
		</div>
	</div><!--/span-->
</div><!--/row-->

