<!-- jquery script adapted from Lecture 9m -->
<script src="/js/jquery-1.8.2.js"></script>
<script>

    // onready
    $(document).ready(function() {

        // onsubmit
        $('#registration').submit(function() {

            //set regex for email checking
            var emailregex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            
            // validate username
            if ($('#registration input[name=username]').val() == '')
            {
                alert('You must provide a username!');
                return false;
            }

            // validate password
            else if ($('#registration input[name=password]').val() == '')
            {
                alert('You must provide a password!');
                return false;
            }

            // validate confirmation
            else if ($('#registration input[name=password]').val() != $('#registration input[name=confirmation]').val()) 
            {
                alert('Passwords do not match!');
                return false;
            }
            
            // validate gender
            else if ($('#registration input[name=gender]').val() ==''()) 
            {
                alert('You must fill gender info!');
                return false;
            }
            
            // validate email
            else if ($('#registration input[name=email]').val() ==''()) 
            {
                alert('You must provide an email!');
                return false;
            }
            else if ($('#registration input[name=email]').val() ==''()) 
            {
                alert('You must provide an email!');
                return false;
            }
            
            
            // valid!
            return true;

        });

    });

</script>


<div class="row-fluid">
	<div class="span4 offset4">
		<form action="register.php" method="post" id="registration" class="form-horizontal well">
			<fieldset>
				<legend>Registar</legend>
				<div class="control-group">
					<label class="control-label" for="username">Username</label>
					<div class="controls">
						<input autofocus name="username" placeholder="Username" type="text"/>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="password">Password</label>
					<div class="controls">
						<input name="password" placeholder="Password" type="password"/>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="confirmation">Password Confirmation</label>
					<div class="controls">
						<input name="confirmation" placeholder="Password Confirmation" type="password"/>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="email">E-mail</label>
					<div class="controls">
						<input name="email" placeholder="E-mail" type="text"/>
					</div>
				</div>
			  				
				<div class="form-actions">
					<button type="submit" class="btn btn-primary">Registar</button>
				</div>
			</fieldset>
		</form>
	</div>
</div>
<div class="row-fluid">
	<div class="span12">
		<p class="text-center">or <a href="login.php">log in</a></p>
	</div>
</div>

