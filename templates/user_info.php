<h2>User info</h2>
<br/>
<div id="container">
    <div id="baby-list">
        <h4> List of pregnancies </h4>
            <?php if (isset($babies) && count($babies) > 0) {?>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th> Baby's name </th>
                            <th> Time </th>
                        </tr>
                    </thead> 
                    <tbody>
                        <?php foreach ($babies as $baby) { ?>
                            <tr>
                                <td><?="<a href='index.php?bid=" . $baby["baby_id"] ."'>" . BabyNameOrPlaceholder($baby["baby_name"]) . "</a>" ?></td>
                                <td><?= calcWeeks($baby["last_period_date"]) ?></td>
                            <tr>
                        <?php } ?>
                    </tbody>
                </table>
            <?php } else {?>
                <br/>
                <p>No pregnancies registered yet!</p>
            <?php } ?>
            <br/>
            <a href="add_baby.php"class="btn">Add pregnancy</a>
    </div>
    <div id="user-info">
        <h4> <?= $user['username'] ?> </h4>
        
        
        <table class="table table-striped">
            <tr>
                <td> Username </td>
                <td> <?= $user['username'] ?> </td>
            </tr>
            <tr>
                <td> Email </td>
                <td> <?= $user['email'] ?> </td>
            </tr>
            <tr>
                <td> Gender </td>
                <td> <?= ($user['gender']=="F" ? "Female" : "Male") ?> </td>
            </tr>


        </table>
        <br/>
        <a href="edit_user.php"class="btn">Edit user info</a>
    </div>
    <div style="clear:both"></div>
</div>


