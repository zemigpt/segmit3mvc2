<h2>Delete Calendar event?</h2>
<br/>
<h5><?=$calendar['start_date_time'] ?></h5>
</br>

<div>
    <?=$calendar['calendar_description'] ?>
</div>
<br/>
<br/>
<div style="width:150px; margin: 0px auto;">
    <div style="float:left">
        <form action="delete_calendar.php" method="post" id="deletecalendar"> 
            <button type="submit" class="btn btn-danger">Delete</button>    
            <input name="delete_confirmation" value="true" type="hidden">
            <input name="calendar_id" value="<?=$calendar['calendar_id'] ?>" type="hidden">
            <input name="referrer" type="hidden" value="<?= $referrer ?>"/>
        </form>
    </div>
    <div style="float:right">
            <a href="/" class="btn">Cancel</a>
    </div>  
    <div style="clear:both"> </div>
</div>



