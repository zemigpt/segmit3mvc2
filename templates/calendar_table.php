<?php

/*
*
* Calendar.
* Adapted from http://alpho011.hubpages.com/hub/Simple-Event-calendar-PHP--MySQL
*
*/

?>

<script>
    function goLastMonth(month, year){
        // If the month is January, decrement the year
        if(month == 1){
            --year;
            month = 13;
        }
        document.location.href = 'calendar.php?month='+(month-1)+'&year='+year;
    }
    
    //next function
    function goNextMonth(month, year){
        // If the month is December, increment the year
        if(month == 12){
            ++year;
            month = 0;
        }
        document.location.href = 'calendar.php?month='+(month+1)+'&year='+year;
    } 

    function remChars(txtControl, txtCount, intMaxLength)
    {
        if(txtControl.value.length > intMaxLength)
            txtControl.value = txtControl.value.substring(0, (intMaxLength-1));
        else
            txtCount.value = intMaxLength - txtControl.value.length;
    }

    function checkFilled() {
        var filled = 0
        var x = document.form1.calName.value;
        
        //x = x.replace(/^\s+/,""); // strip leading spaces
        if (x.length > 0) 
        {
            filled ++
        }

        var y = document.form1.calDesc.value;
        //y = y.replace(/^s+/,""); // strip leading spaces
        if (y.length > 0) 
        {
            filled ++
        }

        if (filled == 2) 
        {
            document.getElementById("Submit").disabled = false;
        }
        else 
        {
            document.getElementById("Submit").disabled = true
        } // in case a field is filled then erased
    }
</script>

<script>
    $(document).ready(
        function() 
        { 
        $('div.calendar.event-item').hover(function(){     
            $(this).find('.calendar-edit-delete').fadeIn('fast');
        },(function(){     
            $(this).find('.calendar-edit-delete').fadeOut('fast');
        }));
    });
</script>

<div id="main-content" class="row-fluid">
    <h2>Calendar</h2>
    <table width="95%" cellpadding="0" cellspacing="0" class="calendar">
          <tr>
            <td width="14%" colspan="1" class="calendar-controls"><input type="button" value=" < " onClick="goLastMonth(<?= $data['month'] . ', ' . $data['year']; ?>);"></td>
            <td width="72%" colspan="5" class="calendar-controls"><span class="title"><h5><?= $data["monthName"] . ' ' . $data['year']; ?></h5></span><br></td>
            <td width="14%" colspan="1"  class="calendar-controls"><input type="button" class="btn btn-small" value=" > " onClick="goNextMonth(<?= $data['month'] . ', ' . $data['year']; ?>);"></td>
          </tr>
          <tr>
            <th class="calendar">Sun
              </th>
            <th class="calendar">Mon
              </th>
            <th class="calendar">Tue
              </th>
            <th class="calendar">Wed
              </th>
            <th class="calendar">Thu
              </th>
            <th class="calendar">Fri
              </th>
            <th class="calendar">Sat
              </th>
          </tr>
          <tr>
            <?php
            for($i = 1; $i < $data['numDays']+1; $i++, $data['counter']++)
            {
                $dateToCompare = $data['month'] . '/' . $i . '/' . $data['year'];
                $timeStamp = strtotime($data['year'] . '-' . $data['month'] .'-' . $i);
                //echo $timeStamp . '<br/>';
                if($i == 1){
                    // Workout when the first day of the month is
                    $firstDay = date("w", $timeStamp);
                    for($j = 0; $j < $firstDay; $j++, $data['counter']++)
                    {
                        echo '<td class="calendar day-cell">&nbsp;</td>';
                    } 
                }
                if($data['counter'] % 7 == 0)
                { ?>
                  </tr>
                  <tr>
                <?php } ?>
                
                
                <!--right here-->
                <td class="calendar day-cell <?php if (isset($calendar_data[$i]['class'])) { echo $calendar_data[$i]['class']; } ?>">                 
                    <div class="calendar day-title <?php if (isset($calendar_data[$i]['class'])) { echo $calendar_data[$i]['class']; } ?>">
                        <?=$i;?>
                    </div>
                    
                    <?php if (isset($calendar_data[$i]["events"])) { ?>
                        <?php foreach ($calendar_data[$i]["events"] as $calendar_day) { ?>
                        <div class="calendar event-item">
                            <div class="calendar-edit-delete">
                                <div style="float:left">
                                    <form action="edit_calendar.php" method="post" >
                                        <input name="formname" value="edit_calendar" type="hidden">
                                        <input name="referrer" value="calendar.php" type="hidden">
                                        <input name="calendar_id" value="<?= $calendar_day['calendar_id']?>" type="hidden">
                                        <button type="submit" class="btn btn-mini btn-warning" title="Edit"><i class="icon-edit"></i></button>
                                    </form>
                                </div>
                                <div style="float:left; margin-left:5px;">
                                    <form action="delete_calendar.php" method="post" >
                                        <input name="formname" value="delete_calendar" type="hidden">
                                        <input name="referrer" value="calendar.php" type="hidden">
                                        <input name="calendar_id" value="<?= $calendar_day['calendar_id']?>" type="hidden">
                                        <button type="submit" class="btn btn-mini btn-danger" title="Delete"><i class="icon-remove"></i></button>
                                    </form>
                                </div>
                            </div>
                            <?= date("H:i", strtotime($calendar_day["start_date_time"])) . ' - ' . $calendar_day["calendar_description"] ?>
                        </div>
                        <?php } ?>
                    <?php } ?>
                </td>
       <?php } ?>
    </table>




</div>
