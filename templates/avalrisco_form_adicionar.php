﻿<div class="row-fluid">
	<div class="span6 offset3">
		<form action="avalrisco.php" method="post" enctype="multipart/form-data" id="adicionar-avalrisco" class="form-horizontal well" >
			<fieldset>
				<legend><?=$title?></legend>
				
				<div class="control-group"> 
					<label class="control-label" for="avalrisco_data">Data Avaliação </label> 
					<div class="controls"> 
						<input type="text" class="datepicker" name="avalrisco_data"/>	
					</div>
				</div>

				<div class="control-group"> 
					<label class="control-label" for="avalrisco_ptid">Posto Trabalho</label> 
					<div class="controls"> 
						<div class="pull-left">
							<select name="avalrisco_ptid" id="sel_posto_trabalho">
								<option value="0">- posto trabalho -</option>
								<?php foreach ($ptrabalhos as $ptrabalho) { ?>
									<option value="<?= $ptrabalho['ptid'] ?>"><?= $ptrabalho['nome'] ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					
				</div>
				
				
				<div class="control-group"> 
					<label class="control-label" for="avalrisco_tarefa">Tarefa</label>
					<div class="controls"> 
						<input type="text" name="avalrisco_tarefa"/>  
					</div>
				</div>

				<div class="control-group"> 
					<label class="control-label" for="avalrisco_perigo">Perigo</label> 
					<div class="controls"> 
						<input type="text" name="avalrisco_perigo"/> 
					</div> 
				</div>

				<div class="control-group"> 
					<label class="control-label" for="avalrisco_risco">Risco</label> 
					<div class="controls"> 
						<div class="pull-left">
						<select name="avalrisco_risco">
							<option value="" disabled selected> -- Riscos --</option>
							<option value="abrasao"> abrasao</option>
							<option value="afogamento"> afogamento</option>
							<option value="agarramento"> agarramento</option>
							<option value="atropelamento"> atropelamento</option>
							<option value="capotamento"> capotamento</option>
							<option value="choque objectos imoveis"> choque obejctos imoveis</option>
							<option value="choque objectos moveis"> choque objectos moveis</option>
							<option value="colpaso estrutural"> colpaso estrutural</option>
							<option value="corte"> corte</option>
							<option value="desabamento"> desabamento</option>
							<option value="entalamento"> entalamento</option>
							<option value="queda altura"> queda altura</option>
							<option value="queda desnivel"> queda desnivel</option>
							<option value="queda mesmo nivel"> queda mesmo nivel</option>
							<option value="queda de objectos"> queda de objectos</option>
							<option value="queda de objectos em altura"> queda de objectos em altura</option>
							<option value="projeccao de objectos"> projeccao de objectos</option>
							<option value="projeccao de fragmentos"> projeccao de fragmentos</option>
							<option value="projeccao liquidos"> projeccao liquidos</option>
							<option value="soterramento"> soterramento</option>
							<option value="contacto directo electrico"> contacto directo electrico</option>
							<option value="contacto indirecto electrico"> contacto indirecto electrico</option>
							<option value="electricidade estatica"> electricidade estatica</option>
							<option value="ambiente termico"> ambiente termico</option>
							<option value="contacto superficies frias"> contacto superficies frias</option>
							<option value="contacto superficies quentes"> contacto superficies quentes</option>
							<option value="iluminacao"> iluminacao</option>
							<option value="radiacoes não ionizantes"> radiacoes não ionizantes</option>
							<option value="radiacoes ionizantes"> radiacoes ionizantes</option>
							<option value="ruido"> ruido</option>
							<option value="vibracoes"> vibracoes</option>
							<option value="contacto liquidos"> contacto liquidos</option>
							<option value="inalacao gases"> inalacao gases</option>
							<option value="intoxicaoes liquidos"> intoxicaoes liquidos</option>
							<option value="bacterias"> bacterias</option>
							<option value="fungos"> fungos</option>
							<option value="parasitas"> parasitas</option>
							<option value="virus"> virus</option>
							<option value="acidente causadao seres vivos"> acidente causadao seres vivos</option>
							<option value="posturas trabalho"> posturas trabalho</option>
							<option value="sobrecarga"> sobrecarga</option>
							<option value="trabalho repetitivo"> trabalho repetitivo</option>
							<option value="incendio"> incendio</option>
							<option value="fadiga"> fadiga</option>
							<option value="stress"> stress</option>
							<option value="monotonia"> monotonia</option>
							<option value="atendimento publico"> atendimento publico</option>
						</select>
						</div>
					</div> 
				</div>
				
				<div class="control-group"> 
					<label class="control-label" for="avalrisco_probabilidade">Probabilidade</label> 
					<div class="controls"> 
						<div class="pull-left">
						<select name="avalrisco_probabilidade">
							<option value="" disabled selected> -- Probabilidade --</option>
							<option value="10">Muito Provável</option>
							<option value="6">Possível</option>
							<option value="3">Raro</option>
							<option value="1">Repetição improvável</option>
							<option value="0.5">Nunca aconteceu</option>
							<option value="0.1">Praticamente impossível</option>
						</select>
						</div>
						<div class="pull-left" style="padding:5px">
							<a href="#" class="info" data-rel="popover" data-content="<b>Muito Provável</b> -> acidente como resultado mais provável e esperado, se a situação de risco ocorrer <p> <b>Possível</b> -> Acidente como perfeitamente possível. Probabilidade de 50%. <p> <b>Raro</b> -> Acidente como coincidência rara. Probabilidade de 10%. <p> <b> Repetição improvável</b> -> Acidente como consequência remotamente possível. Sabe-se que já ocorreu. Probabilidade de 1%. <p><b> Nunca aconteceu</b> -> Acidente como coincidência extremamente remota <p><b> Praticamente Impossível</b> -> Acidente como praticamente impossível. Nunca aconteceu em muitos anos de exposição." title="Probabilidade"> <i class="icon icon-color icon-info"></i></a>
							
						</div>
					</div> 
				</div>
				
				
				<div class="control-group">
					<label class="control-label" for="avalrisco_exposicao">Exposição</label> 
					<div class="controls"> 
					<div class="pull-left">
						<select name="avalrisco_exposicao">
							<option value="" disabled selected> -- Exposição --</option>
							<option value="10">Contínua</option>
							<option value="6">Frequente</option>
							<option value="5">Ocasional</option>
							<option value="4">Irregular</option>
							<option value="1">Raro</option>
							<option value="0.5">Pouco provável</option>
						</select>
						</div>
						<div class="pull-left" style="padding:5px">
							<a href="#" data-rel="popover" data-content="<b>Contínua</b> -> Muitas vezes por dia<p> <b>Frequente</b> -> Aproximadamente uma vez por dia<p> <b>Ocasional</b> -> > 1 vez por semana e < 1 vez por ano <p> <b> Irregular</b> -> >= 1 vez por ano e < 1 vez por ano<p><b> Raro</b> -> Sabe-se que ocorre, mas com baixíssima frequência <p><b> Pouco Provável</b> -> Não se sabe se ocorre, mas é possível que possa acontecer" title="Probabilidade"> <i class="icon icon-color icon-info"></i></a>
							
						</div>
						
					</div>
				</div>
				
				<div class="control-group"> 
					<label class="control-label" for="avalrisco_consequencia">Consequência</label> 
					<div class="controls"> 
						<div class="pull-left">
						<select name="avalrisco_consequencia">
							<option value="" disabled selected> -- Consequência --</option>
							<option value="100">Catástrofe</option>
							<option value="50">Várias mortes</option>
							<option value="25">Morte</option>
							<option value="15">Lesões Graves</option>
							<option value="5">Lesões com baixa</option>
							<option value="1">Pequenas feridas</option>
						</select>
						</div>
						<div class="pull-left" style="padding:5px">
							<a href="#" class="info" data-rel="popover" data-content="<b>Catástrofe</b> -> Elevado número de mortes, perdas >= 1.000.000€ <p> <b>Várias mortes</b> -> Predas >= 500.00 € e < 1.000.000 €<p> <b>Morte</b> -> Acidente mortal. Perdas >= 100.000 € e < 500.000 € <p> <b> Lesões Graves</b> ->Incapacidade Permanente. Perdas >= 1.000 € e < 100.000 €<p><b>Lesões com baixa</b> -> Incapacidade Temporária. Perdas < 1.000 €<p><b>Pequenas Feridas</b> -> Lesões ligeiras. Contusões, golpes, etc." title="Probabilidade"> <i class="icon icon-color icon-info"></i></a>
							
						</div>
						
					</div> 
				</div>
				
				<div class="control-group"> 
					<label class="control-label" for="avalrisco_grau_perigosidade">Grau Perigosidade</label> 
					<div class="controls"> 
						<input type="text" name="avalrisco_grau_perigosidade" readonly/>  
					</div> 
				</div>
				
				<div class="control-group"> 
					<label class="control-label" for="avalrisco_factor_custo">Factor Custo</label> 
					<div class="controls"> 
					
						<select name="avalrisco_factor_custo">
							<option value="" disabled selected> -- Factor de Custo -- </option>
							<option value="10">Acima de 2500€</option>
							<option value="6">De 1250 a 2500€</option>
							<option value="4">De 675 a 1250€</option>
							<option value="3">De 335 a 675€</option>
							<option value="2">De 150 a 335€</option>
							<option value="1">De 75 a 150€</option>
							<option value="0.5">Abaixo de 75€</option>
						</select> 
					</div>
				</div>
				
				<div class="control-group"> 
					<label class="control-label" for="avalrisco_grau_correccao">Grau Correcção</label>
					<div class="controls"> 
					<select name="avalrisco_grau_correccao">
							<option value="" disabled selected> -- Grau Correcção -- </option>
							<option value="1">Risco completamente eliminado</option>
							<option value="2">Risco reduzido a 75%</option>
							<option value="3">Risco reduzido entre 50 a 75%</option>
							<option value="4">Risco reduzido entre 25 e 50%</option>
							<option value="6">Ligeiro efeito sobre o risco (inferior a 25%)</option>
						</select> 
					</div> 
				</div>
				
				<div class="control-group"> 
					<label class="control-label" for="avalrisco_indice_justificacao">Justificação Implementação</label> 
					<div class="controls"> 
						<input type="text" name="avalrisco_indice_justificacao" readonly/>  
					</div> 
				</div>
				
				<div class="control-group"> 
					<label class="control-label" for="avalrisco_medidas">Medidas</label> 
					<div class="controls"> 
						<input type="text" name="avalrisco_medidas"/>  
					</div> 
				</div>
				
				<div class="control-group"> 
					<label class="control-label" for="avalrisco_responsavel">Responsável</label> 
					<div class="controls"> 
						<input type="text" name="avalrisco_responsavel"/>  
					</div> 
				</div>

				<div class="control-group"> 
					<label class="control-label" for="avalrisco_data_prevista_resolucao">Data Prevista de Resolucao</label> 
					<div class="controls"> 
						<input type="text" name="avalrisco_data_prevista_resolucao"/>  
					</div> 
				</div>

				<div class="control-group"> 
					<label class="control-label" for="avalrisco_custo_previsto_resolucao">Custo Previsto de Resolucao</label> 
					<div class="controls"> 
						<input type="text" name="avalrisco_custo_previsto_resolucao"/>  
					</div> 
				</div>

				<div class="control-group"> 
					<label class="control-label" for="avalrisco_data_efectiva_resolucao">Data Efectiva de Resolucao</label> 
					<div class="controls"> 
						<input type="text" name="avalrisco_data_efectiva_resolucao"/>  
					</div>
				</div>

				<div class="control-group"> 
					<label class="control-label" for="avalrisco_custo_efectivo_resolucao">Custo Efectivo de Resolucao</label> 
					<div class="controls"> 
						<input type="text" name="avalrisco_custo_efectivo_resolucao"/>  
					</div> 
				</div>


								
				<input type="hidden" name="posto-trabalho_eid" value="<?=$eid?>">
				<!-- <input type="hidden" name="posto-trabalho_ptid" value="<?=$ptid?>"> -->
				<input type="hidden" name="avalrisco_grau_perigosidade_valor" value="">
				<input type="hidden" name="avalrisco_indice_justificacao_valor" value="">
				<input type="hidden" name="action" value="add">
				
				<div class="form-actions">
					<button type="submit" class="btn btn-primary">Adicionar Avaliação Risco</button>
				</div>
								
			</fieldset>
		</form>
	</div>
</div>
