﻿<div class="row-fluid">
	<div class="span6 offset3">
		<form action="adicionar_avaliacao_risco.php" method="post" enctype="multipart/form-data" id="adicionar-avaliacao-risco" class="form-horizontal well" >
			<fieldset>
				<legend><?=$title?></legend>
				
				<div class="control-group"> 
					<label class="control-label" for="posto_avaliacao_data">Data Avaliação </label> 
					<div class="controls"> 
						<input type="text" class="datepicker" name="posto_avaliacao_data"/>	
					</div>
				</div>

				<div class="control-group"> 
					<label class="control-label" for="posto_avaliacao_tarefa">Tarefa</label>
					<div class="controls"> 
						<input type="text" name="posto_avaliacao_tarefa"/>  
					</div>
				</div>

				<div class="control-group"> 
					<label class="control-label" for="posto_avaliacao_perigo">Perigo</label> 
					<div class="controls"> 
						<input type="text" name="posto_avaliacao_perigo"/> 
					</div> 
				</div>

				<div class="control-group"> 
					<label class="control-label" for="posto_avaliacao_risco">Risco</label> 
					<div class="controls"> 
						<input type="text" name="posto_avaliacao_risco"/>  
					</div> 
				</div>
				
				<div class="control-group"> 
					<label class="control-label" for="posto_avaliacao_probabilidade">Probabilidade</label> 
					<div class="controls"> 
						<div class="pull-left">
						<select name="posto_avaliacao_probabilidade">
							<option value="0"> -- Probabilidade --</option>
							<option value="10">Muito Provável</option>
							<option value="6">Possível</option>
							<option value="3">Raro</option>
							<option value="1">Repetição improvável</option>
							<option value="0.5">Nunca aconteceu</option>
							<option value="0.1">Praticamente impossível</option>
						</select>
						</div>
						<div class="pull-left" style="padding:5px">
							<a href="#" class="info" data-rel="popover" data-content="<b>Muito Provável</b> -> acidente como resultado mais provável e esperado, se a situação de risco ocorrer <p> <b>Possível</b> -> Acidente como perfeitamente possível. Probabilidade de 50%. <p> <b>Raro</b> -> Acidente como coincidência rara. Probabilidade de 10%. <p> <b> Repetição improvável</b> -> Acidente como consequência remotamente possível. Sabe-se que já ocorreu. Probabilidade de 1%. <p><b> Nunca aconteceu</b> -> Acidente como coincidência extremamente remota <p><b> Praticamente Impossível</b> -> Acidente como praticamente impossível. Nunca aconteceu em muitos anos de exposição." title="Probabilidade"> <i class="icon icon-color icon-info"></i></a>
							
						</div>
					</div> 
				</div>
				
				
				<div class="control-group">
					<label class="control-label" for="posto_avaliacao_exposicao">Exposição</label> 
					<div class="controls"> 
					<div class="pull-left">
						<select name="posto_avaliacao_exposicao">
							<option value="0"> -- Exposição --</option>
							<option value="10">Contínua</option>
							<option value="6">Frequente</option>
							<option value="5">Ocasional</option>
							<option value="4">Irregular</option>
							<option value="1">Raro</option>
							<option value="0.5">Pouco provável</option>
						</select>
						</div>
						<div class="pull-left" style="padding:5px">
							<a href="#" data-rel="popover" data-content="<b>Contínua</b> -> Muitas vezes por dia<p> <b>Frequente</b> -> Aproximadamente uma vez por dia<p> <b>Ocasional</b> -> > 1 vez por semana e < 1 vez por ano <p> <b> Irregular</b> -> >= 1 vez por ano e < 1 vez por ano<p><b> Raro</b> -> Sabe-se que ocorre, mas com baixíssima frequência <p><b> Pouco Provável</b> -> Não se sabe se ocorre, mas é possível que possa acontecer" title="Probabilidade"> <i class="icon icon-color icon-info"></i></a>
							
						</div>
						
					</div>
				</div>
				
				<div class="control-group"> 
					<label class="control-label" for="posto_avaliacao_consequencia">Consequência</label> 
					<div class="controls"> 
						<div class="pull-left">
						<select name="posto_avaliacao_consequencia">
							<option value="0"> -- Consequência --</option>
							<option value="100">Catástrofe</option>
							<option value="50">Várias mortes</option>
							<option value="25">Morte</option>
							<option value="15">Lesões Graves</option>
							<option value="5">Lesões com baixa</option>
							<option value="1">Pequenas feridas</option>
						</select>
						</div>
						<div class="pull-left" style="padding:5px">
							<a href="#" class="info" data-rel="popover" data-content="<b>Catástrofe</b> -> Elevado número de mortes, perdas >= 1.000.000€ <p> <b>Várias mortes</b> -> Predas >= 500.00 € e < 1.000.000 €<p> <b>Morte</b> -> Acidente mortal. Perdas >= 100.000 € e < 500.000 € <p> <b> Lesões Graves</b> ->Incapacidade Permanente. Perdas >= 1.000 € e < 100.000 €<p><b>Lesões com baixa</b> -> Incapacidade Temporária. Perdas < 1.000 €<p><b>Pequenas Feridas</b> -> Lesões ligeiras. Contusões, golpes, etc." title="Probabilidade"> <i class="icon icon-color icon-info"></i></a>
							
						</div>
						
					</div> 
				</div>
				
				<div class="control-group"> 
					<label class="control-label" for="posto_avaliacao_grau_perigosidade">Grau Perigosidade</label> 
					<div class="controls"> 
						<input type="text" name="posto_avaliacao_grau_perigosidade" readonly/>  
					</div> 
				</div>
				
				<div class="control-group"> 
					<label class="control-label" for="posto_avaliacao_factor_custo">Factor Custo</label> 
					<div class="controls"> 
					
						<select name="posto_avaliacao_factor_custo">
							<option value="0"> -- Factor de Custo -- </option>
							<option value="10">Acima de 2500€</option>
							<option value="6">De 1250 a 2500€</option>
							<option value="4">De 675 a 1250€</option>
							<option value="3">De 335 a 675€</option>
							<option value="2">De 150 a 335€</option>
							<option value="1">De 75 a 150€</option>
							<option value="0.5">Abaixo de 75€</option>
						</select> 
					</div>
				</div>
				
				<div class="control-group"> 
					<label class="control-label" for="posto_avaliacao_grau_correccao">Grau Correcção</label>
					<div class="controls"> 
					<select name="posto_avaliacao_grau_correccao">
							<option value="0"> -- Grau Correcção -- </option>
							<option value="1">Risco completamente eliminado</option>
							<option value="2">Risco reduzido a 75%</option>
							<option value="3">Risco reduzido entre 50 a 75%</option>
							<option value="4">Risco reduzido entre 25 e 50%</option>
							<option value="6">Ligeiro efeito sobre o risco (inferior a 25%)</option>
						</select> 
					</div> 
				</div>
				
				<div class="control-group"> 
					<label class="control-label" for="posto_avaliacao_indice_justificacao">Justificação Implementação</label> 
					<div class="controls"> 
						<input type="text" name="posto_avaliacao_indice_justificacao" readonly/>  
					</div> 
				</div>
				
				<div class="control-group"> 
					<label class="control-label" for="posto_avaliacao_medidas">Medidas</label> 
					<div class="controls"> 
						<input type="text" name="posto_avaliacao_medidas"/>  
					</div> 
				</div>


								
				<input type="hidden" name="posto-trabalho_eid" value="<?=$eid?>">
				<input type="hidden" name="posto-trabalho_ptid" value="<?=$ptid?>">
				<input type="hidden" name="posto_avaliacao_grau_perigosidade_valor" value="">
				<input type="hidden" name="posto_avaliacao_indice_justificacao_valor" value="">
				
				<div class="form-actions">
					<button type="submit" class="btn btn-primary">Adicionar Avaliação Risco</button>
				</div>
								
			</fieldset>
		</form>
	</div>
</div>
