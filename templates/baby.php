<?php //dump($data);?>
<script>
    $(document).ready(
    function() 
    { 
        
        $('.week').click(function() {
            var week = $(this);
            $(week).nextUntil('week-line').slideToggle('fast', function() {
            // Animation complete.
            });
          
            var i=$(week).find('i');
                if ($(i).hasClass('icon-minus-sign')) {
                    $(i).removeClass('icon-minus-sign');
                    $(i).addClass('icon-plus-sign');
                } else {
                    $(i).removeClass('icon-plus-sign');
                    $(i).addClass('icon-minus-sign');
                }  
        });
        
    
    
        

        
        
        
        
        $('div.eventtitle').hover(function(){     
            $(this).find('.event-edit-delete').fadeIn('fast');
        },(function(){     
            $(this).find('.event-edit-delete').fadeOut('fast');
        }));
        
        $('div.calendar-line').hover(function(){     
            $(this).find('.calendar-edit-delete').fadeIn('fast');
        },(function(){     
            $(this).find('.calendar-edit-delete').fadeOut('fast');
        }));

       
        
        
    });
</script>
<div class="baby-info">
    <h2><?= BabyNameOrPlaceholder($baby["baby_name"]) ?></h2>
    <h4><?= calcWeeks($baby["last_period_date"]) ?></h4>
    <div style="width: 180px; margin: 0 auto;">
        <div class="progress">
            <div class="bar" style="width: <?= intval(calcDays($baby['last_period_date'])/(40*7)*100) ?>%;"></div>
            <div class="caret progressbar"></div>
        </div>
    </div>
</div>
<div id="main-content" class="row-fluid">
    <div id="sidebar1" class="span3">
        <div id="calendar"> 
            <h5>Upcoming calendar events</h5>
            <?php if(count($calendar) == 0) { ?>
                <div>No events</div>
            <?php } ?>
            <?php for($i = 0; $i < count($calendar); $i++) {  ?>
                <div class="calendar-line">
                    <div class="calendar-edit-delete">
                        <div style="float:left">
                            <form action="edit_calendar.php" method="post" >
                                <input name="formname" value="edit_calendar" type="hidden">
								<input name="referrer" value="baby.php" type="hidden">
                                <input name="calendar_id" value="<?= $calendar[$i]['calendar_id']?>" type="hidden">
                                <button type="submit" class="btn btn-mini btn-warning" title="Edit"><i class="icon-edit"></i></button>
                            </form>
                        </div>
                        <div style="float:left; margin-left:5px;">
                            <form action="delete_calendar.php" method="post" >
                                <input name="formname" value="delete_calendar" type="hidden">
								<input name="referrer" value="baby.php" type="hidden">
                                <input name="calendar_id" value="<?= $calendar[$i]['calendar_id']?>" type="hidden">
                                <button type="submit" class="btn btn-mini btn-danger" title="Delete"><i class="icon-remove"></i></button>
                            </form>
                        </div>
                    </div>
                                
                                
                    <div class="calendar-date">
                        <div class="caldate">
                            <?= date("M d", strtotime($calendar[$i]["start_date_time"])) ?>
                        </div>
                        <div class="caltime">
                            <?= date("H:i", strtotime($calendar[$i]["start_date_time"])) ?>
                        </div>
                    </div>
                    <div class="calendar-text" title="<?= $calendar[$i]['calendar_description'] ?>">
                        <?= $calendar[$i]["calendar_description"] ?> 
                    </div>
                </div>
                <div style="clear:both"></div>
            <?php } ?>
        </div>
    </div>
    <div id="events" class="span9">
        <?php if ($data != "noevents") { ?>
        
            <?php for($i = 0; $i < count($data); $i++) {  ?>
                <?php if ($i == 0) {  ?>
                    <div class="row-fluid week-line">
                            <div class="week">
                                <h4><i class="icon-minus-sign icon-white"></i> Week <?= $data[$i]['week']?> </h4>
                            </div>
                    
                <?php } elseif ($i > 0 && $data[$i]['week'] != $data[$i-1]['week']) { ?>
                    </div>
                    <div class="row-fluid week-line">
                            <div class="week">
                                <h4><i class="icon-minus-sign icon-white" title="collapse"></i> Week <?= $data[$i]['week']?> </h4>
                            </div>
                
                <?php } else { ?>
                    
                <?php } ?> 
                        <div class="row-fluid event-line">
                            <div class="span2">
                                <?php if ($i == 0 || ($i > 0 && $data[$i]['event_date'] != $data[$i-1]['event_date'])) { ?>
                                  <div class="date">
                                      <span class="day"><?=date("d",strtotime($data[$i]['event_date'])) ?></span>
                                      <span class="month"><?=date("M",strtotime($data[$i]['event_date'])) ?></span>
                                      <span class="year"><?=date("Y",strtotime($data[$i]['event_date'])) ?></span>
                                     
                                </div>  
                                <?php } ?>              
                            </div>
                            <div class="span10 event-detail <?= $data[$i]['event_type_value']?>"> 
                                <div class="eventtitle <?= $data[$i]['event_type_value']?>">
                                    <div class="eventicon"><img src="../img/<?= $data[$i]['event_type_value']?>.png" alt="<?= $data[$i]['event_type_description']?>" title="<?= $data[$i]['event_type_description']?>"> <span class="eventtype">  <?= $data[$i]['event_title']?></span></div>
                                    <div class="event-edit-delete">
                                        <div style="float:left">
                                            <form action="edit_event.php" method="post" >
                                                <input name="formname" value="edit_event" type="hidden">
                                                <input name="event_id" value="<?= $data[$i]['event_id']?>" type="hidden">
                                                <button type="submit" class="btn btn-mini btn-warning" title="Edit"><i class="icon-edit"></i></button>
                                            </form>
                                        </div>
                                        <div style="float:left; margin-left:5px;">
                                            <form action="delete_event.php" method="post" >
                                                <input name="formname" value="delete_event" type="hidden">
                                                <input name="event_id" value="<?= $data[$i]['event_id']?>" type="hidden">
                                                <button type="submit" class="btn btn-mini btn-danger" title="Delete"><i class="icon-remove"></i></button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <?php //if(count($data["images"])>0)?>
                                        <?php foreach ($data[$i]["images"] as $image) { ;?>
                                            <div class="event-image">
                                                <img src="<?= $image['image_url']?>" alt="<?= $image['image_description']?>" class="image-frame img-polaroid">
                                            </div>
                                        <?php } ?>  
                                    <?=$data[$i]['event_description'] ?>
                                </div>
                            </div>
                       </div>
                
            <?php } ?>
                </div>
       <?php } else { ?>
                <div id="noevents">
                
                    There are no posts yet!
                    <br/>
                    <br/>
                    <a href="add_event.php"class="btn">Add post</a>
                </div>
       <?php } ?>
       
    </div>
</div>

