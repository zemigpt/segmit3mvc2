﻿<div class="row-fluid">	
	<div class="span2">
		<div class="box span12">
			<div class="box-header well" data-original-title>
				
			</div>
			<div>
			<ul class="nav nav-tabs nav-stacked">
				<li>
					<a href="#dadospt">Dados Posto Trabalho</a>
				</li>
				<li>
					<a href="#trabalhadores">Trabalhadores associados</a>
				</li>
				<li>
					<a href="#maquinas">Máquinas</a>
				</li>
				<li>
					<a href="#epi">EPI's</a>
				</li>
				<li>
					<a href="#afruido">AF:Ruido</a>
				</li>
				<li>
					<a href="#afiluminancia">AF:Iluminancia</a>
				</li>
				<li>
					<a href="#afambterm">AF:Amb. Térmico</a>
				</li>
				<li>
					<a href="#afvibracoes">AF:Vibrações</a>
				</li>
				<li>
					<a href="#agbiologico">AB</a>
				</li>
				<li>
					<a href="#agquimico">AQ</a>
				</li>
				<li>
					<a href="#avalrisco">Avaliações de Risco</a>
				</li>				
				<li>
					<a href="#forma2">Formação Requerida</a>
				</li>
				<li>
					<a href="#regacidente">Registo Acidentes</a>
				</li>
			</ul>
			</div>
		</div>
	</div>
	<div class="span10">	
		<div class="row-fluid">	
		<div class="box span12">
			<div class="box-header well" data-original-title>
				<a id="dadospt"></a><h2><i class="icon-user"></i> Dados do Posto Trabalho</h2>
				<div class="box-icon">
					<a href="ptrabalho.php?id=<?=$ptid?>&action=edit" title="Editar Posto Trabalho" class="btn btn-round"><i class="icon-pencil"></i></a>
					<a href="ptrabalho.php?id=<?=$ptid?>&action=delete" title="Remover Posto Trabalho" class="btn btn-round"><i class="icon-remove"></i></a>
					<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">
				<table class="table table-striped">


					<thead>
						<tr>
							<th colspan='2'><p class="lead"><img style="height:50px" src="<?=$ptrabalho['foto']?>"> <?=$ptrabalho['nome']?></p></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<th width="33%">Descrição</th>
							<td width="67%"><?=$ptrabalho['descricao']?></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div><!--/box dados ptrabalhador-->
		</div>

		<div class="row-fluid"> <!--/ inicio box dados trabalhadores-->	
		
		<div class="box span12">
			<div class="box-header well" data-original-title>
				<a id="trabalhadores"></a><h2><i class="icon icon-user icon-black"></i> Trabalhadores</h2>
				<div class="box-icon">
					<a href="#myModalAdicionarTrabalhador" role="button" data-toggle="modal" title="Adicionar Trabalhador" class="btn btn-round"><i class="icon-plus"></i></a>
					<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>
								Nome
							</th>
							<th>
								Número Mecanográfico
							</th>
							<th>
								Opção
							</th>
						</tr>
					</thead>
					<tbody>				
					<?php if (empty($trabalhadores)) {?>
						<tr>
							<td colspan="3">
								Não há trabalhadores associados
							</td>
						</tr>
					<?php } else {?>
						<?php foreach($trabalhadores as $trabalhador) { ?>
							<tr>
								<td>
									<?=$trabalhador["nome"]?>
								</td>
								<td>
									<?=$trabalhador["num_mecanografico"]?>
								</td>
								<td>
									<a class="btn" href="trabalhador.php?id=<?= $trabalhador["tid"]?>"><i class="icon-eye-open" title="Ver detalhes do trabalhador"></i></a> 
								</td>
							</tr>
						<?php }?>
					<?php }?>
					</tbody>
				</table>
			</div>
		</div><!--/ fim box dados trabalhadores-->	

		<div class="row-fluid"><!--/ inicio box dados maquinas-->	
		
		<div class="box span12">
			<div class="box-header well" data-original-title>
				<a id="maquinas"></a><h2><i class="icon icon-wrench icon-black"></i> Máquinas</h2>
				<div class="box-icon">
					<a href="#myModalAdicionarMaquina" role="button" data-toggle="modal"  title="Adicionar Máquina" class="btn btn-round"><i class="icon-plus"></i></a>
					<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>
								Nome
							</th>
							<th>
								Núm. Série
							</th>
							<th>
								Opção
							</th>
						</tr>
					</thead>
					<tbody>				
					<?php if (empty($maquinas)) {?>
						<tr>
							<td colspan="3">
								Não há máquinas associadas
							</td>
						</tr>
					<?php } else {?>
						<?php foreach($maquinas as $maquina) { ?>
							<tr>
								<td>
									<?=$maquina["nome"]?>
								</td>
								<td>
									<?=$maquina["num_serie"]?>
								</td>
								<td>
									<a class="btn" href="maquina.php?id=<?=$maquina["mid"]?>"><i class="icon-eye-open" title="Ver detalhes da máquina"></i></a> 
								</td>
							</tr>
						<?php }?>
					<?php }?>
					</tbody>
				</table>
			</div>
		</div><!--/box-->
		
		<div class="row-fluid"><!--/ inicio box dados EPIs-->	
		
		<div class="box span12">
			<div class="box-header well" data-original-title>
				<a id="epi"></a><h2><i class="icon icon-th icon-black"></i> Equipamento Protecção Individual</h2>
				<div class="box-icon">
					<a href="#myModalAdicionarEPI" role="button" data-toggle="modal"  title="Adicionar EPI" class="btn btn-round"><i class="icon-plus"></i></a>
					<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>
								Nome
							</th>
						</tr>
					</thead>
					<tbody>				
					<?php if (empty($epis)) {?>
						<tr>
							<td colspan="1">
								Não há EPI's associados a este posto de trabalho
							</td>
						</tr>
					<?php } else {?>
						<?php foreach($epis as $epi) { ?>
							<tr>
								<td>
									<?=$epi["nome"]?>
								</td>
							</tr>
						<?php }?>
					<?php }?>
					</tbody>
				</table>
			</div>
		</div><!--/box final EPI's-->
		
		<div class="row-fluid"><!--/ inicio box dados avaliacoes fisico ruido-->	
		
		<div class="box span12">
			<div class="box-header well" data-original-title>
				<a id="afruido"></a><h2><i class="icon icon-profile icon-black"></i> Avaliação Agente Físico: Ruído</h2>
				<div class="box-icon">
					<a href="#myModalAdicionarAgFisRuido" role="button" data-toggle="modal"  title="Adicionar Avaliação Agente Físico: Ruído" class="btn btn-round"><i class="icon-plus"></i></a>
					<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>
								Data
							</th>
							<th>
								Valor Registado
							</th>
						</tr>
					</thead>
					<tbody>				
					<?php if (empty($afruidos)) {?>
						<tr>
							<td colspan="2">
								Não há avaliações de agentes físicos ruído associadas
							</td>
						</tr>
					<?php } else {?>
						<?php foreach($afruidos as $afruido) { ?>
							<tr>
								<td>
									<?=$afruido["data"]?>
								</td>
								<td>
									<?=$afruido["valor"]?>
								</td>
							</tr>
						<?php }?>
					<?php }?>
					</tbody>
				</table>
			</div>
		</div><!--/box avaliação ruido-->
		
		<div class="row-fluid"><!--/ inicio box dados avaliacoes fisico iluminancia-->	
		
		<div class="box span12">
			<div class="box-header well" data-original-title>
				<a id="afiluminancia"></a><h2><i class="icon icon-profile icon-black"></i> Avaliação Agente Físico: Iluminancia</h2>
				<div class="box-icon">
					<a href="#myModalAdicionarAgFisIlum" role="button" data-toggle="modal"  title="Adicionar Avaliação Agente Físico: Iluminancia" class="btn btn-round"><i class="icon-plus"></i></a>
					<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>
								Data
							</th>
							<th>
								Valor Registado
							</th>
						</tr>
					</thead>
					<tbody>				
					<?php if (empty($afiluminancias)) {?>
						<tr>
							<td colspan="2">
								Não há avaliações de agentes físicos iluminância associadas
							</td>
						</tr>
					<?php } else {?>
						<?php foreach($afiluminancias as $afilumin) { ?>
							<tr>
								<td>
									<?=$afilumin["data"]?>
								</td>
								<td>
									<?=$afilumin["valor"]?>
								</td>
							</tr>
						<?php }?>
					<?php }?>
					</tbody>
				</table>
			</div>
		</div><!--/box avaliação risco iluminancia-->
		
		<div class="row-fluid"><!--/ inicio box dados avaliacoes fisico ambtermico-->	
		
		<div class="box span12">
			<div class="box-header well" data-original-title>
				<a id="afambterm"></a><h2><i class="icon icon-profile icon-black"></i> Avaliação Agente Físico: Ambiente Térmico</h2>
				<div class="box-icon">
					<a href="#myModalAdicionarAgFisTerm" role="button" data-toggle="modal"  title="Adicionar Avaliação Agente Físico: Ambiente Térmico" class="btn btn-round"><i class="icon-plus"></i></a>
					<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>
								Data
							</th>
							<th>
								Valor Registado
							</th>
						</tr>
					</thead>
					<tbody>				
					<?php if (empty($afambterms)) {?>
						<tr>
							<td colspan="2">
								Não há avaliações de agentes físicos ambiente térmico associadas
							</td>
						</tr>
					<?php } else {?>
						<?php foreach($afambterms as $afambterm) { ?>
							<tr>
								<td>
									<?=$afambterm["data"]?>
								</td>
								<td>
									<?=$afambterm["valor"]?>
								</td>
							</tr>
						<?php }?>
					<?php }?>
					</tbody>
				</table>
			</div>
		</div><!--/box avaliação risco ambiente térmico-->
		
		<div class="row-fluid"><!--/ inicio box dados avaliacoes fisico vibracoes-->	
		
		<div class="box span12">
			<div class="box-header well" data-original-title>
				<a id="afvibracoes"></a><h2><i class="icon icon-profile icon-black"></i> Avaliação Agente Físico: Vibrações</h2>
				<div class="box-icon">
					<a href="#myModalAdicionarAgFisVib" role="button" data-toggle="modal"  title="Adicionar Avaliação Agente Físico: Vibrações" class="btn btn-round"><i class="icon-plus"></i></a>
					<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>
								Data
							</th>
							<th>
								Valor Registado
							</th>
						</tr>
					</thead>
					<tbody>				
					<?php if (empty($afvibracoes)) {?>
						<tr>
							<td colspan="2">
								Não há avaliações de agentes físicos ambiente vibrações associadas
							</td>
						</tr>
					<?php } else {?>
						<?php foreach($afvibracoes as $afvibracao) { ?>
							<tr>
								<td>
									<?=$afvibracao["data"]?>
								</td>
								<td>
									<?=$afvibracao["valor"]?>
								</td>
							</tr>
						<?php }?>
					<?php }?>
					</tbody>
				</table>
			</div>
		</div><!--/box avaliação risco ambiente vibracoes-->
		
		<div class="row-fluid"><!--/ inicio box dados avaliacoes biologico-->	
		
		<div class="box span12">
			<div class="box-header well" data-original-title>
				<a id="agbiologico"></a><h2><i class="icon icon-profile icon-black"></i> Avaliação Agentes Biologicos</h2>
				<div class="box-icon">
					<a href="#myModalAdicionarAgBiologico" role="button" data-toggle="modal" title="Adicionar Avaliação Agente Biológicos" class="btn btn-round"><i class="icon-plus"></i></a>
					<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>
								Data
							</th>
							<th>
								Valor Registado
							</th>
						</tr>
					</thead>
					<tbody>				
					<?php if (empty($agbiologicos)) {?>
						<tr>
							<td colspan="2">
								Não há avaliações de agentes biológicos associadas
							</td>
						</tr>
					<?php } else {?>
						<?php foreach($agbiologicos as $agbiologico) { ?>
							<tr>
								<td>
									<?=$agbiologico["data"]?>
								</td>
								<td>
									<?=$agbiologico["valor"]?>
								</td>
							</tr>
						<?php }?>
					<?php }?>
					</tbody>
				</table>
			</div>
		</div><!--/box avaliação risco agentes biologicos-->
		
		<div class="row-fluid"><!--/ inicio box dados avaliacoes quimicos-->	
		
		<div class="box span12">
			<div class="box-header well" data-original-title>
				<a id="agquimicos"></a><h2><i class="icon icon-profile icon-black"></i> Avaliação Agentes Químicos</h2>
				<div class="box-icon">
					<a href="#myModalAdicionarAgQuim" role="button" data-toggle="modal" title="Adicionar Avaliação Agente Químicos" class="btn btn-round"><i class="icon-plus"></i></a>
					<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>
								Data
							</th>
							<th>
								Agente
							</th>
							<th>
								Valor Registado
							</th>
						</tr>
					</thead>
					<tbody>				
					<?php if (empty($agquimicos)) {?>
						<tr>
							<td colspan="3">
								Não há avaliações de agentes químicos associadas
							</td>
						</tr>
					<?php } else {?>
						<?php foreach($agquimicos as $agquimico) { ?>
							<tr>
								<td>
									<?=$agquimico["data"]?>
								</td>
								<td>
									<?=$agquimico["agente"]?>
								</td>
								<td>
									<?=$agquimico["dose"]?>
								</td>
							</tr>
						<?php }?>
					<?php }?>
					</tbody>
				</table>
			</div>
		</div><!--/box avaliação risco agentes quimicos-->
		
		<div class="row-fluid"><!--/ inicio box dados avaliacoes risco-->	
		
		<div class="box span12">
			<div class="box-header well" data-original-title>
				<a id="avalrisco"></a><h2><i class="icon icon-profile icon-black"></i> Avaliação Riscos</h2>
				<div class="box-icon">
					<a href="avalrisco.php?action=add" title="Adicionar Avaliação Risco" class="btn btn-round"><i class="icon-plus"></i></a>
					<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>
								Data
							</th>
							<th>
								Tarefa
							</th>
							<th>
								Perigo
							</th>
							<th>
								Risco
							</th>
							<th>
								Grau Perigosidade
							</th>
							<th>
								Indice Justificação
							</th>
							<th>
								Responsável
							</th>
							<th>
								Medidas
							</th>
							<th>
								Opção
							</th>
						</tr>
					</thead>
					<tbody>				
					<?php if (empty($avalriscos)) {?>
						<tr>
							<td colspan="9">
								Não há avaliações de risco associadas
							</td>
						</tr>
					<?php } else {?>
						<?php foreach($avalriscos as $avalrisco) { ?>
							<tr>
								<td>
									<?=$avalrisco["data"]?>
								</td>
								<td>
									<?=$avalrisco["tarefa"]?>
								</td>
								<td>
									<?=$avalrisco["perigo"]?>
								</td>
								<td>
									<?=$avalrisco["risco"]?>
								</td>
								<td>
									<?=$avalrisco["grau_perigosidade"]?>
								</td>
								<td>
									<?=$avalrisco["indice_justificacao"]?>
								</td>
								<td>
									<?=$avalrisco["medidas"]?>
								</td>
								<td>
									<?=$avalrisco["responsavel"]?>
								</td>
								<td>
									<a class="btn" href="avalrisco.php?id=<?=$avalrisco["pta_id"]?>"><i class="icon-eye-open" title="Ver detalhes da Avaliação de Risco"></i></a> 
									<a class="btn" href="avalrisco.php?action=edit&id=<?= $avalrisco["pta_id"]?>"><i class="icon-pencil" title="Editar Avaliação de Risco"></i></a>
								</td>
							</tr>
						<?php }?>
					<?php }?>
					</tbody>
				</table>
			</div>
		</div><!--/box avaliação risco-->
		
		<div class="row-fluid"><!--/box formação requerida-->
		
		<div class="box span12">
			<div class="box-header well" data-original-title>
				<a id="forma2"></a><h2><i class="icon icon-wrench icon-black"></i> Formação Requerida</h2>
				<div class="box-icon">
					<!-- não adicionar formação pelo posto de trabalho
					<a href="adicionar_formacao.php?ptid=<?=$ptid?>" title="Adicionar Formação" class="btn btn-round"><i class="icon-plus"></i></a>
					-->
					<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>
								Tema
							</th>
							<th>
								Núm. Horas
							</th>
							<th>
								Máquina Associada
							</th>
							<th>
								Opção
							</th>
						</tr>
					</thead>
					<tbody>				
					<?php if (empty($forrequeridas)) {?>
						<tr>
							<td colspan="4">
								Não há formações associadas
							</td>
						</tr>
					<?php } else {?>
						<?php foreach($forrequeridas as $forrequerida) { ?>
							<tr>
								<td>
									<?=$forrequerida["tema"]?>
								</td>
								<td>
									<?=$forrequerida["duracao"]?>
								</td>
								<td>
									<?=$forrequerida["maq_nome"]?>
								</td>
								<td>
									<a class="btn" href="formacao.php?id=<?=$forrequerida["for_id"]?>"><i class="icon-eye-open" title="Ver detalhes da formação"></i></a> 
								</td>
							</tr>
						<?php }?>
					<?php }?>
					</tbody>
				</table>
			</div>
		</div><!--/box formação detida-->
		
		<div class="row-fluid"><!--/box registo acidentes-->
		
		<div class="box span12">
			<div class="box-header well" data-original-title>
				<a id="regacidente"></a><h2><i class="icon icon-wrench icon-black"></i> Registo de Acidentes</h2>
				<div class="box-icon">
					<!-- não adicionar registo de acidentes pelo posto de trabalho
					<a href="adicionar_regacidente.php?ptid=<?=$ptid?>" title="Adicionar Registo Acidente" class="btn btn-round"><i class="icon-plus"></i></a>
					-->
					<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>
								Data
							</th>
							<th>
								Estado
							</th>
							<th>
								Opção
							</th>
						</tr>
					</thead>
					<tbody>				
					<?php if (empty($regacidentes)) {?>
						<tr>
							<td colspan="3">
								Não há registo de acidentes associados
							</td>
						</tr>
					<?php } else {?>
						<?php foreach($regacidentes as $regacidente) { ?>
							<tr>
								<td>
									<?=$regacidente["data"]?>
								</td>
								<td>
									<?=$regacidente["estado"]?>
								</td>
								
								<td>
									<a class="btn" href="regacidente.php?id=<?=$regacidente["ra_id"]?>"><i class="icon-eye-open" title="Ver detalhes do Registo de Acidente"></i></a> 
								</td>
							</tr>
						<?php }?>
					<?php }?>
					</tbody>
				</table>
			</div>
		</div><!--/box registo acidentes-->
		
		</div>
	</div><!--/span-->					
</div><!--/row-->

<!-- Modal Seleccionar Trabalhador -->
<div id="myModalAdicionarTrabalhador" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">Adicionar Trabalhador</h3>
	</div>
	<div class="modal-body">
		<p>&nbsp;</p>
		<form action="associar_trabalhador_ao_posto-trabalho.php" method="post" id="associar_trabalhador_ao_posto-trabalho" class="form-horizontal" >
			<fieldset>
				<div class="control-group"> 
					<label class="control-label" for="tid">Seleccionar Trabalhador</label> 
					<div class="controls"> 
						<div class="pull-left">
								<?php if (empty($todos_trabalhadores)) { ?>
									 <input type="text" class="input-xlarge uneditable-input info" placeholder="Já todos pertencem a este posto de trabalho">
									 <input type="hidden" name="tid" value="0">
								<?php } else { ?>
									<select name="tid" id="sel_trabalhador" >
										<option value="0">- Trabalhadores -</option>
										<?php foreach ($todos_trabalhadores as $trab) { ?>
											<option value="<?= $trab['tid'] ?>"><?= $trab['nome'] ?></option>
										<?php } ?>
									</select>
								<?php } ?>
						</div>
					</div>
				</div>
				<input type="hidden" name="ptid" value="<?=$ptid?>">
				<input type="hidden" name="ajax" value="true">
			</fieldset>
		</form>
	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
		<button id="associar_trabalhador_ao_posto-trabalho-submit" type="submit" class="btn btn-primary" <?php if (empty($todos_trabalhadores)) echo "disabled" ?>>Adicionar</button>
	</div>
</div>

<!-- Modal Seleccionar Máquina -->
<div id="myModalAdicionarMaquina" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">Adicionar Máquina</h3>
	</div>
	<div class="modal-body">
		<p>&nbsp;</p>
		<form action="associar_maquina_ao_posto-trabalho.php" method="post" id="associar_maquina_ao_posto-trabalho" class="form-horizontal" >
			<fieldset>
				<div class="control-group"> 
					<label class="control-label" for="mid">Seleccionar Máquina</label> 
					<div class="controls"> 
						<div class="pull-left">
								<?php if (empty($todas_maquinas)) { ?>
									 <input type="text" class="input-xlarge uneditable-input info" placeholder="Já todas pertencem a este posto de trabalho">
									 <input type="hidden" name="mid" value="0">
								<?php } else { ?>
									<select name="mid" id="sel_maquina" >
										<option value="0">- Máquinas -</option>
										<?php foreach ($todas_maquinas as $maq) { ?>
											<option value="<?= $maq['mid'] ?>"><?= $maq['nome'] ?></option>
										<?php } ?>
									</select>
								<?php } ?>
						</div>
					</div>
				</div>
				<input type="hidden" name="ptid" value="<?=$ptid?>">
				<input type="hidden" name="ajax" value="true">
			</fieldset>
		</form>
	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
		<button id="associar_maquina_ao_posto-trabalho-submit" type="submit" class="btn btn-primary" <?php if (empty($todas_maquinas)) echo "disabled" ?>>Adicionar</button>
	</div>
</div>

<div id="agentesfisicos">
	<!-- Modal Seleccionar Agente Físico Avaliação Ruido -->
	<div id="myModalAdicionarAgFisRuido" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<form action="adicionar_aval.php" method="post"  enctype="multipart/form-data" id="adicionar_afar_ao_posto-trabalho" class="form-horizontal" >
			<fieldset>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 id="myModalLabel">Adicionar Avaliação de Ruído</h3>
				</div>
				<div class="modal-body">
					<p>&nbsp;</p>
							<div class="control-group"> 
								<label class="control-label" for="valor_registado">Valor Registado</label> 
								<div class="controls"> 
									<div class="pull-left">
											<input type="text" name="valor_registado" required/>
									</div>
								</div>
							</div>
							<div class="control-group"> 
								<label class="control-label" for="data_execucao">Data da avaliação</label> 
								<div class="controls"> 
									<div class="pull-left">
											<input type="text" class="datepicker" name="data_execucao" required/> 
									</div>
								</div>
							</div>
							<div class="control-group"> 
								<label class="control-label" for="data_validade">Data de validade</label> 
								<div class="controls"> 
									<div class="pull-left">
											<input type="text" class="datepicker" name="data_validade" required/> 
									</div>
								</div>
							</div>
							<div class="control-group"> 
								<label class="control-label" for="entidade_executante">Entidade Executante</label> 
								<div class="controls"> 
									<div class="pull-left">
											<input type="text" name="entidade_executante" required/> 
									</div>
								</div>
							</div>
							<div class="control-group"> 
								<label class="control-label" for="relatorio">Relatório de avaliação</label> 
								<div class="controls"> 
									<div class="pull-left">
										<input type="file" name="relatorio" required/> 
									</div>
								</div>
							</div>
							<input type="hidden" name="ptid" value="<?=$ptid?>">
							<input type="hidden" name="ag_type" value="afar">
							<input type="hidden" name="ajax" value="true">

				</div>
				<div class="modal-footer">
					<button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
					<button id="adicionar_afar_ao_posto-trabalho-submit" type="submit" class="btn btn-primary">Adicionar</button>
				</div>
			</fieldset>
		</form>
	</div>

	<!-- Modal Seleccionar Agente Físico Avaliação Iluminância -->
	<div id="myModalAdicionarAgFisIlum" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<form action="adicionar_aval.php" method="post"  enctype="multipart/form-data" id="adicionar_afai_ao_posto-trabalho" class="form-horizontal" >
			<fieldset>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 id="myModalLabel">Adicionar Avaliação de Iluminância</h3>
				</div>
				<div class="modal-body">
					<p>&nbsp;</p>

							<div class="control-group"> 
								<label class="control-label" for="valor_registado">Valor Registado</label> 
								<div class="controls"> 
									<div class="pull-left">
											<input type="text" name="valor_registado" />
									</div>
								</div>
							</div>
							<div class="control-group"> 
								<label class="control-label" for="data_execucao">Data da avaliação</label> 
								<div class="controls"> 
									<div class="pull-left">
											<input type="text" class="datepicker" name="data_execucao" /> 
									</div>
								</div>
							</div>
							<div class="control-group"> 
								<label class="control-label" for="data_validade">Data de validade</label> 
								<div class="controls"> 
									<div class="pull-left">
											<input type="text" class="datepicker" name="data_validade" /> 
									</div>
								</div>
							</div>
							<div class="control-group"> 
								<label class="control-label" for="entidade_executante">Entidade Executante</label> 
								<div class="controls"> 
									<div class="pull-left">
											<input type="text" name="entidade_executante" /> 
									</div>
								</div>
							</div>
							<div class="control-group"> 
								<label class="control-label" for="relatorio">Relatório de avaliação</label> 
								<div class="controls"> 
									<div class="pull-left">
											<input type="file" name="relatorio" /> 
									</div>
								</div>
							</div>
							<input type="hidden" name="ptid" value="<?=$ptid?>">
							<input type="hidden" name="ag_type" value="afai">
							<input type="hidden" name="ajax" value="true">
				</div>
				<div class="modal-footer">
					<button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
					<button id="adicionar_afai_ao_posto-trabalho-submit" type="submit" class="btn btn-primary">Adicionar</button>
				</div>
			</fieldset>
		</form>
	</div>

	<!-- Modal Seleccionar Agente Físico Avaliação Térmica -->
	<div id="myModalAdicionarAgFisTerm" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<form action="adicionar_aval.php" method="post"  enctype="multipart/form-data" id="adicionar_afat_ao_posto-trabalho" class="form-horizontal" >
			<fieldset>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 id="myModalLabel">Adicionar Avaliação de Amb. Térmico</h3>
				</div>
				<div class="modal-body">
					<p>&nbsp;</p>

							<div class="control-group"> 
								<label class="control-label" for="valor_registado">Valor Registado</label> 
								<div class="controls"> 
									<div class="pull-left">
											<input type="text" name="valor_registado" />
									</div>
								</div>
							</div>
							<div class="control-group"> 
								<label class="control-label" for="data_execucao">Data da avaliação</label> 
								<div class="controls"> 
									<div class="pull-left">
											<input type="text" class="datepicker" name="data_execucao" /> 
									</div>
								</div>
							</div>
							<div class="control-group"> 
								<label class="control-label" for="data_validade">Data de validade</label> 
								<div class="controls"> 
									<div class="pull-left">
											<input type="text" class="datepicker" name="data_validade" /> 
									</div>
								</div>
							</div>
							<div class="control-group"> 
								<label class="control-label" for="entidade_executante">Entidade Executante</label> 
								<div class="controls"> 
									<div class="pull-left">
											<input type="text" name="entidade_executante" /> 
									</div>
								</div>
							</div>
							<div class="control-group"> 
								<label class="control-label" for="relatorio">Relatório de avaliação</label> 
								<div class="controls"> 
									<div class="pull-left">
											<input type="file" name="relatorio" /> 
									</div>
								</div>
							</div>
							<input type="hidden" name="ptid" value="<?=$ptid?>">
							<input type="hidden" name="ag_type" value="afat">
							<input type="hidden" name="ajax" value="true">

				</div>
				<div class="modal-footer">
					<button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
					<button id="adicionar_afat_ao_posto-trabalho-submit" type="submit" class="btn btn-primary">Adicionar</button>
				</div>				
			</fieldset>
		</form>
	</div>


	<!-- Modal Seleccionar Agente Físico Avaliação Vibrações -->
	<div id="myModalAdicionarAgFisVib" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<form action="adicionar_aval.php" method="post"  enctype="multipart/form-data" id="adicionar_afav_ao_posto-trabalho" class="form-horizontal" >
			<fieldset>		
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 id="myModalLabel">Adicionar Avaliação de Vibrações</h3>
				</div>
				<div class="modal-body">
					<p>&nbsp;</p>
					<div class="control-group"> 
						<label class="control-label" for="valor_registado">Valor Registado</label> 
						<div class="controls"> 
							<div class="pull-left">
									<input type="text" name="valor_registado" />
							</div>
						</div>
					</div>
					<div class="control-group"> 
						<label class="control-label" for="data_execucao">Data da avaliação</label> 
						<div class="controls"> 
							<div class="pull-left">
									<input type="text" class="datepicker" name="data_execucao" /> 
							</div>
						</div>
					</div>
					<div class="control-group"> 
						<label class="control-label" for="data_validade">Data de validade</label> 
						<div class="controls"> 
							<div class="pull-left">
									<input type="text" class="datepicker" name="data_validade" /> 
							</div>
						</div>
					</div>
					<div class="control-group"> 
						<label class="control-label" for="entidade_executante">Entidade Executante</label> 
						<div class="controls"> 
							<div class="pull-left">
									<input type="text" name="entidade_executante" /> 
							</div>
						</div>
					</div>
					<div class="control-group"> 
						<label class="control-label" for="relatorio">Relatório de avaliação</label> 
						<div class="controls"> 
							<div class="pull-left">
									<input type="file" name="relatorio" /> 
							</div>
						</div>
					</div>
					<input type="hidden" name="ptid" value="<?=$ptid?>">
					<input type="hidden" name="ag_type" value="afav">
					<input type="hidden" name="ajax" value="true">

				</div>
				<div class="modal-footer">
					<button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
					<button id="adicionar_afav_ao_posto-trabalho-submit" type="submit" class="btn btn-primary">Adicionar</button>
				</div>
			</fieldset>
		</form>
	</div>
	
	<!-- Modal Seleccionar Agente Biológico -->
	<div id="myModalAdicionarAgBiologico" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<form action="adicionar_aval.php" method="post"  enctype="multipart/form-data" id="adicionar_agbiologico_ao_posto-trabalho" class="form-horizontal" >
			<fieldset>		
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 id="myModalLabel">Adicionar Avaliação Agente Biológico</h3>
				</div>
				<div class="modal-body">
					<p>&nbsp;</p>
					<div class="control-group"> 
						<label class="control-label" for="valor_registado">Valor Registado</label> 
						<div class="controls"> 
							<div class="pull-left">
									<input type="text" name="valor_registado" />
							</div>
						</div>
					</div>
					<div class="control-group"> 
						<label class="control-label" for="data_execucao">Data da avaliação</label> 
						<div class="controls"> 
							<div class="pull-left">
									<input type="text" class="datepicker" name="data_execucao" /> 
							</div>
						</div>
					</div>
					<div class="control-group"> 
						<label class="control-label" for="data_validade">Data de validade</label> 
						<div class="controls"> 
							<div class="pull-left">
									<input type="text" class="datepicker" name="data_validade" /> 
							</div>
						</div>
					</div>
					<div class="control-group"> 
						<label class="control-label" for="entidade_executante">Entidade Executante</label> 
						<div class="controls"> 
							<div class="pull-left">
									<input type="text" name="entidade_executante" /> 
							</div>
						</div>
					</div>
					<div class="control-group"> 
						<label class="control-label" for="relatorio">Relatório de avaliação</label> 
						<div class="controls"> 
							<div class="pull-left">
									<input type="file" name="relatorio" /> 
							</div>
						</div>
					</div>
					<input type="hidden" name="ptid" value="<?=$ptid?>">
					<input type="hidden" name="ag_type" value="abio">
					<input type="hidden" name="ajax" value="true">

				</div>
				<div class="modal-footer">
					<button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
					<button id="adicionar_agbio_ao_posto-trabalho-submit" type="submit" class="btn btn-primary">Adicionar</button>
				</div>
			</fieldset>
		</form>
	</div>
	
	
	<!-- Modal Seleccionar Agente Quimico -->
	<div id="myModalAdicionarAgQuim" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<form action="adicionar_aval.php" method="post"  enctype="multipart/form-data" id="adicionar_agquimico_ao_posto-trabalho" class="form-horizontal" >
			<fieldset>		
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 id="myModalLabel">Adicionar Avaliação Agente Químico</h3>
				</div>
				<div class="modal-body">
					<p>&nbsp;</p>
					<div class="control-group"> 
						<label class="control-label" for="valor_registado">Dose Registada</label> 
						<div class="controls"> 
							<div class="pull-left">
									<input type="text" name="dose" />
							</div>
						</div>
					</div>
					<div class="control-group"> 
						<label class="control-label" for="data_execucao">Data da avaliação</label> 
						<div class="controls"> 
							<div class="pull-left">
									<input type="text" class="datepicker" name="data_execucao" /> 
							</div>
						</div>
					</div>
					<div class="control-group"> 
					<label class="control-label" for="data_validade">Agente Químico</label> 
					<div class="controls"> 
							<div class="pull-left">
									<select id="agentesquimicos" data-rel="chosen"  class="chosen" name="agente_quimico" placeholder="Agente químico"">
										<?php foreach ($lista_agquimicos as $agquim) { ?>
											<option value="<?=$agquim['agquim_id'] ?>"><?=$agquim['codigo'] ?> - <?=$agquim['designacao'] ?></option>
										<?php } ?>
									</select>
							</div>
						</div>
					</div>
					
					<div class="control-group"> 
						<label class="control-label" for="entidade_executante">Entidade Executante</label> 
						<div class="controls"> 
							<div class="pull-left">
									<input type="text" name="entidade_executante" /> 
							</div>
						</div>
					</div>
					
					<div class="control-group"> 
						<label class="control-label" for="relatorio">Relatório de avaliação</label> 
						<div class="controls"> 
							<div class="pull-left">
									<input type="file" name="relatorio" /> 
							</div>
						</div>
					</div>
					<input type="hidden" name="ptid" value="<?=$ptid?>">
					<input type="hidden" name="ag_type" value="aquim">
					<input type="hidden" name="ajax" value="true">

					</div>
				<div class="modal-footer">
					<button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
					<button id="adicionar_agquim_ao_posto-trabalho-submit" type="submit" class="btn btn-primary">Adicionar</button>
				</div>
			</fieldset>
		</form>
	</div>
</div>


<!-- Modal Seleccionar EPI's -->
<div id="myModalAdicionarEPI" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">Adicionar EPI</h3>
	</div>
	<div class="modal-body">
		<p>&nbsp;</p>
		<form action="associar_epi_ao_posto-trabalho.php" method="post" id="associar_epi_ao_posto-trabalho" class="form-horizontal" >
			<fieldset>
				<div class="control-group"> 
					<label class="control-label" for="mid">Seleccionar EPI</label> 
					<div class="controls"> 
						<div class="pull-left">
								<?php if (empty($todos_epi)) { ?>
									 <input type="text" class="input-xlarge uneditable-input info" placeholder="Já todos EPI's pertencem a este posto de trabalho">
									 <input type="hidden" name="mid" value="0">
								<?php } else { ?>
									<select name="epi_id" id="sel_epi" >
										<option value="0">- EPI -</option>
										<?php foreach ($todos_epi as $epi) { ?>
											<option value="<?= $epi['epi_id'] ?>"><?= $epi['nome'] ?></option>
										<?php } ?>
									</select>
								<?php } ?>
						</div>
					</div>
				</div>
				<input type="hidden" name="ptid" value="<?=$ptid?>">
				<input type="hidden" name="ajax" value="true">
			</fieldset>
		</form>
	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
		<button id="associar_epi_ao_posto-trabalho-submit" type="submit" class="btn btn-primary" <?php if (empty($todos_epi)) echo "disabled" ?>>Adicionar</button>
	</div>
</div>