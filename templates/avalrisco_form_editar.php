﻿<div class="row-fluid">
	<div class="span6 offset3">
		<form action="avalrisco.php" method="post" enctype="multipart/form-data" id="editar-avalrisco" class="form-horizontal well" >
			<fieldset>
				<legend><?=$title?></legend>
								<div class="control-group"> 
					<label class="control-label" for="avalrisco_data">Data Avaliação </label> 
					<div class="controls"> 
						<input type="text" class="datepicker" name="avalrisco_data" value="<?=$avalrisco['data'] ?>"/>	
					</div>
				</div>
				
				<div class="control-group"> 
					<label class="control-label" for="avalrisco_ptid">Posto Trabalho</label> 
					<div class="controls"> 
						<div class="pull-left">
							<select name="avalrisco_ptid" id="sel_posto_trabalho">
								<option value="0">- posto trabalho -</option>
								<?php foreach ($ptrabalhos as $ptrabalho) { ?>
									
									<option value="<?= $ptrabalho['ptid'] ?>" <?php if ($avalrisco['ptid'] == $ptrabalho['ptid']) echo "selected"?>><?= $ptrabalho['nome'] ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					
				</div>

				<div class="control-group"> 
					<label class="control-label" for="avalrisco_tarefa">Tarefa</label>
					<div class="controls"> 
						<input type="text" name="avalrisco_tarefa" value="<?=$avalrisco['tarefa'] ?>"/>  
					</div>
				</div>

				<div class="control-group"> 
					<label class="control-label" for="avalrisco_perigo">Perigo</label> 
					<div class="controls"> 
						<input type="text" name="avalrisco_perigo" value="<?=$avalrisco['perigo'] ?>"/> 
					</div> 
				</div>

				<div class="control-group"> 
					<label class="control-label" for="avalrisco_risco">Risco</label> 
					<div class="controls"> 
						<div class="pull-left">
						<select name="avalrisco_risco">
							<option value="" disabled selected> -- Riscos --</option>
							<option value="abrasao" <?php if ($avalrisco['risco'] == "abrasao") echo "selected" ?> >abrasao</option>
								<option value="afogamento" <?php if ($avalrisco['risco'] == "afogamento") echo "selected" ?> >afogamento</option>
								<option value="agarramento" <?php if ($avalrisco['risco'] == "agarramento") echo "selected" ?> >agarramento</option>
								<option value="atropelamento" <?php if ($avalrisco['risco'] == "atropelamento") echo "selected" ?> >atropelamento</option>
								<option value="capotamento" <?php if ($avalrisco['risco'] == "capotamento") echo "selected" ?> >capotamento</option>
								<option value="choque objectos imoveis" <?php if ($avalrisco['risco'] == "choque obejctos imoveis") echo "selected" ?> >choque obejctos imoveis</option>
								<option value="choque objectos moveis" <?php if ($avalrisco['risco'] == "choque objectos moveis") echo "selected" ?> >choque objectos moveis</option>
								<option value="colpaso estrutural" <?php if ($avalrisco['risco'] == "colpaso estrutural") echo "selected" ?> >colpaso estrutural</option>
								<option value="corte" <?php if ($avalrisco['risco'] == "corte") echo "selected" ?> >corte</option>
								<option value="desabamento" <?php if ($avalrisco['risco'] == "desabamento") echo "selected" ?> >desabamento</option>
								<option value="entalamento" <?php if ($avalrisco['risco'] == "entalamento") echo "selected" ?> >entalamento</option>
								<option value="queda altura" <?php if ($avalrisco['risco'] == "queda altura") echo "selected" ?> >queda altura</option>
								<option value="queda desnivel" <?php if ($avalrisco['risco'] == "queda desnivel") echo "selected" ?> >queda desnivel</option>
								<option value="queda mesmo nivel" <?php if ($avalrisco['risco'] == "queda mesmo nivel") echo "selected" ?> >queda mesmo nivel</option>
								<option value="queda de objectos" <?php if ($avalrisco['risco'] == "queda de objectos") echo "selected" ?> >queda de objectos</option>
								<option value="queda de objectos em altura" <?php if ($avalrisco['risco'] == "queda de objectos em altura") echo "selected" ?> >queda de objectos em altura</option>
								<option value="projeccao de objectos" <?php if ($avalrisco['risco'] == "projeccao de objectos") echo "selected" ?> >projeccao de objectos</option>
								<option value="projeccao de fragmentos" <?php if ($avalrisco['risco'] == "projeccao de fragmentos") echo "selected" ?> >projeccao de fragmentos</option>
								<option value="projeccao liquidos" <?php if ($avalrisco['risco'] == "projeccao liquidos") echo "selected" ?> >projeccao liquidos</option>
								<option value="soterramento" <?php if ($avalrisco['risco'] == "soterramento") echo "selected" ?> >soterramento</option>
								<option value="contacto directo electrico" <?php if ($avalrisco['risco'] == "contacto directo electrico") echo "selected" ?> >contacto directo electrico</option>
								<option value="contacto indirecto electrico" <?php if ($avalrisco['risco'] == "contacto indirecto electrico") echo "selected" ?> >contacto indirecto electrico</option>
								<option value="electricidade estatica" <?php if ($avalrisco['risco'] == "electricidade estatica") echo "selected" ?> >electricidade estatica</option>
								<option value="ambiente termico" <?php if ($avalrisco['risco'] == "ambiente termico") echo "selected" ?> >ambiente termico</option>
								<option value="contacto superficies frias" <?php if ($avalrisco['risco'] == "contacto superficies frias") echo "selected" ?> >contacto superficies frias</option>
								<option value="contacto superficies quentes" <?php if ($avalrisco['risco'] == "contacto superficies quentes") echo "selected" ?> >contacto superficies quentes</option>
								<option value="iluminacao" <?php if ($avalrisco['risco'] == "iluminacao") echo "selected" ?> >iluminacao</option>
								<option value="radiacoes não ionizantes" <?php if ($avalrisco['risco'] == "radiacoes não ionizantes") echo "selected" ?> >radiacoes não ionizantes</option>
								<option value="radiacoes ionizantes" <?php if ($avalrisco['risco'] == "radiacoes ionizantes") echo "selected" ?> >radiacoes ionizantes</option>
								<option value="ruido" <?php if ($avalrisco['risco'] == "ruido") echo "selected" ?> >ruido</option>
								<option value="vibracoes" <?php if ($avalrisco['risco'] == "vibracoes") echo "selected" ?> >vibracoes</option>
								<option value="contacto liquidos" <?php if ($avalrisco['risco'] == "contacto liquidos") echo "selected" ?> >contacto liquidos</option>
								<option value="inalacao gases" <?php if ($avalrisco['risco'] == "inalacao gases") echo "selected" ?> >inalacao gases</option>
								<option value="intoxicaoes liquidos" <?php if ($avalrisco['risco'] == "intoxicaoes liquidos") echo "selected" ?> >intoxicaoes liquidos</option>
								<option value="bacterias" <?php if ($avalrisco['risco'] == "bacterias") echo "selected" ?> >bacterias</option>
								<option value="fungos" <?php if ($avalrisco['risco'] == "fungos") echo "selected" ?> >fungos</option>
								<option value="parasitas" <?php if ($avalrisco['risco'] == "parasitas") echo "selected" ?> >parasitas</option>
								<option value="virus" <?php if ($avalrisco['risco'] == "virus") echo "selected" ?> >virus</option>
								<option value="acidente causadao seres vivos" <?php if ($avalrisco['risco'] == "acidente causadao seres vivos") echo "selected" ?> >acidente causadao seres vivos</option>
								<option value="posturas trabalho" <?php if ($avalrisco['risco'] == "posturas trabalho") echo "selected" ?> >posturas trabalho</option>
								<option value="sobrecarga" <?php if ($avalrisco['risco'] == "sobrecarga") echo "selected" ?> >sobrecarga</option>
								<option value="trabalho repetitivo" <?php if ($avalrisco['risco'] == "trabalho repetitivo") echo "selected" ?> >trabalho repetitivo</option>
								<option value="incendio" <?php if ($avalrisco['risco'] == "incendio") echo "selected" ?> >incendio</option>
								<option value="fadiga" <?php if ($avalrisco['risco'] == "fadiga") echo "selected" ?> >fadiga</option>
								<option value="stress" <?php if ($avalrisco['risco'] == "stress") echo "selected" ?> >stress</option>
								<option value="monotonia" <?php if ($avalrisco['risco'] == "monotonia") echo "selected" ?> >monotonia</option>
								<option value="atendimento publico" <?php if ($avalrisco['risco'] == "atendimento publico") echo "selected" ?> >atendimento publico</option>
						</select>
						</div>
					</div> 
				</div>
				
				<div class="control-group"> 
					<label class="control-label" for="avalrisco_probabilidade">Probabilidade</label> 
					<div class="controls"> 
						<div class="pull-left">
						<select name="avalrisco_probabilidade">
							<option value="" disabled selected> -- Probabilidade --</option>
							<option value="10" <?php if ($avalrisco['probabilidade'] == 10) echo "selected" ?> >Muito Provável</option>
							<option value="6" <?php if ($avalrisco['probabilidade'] == 6) echo "selected" ?> >Possível</option>
							<option value="3" <?php if ($avalrisco['probabilidade'] == 3) echo "selected" ?> >Raro</option>
							<option value="1" <?php if ($avalrisco['probabilidade'] == 1) echo "selected" ?> >Repetição improvável</option>
							<option value="0.5" <?php if ($avalrisco['probabilidade'] == 0.5) echo "selected" ?> >Nunca Aconteceu</option>
							<option value="0.1" <?php if ($avalrisco['probabilidade'] == 0.1) echo "selected" ?> >Praticamente Impossível</option>
						</select>
						</div>
						<div class="pull-left" style="padding:5px">
							<a href="#" class="info" data-rel="popover" data-content="<b>Muito Provável</b> -> acidente como resultado mais provável e esperado, se a situação de risco ocorrer <p> <b>Possível</b> -> Acidente como perfeitamente possível. Probabilidade de 50%. <p> <b>Raro</b> -> Acidente como coincidência rara. Probabilidade de 10%. <p> <b> Repetição improvável</b> -> Acidente como consequência remotamente possível. Sabe-se que já ocorreu. Probabilidade de 1%. <p><b> Nunca aconteceu</b> -> Acidente como coincidência extremamente remota <p><b> Praticamente Impossível</b> -> Acidente como praticamente impossível. Nunca aconteceu em muitos anos de exposição." title="Probabilidade"> <i class="icon icon-color icon-info"></i></a>
							
						</div>
					</div> 
				</div>
				
				
				<div class="control-group">
					<label class="control-label" for="avalrisco_exposicao">Exposição</label> 
					<div class="controls"> 
					<div class="pull-left">
						<select name="avalrisco_exposicao">
							<option value="" disabled selected> -- Exposição --</option>
							<option value="10" <?php if ($avalrisco['exposicao'] == 10) echo "selected" ?> >Contínua</option>
							<option value="6" <?php if ($avalrisco['exposicao'] == 6) echo "selected" ?> >Frequente</option>
							<option value="5" <?php if ($avalrisco['exposicao'] == 5) echo "selected" ?> >Ocasional</option>
							<option value="4" <?php if ($avalrisco['exposicao'] == 4) echo "selected" ?> >Irregular</option>
							<option value="1" <?php if ($avalrisco['exposicao'] == 1) echo "selected" ?> >Raro</option>
							<option value="0.5" <?php if ($avalrisco['exposicao'] == 0.5) echo "selected" ?> >Pouco Provável</option>
						</select>
						</div>
						<div class="pull-left" style="padding:5px">
							<a href="#" data-rel="popover" data-content="<b>Contínua</b> -> Muitas vezes por dia<p> <b>Frequente</b> -> Aproximadamente uma vez por dia<p> <b>Ocasional</b> -> > 1 vez por semana e < 1 vez por ano <p> <b> Irregular</b> -> >= 1 vez por ano e < 1 vez por ano<p><b> Raro</b> -> Sabe-se que ocorre, mas com baixíssima frequência <p><b> Pouco Provável</b> -> Não se sabe se ocorre, mas é possível que possa acontecer" title="Probabilidade"> <i class="icon icon-color icon-info"></i></a>
							
						</div>
						
					</div>
				</div>
				
				<div class="control-group"> 
					<label class="control-label" for="avalrisco_consequencia">Consequência</label> 
					<div class="controls"> 
						<div class="pull-left">
						<select name="avalrisco_consequencia">
							<option value="100" <?php if ($avalrisco['consequencia'] == 100) echo "selected" ?> >Catástrofe</option>
							<option value="50" <?php if ($avalrisco['consequencia'] == 50) echo "selected" ?> >Várias Mortes</option>
							<option value="25" <?php if ($avalrisco['consequencia'] == 25) echo "selected" ?> >Morte</option>
							<option value="15" <?php if ($avalrisco['consequencia'] == 15) echo "selected" ?> >Lesões Graves</option>
							<option value="5" <?php if ($avalrisco['consequencia'] == 5) echo "selected" ?> >Lesões com baixa</option>
							<option value="1" <?php if ($avalrisco['consequencia'] == 1) echo "selected" ?> >Pequenas feridas</option>
						</select>
						</div>
						<div class="pull-left" style="padding:5px">
							<a href="#" class="info" data-rel="popover" data-content="<b>Catástrofe</b> -> Elevado número de mortes, perdas >= 1.000.000€ <p> <b>Várias mortes</b> -> Predas >= 500.00 € e < 1.000.000 €<p> <b>Morte</b> -> Acidente mortal. Perdas >= 100.000 € e < 500.000 € <p> <b> Lesões Graves</b> ->Incapacidade Permanente. Perdas >= 1.000 € e < 100.000 €<p><b>Lesões com baixa</b> -> Incapacidade Temporária. Perdas < 1.000 €<p><b>Pequenas Feridas</b> -> Lesões ligeiras. Contusões, golpes, etc." title="Probabilidade"> <i class="icon icon-color icon-info"></i></a>
							
						</div>
						
					</div> 
				</div>
				
				<div class="control-group"> 
					<label class="control-label" for="avalrisco_grau_perigosidade">Grau Perigosidade</label> 
					<div class="controls"> 
						<input type="text" name="avalrisco_grau_perigosidade"  value="<?= $gp ?> - <?= $gp_texto ?>" readonly/>  
					</div> 
				</div>
				
				<div class="control-group"> 
					<label class="control-label" for="avalrisco_factor_custo">Factor Custo</label> 
					<div class="controls"> 
					
						<select name="avalrisco_factor_custo">
							<option value="" disabled selected> -- Factor de Custo -- </option>
							<option value="10" <?php if ($avalrisco['factor_custo'] == 10) echo "selected" ?> >Acima de 2500€</option>
							<option value="6" <?php if ($avalrisco['factor_custo'] == 6) echo "selected" ?> >De 1250 a 2500 €</option>
							<option value="4" <?php if ($avalrisco['factor_custo'] == 4) echo "selected" ?> >De 675 a 1250 €</option>
							<option value="3" <?php if ($avalrisco['factor_custo'] == 3) echo "selected" ?> >De 335 a 675 €</option>
							<option value="2" <?php if ($avalrisco['factor_custo'] == 2) echo "selected" ?> >De 150 a 335 €</option>
							<option value="1" <?php if ($avalrisco['factor_custo'] == 1) echo "selected" ?> >De 75 a 150 €</option>
							<option value="0.5" <?php if ($avalrisco['factor_custo'] == 0.5) echo "selected" ?> >Abaixo de 75 €</option>
						</select> 
					</div>
				</div>
				
				<div class="control-group"> 
					<label class="control-label" for="avalrisco_grau_correccao">Grau Correcção</label>
					<div class="controls"> 
					<select name="avalrisco_grau_correccao">
							<option value="" disabled selected> -- Grau Correcção -- </option>
							<option value="1" <?php if ($avalrisco['grau_correccao'] == 1) echo "selected" ?> >Risco completamente eliminado</option>
							<option value="2" <?php if ($avalrisco['grau_correccao'] == 2) echo "selected" ?> >Risco reduzido a 75%</option>
							<option value="3" <?php if ($avalrisco['grau_correccao'] == 3) echo "selected" ?> >Risco reduzido entre 50 a 75%</option>
							<option value="4" <?php if ($avalrisco['grau_correccao'] == 4) echo "selected" ?> >Risco reduzido entre 25 e 50%</option>
							<option value="6" <?php if ($avalrisco['grau_correccao'] == 6) echo "selected" ?> >Ligeiro efeito sobre o risco (inferior a 25%)</option>
						</select> 
					</div> 
				</div>
				
				<div class="control-group"> 
					<label class="control-label" for="avalrisco_indice_justificacao">Justificação Implementação</label> 
					<div class="controls"> 
						<input type="text" name="avalrisco_indice_justificacao" value="<?= $ji ?> - <?= $ji_texto ?>" readonly/>  
					</div> 
				</div>
				
				<div class="control-group"> 
					<label class="control-label" for="avalrisco_medidas">Medidas</label> 
					<div class="controls"> 
						<input type="text" name="avalrisco_medidas"/>  
					</div> 
				</div>
				
				<div class="control-group"> 
					<label class="control-label" for="avalrisco_responsavel">Responsável</label> 
					<div class="controls"> 
						<input type="text" name="avalrisco_responsavel"/>  
					</div> 
				</div>

				<div class="control-group"> 
					<label class="control-label" for="avalrisco_data_prevista_resolucao">Data Prevista de Resolução</label> 
					<div class="controls"> 
						<input type="text" name="avalrisco_data_prevista_resolucao"/>  
					</div> 
				</div>

				<div class="control-group"> 
					<label class="control-label" for="avalrisco_custo_previsto_resolucao">Custo Previsto de Resolução</label> 
					<div class="controls"> 
						<input type="text" name="avalrisco_custo_previsto_resolucao"/>  
					</div> 
				</div>

				<div class="control-group"> 
					<label class="control-label" for="avalrisco_data_efectiva_resolucao">Data Efectiva de Resolução</label> 
					<div class="controls"> 
						<input type="text" name="avalrisco_data_efectiva_resolucao"/>  
					</div>
				</div>

				<div class="control-group"> 
					<label class="control-label" for="avalrisco_custo_efectivo_resolucao">Custo Efectivo de Resolução</label> 
					<div class="controls"> 
						<input type="text" name="avalrisco_custo_efectivo_resolucao"/>  
					</div> 
				</div>
				<br/><br/>
				<hr>
				<div class="control-group"> 
					<label class="control-label" for="avalrisco_motivo_revisao">Motivo da alteração</label> 
					<div class="controls"> 
						<textarea rows="4" name="avalrisco_motivo_revisao"/></textarea>
					</div> 
				</div>


				
				<input type="hidden" name="avalrisco_eid" value="<?=$_SESSION['cur_eid'] ?>">
				<input type="hidden" name="avalrisco_pta_id" value="<?=$avalrisco['pta_id']?>">
				<input type="hidden" name="avalrisco_grau_perigosidade_valor" value="<?=$gp?>">
				<input type="hidden" name="avalrisco_indice_justificacao_valor" value="<?=$ji?>">
				<input type="hidden" name="action" value="edit">
				<div class="form-actions">
					<button type="submit" class="btn btn-primary">Gravar alterações</button>
					<a href="#" class="btn" onclick="history.go(-1);">Cancelar</a>
				</div>
								
			</fieldset>
		</form>
	</div>
</div>
