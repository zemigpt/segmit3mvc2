﻿<div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><a href="#" class="btn-minimize"><i class="icon-chevron-up"></i></a> <i class="icon-user"></i> Registo de Acidentes</h2>
			<div class="box-icon">
				<a href="adicionar_regacidentes.php?eid=<?=$eid?>" title="Adicionar Registo de Acidentes" class="btn btn-setting btn-round"><i class="icon-plus"></i></a>
			 
			</div>
		</div>
		<div class="box-content">
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>Data</th>
						<th>Estado</th>
						<th>Opções</th>
					</tr>
				</thead>

				<tbody>
					<?php if (empty($regacidentes)) echo '<tr><td colspan="3">Esta empresa não tem registo de acidentes associados</td></tr>'; ?>
						<?php foreach ($regacidentes as $regac): ?>				
						<tr>
							<td><?= $regac['data'] ?></td>
							<td><?= $regac["estado"]?></td>
							<td><a class="btn" href="index.php?ra_id=<?= $regac["ra_id"]?>"><i class="icon-eye-open" title="Ver detalhes do registo de acidentes"></i></a> <a class="btn" href="#"  title="Editar registo de acidente"><i class="icon-pencil"></i></a></td>
						</tr>
						<? endforeach ?>
				</tbody>
			</table>
		</div>
	</div><!--/span-->
</div><!--/row-->

