﻿<div class="row-fluid">	
	<div class="span2">
		<div class="box span12">
			<div class="box-header well" data-original-title>
				
			</div>
			<div>
			<ul class="nav nav-tabs nav-stacked">
				<li>
					<a href="#dadostrab">Dados Avaliação Risco</a>
				</li>
				<li>
					<a href="#revisoes">Revisoes</a>
				</li>
			</ul>
			</div>
		</div>
	</div>
	<div class="span10">	
		<div class="row-fluid">	
		<div class="box span12">
			<div class="box-header well" data-original-title>
				<a id="dadostrab"></a><h2><i class="icon-user"></i> Dados da Avaliação Risco - Método William Fine</h2>
				<div class="box-icon">
					<a href="avalrisco.php?id=<?=$avalrisco['pta_id']?>&action=edit" title="Editar Avaliação Risco" class="btn btn-round"><i class="icon-pencil"></i></a>
					<a href="avalrisco.php?id=<?=$avalrisco['pta_id']?>&action=delete" title="Remover Avaliação Risco" class="btn btn-round"><i class="icon-remove"></i></a>
					<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">
				<table class="table table-striped">


					<thead>
						<tr>
							<th colspan='2'><?=$avalrisco['tarefa']?> | <?=$avalrisco['perigo']?></th>
						</tr>
					</thead>

					<tbody>
						<tr>
							<th width="33%">Data</th>
							<td width="67%"><?=$avalrisco["data"]?></td>
						</tr>
						<tr>
							<th width="33%">Posto Trabalho</th>
							<td width="67%"><?=$avalrisco['nome']?></td>
						</tr>
						<tr>
							<th width="33%">Tarefa</th>
							<td width="67%"><?=$avalrisco['tarefa']?></td>
						</tr>
						<tr>
							<th>Perigo</th>
							<td><?=$avalrisco['perigo']?></td>
						</tr>
						<tr>
							<th>Risco</th>
							<td><?=$avalrisco['risco']?></td>
						</tr>
						<tr>
							<th>Probabilidade</th>
							<td><?=$avalrisco['probabilidade']?></td>
						</tr>
						<tr>
							<th>Exposição</th>
							<td><?=$avalrisco['exposicao']?></td>
						</tr>
						<tr>
							<th>Consequência</th>
							<td><?=$avalrisco['consequencia']?></td>
						</tr>
						<tr>
							<th>Grau Perigosidade</th>
							<td class="<?=$gp_class?>"><?=$avalrisco['grau_perigosidade']?> - <?=$gp_texto?></td>
						</tr>
						<tr>
							<th>Factor Custo</th>
							<td><?=$avalrisco['factor_custo']?></td>
						</tr>
						<tr>
							<th>Grau Correcção</th>
							<td><?=$avalrisco['grau_correccao']?></td>
						</tr>
						<tr>
							<th>Indice Justificação</th>
							<td><?=$avalrisco['indice_justificacao']?> - <?=$ji_texto?></td>
						</tr>
						<tr>
							<th>Medidas</th>
							<td><?=$avalrisco['medidas']?></td>
						</tr>
						<tr>
							<th>Responsável</th>
							<td><?=$avalrisco['responsavel']?></td>
						</tr>
						<tr>
							<th>Data Prevista Resolução</th>
							<td><?=$avalrisco['data_prevista_resolucao']?></td>
						</tr>
						<tr>
							<th>Custo Previsto Resolução</th>
							<td><?=$avalrisco['custo_previsto_resolucao']?></td>
						</tr>
						<tr>
							<th>Data Efectiva Resolução</th>
							<td><?=$avalrisco['data_efectiva_resolucao']?></td>
						</tr>
						<tr>
							<th>Custo Efectivo Resolução</th>
							<td><?=$avalrisco['custo_efectivo_resolucao']?></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="row-fluid"> <!--/ inicio box revisoes-->	
		
			<div class="box span12">
				<div class="box-header well" data-original-title>
					<a id="revisoes"></a><h2><i class="icon icon-wrench icon-black"></i> Revisoes</h2>
					<div class="box-icon">
						<!--<a href="adicionar_manutencao.php?mid=<?=$mid?>" title="Adicionar Manutenção" class="btn btn-round"><i class="icon-plus"></i></a> -->
						<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
					</div>
				</div>
				<div class="box-content">
					<table class="table table-striped">
						<thead>
							<tr>
								<th>
									Data Revisão
								</th>
								<th>
									Valor de Risco
								</th>
								<th>
									Motivo Revisão
								</th>
								<th>
									Opções
								</th>
								
							</tr>
						</thead>
						<tbody>				
						<?php if (empty($revisoes)) {?>
							<tr>
								<td colspan="4">
									Não há revisões da avaliação de risco associadas
								</td>
							</tr>
						<?php } else {?>
							<?php foreach($revisoes as $revisao) { ?>
								<tr>
									<td>
										<?=$revisao["timestamp"]?>
									</td>
									<td>
										<?=$revisao["grau_perigosidade"]?>
									</td>
									<td>
										<?=$revisao["motivo"]?>
									</td>
									<td>
										<!--<a class="btn" href="ptrabalho.php?id=<?= $posto_trabalho["ptid"]?>"><i class="icon-eye-open" title="Ver detalhes do posto de trabalho"></i></a> -->
										-- !!! a implementar !!! --
									</td>
								</tr>
							<?php }?>
						<?php }?>
						</tbody>
					</table>
				</div>
			</div><!--/ fim box dados manutencoes-->	
			</div><!--/row-->
	
		
		
		
		</div>
	</div>	
</div>

<d