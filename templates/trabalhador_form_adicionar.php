﻿<div class="row-fluid">
	<div class="span6 offset3">
		<form action="trabalhador.php" method="post" enctype="multipart/form-data" id="adicionar-trabalhador" class="form-horizontal well" >
			<fieldset>
				<legend><?=$title?></legend>
				<div class="control-group">
					<label class="control-label" for="baby_name">Nome do Trabalhador</label>
					<div class="controls">
						<input autofocus name="trab_nome" type="text"/>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="trab_foto">Fotografia</label>
					<div class="controls">
						<input type="file" name="trab_foto"/>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="trab_num_mecanografico">Número Mecanográfico</label>
					<div class="controls">
						<input type="text" name="trab_num_mecanografico"/>
					</div>
				</div>
				
				
				<div class="control-group">
					<label class="control-label" for="trab_morada">Morada</label>
					<div class="controls">
						<input type="text" name="trab_morada"/>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="trab_codpostal">Cod. Postal</label>
					<div class="controls">
						<input type="text" name="trab_codpostal"/>
				   </div>
				</div>
				<div class="control-group">
					<label class="control-label" for="trab_localidade">Localidade</label>
					<div class="controls">
						<input type="text" name="trab_localidade"/>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="trab_telefone">Telefone</label>
					<div class="controls">
						<input type="text" name="trab_telefone"/>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="trab_email">Email</label>
					<div class="controls">
						<input type="text" name="trab_email" required/>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="trab_contacto_sos_nome">Contacto Emergência</label>
					<div class="controls">
						<input type="text" name="trab_contacto_sos_nome"/>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label" for="trab_contacto_sos_telefone">Telefone Emergência</label>
					<div class="controls">
						<input type="text" name="trab_contacto_sos_telefone"/>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label" for="trab_bi">Doc. Identificação</label>
					<div class="controls">
						<input type="text" name="trab_bi" required/>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="datepicker">Validade Doc. Ident.</label>
					<div class="controls">
						<input type="text" class="datepicker" name="trab_bi_validade" />
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="trab_niss">Núm. Seg. Social</label>
					<div class="controls">
						<input type="text" name="trab_niss" required/>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="trab_nif">Número Contribuinte</label>
					<div class="controls">
						<input type="text" name="trab_nif" required/>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="trab_apolice_seguro">Apólice Seguro</label>
					<div class="controls">
						<input type="text" name="trab_apolice_seguro"/>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label" for="trab_tamanho_sapato">Tamanho Sapato</label> 
					<div class="controls"> 
						<select name="trab_tamanho_sapato" placeholder="Escolher Tamanho">
							<option value="" disabled selected>Escolher Tamanho</option>
							<option value="34">34</option>
							<option value="35">35</option>
							<option value="36">36</option>
							<option value="37">37</option>
							<option value="38">38</option>
							<option value="39">39</option>
							<option value="40">40</option>
							<option value="41">41</option>
							<option value="42">42</option>
							<option value="43">43</option>
							<option value="44">44</option>
							<option value="45">45</option>
							<option value="46">46</option>
							<option value="47">47</option>
						</select>
					</div>
				</div>
				
				<div class="control-group"> 
					<label class="control-label" for="trab_tamanho_roupa">Tamanho Roupa</label> 
					<div class="controls"> 
						<select name="trab_tamanho_roupa" placeholder="Escolher Tamanho">
							<option value="" disabled selected>Escolher Tamanho</option>
							<option value="XS">XS</option>
							<option value="S">S</option>
							<option value="M">M</option>
							<option value="L">L</option>
							<option value="XL">XL</option>
							<option value="XXL">XXL</option>
						</select>
					</div> 
				</div>


				
				<input type="hidden" name="trab_eid" value="<?=$eid?>">
				<input type="hidden" name="action" value="add">
				
				<div class="form-actions">
					<button type="submit" class="btn btn-primary">Adicionar trabalhador</button>
					<a href="#" class="btn" onclick="history.go(-1);">Cancelar</a>
				</div>
								
			</fieldset>
		</form>
	</div>
</div>
