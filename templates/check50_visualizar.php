﻿<div class="row-fluid">	
	<div class="span2">
		<div class="box span12">
			<div class="box-header well" data-original-title>
				
			</div>
			<div>
			<ul class="nav nav-tabs nav-stacked">
				<li>
					<a href="#dadoscheck50">Checklist</a>
				</li>
			</ul>
			</div>
		</div>
	</div>
	<div class="span10">	
		<div class="row-fluid">	
		<div class="box span12">
			<div class="box-header well" data-original-title>
				<a id="dadoscheck50"></a><h2><i class="icon-user"></i> Checklists</h2>
				<div class="box-icon">
					<a href="check50.php?id=<?=$check50_id?>&action=edit" title="Editar Checklist" class="btn btn-round"><i class="icon-pencil"></i></a>
					<a href="check50.php?id=<?=$check50_id?>&action=delete" title="Remover Checklist" class="btn btn-round"><i class="icon-remove"></i></a>
					<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">
				<table class="table table-striped">
					<thead>
						<th width="40%"> Verificação </th>
						<th width="20%"> S/N/NA </th>
						<th width="40%"> Obs. </th>
						
					
					</thead>

					<tbody>
						<tr><td colspan="3"></td></tr>
						<tr><th colspan="3">Art.º 11.º - Sistemas de comando </th> </tr>
						
						<tr><td colspan="3">1 Os sistemas de comando que tenham incidência sobre a segurança estão claramente:</td></tr>
						<tr><td width="60%">1.1 Visíveis</td> <td width="12%"><?=$checklist['check50_1']?></td><td width="28%"><?=$checklist['check50_1_obs']?></td></tr>
						<tr><td width="60%">1.2 Identificáveis</td> <td width="12%"><?=$checklist['check50_2']?></td><td width="28%"><?=$checklist['check50_2_obs']?></td></tr>
						<tr><td width="60%">1.3 Marcação apropriada (se for caso disso)</td> <td width="12%"><?=$checklist['check50_3']?></td><td width="28%"><?=$checklist['check50_3_obs']?></td></tr>
						<tr><td width="60%">2 Os sistemas de comando estão colocados fora de zonas perigosas</td> <td width="12%"><?=$checklist['check50_4']?></td><td width="28%"><?=$checklist['check50_4_obs']?></td></tr>
						<tr><td width="60%">2.1 Os comandos estão colocados de modo que o seu accionamento nomeadamente por uma manobra não intencional, não ocasiona riscos suplementares.</td> <td width="12%"><?=$checklist['check50_5']?></td><td width="28%"><?=$checklist['check50_5_obs']?></td></tr>
						<tr><td width="60%">3 O operador pode certificar-se a partir do seu posto de trabalho principal, da ausência de pessoas nas zonas perigosas (se responder não averigue as duas questões seguintes)</td> <td width="12%"><?=$checklist['check50_6']?></td><td width="28%"><?=$checklist['check50_6_obs']?></td></tr>
						<tr><td width="60%">3.1 O arranque é automaticamente precedido de um sistema de aviso seguro (sinal sonoro ou visual)</td> <td width="12%"><?=$checklist['check50_7']?></td><td width="28%"><?=$checklist['check50_7_obs']?></td></tr>
						<tr><td width="60%">4 Após o aviso referido anteriormente no ponto 3.1, o trabalhador exposto dispõe de tempo e, se necessário dos meios indispensáveis para se afastar imediatamente da zona perigosa</td> <td width="12%"><?=$checklist['check50_8']?></td><td width="28%"><?=$checklist['check50_8_obs']?></td></tr>
						<tr><td width="60%">5 Os sistemas de comando são seguros</td> <td width="12%"><?=$checklist['check50_9']?></td><td width="28%"><?=$checklist['check50_9_obs']?></td></tr>
						<tr><td width="60%">5.1 Os sistemas de comando foram escolhidos tendo em conta as falhas, perturbações e limitações previsíveis de utilização para que foram projectados.</td> <td width="12%"><?=$checklist['check50_10']?></td><td width="28%"><?=$checklist['check50_10_obs']?></td></tr>
						
						<tr><td colspan="3"></td></tr>
						<tr><th colspan="3">Art.º 12.º - Arranque do equipamento </th> </tr>
						
						<tr><td colspan="3">1 O equipamento de trabalho está provido de um sistema de comando, de modo que seja necessária uma acção voluntária sobre um comando com essa finalidade para que possa:</td> </tr>
						<tr><td width="60%">a) Ser posto em funcionamento</td> <td width="12%"><?=$checklist['check50_11']?></td><td width="28%"><?=$checklist['check50_11_obs']?></td></tr>
						<tr><td width="60%">b) Arrancar após uma paragem, qq que seja a origem desta</td> <td width="12%"><?=$checklist['check50_12']?></td><td width="28%"><?=$checklist['check50_12_obs']?></td></tr>
						<tr><td width="60%">c) Sofrer uma modificação importante das condições de funcionamento, nomeadamente velocidade ou pressão</td> <td width="12%"><?=$checklist['check50_13']?></td><td width="28%"><?=$checklist['check50_13_obs']?></td></tr>
						<tr><td width="60%">2 O disposto no numero anterior não é aplicável se esse arranque ou essa modificação não representar qualquer risco para os trabalhadores expostos ou se resultar da sequência normal de um ciclo automático.</td> <td width="12%"><?=$checklist['check50_14']?></td><td width="28%"><?=$checklist['check50_14_obs']?></td></tr>
						
						<tr><td colspan="3"></td></tr>
						<tr><th colspan="3">Art.º 13.º - Paragem do equipamento </th> </tr>
						
						<tr><td width="60%">1 O equipamento está provido de um sistema de comando que permita a sua paragem geral em condições de segurança.</td> <td width="12%"><?=$checklist['check50_15']?></td><td width="28%"><?=$checklist['check50_15_obs']?></td></tr>
						<tr><td width="60%">1.1 O equipamento está provido de um dispositivo de paragem de emergência, em função dos perigos inerentes ao equipamento e ao tempo normal de paragem.</td> <td width="12%"><?=$checklist['check50_16']?></td><td width="28%"><?=$checklist['check50_16_obs']?></td></tr>
						<tr><td width="60%">2 Os postos de trabalho dispõem de um sistema de comando que permite, em função dos riscos existentes, parar todo ou parte do equipamento de trabalho de forma que o mesmo fique em situação de segurança</td> <td width="12%"><?=$checklist['check50_17']?></td><td width="28%"><?=$checklist['check50_17_obs']?></td></tr>
						<tr><td width="60%">2.1 A ordem de paragem tem prioridade sobre as ordens de arranque</td> <td width="12%"><?=$checklist['check50_18']?></td><td width="28%"><?=$checklist['check50_18_obs']?></td></tr>
						<tr><td width="60%">3 A alimentação de energia dos accionadores do equipamento de trabalho é interrompida sempre que se verifique a paragem do mesmo ou dos seus elementos perigosos.</td> <td width="12%"><?=$checklist['check50_19']?></td><td width="28%"><?=$checklist['check50_19_obs']?></td></tr>
						
						<tr><td colspan="3"></td></tr>
						<tr><th colspan="3">Art.º 14.º - Estabilidade e rotura </th> </tr>
						
						<tr><td width="60%">1 O equipamento de trabalho e os respectivos elementos estão estabilizados por fixação ou por outros meios (sempre que se justifique).</td> <td width="12%"><?=$checklist['check50_20']?></td><td width="28%"><?=$checklist['check50_20_obs']?></td></tr>
						<tr><td width="60%">2 Devem ser tomadas medidas adequadas se existirem riscos de estilhaçamento ou de rotura de elementos de um equipamento susceptíveis de pôr em perigo a segurança ou a saude dos trabalhadores.</td> <td width="12%"><?=$checklist['check50_21']?></td><td width="28%"><?=$checklist['check50_21_obs']?></td></tr>
						
						<tr><td colspan="3"></td></tr>
						<tr><th colspan="3">Art.º 15.º - Projecções e emanações </th> </tr>
						
						<tr><td width="60%">1 O equipamento de trabalho não provoca riscos devido a quedas de objectos ou projecções de objectos (se responder não averigue a questão seguinte).</td> <td width="12%"><?=$checklist['check50_22']?></td><td width="28%"><?=$checklist['check50_22_obs']?></td></tr>
						<tr><td width="60%">1.1 O equipamento dispõe de dispositivos de segurança adequados no caso da existência de riscos devidas a quedas de objectos ou projecções de objectos.</td> <td width="12%"><?=$checklist['check50_23']?></td><td width="28%"><?=$checklist['check50_23_obs']?></td></tr>
						<tr><td width="60%">2 O equipamento de trabalho não provoca riscos devidos a emanações de gases, vapores ou líquidos ou a emissão de poeiras (se responder não averigue a questão seguinte).</td> <td width="12%"><?=$checklist['check50_24']?></td><td width="28%"><?=$checklist['check50_24_obs']?></td></tr>
						<tr><td width="60%">2.1 O equipamento dispõe de dispositivos de retenção ou extracção eficazes, instalados na proximidade da respectiva fonte.</td> <td width="12%"><?=$checklist['check50_25']?></td><td width="28%"><?=$checklist['check50_25_obs']?></td></tr>
						
						<tr><td colspan="3"></td></tr>
						<tr><th colspan="3">Art.º 16.º - Riscos de contacto mecânico </th> </tr>
						
						<tr><td width="60%">1 Os elementos móveis do equipamento não podem causar acidentes por contacto mecânico (se responder não averigue as questões seguintes).</td> <td width="12%"><?=$checklist['check50_26']?></td><td width="28%"><?=$checklist['check50_26_obs']?></td></tr>
						<tr><td width="60%">1.1 Os elementos móveis do equipamento que possam causar acidentes por contacto mecânico dispõem de protectores que impeçam o acesso á zona perigosa (se responder sim averigue a questão n.º 2.1</td> <td width="12%"><?=$checklist['check50_27']?></td><td width="28%"><?=$checklist['check50_27_obs']?></td></tr>
						<tr><td width="60%">1.2 Os elementos móveis do equipamento que possam causar acidentes por contacto mecânico dispõem de dispositivos que interrompam o movimento dos elementos móveis antes do acesso a zona perigosa (se responder sim averigue a questão n.º 2.2).</td> <td width="12%"><?=$checklist['check50_28']?></td><td width="28%"><?=$checklist['check50_28_obs']?></td></tr>
						<tr><td colspan="3">2 Protetores</td> </tr>
						<tr><td width="60%">2.1.1 São de construção robusta.</td> <td width="12%"><?=$checklist['check50_29']?></td><td width="28%"><?=$checklist['check50_29_obs']?></td></tr>
						<tr><td width="60%">2.1.2 Não ocasionam riscos suplementares.</td> <td width="12%"><?=$checklist['check50_30']?></td><td width="28%"><?=$checklist['check50_30_obs']?></td></tr>
						<tr><td width="60%">2.1.3 Não são facilmente neutralizados ou tornados inoperantes.</td> <td width="12%"><?=$checklist['check50_31']?></td><td width="28%"><?=$checklist['check50_31_obs']?></td></tr>
						<tr><td width="60%">2.1.4 Estão situados a uma distância suficiente da zona perigosa(distância de segurança).</td> <td width="12%"><?=$checklist['check50_33']?></td><td width="28%"><?=$checklist['check50_33_obs']?></td></tr>
						<tr><td width="60%">2.1.5 Não limitam a observação do ciclo de trabalho mais do que necessário.</td> <td width="12%"><?=$checklist['check50_34']?></td><td width="28%"><?=$checklist['check50_34_obs']?></td></tr>
						<tr><td colspan="3">2.2 Os dispositivos de protecção são:</td></tr>
						<tr><td width="60%">2.2.1 São de construção robusta.</td> <td width="12%"><?=$checklist['check50_35']?></td><td width="28%"><?=$checklist['check50_35_obs']?></td></tr>
						<tr><td width="60%">2.2.2 Não ocasionam riscos suplementares</td> <td width="12%"><?=$checklist['check50_36']?></td><td width="28%"><?=$checklist['check50_36_obs']?></td></tr>
						<tr><td width="60%">2.2.3 Não podem ser facilmente neutralizados ou tornados  inoperantes (modo positivo de actuação).</td> <td width="12%"><?=$checklist['check50_37']?></td><td width="28%"><?=$checklist['check50_37_obs']?></td></tr>
						<tr><td width="60%">2.2.4 Estão situados a uma distância suficiente da zona perigosa (distância de segurança).</td> <td width="12%"><?=$checklist['check50_38']?></td><td width="28%"><?=$checklist['check50_38_obs']?></td></tr>
						<tr><td width="60%">2.2.5  Não limitam a observação do ciclo de trabalho mais  do que necessário</td> <td width="12%"><?=$checklist['check50_39']?></td><td width="28%"><?=$checklist['check50_39_obs']?></td></tr>
						<tr><td width="60%">3 Os protectores permitem, se possível sem a sua desmontagem as intervenções necessárias à colocação ou substituição de  elementos do equipamento, bem como à sua manutenção, possibilitando o acesso apenas ao sector em que esta deve ser realizada.</td> <td width="12%"><?=$checklist['check50_40']?></td><td width="28%"><?=$checklist['check50_40_obs']?></td></tr>
						<tr><td width="60%"> Os  dispositivos de protecção permitem, se possível sem a sua  desmontagem as intervenções necessárias à colocação ou substituição de elementos do equipamento, bem como à sua manutenção, possibilitando o acesso apenas ao sector em que esta deve ser realizada.</td> <td width="12%"><?=$checklist['check50_41']?></td><td width="28%"><?=$checklist['check50_41_obs']?></td></tr>
						
						<tr><td colspan="3"></td></tr>
						<tr><th colspan="3">Art.º 17.º - Iluminação e temperatura </th> </tr>
						
						<tr><td width="60%">1 As zonas e pontos de trabalho ou de manutenção do equipamento estão convenientemente iluminados em função dos trabalhos a realizar.</td> <td width="12%"><?=$checklist['check50_42']?></td><td width="28%"><?=$checklist['check50_42_obs']?></td></tr>
						<tr><td width="60%">2 As partes do equipamento que atinjam temperaturas elevadas ou muito baixas dispõem de uma protecção contra os riscos de contacto ou de proximidade por parte dos trabalhadores.</td> <td width="12%"><?=$checklist['check50_43']?></td><td width="28%"><?=$checklist['check50_43_obs']?></td></tr>
						
						<tr><td colspan="3"></td></tr>
						<tr><th colspan="3">Art.º 18.º - Dispositivos de alerta </th> </tr>
						
						<tr><td width="60%">1 Os dispositivos de alerta do equipamento são ouvidos e compreendidos facilmente e sem ambiguidades. </td> <td width="12%"><?=$checklist['check50_44']?></td><td width="28%"><?=$checklist['check50_44_obs']?></td></tr>
						
						<tr><td colspan="3"></td></tr>
						<tr><th colspan="3">Art.º 19.º - Manutenção do equipamento </th> </tr>
						
						<tr><td width="60%">1 As operações de manutenção podem ser efectuadas com o equipamento parado. (se responder não averigue a questão seguinte).</td> <td width="12%"><?=$checklist['check50_45']?></td><td width="28%"><?=$checklist['check50_45_obs']?></td></tr>
						<tr><td width="60%">1.1 Podem ser tomadas medidas de protecção adequadas à execução dessas operações ou estas podem ser efectuadas fora das áreas perigosas.</td> <td width="12%"><?=$checklist['check50_46']?></td><td width="28%"><?=$checklist['check50_46_obs']?></td></tr>
						<tr><td width="60%">2 O equipamento possui livrete de manutenção (se responder sim averigue a questão seguinte).</td> <td width="12%"><?=$checklist['check50_47']?></td><td width="28%"><?=$checklist['check50_47_obs']?></td></tr>
						<tr><td width="60%">2.1 O livrete de manutenção está actualizado.</td> <td width="12%"><?=$checklist['check50_48']?></td><td width="28%"><?=$checklist['check50_48_obs']?></td></tr>
						<tr><td width="60%">2.2 Para efectuar as operações de produção, regulação e manutenção do equipamento, os trabalhadores têm acesso a todos os locais necessários e permanecem neles em segurança.</td> <td width="12%"><?=$checklist['check50_49']?></td><td width="28%"><?=$checklist['check50_49_obs']?></td></tr>
						
						<tr><td colspan="3"></td></tr>
						<tr><th colspan="3">Art.º 20.º - Riscos eléctricos, de incêndio e de explosão </th> </tr>
						
						<tr><td width="60%">1 O equipamento protege os trabalhadores expostos contra os riscos de contacto directo com a electricidade.</td> <td width="12%"><?=$checklist['check50_50']?></td><td width="28%"><?=$checklist['check50_50_obs']?></td></tr>
						<tr><td width="60%">1.1 O equipamento protege os trabalhadores expostos contra os riscos de contacto indirecto com a electricidade.</td> <td width="12%"><?=$checklist['check50_51']?></td><td width="28%"><?=$checklist['check50_51_obs']?></td></tr>
						<tr><td width="60%">2 O equipamento protege os trabalhadores contra os riscos de incêndio.</td> <td width="12%"><?=$checklist['check50_52']?></td><td width="28%"><?=$checklist['check50_52_obs']?></td></tr>
						<tr><td width="60%">2.1 O equipamento protege os trabalhadores contra os riscos de sobreaquecimento.</td> <td width="12%"><?=$checklist['check50_53']?></td><td width="28%"><?=$checklist['check50_53_obs']?></td></tr>
						<tr><td width="60%">2.2 O equipamento protege os trabalhadores contra os riscos de libertação de gases por ele produzidos, utilizados ou armazenados.</td> <td width="12%"><?=$checklist['check50_54']?></td><td width="28%"><?=$checklist['check50_54_obs']?></td></tr>
						<tr><td width="60%">2.3 O equipamento protege os trabalhadores contra os riscos de libertação de poeiras por ele produzidas, utilizadas ou armazenadas.</td> <td width="12%"><?=$checklist['check50_55']?></td><td width="28%"><?=$checklist['check50_55_obs']?></td></tr>
						<tr><td width="60%">2.4 O equipamento protege os trabalhadores contra os riscos de libertação de líquidos por ele produzidos, utilizados ou armazenados.</td> <td width="12%"><?=$checklist['check50_56']?></td><td width="28%"><?=$checklist['check50_56_obs']?></td></tr>
						<tr><td width="60%">2.5 O equipamento protege os trabalhadores contra os riscos de libertação de vapores por ele produzidos, utilizados ou armazenados.</td> <td width="12%"><?=$checklist['check50_57']?></td><td width="28%"><?=$checklist['check50_57_obs']?></td></tr>
						<tr><td width="60%">2.6 O equipamento protege os trabalhadores contra os riscos de libertação de outras substâncias por eles produzidas, utilizadas ou armazenadas.</td> <td width="12%"><?=$checklist['check50_58']?></td><td width="28%"><?=$checklist['check50_58_obs']?></td></tr>
						<tr><td width="60%">3 O equipamento previne os riscos de explosão de equipamentos ou substâncias por ele produzidas ou nele utilizadas ou armazenadas.</td> <td width="12%"><?=$checklist['check50_59']?></td><td width="28%"><?=$checklist['check50_59_obs']?></td></tr>
						<tr><td width="60%">3.1 O equipamento previne os riscos de explosão de substâncias por ele produzidas ou nele utilizadas ou armazenadas.</td> <td width="12%"><?=$checklist['check50_60']?></td><td width="28%"><?=$checklist['check50_60_obs']?></td></tr>
						
						<tr><td colspan="3"></td></tr>
						<tr><th colspan="3">Art.º 21.º - Fontes de energia </th> </tr>
						
						<tr><td width="60%">1 O equipamento dispõem de dispositivos claramente identificados, que p</td> <td width="12%"><?=$checklist['check50_61']?></td><td width="28%"><?=$checklist['check50_61_obs']?></td></tr>
						<tr><td width="60%">1.1 Em caso de reconexão das energias externas, esta poder ser feita sem </td> <td width="12%"><?=$checklist['check50_62']?></td><td width="28%"><?=$checklist['check50_62_obs']?></td></tr>
						
						<tr><td colspan="3"></td></tr>
						<tr><th colspan="3">Art.º 22.º - Sinalização de segurança </th> </tr>
						
						<tr><td width="60%">1 O equipamento está devidamente sinalizado, com avisos ou outra sinalização indispensável para garantir a segurança dos trabalhadores.</td> <td width="12%"><?=$checklist['check50_63']?></td><td width="28%"><?=$checklist['check50_63_obs']?></td></tr>
						
						<tr><td colspan="3"></td></tr>
						<tr><th colspan="3">Art.º 23.º - Equipamentos que transportem trabalhadores e riscos de capotamento </th> </tr>
						
						<tr><td width="60%">1 O equipamento de trabalho não possui riscos para os trabalhadores durante a deslocação, nomeadamente o risco de contacto com as rodas ou as lagartas ou o seu entalamento por essas peças (se responder não averigue a questão seguinte).</td> <td width="12%"><?=$checklist['check50_64']?></td><td width="28%"><?=$checklist['check50_64_obs']?></td></tr>
						<tr><td width="60%">1.1 O equipamento está adaptado de forma a reduzir os riscos referidos.</td> <td width="12%"><?=$checklist['check50_65']?></td><td width="28%"><?=$checklist['check50_65_obs']?></td></tr>
						<tr><td width="60%">2 O equipamento não possui risco de capotamento (se responder não averigue a questão seguinte).</td> <td width="12%"><?=$checklist['check50_66']?></td><td width="28%"><?=$checklist['check50_66_obs']?></td></tr>
						<tr><td width="60%">2.1 O equipamento limita o risco de capotamento por meio de uma estrutura que o impeça de virar mais de um quarto de volta ou se o movimento exceder um quarto de volta, o equipamento possui uma estrutura que garanta espaço suficiente em torno dos trabalhadores transportados ou outro dispositivo de efeito equivalente.</td> <td width="12%"><?=$checklist['check50_67']?></td><td width="28%"><?=$checklist['check50_67_obs']?></td></tr>
						<tr><td width="60%">3 As estruturas de protecção previstas nos pontos 2.1 são parte integrante do equipamento.</td> <td width="12%"><?=$checklist['check50_68']?></td><td width="28%"><?=$checklist['check50_68_obs']?></td></tr>
						<tr><td width="60%">4 Não existe o risco de esmagamento dos trabalhadores entre o equipamento e o solo no caso de capotamento (se responder não averigue a questão seguinte).</td> <td width="12%"><?=$checklist['check50_69']?></td><td width="28%"><?=$checklist['check50_69_obs']?></td></tr>
						<tr><td width="60%">4.1 Está instalado no equipamento um sistema de retenção dos trabalhadores transportados (caso exista no mercado para o modelo de equipamento em causa).</td> <td width="12%"><?=$checklist['check50_70']?></td><td width="28%"><?=$checklist['check50_70_obs']?></td></tr>
						<tr><td width="60%">5 A instalação das estruturas de protecção do ponto 2.1 não é obrigatória nos seguintes casos:</td> <td width="12%"><?=$checklist['check50_71']?></td><td width="28%"><?=$checklist['check50_71_obs']?></td></tr>
						<tr><td width="60%">5.1 Quando o equipamento se encontra estabilizado durante a sua utilização ou quando a concepção do mesmo impossibilita o seu capotamento.</td> <td width="12%"><?=$checklist['check50_72']?></td><td width="28%"><?=$checklist['check50_72_obs']?></td></tr>
						<tr><td width="60%">5.2 Em tractores agrícolas matriculados antes de 1 de Janeiro de 1994.</td> <td width="12%"><?=$checklist['check50_73']?></td><td width="28%"><?=$checklist['check50_73_obs']?></td></tr>
						<tr><td width="60%">5.3 Em outros equipamentos agrícolas e florestais para os quais não existam no mercado estruturas de protecção.</td> <td width="12%"><?=$checklist['check50_74']?></td><td width="28%"><?=$checklist['check50_74_obs']?></td></tr>
						
						<tr><td colspan="3"></td></tr>
						<tr><th colspan="3">Art.º 24.º - Transmissão de energia </th> </tr>
						
						<tr><td width="60%">1 O equipamento não possui riscos devidos ao bloqueio intempestivo dos elementos de transmissão de energia entre os equipamentos e os seus acessórios ou reboques (se responder não averigue a questão seguinte).</td> <td width="12%"><?=$checklist['check50_75']?></td><td width="28%"><?=$checklist['check50_75_obs']?></td></tr>
						<tr><td width="60%">1.1 O equipamento está equipado ou adaptado de forma a impedir os riscos referidos no ponto 1.</td> <td width="12%"><?=$checklist['check50_76']?></td><td width="28%"><?=$checklist['check50_76_obs']?></td></tr>
						<tr><td width="60%">1.2 No caso de não ser possível impedir esse bloqueio, foram tomadas medidas que garantam a segurança dos trabalhadores.</td> <td width="12%"><?=$checklist['check50_77']?></td><td width="28%"><?=$checklist['check50_77_obs']?></td></tr>
						<tr><td width="60%">2 Os elementos de transmissão de energia entre equipamentos de trabalho móveis não podem sujar-se ou danificar-se ao serem arrastados pelo chão (se responder não averigue a questão seguinte).</td> <td width="12%"><?=$checklist['check50_78']?></td><td width="28%"><?=$checklist['check50_78_obs']?></td></tr>
						<tr><td width="60%">2.1 Foi prevista a sua fixação.</td> <td width="12%"><?=$checklist['check50_79']?></td><td width="28%"><?=$checklist['check50_79_obs']?></td></tr>
						
						<tr><td colspan="3"></td></tr>
						<tr><th colspan="3">Art.º 25.º - Risco de capotamento de empilhadores </th> </tr>
					
						<tr><td width="60%">1 No caso do equipamento ser um empilhador não existe o risco de capotamento (se responder não averigue a questão seguinte).</td> <td width="12%"><?=$checklist['check50_80']?></td><td width="28%"><?=$checklist['check50_80_obs']?></td></tr>
						<tr><td width="60%">1.1 O empilhador está adaptado ou equipado com uma estrutura que impeça o capotamento ou uma cabina ou outra estrutura que em caso de capotamento assegure ao operador um espaço suficiente entre o solo e o empilhador ou uma estrutura que mantenha o operador no posto de condução e o impeça de ser apanhado por alguma parte do empilhador.</td> <td width="12%"><?=$checklist['check50_81']?></td><td width="28%"><?=$checklist['check50_81_obs']?></td></tr>
						
						<tr><td colspan="3"></td></tr>
						<tr><th colspan="3">Art.º 26.º - Equipamentos móveis automotores </th> </tr>
					
						<tr><td width="60%">1 O equipamento não origina riscos devidos à sua movimentação (se responder não averigue as questões seguintes).</td> <td width="12%"><?=$checklist['check50_82']?></td><td width="28%"><?=$checklist['check50_82_obs']?></td></tr>
						<tr><td width="60%">1.1 Evitam a entrada em funciomamento não autorizada.</td> <td width="12%"><?=$checklist['check50_83']?></td><td width="28%"><?=$checklist['check50_83_obs']?></td></tr>
						<tr><td width="60%">1.2 Reduzam as consequências de colisão em caso de movimentação simultânea de diversos equipamentos de trabalho que se  desloquem sobre carris.</td> <td width="12%"><?=$checklist['check50_84']?></td><td width="28%"><?=$checklist['check50_84_obs']?></td></tr>
						<tr><td width="60%">1.3 Permitam a sua travagem e imobilização e que, se o dispositivo principal avariar e a segurança o exigir, assegurem a travagem  e imobilização de emergência.</td> <td width="12%"><?=$checklist['check50_85']?></td><td width="28%"><?=$checklist['check50_85_obs']?></td></tr>
						<tr><td width="60%">1.4 Aumentem a visibilidade quando o campo de visão directa do condutor for insuficiente para garantir a segurança.</td> <td width="12%"><?=$checklist['check50_86']?></td><td width="28%"><?=$checklist['check50_86_obs']?></td></tr>
						<tr><td width="60%">1.5 Em caso de utilização nocturna ou em local mal iluminado, assegurem uma iluminação adequada ao trabalho</td> <td width="12%"><?=$checklist['check50_87']?></td><td width="28%"><?=$checklist['check50_87_obs']?></td></tr>
						<tr><td width="60%">2 O equipamento não comporta risco de incêndio susceptível de pôr em perigo os trabalhadores através da sua estrutura, atrelado ou carga (se responder não averigue a questão seguinte).</td> <td width="12%"><?=$checklist['check50_88']?></td><td width="28%"><?=$checklist['check50_88_obs']?></td></tr>
						<tr><td width="60%">2.1 O equipamento possui dispositivos adequados de combate ao fogo.</td> <td width="12%"><?=$checklist['check50_89']?></td><td width="28%"><?=$checklist['check50_89_obs']?></td></tr>
						<tr><td width="60%">3 No caso de equipamento telecomandado, este deve imobilizar-se automaticamente sempre que sai do campo de controlo.</td> <td width="12%"><?=$checklist['check50_90']?></td><td width="28%"><?=$checklist['check50_90_obs']?></td></tr>
						<tr><td width="60%">3.1 Em condições normais de utilização não trazem o risco de entalar ou colidir com trabalhadores (se responder não averigue a questão seguintes).</td> <td width="12%"><?=$checklist['check50_91']?></td><td width="28%"><?=$checklist['check50_91_obs']?></td></tr>
						<tr><td width="60%">3.2 O equipamento deve dispor de dispositivos de protecção contra o risco de colisão ou entalamento.</td> <td width="12%"><?=$checklist['check50_92']?></td><td width="28%"><?=$checklist['check50_92_obs']?></td></tr>
						
						<tr><td colspan="3"></td></tr>
						<tr><th colspan="3">Art.º 27.º - Instalação </th> </tr>
					
						<tr><td colspan="3">O equipamento de elevação de cargas que esteja instalado permanentemente deve: </td> </tr>
						<tr><td width="60%">1.1 Manter a solidez e estabilidade durante a sua utilização, tendo em conta as cargas a elevar e as forças exercidas nos pontos de suspensão ou de fixação ás estrutura</td> <td width="12%"><?=$checklist['check50_93']?></td><td width="28%"><?=$checklist['check50_93_obs']?></td></tr>
						<tr><td width="60%">1.2 Está instalado de modo a reduzir o risco de as cargas colidirem com os trabalhadores.</td> <td width="12%"><?=$checklist['check50_94']?></td><td width="28%"><?=$checklist['check50_94_obs']?></td></tr>
						<tr><td width="60%">1.3 Está instalado de modo a reduzir o risco de as cargas  balancearem perigosamente.</td> <td width="12%"><?=$checklist['check50_95']?></td><td width="28%"><?=$checklist['check50_95_obs']?></td></tr>
						<tr><td width="60%">1.4 Está instalado de modo a reduzir o risco de as cargas bascularem.</td> <td width="12%"><?=$checklist['check50_96']?></td><td width="28%"><?=$checklist['check50_96_obs']?></td></tr>
						<tr><td width="60%">1.5 Está instalado de modo a reduzir o risco de as cargas caírem  ou de se soltarem involuntariamente.</td> <td width="12%"><?=$checklist['check50_97']?></td><td width="28%"><?=$checklist['check50_97_obs']?></td></tr>
						
						<tr><td colspan="3"></td></tr>
						<tr><th colspan="3">Art.º 28.º - Sinalização e marcação </th> </tr>
						
						<tr><td width="60%">1 O equipamento ostenta, de forma visível, a indicação da sua carga nominal e, se necessário, uma placa que indique a carga nominal para cada configuração da máquina.</td> <td width="12%"><?=$checklist['check50_98']?></td><td width="28%"><?=$checklist['check50_98_obs']?></td></tr>
						<tr><td width="60%">2 Os acessórios de elevação estão marcados de forma que se possam identificar as características essenciais da sua utilização com segurança.</td> <td width="12%"><?=$checklist['check50_99']?></td><td width="28%"><?=$checklist['check50_99_obs']?></td></tr>
						<tr><td width="60%">3 Não se destinando o equipamento de trabalho á elevação de pessoas, este tem aposta, de forma visível, uma sinalização de proibição adequada.</td> <td width="12%"><?=$checklist['check50_100']?></td><td width="28%"><?=$checklist['check50_100_obs']?></td></tr>
						
						<tr><td colspan="3"></td></tr>
						<tr><th colspan="3">Art.º 29.º - Equipamentos de elevação ou transporte de pessoas </th> </tr>

						<tr><td colspan="3">Sendo o equipamento de trabalho destinado á elevação ou transporte de trabalhadores, este permite: </td> </tr>
						<tr><td width="60%">1.1 Evitar os riscos de queda do habitáculo, se este existir, por meio de dispositivos adequados.</td> <td width="12%"><?=$checklist['check50_101']?></td><td width="28%"><?=$checklist['check50_101_obs']?></td></tr>
						<tr><td width="60%">1.2 Evitar os riscos de queda do utilizador para fora do habitáculo, se este exitir.</td> <td width="12%"><?=$checklist['check50_102']?></td><td width="28%"><?=$checklist['check50_102_obs']?></td></tr>
						<tr><td width="60%">1.3 Evitar os riscos de esmagamento, entalamento ou colisão do utilizador, nomeadamente os devidos a contacto fortuito com objectos.</td> <td width="12%"><?=$checklist['check50_103']?></td><td width="28%"><?=$checklist['check50_103_obs']?></td></tr>
						<tr><td width="60%">1.4 Garantir a segurança dos trabalhadores bloqueados em caso de acidente no habitáculo e possibilitar a sua evacuação com Segurança.</td> <td width="12%"><?=$checklist['check50_104']?></td><td width="28%"><?=$checklist['check50_104_obs']?></td></tr>
						<tr><td width="60%">2 No caso referido no ponto 1.1, se os riscos previstos não puderem ser evitados através de um dispositivo de segurança, deve ser instalado um cabo com um coeficiente de segurança reforçado cujo estado de conservação deve ser verificado todos os dias de trabalho.</td> <td width="12%"><?=$checklist['check50_105']?></td><td width="28%"><?=$checklist['check50_105_obs']?></td></tr>
					</tbody>
				</table>
			</div>
		</div><!--/box dados trabalhador-->
		</div>
</div>
	