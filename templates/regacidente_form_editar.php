﻿<div class="row-fluid">
	<div class="span6 offset3">
		<form action="regacidente.php" method="post" enctype="multipart/form-data" id="editar-regacidente" class="form-horizontal well" >
			<fieldset>
				<legend><?=$title?></legend>
				
				<div class="control-group"> 
					<label class="control-label" for="registo_acidente_data">Data do Acidente</label>
					<div class="controls">
						<input type="date" name="registo_acidente_data" value="<?=$regacidente['data']?>" required/> 
						
					</div> 
				</div>

				<div class="control-group"> 
					<label class="control-label" for="registo_acidente_hora">Hora do Acidente</label> 
					<div class="controls"> 
						<input type="text" name="registo_acidente_hora" value="<?=$regacidente['hora']?>"/>  
					</div> 
				</div>

				<div class="control-group"> 
					<label class="control-label" for="registo_acidente_local">Local do acidente</label> 
					<div class="controls"> 
						<input type="text" name="registo_acidente_local" value="<?=$regacidente['local']?>" />  
					</div> 
				</div>

				<div class="control-group"> 
					<label class="control-label" for="registo_acidente_como">Como ocorreu:</label> 
					<div class="controls"> 
						<input type="text" name="registo_acidente_como" value="<?=$regacidente['como']?>" />  
					</div> 
				</div>

				<div class="control-group"> 
					<label class="control-label" for="registo_acidente_regiao">Regiões do corpo atingidas</label> 
					<div class="controls"> 
						<input type="text" name="registo_acidente_regiao" value="<?=$regacidente['regiao']?>" />  
					</div>
				</div>

				<div class="control-group"> 
				<label class="control-label" for="registo_acidente_lesao">Lesões visíveis</label> 
				<div class="controls"> 
				<input type="text" name="registo_acidente_lesao" value="<?=$regacidente['lesao']?>" />  
				</div> 
				</div>

				<div class="control-group"> 
					<label class="control-label" for="registo_acidente_outras_lesoes">Outras Possíveis Lesões</label> 
					<div class="controls"> 
						<input type="text" name="registo_acidente_outras_lesoes" value="<?=$regacidente['outras_lesoes']?>" />  
					</div> 
				</div>

				<div class="control-group"> 
					<label class="control-label" for="registo_acidente_testemunhas">Testemunhas oculares</label> 
					<div class="controls"> 
					<input type="text" name="registo_acidente_testemunhas" value="<?=$regacidente['testemunhas']?>"
 />  
					</div> 
				</div>

				<div class="control-group"> 
					<label class="control-label" for="registo_acidente_assistencia">Assistência</label> 
					<div class="controls"> 
						<select name="registo_acidente_assistencia">
							<option value="incidente">Incidente</option>
							<option value="assistancia_local">Assistência no local</option>
							<option value="assistencia_hospital">Assistência Hospitalar</option>
						</select>
					</div> 
				</div>

				<div class="control-group"> 
					<label class="control-label" for="registo_acidente_tipo_lesao">Tipo Lesão</label> 
					<div class="controls"> 
						<select name="registo_acidente_tipo_lesao">
							<option value="leve">Leve (s/ baixa)</option>
							<option value="grave">Grave (>1 de baixa)</option>
							<option value="mt_grave">Muito Grave (> 30 dias de baixa)</option>
							<option value="morte">Morte</option>
						</select>
					</div> 
				</div>

				<div class="control-group"> 
					<label class="control-label" for="registo_acidente_participacao">Participação</label> 
					<div class="controls"> 
						
						<select name="registo_acidente_participacao">
							<option value="sim">Sim</option>
							<option value="nao">Não</option>
						</select>
					</div> 
				</div>

				<div class="control-group"> 
					<label class="control-label" for="registo_acidente_incapacidade">Incapacidade</label> 
					<div class="controls"> 
					
						<select name="registo_acidente_incapacidade">
							<option value="nao">Não</option>
							<option value="parcial">Parcial</option>
							<option value="total">Total</option>
						</select>						
					</div> 
				</div>

				<div class="control-group"> 
					<label class="control-label" for="registo_acidente_alta_provisoria">Alta Provisória</label> 
					<div class="controls"> 
						<input type="date" name="registo_acidente_alta_provisoria" value="<?=$regacidente['alta_provisoria']?>" />  
					</div> 
				</div>

				<div class="control-group">
					<label class="control-label" for="registo_acidente_alta_definitiva">Alta Definitiva</label> 
					<div class="controls"> 
						<input type="date" name="registo_acidente_alta_definitiva" value="<?=$regacidente['alta_definitiva']?>" />  
					</div> 
				</div>

				<div class="control-group"> 
					<label class="control-label" for="registo_acidente_causa">Causa da Lesão</label> 
					<div class="controls"> 
						<select name="registo_acidente_causa">
							<option value="queda_pessoas_distintos_niveis">Queda de pessoas a distintos níveis</option>
							<option value="queda_pessoas_mm_nivel">Queda de pessoas ao mesmo nível</option>
							<option value="queda_objecto_desprendidmento">Queda de objectos por desprendimento</option>
							<option value="queda_objectos_por_manipulacao">queda_objectos_por_manipulacao</option>
							<option value="queda_objectos_solos">queda_objectos_solos</option>
							<option value="choque_objectos">choque_objectos</option>
							<option value="golpes_objectos_imoveis">golpes_objectos_imoveis</option>
							<option value="golpes_elementos_moveis_maquina">golpes_elementos_moveis_maquina</option>
							<option value="projeccao_fragmentos_particulas">projeccao_fragmentos_particulas</option>
							<option value="entaladela_objecto">entaladela_objecto</option>
							<option value="aprisionamento_por_tombo">aprisionamento_por_tombo</option>
							<option value="esforcos_excessivos">esforcos_excessivos</option>
							<option value="espoxicao_temp_extremas">espoxicao_temp_extremas</option>
							<option value="contacto_termico">contacto_termico</option>
							<option value="exposicao_corrente_electrica">exposicao_corrente_electrica</option>
							<option value="inalacao_ingestao_subst_nocivas">inalacao_ingestao_subst_nocivas</option>
							<option value="contacto_subst_causticas_nocivas">contacto_subst_causticas_nocivas</option>
							<option value="exposicao_radiacoes">exposicao_radiacoes</option>
							<option value="explosoes">explosoes</option>
							<option value="fogos">fogos</option>
							<option value="acidentes_causados_seres_vivos">acidentes_causados_seres_vivos</option>
							<option value="atropelamento_golpe_choque_veiculos">atropelamento_golpe_choque_veiculos</option>
							<option value="acidente_percurso">acidente_percurso</option>
							<option value="causas_naturais">causas_naturais</option>
							<option value="outros">outros</option>
						</select>	
					</div> 
				</div>

				<div class="control-group"> 
					<label class="control-label" for="registo_acidente_localizacao_lesao">Localização Lesão</label> 
					<div class="controls"> 
						<select data-rel="chosen"  multiple="" name="registo_acidente_localizacao_lesao[]">
							
							<?php foreach ($lista_lesoes as $lesao) { ?>
								<option value="<?=$lesao['lesao_id']?>" <?php if (in_array($lesao['lesao_id'], $localizacoes_lesoes_lista)) echo "selected" ?>> <?=$lesao['lesao']?></option>
							<?php } ?>
						</select>							
					</div>
				</div>
				
				<div class="control-group"> 
					<label class="control-label" for="registo_acidente_agente">Agente</label> 
					<div class="controls"> 
						
						<select name="registo_acidente_agente">
							<option value="maquina">maquina</option>
							<option value="veiculo">veiculo</option>
							<option value="fer_manual">fer_manual</option>
							<option value="chapas">chapas</option>
							<option value="mat_manejado">mat_manejado</option>
							<option value="transp_horizontal">transp_horizontal</option>
							<option value="transportador">transportador</option>
							<option value="guinchos">guinchos</option>
							<option value="ascensores">ascensores</option>
							<option value="edificio">edificio</option>
							<option value="pisos_">pisos_</option>
							<option value="escadas">escadas</option>
							<option value="andaimes">andaimes</option>
							<option value="subs_quimicas">subs_quimicas</option>
							<option value="ap_electricos">ap_electricos</option>
							<option value="caldeiras">caldeiras</option>
							<option value="mat_armazenado">mat_armazenado</option>
							<option value="indeterminado">indeterminado</option>
						</select>	
						
					</div> 
				</div>

				<div class="control-group"> 
					<label class="control-label" for="registo_acidente_acto">Acto</label> 
					<div class="controls"> 
						
						<select name="registo_acidente_acto">
							<option value="actuar_sem_autorizacao">actuar_sem_autorizacao</option>
							<option value="actuar_sem_prevenir">actuar_sem_prevenir</option>
							<option value="trabalhar_equip_movimento">trabalhar_equip_movimento</option>
							<option value="trabalhar_veloc_inseguras">trabalhar_veloc_inseguras</option>
							<option value="uso_inseguro_equipamento">uso_inseguro_equipamento</option>
							<option value="uso_equipamento_defeituoso">uso_equipamento_defeituoso</option>
							<option value="execucao_terefas_forma_insegura">execucao_terefas_forma_insegura</option>
							<option value="tornar_seguranca_inoperante">tornar_seguranca_inoperante</option>
							<option value="nao_usar_epi">nao_usar_epi</option>
							<option value="nao_usar_epi_recomendado">nao_usar_epi_recomendado</option>
							<option value="posicoes_inseguras">posicoes_inseguras</option>
							<option value="distraido">distraido</option>
							<option value="ordem_limpeza_deficiente">ordem_limpeza_deficiente</option>
							<option value="desobedecer_instrucoes">desobedecer_instrucoes</option>
							<option value="incapacidade_fisica_psiquica">incapacidade_fisica_psiquica</option>
							<option value="falta_habilidade">falta_habilidade</option>
							<option value="acto_de_outro">acto_de_outro</option>
							<option value="nao_ha">nao_ha</option>
						</select>	

						
					</div> 
				</div>

				<div class="control-group"> 
					<label class="control-label" for="registo_acidente_descricao">Descrição</label> 
					<div class="controls"> 
						<select name="registo_acidente_descricao">
							<option value="fracturas">fracturas</option>
							<option value="luxacoes">luxacoes</option>
							<option value="entorses_distensoes">entorses_distensoes</option>
							<option value="lombalgias">lombalgias</option>
							<option value="hernias_discais">hernias_discais</option>
							<option value="comocoes">comocoes</option>
							<option value="amputacoes">amputacoes</option>
							<option value="feridas_outras">feridas_outras</option>
							<option value="traumatismos_superficiais">traumatismos_superficiais</option>
							<option value="contusoes_esmagamentos">contusoes_esmagamentos</option>
							<option value="corpo_estranho_no_olho">corpo_estranho_no_olho</option>
							<option value="conjutivites">conjutivites</option>
							<option value="queimaduras">queimaduras</option>
							<option value="envenenamento_agudo_intoxicacao">envenenamento_agudo_intoxicacao</option>
							<option value="exposicao_meio_ambiente">exposicao_meio_ambiente</option>
							<option value="asfixias">asfixias</option>
							<option value="efeitos_nocivos_electricidade">efeitos_nocivos_electricidade</option>
							<option value="efeitos_nocivos_radiacao">efeitos_nocivos_radiacao</option>
							<option value="lesoes_multiplas_naturezas">lesoes_multiplas_naturezas</option>
							<option value="enfartes_derrames_cerebrais">enfartes_derrames_cerebrais</option>
						</select>	

						
					</div> 
				</div>

				<div class="control-group"> 
					<label class="control-label" for="registo_acidente_condicao">Condição</label> 
					<div class="controls"> 
						
						<select name="registo_acidente_condicao">
							<option value="cond_climaterica_desfavoraveis">cond_climaterica_desfavoraveis</option>
							<option value="cond_higiene_deficiente">cond_higiene_deficiente</option>
							<option value="desenho_construcao_insegura">desenho_construcao_insegura</option>
							<option value="ventilacao_insuficiente">ventilacao_insuficiente</option>
							<option value="iluminacao_insuficiente">iluminacao_insuficiente</option>
							<option value="vestuario_insuficiente">vestuario_insuficiente</option>
							<option value="arrumacao_defeituosa">arrumacao_defeituosa</option>
							<option value="ordem_limpeza_deficiente">ordem_limpeza_deficiente</option>
							<option value="ausencia_protec_colectiva_eficaz">ausencia_protec_colectiva_eficaz</option>
							<option value="ausencia_protec_individual_eficaz">ausencia_protec_individual_eficaz</option>
							<option value="inst_equi_mal_concebido">inst_equi_mal_concebido</option>
							<option value="inst_equi_nao_protegido">inst_equi_nao_protegido</option>
							<option value="inst_equi_mal_protegido">inst_equi_mal_protegido</option>
							<option value="inst_equi_inadequado">inst_equi_inadequado</option>
							<option value="inst_equi_defeituoso">inst_equi_defeituoso</option>
							<option value="inst_equi_sinalizacao_deficiente">inst_equi_sinalizacao_deficiente</option>
							<option value="inst_equi_sem_sinalizacao">inst_equi_sem_sinalizacao</option>
							<option value="maquina_mal_concebida">maquina_mal_concebida</option>
							<option value="maquina_nao_protegida">maquina_nao_protegida</option>
							<option value="maquina_mal_protegida">maquina_mal_protegida</option>
							<option value="maquina_inadequada">maquina_inadequada</option>
							<option value="maquina_defeituosa">maquina_defeituosa</option>
							<option value="maquina_sinalizacao_deficiente">maquina_sinalizacao_deficiente</option>
							<option value="maquina_sem_sinalizacao">maquina_sem_sinalizacao</option>
							<option value="metodo_processo_operacao_insegura">metodo_processo_operacao_insegura</option>
							<option value="nao_ha">nao_ha</option>
						</select>	
						
						
					</div> 
				</div>
				
				
				
				<div class="control-group"> 
					<label class="control-label" for="relatorio">Relatório</label> 
					<div class="controls">
						<input type="hidden" name="relatorio_existente" value="<?=$regacidente['relatorio']?>" required/> 
						<input type="file" name="relatorio"/>
					</div>
				</div>

				<div class="control-group">
					<label class="control-label" for="registo_acidente_estado">Estado</label> 
					<div class="controls">
						<select name="registo_acidente_estado"  />
							<option value="aberto" <?php if ($regacidente['estado'] == "aberto") echo "selected" ?> >Aberto</option>
							<option value="fechado" <?php if ($regacidente['estado'] == "fechado") echo "selected" ?> >Fechado</option>
							<option value="em_processamento" <?php if ($regacidente['estado'] == "em_processamento") echo "selected" ?> >Em processamento</option>
						</select>
					</div> 
				</div>

				<input type="hidden" name="regacidente_eid" value="<?=$_SESSION['cur_eid'] ?>">
				<input type="hidden" name="ra_id" value="<?=$regacidente['ra_id']?>">
				<input type="hidden" name="action" value="edit">
				<div class="form-actions">
					<button type="submit" class="btn btn-primary">Gravar alterações</button>
					<a href="#" class="btn" onclick="history.go(-1);">Cancelar</a>
				</div>
								
			</fieldset>
		</form>
	</div>
</div>
