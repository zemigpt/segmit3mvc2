<h2>Delete Post?</h2>
<br/>
<h6><?=$event['event_date'] ?>: <?=$event['event_type_description'] ?></h6>
<h5><?=$event['event_title'] ?></h5>
</br>
<?php foreach ($event["images"] as $image) { ;?>
    <div class="event-image">
        <img src="<?= $image['image_url']?>" alt="<?= $image['image_description']?>" class="image-frame img-polaroid">
    </div>
<?php } ?>  
<div>
    <?=$event['event_description'] ?>
</div>
<br/>
<br/>
<div style="width:150px; margin: 0px auto;">
    <div style="float:left">
        <form action="delete_event.php" method="post" id="deleteevent"> 
            <button type="submit" class="btn btn-danger">Delete</button>    
            <input name="delete_confirmation" value="true" type="hidden">
            <input name="event_id" value="<?=$event['event_id'] ?>" type="hidden">
        </form>
    </div>
    <div style="float:right">
            <a href="/" class="btn">Cancel</a>
    </div>  
    <div style="clear:both"> </div>
</div>



