<div id="novo-ag-vibracao-<?=$nafav?>" class="hide">
	<legend>Adicionar Avaliação Vibração</legend>

	<label style="padding-top: 10px;">Valor registado</label> 
	<input type="text" name="afav_valor_registado[]" />
	
	<label style="padding-top: 10px;">Data de avaliação</label> 
	<input type="text" class="datepicker" name="afav_data_execucao[]" /> 
	
	<label style="padding-top: 10px;">Data validade</label> 
	<input type="text" class="datepicker" name="afav_data_validade[]" />
	
	<label>Relatório de avaliação</label> 
	<input type="file" name="afav_relatorio[]" /> 
	
	<label style="padding-top: 10px;">Entidade Executante</label> 
	<input type="text" name="afav_entidade_executante[]" />
	
	<div>
		<br />
		<a href="#" class="btn" id="cancelar-add-ag-fisico-vibracao-<?=$nafav?>">Cancelar</a> 
		<a href="#" id="adicionar-ag-fisico-vibracao-<?=$nafav?>" class="btn btn-primary">Adicionar</a>
	</div>
</div>
