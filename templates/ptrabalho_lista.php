﻿<div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><a href="#" class="btn-minimize"><i class="icon-chevron-up"></i></a> <i class="icon-user"></i> Lista de Postos de Trabalhos</h2>
			<div class="box-icon">
				<a href="ptrabalho.php?action=add" title="Adicionar Posto Trabalho" class="btn btn-round"><i class="icon-plus"></i></a>
			 
			</div>
		</div>
		<div class="box-content">
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>Foto</th>
						<th>Nome Posto Trabalho</th>
						<th>Acções</th>
					</tr>
				</thead>

				<tbody>
					<?php if (empty($ptrabalhos)) echo '<tr><td colspan="8">Esta empresa não tem postos de trabalho associados</td></tr>'; ?>
						<?php foreach ($ptrabalhos as $ptrabalho): ?>				
						<tr>
							<td><img src="<?= $ptrabalho["foto"]?>" width="75px"/></td>
							<td><?= $ptrabalho["nome"]?></td>
							<td><a class="btn" href="ptrabalho.php?id=<?= $ptrabalho["ptid"]?>"><i class="icon-eye-open" title="Ver detalhes do posto de trabalho"></i></a> <a class="btn" href="ptrabalho.php?action=edit&id=<?= $ptrabalho["ptid"]?>"  title="Editar posto de trabalho"><i class="icon-pencil"></i></a> <a class="btn" href="ptrabalho.php?action=delete&id=<?= $ptrabalho["ptid"]?>"  title="Remover posto trabalho"><i class="icon-trash"></i></a></td>
						</tr>
						<? endforeach ?>
				</tbody>
			</table>
		</div>
	</div><!--/span-->
</div><!--/row-->

