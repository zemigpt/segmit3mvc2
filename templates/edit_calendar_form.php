<!-- jquery script adapted from Lecture 9m -->
<script src="/js/jquery-1.8.2.js"></script>
<script src="/js/jquery-ui-1.10.2.custom.min.js"></script>
<script src="/js/jquery-ui-timepicker-addon.js"></script>
<script src="/js/jquery-ui-sliderAccess.js"></script>
<link rel="stylesheet" href="css/jquery-ui/jquery-ui-1.10.2.custom.css" />
<link rel="stylesheet" href="css/jquery-ui/jquery-ui-timepicker-addon.css" />
        <script>

            // onready
            /*$(document).ready(function() {
                              
                      
                
                // onsubmit
                $('#addevent').submit(function() {

                    // validate form
                    if ($("#eventtypeselect").children("option").filter(":selected").val() == '')
                    {
                        alert('You must choose a type of event to log!');
                        return false;
                    }    
                    if ($('#addevent input[name=event_title]').val() == '' && $('#addevent textarea[name=event_description]').val() =='' )
                    {
                        alert("Event title and event description can't be both blank");
                        return false;
                    }
                   
                    
                    // valid!
                    return true;

                });

            });*/
            
            $(function() {
                $("#datepicker").datetimepicker({ dateFormat: "yy-mm-dd", timeFormat: "HH:mm", addSliderAccess: true,
	sliderAccessArgs: { touchonly: false } });
              });
              
              $(function() {
                $("#datepicker1").datetimepicker({ dateFormat: "yy-mm-dd", timeFormat: "HH:mm", addSliderAccess: true,
	sliderAccessArgs: { touchonly: false } });
              });

        </script>

<h2>Edit Calendar event</h2>
<br/>
<form action="edit_calendar.php" method="post" id="editcalendar" class="form-horizontal">
    <fieldset>
        <div class="control-group">
            <label class="control-label" for="calendar-description">Description</label>
            <div class="controls">
                <textarea id="calendar-description" name="calendar_description" rows="5" placeholder="Description" class="input-large"><?= $calendar["calendar_description"]?></textarea>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="calendar-start-date">Start Date</label>
            <div class="controls">
                <input id="datepicker" name="calendar_start_date" placeholder="Start date" type="text" value="<?= date('Y-m-d H:i', strtotime($calendar['start_date_time']))?>"/>
            </div>
        </div>
         
        <input id="calendar_id" name="calendar_id" type="hidden" value="<?= $calendar['calendar_id']?>"/>
        <input name="referrer" type="hidden" value="<?= $referrer ?>"/>
        
        <div class="control-group">
            <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
    </fieldset>
</form>

