<div id="novo-ag-ilum-<?=$nafai?>" class="hide">
	<legend>Adicionar Avaliação Iluminância</legend>

	<label style="padding-top: 10px;">Valor registado</label> 
	<input type="text" name="afai_valor_registado[]" />
	
	<label style="padding-top: 10px;">Data de avaliação</label> 
	<input type="text" class="datepicker" name="afai_data_execucao[]" /> 
	
	<label style="padding-top: 10px;">Data validade</label> 
	<input type="text" class="datepicker" name="afai_data_validade[]" />
	
	<label>Relatório de avaliação</label> 
	<input type="file" name="afai_relatorio[]" /> 
	
	<label style="padding-top: 10px;">Entidade Executante</label> 
	<input type="text" name="afai_entidade_executante[]" />
	
	<div>
		<br />
		<a href="#" class="btn" id="cancelar-add-ag-fisico-ilum-<?=$nafai?>">Cancelar</a> 
		<a href="#" id="adicionar-ag-fisico-ilum-<?=$nafai?>" class="btn btn-primary">Adicionar</a>
	</div>
</div>
