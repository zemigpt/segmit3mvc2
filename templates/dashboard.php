﻿<div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-user"></i> Lista de empresas disponíveis</h2>
		</div>
		<div class="box-content">
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>Empresa</th>
						<th>Link1</th>
						<th>Link2</th>
					</tr>
				</thead>
				<tbody>
					<?php if (empty($empresas)) echo '<tr><td colspan="8">Este utilizador não tem empresas associadas</td></tr>'; ?>
						<?php foreach ($empresas as $empresa): ?>
						<tr>
							<td><a href="index.php?eid=<?= $empresa["eid"] ?>"><?= $empresa["nome"] ?></a></td>
							<td>link 1</td>
							<td>link 2</td>
						</tr>
						<?php endforeach ?>
				</tbody>
			</table>
		</div>
	</div><!--/span-->
</div><!--/row-->

