﻿
<div class="row-fluid">
	<div class="span6 offset3">
		<form action="adicionar_posto_trabalho.php" method="post" enctype="multipart/form-data" id="adicionar-trabalhador" class="form-horizontal well" >
			<fieldset>
				<legend><?=$title?></legend>
				
				<div class="control-group"> 
					<label class="control-label" for="posto_nome">Nome do Posto de Trabalho</label> 
					<div class="controls"> 
						<input autofocus type="text" name="posto_nome"/>  
					</div> 
				</div>
				<div class="control-group"> 
					<label class="control-label" for="posto_foto">Fotografia</label> 
					<div class="controls"> 
						<input type="file" name="posto_foto"/> 
					</div> 
				</div>
				
				<div class="control-group"> 
					<label class="control-label" for="posto_afrid">Caracterização de agentes físicos: Ruído</label> 
					<div class="controls"> 
						<div class="pull-left">
							<table class="table table-striped" id="tableAgRuido">
								<thead>
									<tr>
										<th>
											Lista de agentes físicos: Ruído
										</th>
									</tr>
								</thead>
							</table>
						</div>						
						<div class="box-icon pull-left">
							<a href="#" role="button" title="Adicionar Avaliação Ruído" id="add-ruido-btn" class="btn btn-round" data-toggle="modal"><i class="icon-plus"></i></a>
						</div>
						
						<div class="clear"></div>
						<div id="lista-ag-fis-ruido">
							<table class="table table-bordered">
								
							</table>
						</div>
						
						<div id="novo-ag-ruido-container" class="hide" style="background-color: rgba(158, 195, 255, 0.251); padding: 15px;">
									
						</div>
						
					</div>
				</div>
				
				<div class="control-group"> 
					<label class="control-label" for="posto_afiid">Caracterização de agentes físicos: Iluminância</label> 
					<div class="controls"> 
						<div class="pull-left">
							<table class="table table-striped" id="tableAgIlum">
								<thead>
									<tr>
										<th>
											Lista de agentes físicos: Iluminância
										</th>
									</tr>
								</thead>
							</table>
						</div>
						<div class="box-icon pull-left">
							<a href="#" role="button" title="Adicionar Avaliação Iluminância" id="add-ilum-btn" class="btn btn-round" data-toggle="modal"><i class="icon-plus"></i></a>
						</div>
						
						<div class="clear"></div>
						<div id="lista-ag-fis-ilum">
							<table class="table table-bordered">
								
							</table>
						</div>
						
						<div id="novo-ag-ilum-container" class="hide" style="background-color: rgba(158, 195, 255, 0.251); padding: 15px;">
									
						</div>
						
						
					</div> 
				</div>
				
				<div class="control-group"> 
					<label class="control-label" for="posto_afatid">Caracterização de agentes físicos: Ambiente Térmico</label> 
					<div class="controls"> 
						<div class="pull-left">
							<table class="table table-striped" id="tableAgTermico">
								<thead>
									<tr>
										<th>
											Lista de agentes físicos: Amb. Térmico
										</th>
									</tr>
								</thead>
							</table>
						</div>
						<div class="box-icon pull-left">
							<a href="#" role="button" title="Adicionar Avaliação Amb. Térmico" id="add-termico-btn" class="btn btn-round" data-toggle="modal"><i class="icon-plus"></i></a>
						</div>
						
						<div class="clear"></div>
						<div id="lista-ag-fis-termico">
							<table class="table table-bordered">
								
							</table>
						</div>
						
						<div id="novo-ag-termico-container" class="hide" style="background-color: rgba(158, 195, 255, 0.251); padding: 15px;">
									
						</div>
						
						
					</div> 
				</div>				

				
				<div class="control-group"> 
					<label class="control-label" for="posto_afavid">Caracterização de agentes físicos: Vibrações</label> 
					<div class="controls"> 
						<div class="pull-left">
							<table class="table table-striped" id="tableAgVibracoes">
								<thead>
									<tr>
										<th>
											Lista de agentes físicos: Vibrações
										</th>
									</tr>
								</thead>
							</table>
						</div>
						<div class="box-icon pull-left">
							<a href="#" role="button" title="Adicionar Avaliação Vibrações" id="add-vibracao-btn" class="btn btn-round" data-toggle="modal"><i class="icon-plus"></i></a>
						</div>
						
						<div class="clear"></div>
						<div id="lista-ag-fis-vibracao">
							<table class="table table-bordered">
								
							</table>
						</div>
						
						<div id="novo-ag-vibracao-container" class="hide" style="background-color: rgba(158, 195, 255, 0.251); padding: 15px;">
									
						</div>
						
						
					</div> 
				</div>	
				
				
				<div class="control-group"> 
					<label class="control-label" for="posto_aqid">Caracterização de agentes químicos</label> 
					<div class="controls"> 
						<input type="text" name="posto_aqid"/>  
					</div> 
				</div>
				
				<div class="control-group"> 
					<label class="control-label" for="posto_abid">Caracterização de agentes biológicos</label> 
					<div class="controls"> 
						<input type="text" name="posto_abid"/>  
					</div> 
				</div>
				
				<div class="control-group"> 
					<label class="control-label" for="posto_avriscoid">Avaliação de Riscos do Posto de Trabalho</label>
					<div class="controls"> 
						<input type="text" name="posto_avriscoid"/> 
					</div> 
				</div>
	
				
				<input type="hidden" name="posto-trabalho_eid" value="<?=$eid?>">
				<input type="hidden" name="posto-trabalho_tid" value="<?=$tid?>">
				
				<div class="form-actions">
					<button type="submit" class="btn btn-primary">Adicionar Posto de Trabalho</button>
				</div>
								
			</fieldset>
		</form>
	</div>
</div>

<!-- Modal -->
<div id="myModal_afar" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">Adicionar Avaliação de Ruído</h3>
	</div>
	<div class="modal-body">
		<p>&nbsp;</p>
		<form action="adicionar_ag_fisico_ruido.php" method="post" id="adicionar-ag-fisico-ruido" class="form-horizontal" >
			<fieldset>
				<div class="control-group"> 
					<label class="control-label" for="ag_fisico_aval_ruido_registo">Nova Avaliação de Ruído</label> 
					<div class="controls"> 
						<input type="file" name="ag_fisico_aval_ruido_registo"/>
					</div>
				</div>
				
				<div class="control-group"> 
					<label class="control-label" for="ag_fisico_aval_ruido_data">Data</label> 
					<div class="controls"> 
						<input type="date" name="ag_fisico_aval_ruido_data"/>  
					</div> 
				</div>
				
				<div class="control-group"> 
					<label class="control-label" for="ag_fisico_aval_ruido_intervalo">Intervalo</label> 
					<div class="controls"> 
						<input type="text" name="ag_fisico_aval_ruido_intervalo"/>  
					</div> 
				</div>


				
				
				<input type="hidden" name="ajax" value="true">
			</fieldset>
		</form>
	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
		<button id="modal-adicionar-ag-fisico-ruido-submit" type="submit" class="btn btn-primary">Adicionar</button>
	</div>
</div>

