﻿<div class="row-fluid">	
	<div class="span2">
		<div class="box span12">
			<div class="box-header well" data-original-title>
				
			</div>
			<div>
			<ul class="nav nav-tabs nav-stacked">
				<li>
					<a href="#dadosfor">Dados da formação</a>
				</li>
			
			</ul>
			</div>
		</div>
	</div>
	<div class="span10">	
		<div class="row-fluid">	
		<div class="box span12">
			<div class="box-header well" data-original-title>
				<a id="dadosfor"></a><h2><i class="icon-user"></i>Dados da formação</h2>
				<div class="box-icon">
					<a href="formacao.php?id=<?=$for_id?>&action=edit" title="Editar Formação" class="btn btn-round"><i class="icon-pencil"></i></a>
					<a href="formacao.php?id=<?=$for_id?>&action=delete" title="Remover Formação" class="btn btn-round"><i class="icon-remove"></i></a>
					<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">
				<table class="table table-striped">


					<thead>
						<tr>
							<th colspan='2'>
							<?=$formacao['tema']?></p></th>
						</tr>
					</thead>

					<tbody>					
							<tr>
								<th width="33%">Duração</th>
								<td width="67%"><?=$formacao['duracao']?></td>
							</tr>
							<tr>
								<th width="33%">Relatório</th>
								<td width="67%"><?=$formacao['relatorio']?></td>
							</tr>
							<tr>
								<th>Entidade Formadora</th>
								<td><?=$formacao['entidade_formadora']?></td>
							</tr>
							
					</tbody>
				</table>
			</div>
		</div><!--/box dados trabalhador-->
		</div>

	