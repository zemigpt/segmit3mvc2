<div class="row-fluid">
	<div class="span6">
		<form action="maquina.php" method="post" enctype="multipart/form-data" id="editar-maquina" class="form-horizontal well" >
			<fieldset>
				<legend><?=$title?></legend>
				<div class="control-group"> 
					<label class="control-label" for="maq_nome">Nome da Máquina</label> 
					<div class="controls"> 
						<input type="text" name="maq_nome" value="<?= $maquina['nome'] ?>"/>  
					</div>
				</div>
				<div class="control-group"> 
					<label class="control-label" for="maq_tipo_maquina">Tipo Maquina</label> 
					<div class="controls"> 
						<div class="pull-left">
							<select name="maq_tipo_maquina" id="sel_tipo_maq">
								<option value="0">- tipo de máquina -</option>
								<?php foreach ($tipo_maquinas as $tipo) { ?>
									<option value="<?= $tipo['tm_id'] ?>" <?php if ($maquina['tm_id'] == $tipo['tm_id']) echo "selected"?>><?= $tipo['nome'] ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="box-icon pull-left">
							<a href="#myModalNovoTipoMaquina" role="button" title="Adicionar Tipo de Máquina" class="btn btn-round" data-toggle="modal"><i class="icon-plus"></i></a>
						</div>
					</div>
					
				</div>
				<div class="control-group"> 
					<label class="control-label" for="maq_fabricante">Fabricante</label> 
					<div class="controls">
						<div class="pull-left">
							<select name="maq_fabricante" id="sel_fabricante" >
								<option value="0">- fabricante -</option>
								<?php foreach ($fabricantes as $fabricante) { ?>
									<option value="<?= $fabricante['fid'] ?>" <?php if ($maquina['fid'] == $fabricante['fid']) echo "selected"?>><?= $fabricante['nome'] ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="box-icon pull-left">
							<a href="#myModalNovoFabricante" role="button" title="Adicionar Fabricante" class="btn btn-round" data-toggle="modal"><i class="icon-plus"></i></a>
						</div> 
					</div>
				</div>
				<div class="control-group"> 
					<label class="control-label" for="maq_num_serie">Número de Série</label> 
					<div class="controls"> 
						<input type="text" name="maq_num_serie" value="<?= $maquina['num_serie'] ?>"/> 
					</div> 
				</div>
				<div class="control-group">
					<label class="control-label" for="maq_foto">Fotografia</label>
					<div class="controls">
						<img src="<?= $maquina['foto'] ?>" height="100px"><br/>
						<input type="file" name="maq_foto"/>
						<input type="hidden" name="maq_foto_existente" value="<?=$maquina['foto']?>">
					</div>
				</div>
				<div class="control-group"> 
					<label class="control-label" for="maq_data_compra">Data Compra</label> 
					<div class="controls"> 
						<input type="text" class="datepicker" name="maq_data_compra"  value="<?= $maquina['data_compra'] ?>"/>  
					</div> 
				</div>
				<div class="control-group"> 
					<label class="control-label" for="maq_data_aprov_seguranca">Data Aprovação Seguranca</label> 
					<div class="controls">
						<input type="text" class="datepicker" name="maq_data_aprov_seguranca" value="<?= $maquina['data_aprov_seguranca'] ?>"/> 
					</div> 
				</div>
				<div class="control-group"> 
					<label class="control-label" for="maq_manual_instrucao">Manual Instrução</label>
					<div class="controls"> 
						<input type="file" name="maq_manual_instrucao"/>
					</div> 
				</div>
				<div class="control-group"> 
					<label class="control-label" for="maq_instrucao_trabalho">Instrução Trabalho</label>
					<div class="controls"> 
						<input type="file" name="maq_instrucao_trabalho"/> 
					</div> 
				</div>
				
				<div class="control-group"> 
					<label class="control-label" for="maq_declaracao_CE">Declaracao_Ce</label> 
					<div class="controls">
						<input type="hidden" name="maq_declaracao_CE_existente" value="<?=$maquina['declaracao_CE']?>">
						<input type="file" name="maq_declaracao_CE"/>  
					</div> 
				</div>

				<div class="control-group"> 
				<label class="control-label" for="maq_declaracao_DL50">Declaracao_Dl50</label> 
					<div class="controls"> 
						<input type="hidden" name="maq_declaracao_DL50_existente" value="<?=$maquina['declaracao_DL50']?>">
						<input type="file" name="maq_declaracao_DL50"/> 
					</div> 
				</div>

				
				
				<input type="hidden" name="maq_id" value="<?=$mid?>">
				<!--<input type="hidden" name="pt_id" value="<?=$ptid?>"> -->
				<input type="hidden" name="action" value="edit">
				
				<div class="form-actions">
					<button type="submit" class="btn btn-primary">Gravar alterações</button>
					<a href="#" class="btn" onclick="history.go(-1);">Cancelar</a>
				</div>
								
			</fieldset>
		</form>
	</div>
</div>


<!-- Modal Novo Tipo Máquina-->
<div id="myModalNovoTipoMaquina" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">Adicionar Tipo de Máquina</h3>
	</div>
	<div class="modal-body">
		<p>&nbsp;</p>
		<form action="adicionar_tipo_maquina.php" method="post" id="adicionar-tipo-maquina" class="form-horizontal" >
			<fieldset>
				<div class="control-group"> 
					<label class="control-label" for="tipo_maq_nome">Novo tipo de Máquina</label> 
					<div class="controls"> 
						<input type="text" name="tipo_maq_nome"/>
					</div>
				</div>
				<input type="hidden" name="ajax" value="true">
			</fieldset>
		</form>
	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
		<button id="adicionar-tipo-maquina-submit" type="submit" class="btn btn-primary">Adicionar</button>
	</div>
</div>


<!-- Modal Fabricantes-->
<div id="myModalNovoFabricante" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">Adicionar Fabricante</h3>
	</div>
	<div class="modal-body">
		<p>&nbsp;</p>
		<form action="adicionar_fabricante.php" method="post" id="adicionar-fabricante" class="form-horizontal" >
			<fieldset>
				<div class="control-group"> 
					<label class="control-label" for="fabricante_nome">Novo Fabricante</label> 
					<div class="controls"> 
						<input type="text" name="fabricante_nome"/>
					</div>
				</div>
				<input type="hidden" name="ajax" value="true">
			</fieldset>
		</form>
	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
		<button id="adicionar-fabricante-submit" type="submit" class="btn btn-primary">Adicionar</button>
	</div>
</div>