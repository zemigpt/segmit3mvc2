<div id="novo-ag-termico-<?=$nafat?>" class="hide">
	<legend>Adicionar Avaliação Amb. Térmico</legend>

	<label style="padding-top: 10px;">Valor registado</label> 
	<input type="text" name="afat_valor_registado[]" />
	
	<label style="padding-top: 10px;">Data de avaliação</label> 
	<input type="text" class="datepicker" name="afat_data_execucao[]" /> 
	
	<label style="padding-top: 10px;">Data validade</label> 
	<input type="text" class="datepicker" name="afat_data_validade[]" />
	
	<label>Relatório de avaliação</label> 
	<input type="file" name="afat_relatorio[]" /> 
	
	<label style="padding-top: 10px;">Entidade Executante</label> 
	<input type="text" name="afat_entidade_executante[]" />
	
	<div>
		<br />
		<a href="#" class="btn" id="cancelar-add-ag-fisico-termico-<?=$nafat?>">Cancelar</a> 
		<a href="#" id="adicionar-ag-fisico-termico-<?=$nafat?>" class="btn btn-primary">Adicionar</a>
	</div>
</div>
