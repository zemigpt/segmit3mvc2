﻿<div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><a href="#" class="btn-minimize"><i class="icon-chevron-up"></i></a> <i class="icon-user"></i> Lista de Postos de Trabalhos</h2>
			<div class="box-icon">
				<a href="adicionar_trabalhador.php?eid=<?=$_SESSION['cur_eid']?>" title="Adicionar Posto de Trabalho" class="btn btn-round"><i class="icon-plus"></i></a>
			 
			</div>
		</div>
		<div class="box-content">
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>Foto</th>
						<th>Nome Posto de Trabalho</th>
						<th>Acções</th>
					</tr>
				</thead>

				<tbody>
					<?php if (empty($ptrabalhos)) echo '<tr><td colspan="8">Esta empresa não tem postos de trabalho associados</td></tr>'; ?>
						<?php foreach ($ptrabalhos as $ptrabalho): ?>				
						<tr>
							<td><img src="<?= $ptrabalho["foto"]?>" width="75px"/></td>
							<td><?= $ptrabalho["nome"]?></td>
							<td><a class="btn" href="index.php?ptid=<?= $ptrabalho["ptid"]?>"><i class="icon-eye-open" title="Ver detalhes do posto de trabalho"></i></a> <a class="btn" href="#"  title="Editar posto de trabalho"><i class="icon-pencil"></i></a></td>
						</tr>
						<? endforeach ?>
				</tbody>
			</table>
		</div>
	</div><!--/span-->
</div><!--/row-->

