﻿<div class="row-fluid">
	<div class="span6 offset3">
		<form action="tmaquina.php" method="post" enctype="multipart/form-data" id="adicionar-tmaquina" class="form-horizontal well" >
			<fieldset>
				<legend><?=$title?></legend>
				
				<div class="control-group"> 
					<label class="control-label" for="tmaquina_nome">Nome</label> 
					<div class="controls"> 
						<input type="text" name="tmaquina_nome"/>  
					</div> 
				</div>

				<input type="hidden" name="tm_eid" value="<?=$eid?>">
				<input type="hidden" name="action" value="add">
				
				<div class="form-actions">
					<button type="submit" class="btn btn-primary">Adicionar Tipo de Máquina</button>
					<a href="#" class="btn" onclick="history.go(-1);">Cancelar</a>
				</div>
								
			</fieldset>
		</form>
	</div>
</div>
