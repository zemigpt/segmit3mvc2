<?php if(!isset($show_menus) || $show_menus)	{ ?>
<!-- topbar starts -->
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand" href="index.php"> <span>Segmit3</span></a>
				
				<!-- user dropdown starts -->
				<div class="btn-group pull-right" >
					<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
						<i class="icon-user"></i><span class="hidden-phone"> <?=$_SESSION["username"]?></span>
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<li><a href="#">Profile</a></li>
						<li class="divider"></li>
						<li><a href="logout.php">Logout</a></li>
					</ul>
				</div>
				<!-- user dropdown ends -->
				
				<div class="top-nav nav-collapse">
					<ul class="nav">
						<li>
							<form class="navbar-search pull-left">
								<input placeholder="Search" class="search-query span2" name="query" type="text">
							</form>
						</li>
					</ul>
				</div><!--/.nav-collapse -->
			</div>
		</div>
	</div>
	<!-- topbar ends -->
	<?php } ?>

	<div class="container-fluid">
		<div class="row-fluid">
		<?php if(!isset($show_menu) || !$show_menu) { ?>
		
			<!-- left menu starts -->
			<div class="span2 main-menu-span">
				<div class="well nav-collapse sidebar-nav">
					<ul class="nav nav-tabs nav-stacked main-menu">
						<li class="nav-header hidden-tablet">Main</li>
						<li><a class="ajax-link" href="index.php"><i class="icon-home"></i><span class="hidden-tablet"> Início</span></a></li>
						<li class="nav-header hidden-tablet"> </li>
						<li><a class="ajax-link" href="trabalhador.php"><i class="icon-user"></i><span class="hidden-tablet"> Trabalhadores</span></a></li>
						<li><a class="ajax-link" href="trabalhador.php?action=add"><i class="icon-plus-sign"></i><span class="hidden-tablet"> Adic. Trabalhador</span></a></li>
						<li class="nav-header hidden-tablet"> </li>
						<li><a class="ajax-link" href="ptrabalho.php"><i class="icon-list"></i><span class="hidden-tablet"> Posto de Trabalho</span></a></li>
						<li><a class="ajax-link" href="ptrabalho.php?action=add"><i class="icon-plus-sign"></i><span class="hidden-tablet"> Adic. Posto Trabalho</span></a></li>
						<li class="nav-header hidden-tablet"> </li>
						<li><a class="ajax-link" href="tmaquina.php?tm_id=0"><i class="icon-tasks"></i><span class="hidden-tablet"> Tipo Máquinas</span></a></li>
						<li><a class="ajax-link" href="tmaquina.php?action=add"><i class="icon-plus-sign"></i><span class="hidden-tablet"> Adic. Tipo Máquina</span></a></li>
						<li class="nav-header hidden-tablet"> </li>
						<li><a class="ajax-link" href="maquina.php"><i class="icon-tasks"></i><span class="hidden-tablet"> Máquinas</span></a></li>
						<li><a class="ajax-link" href="maquina.php?action=add"><i class="icon-plus-sign"></i><span class="hidden-tablet"> Adic. Máquina</span></a></li>
						<li class="nav-header hidden-tablet"> </li>
						<li><a class="ajax-link" href="formacao.php"><i class="icon-briefcase"></i><span class="hidden-tablet"> Formações</span></a></li>
						<li><a class="ajax-link" href="regacidente.php"><i class="icon-fire"></i><span class="hidden-tablet"> Registo Acidentes</span></a></li>
						<li><a class="ajax-link" href="regacidente.php?action=add"><i class="icon-plus-sign"></i><span class="hidden-tablet"> Adic. Reg. Acidente</span></a></li>
						<li class="nav-header hidden-tablet"> </li>
						<li><a class="ajax-link" href="avalrisco.php"><i class="icon-briefcase"></i><span class="hidden-tablet"> Avaliações Risco</span></a></li>
						<li><a class="ajax-link" href="avalrisco.php?action=add"><i class="icon-plus-sign"></i><span class="hidden-tablet"> Adic. Avaliações Risco</span></a></li>
						
					</ul>
				</div><!--/.well -->
			</div><!--/span-->
			<!-- left menu ends -->
			<?php } ?>
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<div id="content" class="span10">
			
			
			<!-- breadcrumb starts -->
			<div>
				<ul class="breadcrumb">
					<li>
						<a href="index.php">Home</a> 
					</li>
					<?php if (isset($breadcrumbs)) { ?>
						<?php foreach ($breadcrumbs as $breadcrumb) { ?>
						<li>
							<span class="divider">/</span> <a href="<?=$breadcrumb["link"]?>"><?=$breadcrumb["name"]?></a>
						</li>
						<?php } ?>
					<?php } ?>
				</ul>
			</div>
			<!-- breadcrumb ends-->
			
			<!-- content starts -->
			