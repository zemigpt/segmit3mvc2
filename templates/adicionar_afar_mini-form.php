<div id="novo-ag-ruido-<?=$nafar?>" class="hide">
	<legend>Adicionar Avaliação de Ruído</legend>

	<label style="padding-top: 10px;">Valor registado</label> 
	<input type="text" name="afar_valor_registado[]" />
	
	<label style="padding-top: 10px;">Data de avaliação</label> 
	<input type="text" class="datepicker" name="afar_data_execucao[]" /> 
	
	<label style="padding-top: 10px;">Data validade</label> 
	<input type="text" class="datepicker" name="afar_data_validade[]" />
	
	<label>Relatório de avaliação</label> 
	<input type="file" name="afar_relatorio[]" /> 
	
	<label style="padding-top: 10px;">Entidade Executante</label> 
	<input type="text" name="afar_entidade_executante[]" />
	
	<div>
		<br />
		<a href="#" class="btn" id="cancelar-add-ag-fisico-ruido-<?=$nafar?>">Cancelar</a> 
		<a href="#" id="adicionar-ag-fisico-ruido-<?=$nafar?>" class="btn btn-primary">Adicionar</a>
	</div>
</div>
