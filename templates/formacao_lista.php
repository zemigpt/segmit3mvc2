﻿<div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><a href="#" class="btn-minimize"><i class="icon-chevron-up"></i></a> <i class="icon-user"></i> Lista de Formações</h2>
			<div class="box-icon">
				<a href="formacao.php?action=add" title="Adicionar Formação" class="btn btn-round"><i class="icon-plus"></i></a>
			 
			</div>
		</div>
		<div class="box-content">
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>Nome Formação</th>
						<th>Nº horas</th>
						<th>Opções</th>
				
					</tr>
				</thead>

				<tbody>
					<?php if (empty($formacoes)) echo '<tr><td colspan="8">Esta empresa não tem formacoes associadas</td></tr>'; ?>
						<?php foreach ($formacoes as $formacao): ?>				
						<tr>
							<td><?= $formacao['tema']?></td>
							<td><?= $formacao["duracao"]?></td>
							<td><a class="btn" href="formacao.php?id=<?= $formacao["for_id"]?>"><i class="icon-eye-open" title="Ver detalhes da formação"></i></a>
							<a class="btn" href="formacao.php?action=edit&id=<?= $formacao["for_id"]?>"  title="Editar formação"><i class="icon-pencil"></i></a> 
							<a class="btn" href="formacao.php?action=delete&id=<?= $formacao["for_id"]?>"  title="Remover formação"><i class="icon-trash"></i></a></td>
						</tr>
						<? endforeach ?>
				</tbody>
			</table>
		</div>
	</div><!--/span-->
</div><!--/row-->

