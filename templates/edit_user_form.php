<!-- jquery script adapted from Lecture 9m -->
<script src="/js/jquery-1.8.2.js"></script>
<script>

    // onready
    $(document).ready(function() {

        // onsubmit
        $('#registration').submit(function() {

            // validate email
            if ($('#registration input[name=username]').val() == '')
            {
                alert('You must provide a username!');
                return false;
            }

            // validate password
            else if ($('#registration input[name=password]').val() == '')
            {
                alert('You must provide a password!');
                return false;
            }

            // validate confirmation
            else if ($('#registration input[name=password]').val() != $('#registration input[name=confirmation]').val()) 
            {
                alert('Passwords do not match!');
                return false;
            }
            
            // validate gender
            else if ($('#registration input[name=gender]').val() ==''()) 
            {
                alert('You must fill gender info!');
                return false;
            }
            
            // validate email
            else if ($('#registration input[name=email]').val() ==''()) 
            {
                alert('You must provide an email!');
                return false;
            }
            
            // valid!
            return true;

        });

    });

</script>

<h2>Register</h2>
<br/>
<form action="edit_user.php" method="post" id="edit-user" class="form-horizontal">
    <fieldset>
        <div class="control-group">
            <label class="control-label" for="username">Username</label>
            <input autofocus name="username" placeholder="Username" type="text" value="<?= $user['username'] ?>"/>
        </div>
        <div class="control-group">
            <label class="control-label" for="password">Password</label>
            <input name="password" placeholder="Password" type="password" value="<?= $user['password'] ?>"/>
        </div>
        <div class="control-group">
            <label class="control-label" for="confirmation">Password Confirmation</label>
            <input name="confirmation" placeholder="Password Confirmation" type="password" value="<?= $user['password'] ?>"/>
        </div>
        <div class="control-group">
            <label class="control-label" for="email">E-mail</label>
            <input name="email" placeholder="E-mail" type="text" value="<?= $user['email'] ?>"/>
        </div>
      
        <div class="control-group">
            <label class="control-label" for="gender">Gender</label>
            <label class="radio inline"> 
                <input type="radio" name="gender" id="genderfemale" value="F" <?= ($user['gender']=="F" ? "checked" : "") ?>>Female
            </label>
            <label class="radio inline">
                <input type="radio" name="gender" id="gendermale" value="M" <?= ($user['gender']=="M" ? "checked" : "") ?>>Male
            </label>
        </div>
        
        <div class="control-group">
            <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
    </fieldset>
</form>


