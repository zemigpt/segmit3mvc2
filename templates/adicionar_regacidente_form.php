﻿
<div class="row-fluid">
	<div class="span6 offset3">
		<form action="adicionar_regacidente.php" method="post" enctype="multipart/form-data" id="adicionar-formacao" class="form-horizontal well" >
			<fieldset>
				<legend><?=$title?></legend>
				
				<div class="control-group"> 
					<label class="control-label" for="registo_acidente_data">Data</label>
					<div class="controls">
						<input type="date" name="registo_acidente_data"/>  
					</div> 
				</div>

				<div class="control-group"> 
					<label class="control-label" for="relatorio">Relatorio</label> 
					<div class="controls">
						<input type="file" name="relatorio"/> 
					</div>
				</div>

				<div class="control-group">
					<label class="control-label" for="registo_acidente_estado">Estado</label> 
					<div class="controls">
						<select name="registo_acidente_estado">
							<option value="aberto">Aberto</option>
							<option value="fechado">Fechado</option>
							<option value="em processamento">Em processamento</option>
						</select>
					</div> 
				</div>


								
				<input type="hidden" name="registo_acidente__eid" value="<?=$eid?>">
								
				<div class="form-actions">
					<button type="submit" class="btn btn-primary">Adicionar Registo Acidente</button>
				</div>
								
			</fieldset>
		</form>
	</div>
</div>

