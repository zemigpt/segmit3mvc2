﻿<div class="row-fluid">
	<div class="span6 offset3">
		<form action="formacao.php" method="post" enctype="multipart/form-data" id="editar-formacao" class="form-horizontal well" >
			<fieldset>
				<legend><?=$title?></legend>
				
				<div class="control-group"> 
					<label class="control-label" for="formacao_tema">Tema</label> 
					<div class="controls"> 
						<input type="text" name="formacao_tema" value="<?=$formacao['tema']?>"/>  
					</div> 
				</div>

				<div class="control-group"> 
					<label class="control-label" for="formacao_duracao">Duracao</label>
					<div class="controls"> 
						<input type="text" name="formacao_duracao" value="<?=$formacao['duracao']?>"/>  
					</div> 
				</div>

				<div class="control-group"> 
					<label class="control-label" for="formacao_entidade_formadora">Entidade Formadora</label>
					<div class="controls"> 
						<input type="text" name="formacao_entidade_formadora" value="<?=$formacao['entidade_formadora']?>"/>
					</div> 
				</div>
				
				<div class="control-group"> 
					<label class="control-label" for="formacao_relatorio">Relatorio</label> 
					<div class="controls"> 
						<input type="hidden" name="formacao_relatorio" value="<?=$formacao['relatorio']?>"/> 
						<input type="file" name="formacao_relatorio"/>						
					</div> 
				</div>

				<input type="hidden" name="formacao_eid" value="<?=$_SESSION['cur_eid'] ?>">
				<input type="hidden" name="for_id" value="<?=$formacao['for_id']?>">
				<input type="hidden" name="action" value="edit">
				<div class="form-actions">
					<button type="submit" class="btn btn-primary">Gravar alterações</button>
					<a href="#" class="btn" onclick="history.go(-1);">Cancelar</a>
				</div>
								
			</fieldset>
		</form>
	</div>
</div>
