﻿<script src="js/jquery-ui-1.10.2.custom.min.js"></script>
<link rel="stylesheet" href="css/jquery-ui/jquery-ui-1.10.2.custom.css" />

  <script>
  $(function() {
    $( "#datepicker" ).datepicker({ dateFormat: "yy-mm-dd" });
  });
  </script>

<div class="row-fluid">
	<div class="span6 offset3">
		<form action="adicionar_revisao.php" method="post" enctype="multipart/form-data" id="adicionar-revisao" class="form-horizontal well" >
			<fieldset>
				<legend><?=$title?></legend>
								
				<div class="control-group"> 
					<label class="control-label" for="maq_revisao_data_revisao">Data Revisão</label> 
					<div class="controls"> 
						<input autofocus type="date" name="maq_revisao_data_revisao"/> 
					</div>
				</div>

				<div class="control-group"> 
					<label class="control-label" for="maq_revisao_relatorio_revisao">Relatorio Revisão</label>
					<div class="controls"> 
						<input type="file" name="maq_revisao_relatorio_revisao"/> 
					</div> 
				</div>

				<div class="control-group">
					<label class="control-label" for="maq_revisao_data_prox_revisao">Data Próxima Revisão</label>
					<div class="controls"> 
						<input type="date" name="maq_revisao_data_prox_revisao"/> 
					</div> 
				</div>

				
				<input type="hidden" name="maquina-revisao_eid" value="<?=$eid?>">
				
				<div class="form-actions">
					<button type="submit" class="btn btn-primary">Adicionar Revisão de Máquina</button>
				</div>
								
			</fieldset>
		</form>
	</div>
</div>
