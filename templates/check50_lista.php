﻿<div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><a href="#" class="btn-minimize"><i class="icon-chevron-up"></i></a> <i class="icon-user"></i> Lista de Checklist</h2>
			<div class="box-icon">
				<!-- <a href="check50.php?action=add" title="Adicionar Máquina" class="btn btn-round"><i class="icon-plus"></i></a> -->
			 
			</div>
		</div>
		<div class="box-content">
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>Máquina</th>
						<th>Acções</th>
					</tr>
				</thead>

				<tbody>
					<?php if (empty($checklists)) echo '<tr><td colspan="8">Esta empresa não tem checklists DL 50/2005 associadas</td></tr>'; ?>
						<?php foreach ($checklists as $checklist): ?>				
						<tr>
							<td><?= $checklist["mid"]?></td>
							<td><a class="btn" href="check50.php?id=<?= $checklist["check50_id"]?>"><i class="icon-eye-open" title="Ver detalhes da checklist"></i></a>
							<a class="btn" href="check50.php?action=edit&id=<?= $checklist["check50_id"]?>"  title="Editar Checklist"><i class="icon-pencil"></i></a>
							<a class="btn" href="check50.php?action=delete&id=<?= $checklist["check50_id"]?>"  title="Remover Checklist"><i class="icon-trash"></i></a></td>
						</tr>
						<? endforeach ?>
				</tbody>
			</table>
		</div>
	</div><!--/span-->
</div><!--/row-->

