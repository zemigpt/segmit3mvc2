﻿<style>
.grave {
	background-color: #FF9999;
}

.elevado {
	background-color: #FFB299;
}

.notavel {
	background-color: #FFCC99;
}

.moderado {
	background-color: #FFFFBF;
}

.aceitavel {
	background-color: #B2FF99;
}
</style>
<div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><a href="#" class="btn-minimize"><i class="icon-chevron-up"></i></a> <i class="icon-user"></i> Lista de Avaliações de Risco - Método William Fine</h2>
			<div class="box-icon">
				<a href="avalrisco.php?action=add" title="Adicionar Avaliação de Risco" class="btn btn-round"><i class="icon-plus"></i></a>
			 
			</div>
		</div>
		<div class="box-content">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>Posto Trabalho</th>
						<th>Data</th>
						<th>Tarefa</th>
						<th>Perigo</th>
						<th>Risco</th>
						<th>Grau Perigosidade</th>
						<th>Indice Justificação</th>
						<th>Medidas</th>
						<th>Responsável</th>
						<th>Data Prevista Resolução</th>
						<th>Acções</th>
					</tr>
				</thead>

				<tbody>
					<?php if (empty($avalriscos)) echo '<tr><td colspan="11">Esta empresa não tem avaliações de risco associadas.</td></tr>'; ?>
						<?php foreach ($avalriscos as $avalrisco): ?>				
						<tr class="<?=$avalrisco["gp_class"]?>">
							<td>
								<?=$avalrisco["ptnome"]?>
							</td>
							<td>
								<?=$avalrisco["data"]?>
							</td>
							<td>
								<?=$avalrisco["tarefa"]?>
							</td>
							<td>
								<?=$avalrisco["perigo"]?>
							</td>
							<td>
								<?=$avalrisco["risco"]?>
							</td>
							<td>
								<?=$avalrisco["grau_perigosidade"]?> - <?=$avalrisco["gp_texto"]?>
							</td>
							<td>
								<?=$avalrisco["indice_justificacao"]?> - <?=$avalrisco["ji_texto"]?>
							</td>
							<td>
								<?=$avalrisco["medidas"]?>
							</td>
							<td>
								<?=$avalrisco["responsavel"]?>
							</td>
							<td>
								<?=$avalrisco["data_prevista_resolucao"]?>
							</td>
							<td>
								<a class="btn" href="avalrisco.php?id=<?=$avalrisco["pta_id"]?>"><i class="icon-eye-open" title="Ver detalhes da Avaliação de Risco"></i></a> 
								<a class="btn" href="avalrisco.php?action=edit&id=<?= $avalrisco["pta_id"]?>"><i class="icon-pencil" title="Editar Avaliação de Risco"></i></a>
								<a class="btn" href="avalrisco.php?action=delete&id=<?= $avalrisco["pta_id"]?>"  title="Remover Avaliação de Risco"><i class="icon-trash"></i></a>
							</td>					
						</tr>
						<? endforeach ?>
				</tbody>
			</table>
		</div>
	</div><!--/span-->
</div><!--/row-->

