﻿<div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><a href="#" class="btn-minimize"><i class="icon-chevron-up"></i></a> <i class="icon-user"></i> Lista de Formação</h2>
			<div class="box-icon">
				<a href="adicionar_formacao.php?eid=<?=$eid?>" title="Adicionar Formacao" class="btn btn-round"><i class="icon-plus"></i></a>
			 
			</div>
		</div>
		<div class="box-content">
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>Nome Formação</th>
						<th>Nº horas</th>
						<th>Opções</th>
					</tr>
				</thead>

				<tbody>
					<?php if (empty($formacoes)) echo '<tr><td colspan="3">Esta empresa não tem formações associadas</td></tr>'; ?>
						<?php foreach ($formacoes as $formacao): ?>				
						<tr>
							<td><?= $formacao['tema']?></td>
							<td><?= $formacao["duracao"]?></td>
							<td><a class="btn" href="index.php?for_id=<?= $formacao["for_id"]?>"><i class="icon-eye-open" title="Ver detalhes da formação"></i></a> <a class="btn" href="#"  title="Editar formação"><i class="icon-pencil"></i></a></td>
						</tr>
						<? endforeach ?>
				</tbody>
			</table>
		</div>
	</div><!--/span-->
</div><!--/row-->

