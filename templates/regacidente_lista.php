﻿<div class="row-fluid">	
		<div class="box span3">
			<div class="box-header well" data-original-title>
				<h2><i class="icon-fire"></i> Acidentes por ano</h2>
				<div class="box-icon">
					<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>Ano</th>
							<th>Num. Acidentes</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($acidentesporano as $linha) { ?>
						<tr>
							<td><?= $linha['ano'] ?></td>
							<td><?= $linha['acidentes'] ?></td>
						</tr>
						<?php }?>
					</tbody>
				</table>
			</div>
		</div>

		<div class="box span4">
			<div class="box-header well" data-original-title>
				<h2><i class="icon-fire"></i> Acidentes por Posto de Trabalho</h2>
				<div class="box-icon">
					<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>Posto Trabalho</th>
							<th>Num. Acidentes</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($acidentesporpostotrabalho as $linha) { ?>
						<tr>
							<td><?= $linha['posto'] ?></td>
							<td><?= $linha['acidentes'] ?></td>
						</tr>
						<?php }?>
					</tbody>
				</table>
			</div>
		</div>
		
		<div class="box span5">
			<div class="box-header well" data-original-title>
				<h2><i class="icon-fire"></i> Acidentes por Trabalhador</h2>
				<div class="box-icon">
					<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>Trabalhador</th>
							<th>Num. Acidentes</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($acidentesportrabalhador as $linha) { ?>
						<tr>
							<td><?= $linha['trabalhador'] ?></td>
							<td><?= $linha['acidentes'] ?></td>
						</tr>
						<?php }?>
					</tbody>
				</table>
			</div>
		</div>
</div>
<div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><a href="#" class="btn-minimize"><i class="icon-chevron-up"></i></a> <i class="icon-user"></i> Lista de Registo de Acidentes</h2>
			<div class="box-icon">
				<a href="regacidente.php?action=add" title="Adicionar Registo Acidente" class="btn btn-round"><i class="icon-plus"></i></a>
			 
			</div>
		</div>
		<div class="box-content">
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>Data</th>
						<th>Estado</th>
						<th>Opções</th>
					</tr>
				</thead>

				<tbody>
					<?php if (empty($regacidentes)) echo '<tr><td colspan="3">Esta empresa não tem registo de acidentes associados</td></tr>'; ?>
						<?php foreach ($regacidentes as $regac): ?>				
						<tr>
							<td><?= $regac['data'] ?></td>
							<td><?= $regac["estado"]?></td>
							<td>
								<a class="btn" href="regacidente.php?id=<?= $regac["ra_id"]?>"><i class="icon-eye-open" title="Ver detalhes do registo de acidentes"></i></a> 
								<a class="btn" href="regacidente.php?action=edit&id=<?= $regac["ra_id"]?>"  title="Editar registo de acidente"><i class="icon-pencil"></i></a>
								<a class="btn" href="regacidente.php?action=delete&id=<?= $regac["ra_id"]?>"  title="Remover registo de acidente"><i class="icon-trash"></i></a></td>
						</tr>
						<? endforeach ?>
				</tbody>
			</table>
		</div>
	</div><!--/span-->
</div><!--/row-->

