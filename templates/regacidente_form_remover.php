﻿<div class="row-fluid">
	<div class="span6 offset3">
		<form action="regacidente.php" method="post" id="remover-regacidente" class="form well">
			<fieldset>
				<legend><?=$title?></legend>
				<div class="center">
					<br/>
					<h4>Tem a certeza que deseja remover o registo de acidente</h4>
					<br/>
					<p><?=$regacidente['data']?> ?</p>
				</div>

				<input type="hidden" name="regacidente_eid" value="<?=$eid?>">
				<input type="hidden" name="ra_id" value="<?=$regacidente['ra_id']?>">
				<input type="hidden" name="action" value="delete">
				
				<div class="form-actions center">
					<button type="submit" class="btn btn-danger">Sim, remover</button>
					<a href="#" class="btn" onclick="history.go(-1);">Cancelar</a>
				</div>					
			</fieldset>
		</form>
	</div>
</div>
