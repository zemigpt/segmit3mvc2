﻿<div class="row-fluid">
	<div class="span6 offset3">
		<form action="tmaquina.php" method="post" enctype="multipart/form-data" id="editar-tmaquina" class="form-horizontal well" >
			<fieldset>
				<legend><?=$title?></legend>
				
				<div class="control-group"> 
					<label class="control-label" for="tmaquina_nome">Nome</label> 
					<div class="controls"> 
						<input type="text" name="tmaquina_nome" value="<?=$tmaquina['nome']?>"/>  
					</div> 
				</div>

								
				<input type="hidden" name="tmaquina_eid" value="<?=$_SESSION['cur_eid'] ?>">
				<input type="hidden" name="tm_id" value="<?=$tmaquina['tm_id']?>">
				<input type="hidden" name="action" value="edit">
				<div class="form-actions">
					<button type="submit" class="btn btn-primary">Gravar alterações</button>
					<a href="#" class="btn" onclick="history.go(-1);">Cancelar</a>
				</div>
								
			</fieldset>
		</form>
	</div>
</div>
