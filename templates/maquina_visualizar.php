<div class="row-fluid">	
	<div class="span2">
		<div class="box span12">
			<div class="box-header well" data-original-title>
				
			</div>
			<div>
			<ul class="nav nav-tabs nav-stacked">
				<li>
					<a href="#dadosmaq">Dados da Máquina</a>
				</li>
				<li>
					<a href="#manutencoes">Manutenções</a>
				</li>
				<li>
					<a href="#revisoes">Revisões</a>
				</li>
				<li>
					<a href="#postotrab">Posto de Trabalho</a>
				</li>
				<li>
					<a href="#trabalhadores">Trabalhadores associados</a>
				</li>
				<li>
					<a href="#manutencoes">Manutencoes</a>
				</li>
				<li>
					<a href="#forma2">Formação Requerida</a>
				</li>
				<li>
					<a href="#regacidente">Registo Acidentes</a>
				</li>
				<li>
					<a href="#check50">CheckList DL 50/2005</a>
				</li>
			</ul>
			</div>
		</div>
	</div><!--/span-->
	<div class="span10">	
		<div class="row-fluid">	<!--/ inicio box dados maquina-->
			<div class="box span12">
				<div class="box-header well" data-original-title>
					<a id="dadosmaq"></a><h2><i class="icon-user"></i> Dados da Máquina</h2>
					<div class="box-icon">
						<a href="maquina.php?id=<?=$mid?>&action=edit" title="Editar Máquina" class="btn  btn-round"><i class="icon-pencil"></i></a>
						<a href="maquina.php?id=<?=$mid?>&action=delete" title="Remover Máquina" class="btn btn-round"><i class="icon-remove"></i></a>
						<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
					</div>
				</div>
				<div class="box-content">
					<table class="table table-striped">


						<thead>
							<tr>
								<th colspan='2'><p class="lead"><img style="height:50px" src="<?=$maquina['foto']?>"> <?=$maquina['maquina']?></p></th>
							</tr>
						</thead>

						<tbody>					
								<tr>
									<th width="33%">Tipo de Máquina</th>
									<td width="67%"><?=$maquina['tipo_maquina']?></td>
								</tr>
								<tr>
									<th width="33%">Fabricante</th>
									<td width="67%"><?=$maquina['fabricante']?></td>
								</tr>
								<tr>
									<th width="33%">Número de Série</th>
									<td width="67%"><?=$maquina['num_serie']?></td>
								</tr>
								<tr>
									<th>Data Compra</th>
									<td><?=$maquina['data_compra']?></td>
								</tr>
								<tr>
									<th>Data Aprovação Segurança</th>
									<td><?=$maquina['data_aprov_seguranca']?></td>
								</tr>
								<tr>
									<th>Manual Instruções</th>
									<td><?=$maquina['manual_instrucao']?></td>
								</tr>
								<tr>
									<th>Instruções de Trabalho</th>
									<td><?=$maquina['instrucao_trabalho']?></td>
								</tr>
								<tr>
									<th>Declaração Conformidade CE</th>
									<td><?=$maquina['declaracao_CE']?></td>
								</tr>
								<tr>
									<th>Declaração Conformidade DL50/2005</th>
									<td><?=$maquina['declaracao_DL50']?></td>
								</tr>
								
						</tbody>
					</table>
				</div>
			</div><!--/ fim box dados maquina-->
		</div><!--/row-->

		<div class="row-fluid"> <!--/ inicio box dados manutencoes-->	
		
			<div class="box span12">
				<div class="box-header well" data-original-title>
					<a id="manutencoes"></a><h2><i class="icon icon-wrench icon-black"></i> Manutenções</h2>
					<div class="box-icon">
						<a href="adicionar_manutencao.php?mid=<?=$mid?>" title="Adicionar Manutenção" class="btn btn-round"><i class="icon-plus"></i></a>
						<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
					</div>
				</div>
				<div class="box-content">
					<table class="table table-striped">
						<thead>
							<tr>
								<th>
									Data
								</th>
								<th>
									Relatório Manutenção
								</th>
								<th>
									Data Próxima Manutenção
								</th>
							</tr>
						</thead>
						<tbody>				
						<?php if (empty($manutencoes)) {?>
							<tr>
								<td colspan="2">
									Não há manutenções associadas
								</td>
							</tr>
						<?php } else {?>
							<?php foreach($manutencoes as $manutencao) { ?>
								<tr>
									<td>
										<?=$manutencao["data_manutencao"]?>
									</td>
									<td>
										<?=$manutencao["relatorio_manutencao"]?>
									</td>
									<td>
										<?=$manutencao["data_prox_manutencao"]?>
									</td>
								</tr>
							<?php }?>
						<?php }?>
						</tbody>
					</table>
				</div>
			</div><!--/ fim box dados manutencoes-->	
		</div><!--/row-->
		
		<div class="row-fluid"> <!--/ inicio box dados revisoes-->	
		
			<div class="box span12">
				<div class="box-header well" data-original-title>
					<a id="revisoes"></a><h2><i class="icon icon-wrench icon-black"></i> Revisões</h2>
					<div class="box-icon">
						<a href="adicionar_revisao.php?mid=<?=$mid?>" title="Adicionar Revisão" class="btn btn-round"><i class="icon-plus"></i></a>
						<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
					</div>
				</div>
				<div class="box-content">
					<table class="table table-striped">
						<thead>
							<tr>
								<th>
									Data
								</th>
								<th>
									Relatório Revisão
								</th>
								<th>
									Data Próxima Revisão
								</th>
							</tr>
						</thead>
						<tbody>				
						<?php if (empty($revisoes)) {?>
							<tr>
								<td colspan="2">
									Não há revisões associadas
								</td>
							</tr>
						<?php } else {?>
							<?php foreach($revisoes as $revisao) { ?>
								<tr>
									<td>
										<?=$revisao["data_revisao"]?>
									</td>
									<td>
										<?=$revisao["relatorio_revisao"]?>
									</td>
									<td>
										<?=$revisao["data_prox_revisao"]?>
									</td>
								</tr>
							<?php }?>
						<?php }?>
						</tbody>
					</table>
				</div>
			</div><!--/ fim box dados revisoes-->	
		</div><!--/row-->
		
		<div class="row-fluid"> <!--/ inicio box dados posto trabalho-->
			<div class="box span12">
				<div class="box-header well" data-original-title>
					<a id="postotrab"></a><h2><i class="icon-briefcase"></i> Posto de Trabalho</h2>
					<div class="box-icon">
						<a href="#myModalAdicionarPtrabalho" role="button" data-toggle="modal" title="Adicionar Posto de Trabalho" class="btn  btn-round"><i class="icon-plus"></i></a>
						<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
					</div>
				</div>
				<div class="box-content">
					<table class="table table-striped">
						<thead>
							<tr>
								<th>
									Nome
								</th>
								<th>
									Foto
								</th>
								<th>
									Opção
								</th>							
							</tr>
						</thead>
						<tbody>					
							<?php if (empty($postos_trabalho)) {?>
								<tr>
									<td colspan="3">
										Não há posto de trabalho associado ao trabalhador
									</td>
								</tr>
							<?php } else {?>
								<?php foreach($postos_trabalho as $posto_trabalho) { ?>
									<tr>
										<td>
											<?=$posto_trabalho["nome"]?>
										</td>
										<td>
											<img style="height:50px" src="<?=$posto_trabalho['foto']?>">
										</td>
										<td>
											<a class="btn" href="ptrabalho.php?id=<?= $posto_trabalho["ptid"]?>"><i class="icon-eye-open" title="Ver detalhes do posto de trabalho"></i></a> 
										</td>
									</tr>
								<?php }?>
							<?php }?>
						</tbody>
					</table>
				</div>
			</div><!--/ fim box dados posto trabalho-->	
		</div><!--/row-->
		
		<div class="row-fluid"> <!--/ inicio box dados trabalhadores-->	
		
			<div class="box span12">
				<div class="box-header well" data-original-title>
					<a id="trabalhadores"></a><h2><i class="icon icon-wrench icon-black"></i> Trabalhadores</h2>
					<div class="box-icon">
						<a href="#myModalAdicionarTrabalhadores" role="button" data-toggle="modal" title="Adicionar Trabalhador" class="btn  btn-round"><i class="icon-plus"></i></a>
						<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
					</div>
				</div>
				<div class="box-content">
					<table class="table table-striped">
						<thead>
							<tr>
								<th>
									Nome
								</th>
								<th>
									Número Mecanográfico
								</th>
								<th>
									Opção
								</th>
							</tr>
						</thead>
						<tbody>				
						<?php if (empty($trabalhadores)) {?>
							<tr>
								<td colspan="3">
									Não há trabalhadores associados
								</td>
							</tr>
						<?php } else {?>
							<?php foreach($trabalhadores as $trabalhador) { ?>
								<tr>
									<td>
										<?=$trabalhador["nome"]?>
									</td>
									<td>
										<?=$trabalhador["num_mecanografico"]?>
									</td>
									<td>
										<a class="btn" href="trabalhador.php?id=<?= $trabalhador["tid"]?>"><i class="icon-eye-open" title="Ver detalhes do trabalhador"></i></a>
									</td>
								</tr>
							<?php }?>
						<?php }?>
						</tbody>
					</table>
				</div>
			</div><!--/ fim box dados trabalhadores-->	
		</div><!--/row-->
		
		<div class="row-fluid"><!--/box formação requerida-->
			
			<div class="box span12">
				<div class="box-header well" data-original-title>
					<a id="forma2"></a><h2><i class="icon icon-wrench icon-black"></i> Formação Requerida</h2>
					<div class="box-icon">
						<!-- removida adicionar formação porque faz parte apenas dos tipos de máquinas
						<a href="adicionar_formacao.php?mid=<?=$mid?>" title="Adicionar Formação" class="btn btn-round"><i class="icon-plus"></i></a>  -->
						<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
					</div>
				</div>
				<div class="box-content">
					<table class="table table-striped">
						<thead>
							<tr>
								<th>
									Tema
								</th>
								<th>
									Núm. Horas
								</th>
								<th>
									Máquina Associada
								</th>
								<th>
									Opção
								</th>
							</tr>
						</thead>
						<tbody>				
						<?php if (empty($forrequeridas)) {?>
							<tr>
								<td colspan="4">
									Não há formações associadas
								</td>
							</tr>
						<?php } else {?>
							<?php foreach($forrequeridas as $forrequerida) { ?>
								<tr>
									<td>
										<?=$forrequerida["tema"]?>
									</td>
									<td>
										<?=$forrequerida["duracao"]?>
									</td>
									<td>
										<?=$forrequerida["maq_nome"]?>
									</td>
									<td>
										<a class="btn" href="index.php?mid=<?=$forrequerida["for_id"]?>"><i class="icon-eye-open" title="Ver detalhes da formação"></i></a> 
									</td>
								</tr>
							<?php }?>
						<?php }?>
						</tbody>
					</table>
				</div>
			</div><!--/box formação detida-->
		</div><!--/row-->
		
		<div class="row-fluid"><!--/box registo acidentes-->
			
			<div class="box span12">
				<div class="box-header well" data-original-title>
					<a id="regacidente"></a><h2><i class="icon icon-wrench icon-black"></i> Registo de Acidentes</h2>
					<div class="box-icon">
						<!-- removida adicionar de registo de acidente porque deve ser uma entrada externa
						<a href="adicionar_regacidente.php?mid=<?=$mid?>" title="Adicionar Registo Acidente" class="btn btn-round"><i class="icon-plus"></i></a> -->
						<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
					</div>
				</div>
				<div class="box-content">
					<table class="table table-striped">
						<thead>
							<tr>
								<th>
									Data
								</th>
								<th>
									Estado
								</th>
								<th>
									Opção
								</th>
							</tr>
						</thead>
						<tbody>				
						<?php if (empty($regacidentes)) {?>
							<tr>
								<td colspan="3">
									Não há registo de acidentes associados
								</td>
							</tr>
						<?php } else {?>
							<?php foreach($regacidentes as $regacidente) { ?>
								<tr>
									<td>
										<?=$regacidente["data"]?>
									</td>
									<td>
										<?=$regacidente["estado"]?>
									</td>
									
									<td>
										<a class="btn" href="regacidente.php?id=<?=$regacidente["ra_id"]?>"><i class="icon-eye-open" title="Ver detalhes da formação"></i></a> 
									</td>
								</tr>
							<?php }?>
						<?php }?>
						</tbody>
					</table>
				</div>
			</div><!--/box registo acidentes-->	
		</div><!--/row-->
		
		<div class="row-fluid"><!--/box check50-->
			
			<div class="box span12">
				<div class="box-header well" data-original-title>
					<a id="check50"></a><h2><i class="icon icon-wrench icon-black"></i> Checklist DL 50/2005</h2>
					<div class="box-icon">
						
						<a href="check50.php?action=add&id=<?=$mid?>" title="Adicionar Checklist" class="btn btn-round"><i class="icon-plus"></i></a> 
						<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
					</div>
				</div>
				<div class="box-content">
					<table class="table table-striped">
						<thead>
							<tr>
								<th>
									ID
								</th>
								<th>
									Opção
								</th>
							</tr>
						</thead>
						<tbody>				
						<?php if (empty($check50s)) {?>
							<tr>
								<td colspan="2">
									Não há checklist associadas
								</td>
							</tr>
						<?php } else {?>
							<?php foreach($check50s as $check50) { ?>
								<tr>
									<td>
										<?=$check50["check50_id"]?>
									</td>
									<td>
										<a class="btn" href="check50.php?id=<?=$check50["check50_id"]?>"><i class="icon-eye-open" title="Ver Checklist DL50/2005"></i></a> 
									</td>
								</tr>
							<?php }?>
						<?php }?>
						</tbody>
					</table>
				</div>
			</div><!--/box registo acidentes-->	
		</div><!--/row-->
		
		</div><!--/span-->
</div><!--/row-->


<!-- Modal Seleccionar Posto Trabalho -->
<div id="myModalAdicionarPtrabalho" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabeladdPt" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabeladdPt">Adicionar Posto Trabalho</h3>
	</div>
	<div class="modal-body">
		<p>&nbsp;</p>
		<form action="associar_ptrabalho_a_maquina.php" method="post" id="associar_ptrabalho_a_maquina" class="form-horizontal" >
			<fieldset>
				<div class="control-group"> 
					<label class="control-label" for="ptid">Seleccionar Posto Trabalho</label> 
					<div class="controls"> 
						<div class="pull-left">
								<?php if (empty($todos_ptrabalhos)) { ?>
									 <input type="text" class="input-xlarge uneditable-input info" placeholder="Já todos os postos de trabalho pertencem a esta máquina">
									 <input type="hidden" name="ptid" value="0">
								<?php } else { ?>
									<select name="ptid" id="sel_ptrabalho" >
										<option value="0">- Postos de Trabalho -</option>
										<?php foreach ($todos_ptrabalhos as $ptrabalho) { ?>
											<option value="<?= $ptrabalho['ptid'] ?>"><?= $ptrabalho['nome'] ?></option>
										<?php } ?>
									</select>
								<?php } ?>
						</div>
					</div>
				</div>
				<input type="hidden" name="mid" value="<?=$mid?>">
				<input type="hidden" name="ajax" value="true">
			</fieldset>
		</form>
	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
		<button id="associar_ptrabalho_a_maquina-submit" type="submit" class="btn btn-primary" <?php if (empty($todos_ptrabalhos)) echo "disabled" ?>>Adicionar</button>
	</div>
</div>

<!-- Modal Seleccionar Trabalhadores -->
<div id="myModalAdicionarTrabalhadores" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabeladdTrab" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabeladdTrab">Adicionar Trabalhadores</h3>
	</div>
	<div class="modal-body">
		<p>&nbsp;</p>
		<form action="associar_trabalhador_a_maquina.php" method="post" id="associar_trabalhador_a_maquina" class="form-horizontal" >
			<fieldset>
				<div class="control-group"> 
					<label class="control-label" for="tid">Seleccionar Trabalhador</label> 
					<div class="controls"> 
						<div class="pull-left">
								<?php if (empty($todos_trabalhadores)) { ?>
									 <input type="text" class="input-xlarge uneditable-input info" placeholder="Já todos os trabalhadores pertencem a esta máquina">
									 <input type="hidden" name="tid" value="0">
								<?php } else { ?>
									<select name="tid" id="sel_trabalhadores" >
										<option value="0">- Trabalhadores -</option>
										<?php foreach ($todos_trabalhadores as $trabalhador) { ?>
											<option value="<?= $trabalhador['tid'] ?>"><?= $trabalhador['nome'] ?></option>
										<?php } ?>
									</select>
								<?php } ?>
						</div>
					</div>
				</div>
				<input type="hidden" name="mid" value="<?=$mid?>">
				<input type="hidden" name="ajax" value="true">
			</fieldset>
		</form>
	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
		<button id="associar_trabalhador_a_maquina-submit" type="submit" class="btn btn-primary" <?php if (empty($todos_trabalhadores)) echo "disabled" ?>>Adicionar</button>
	</div>
</div>