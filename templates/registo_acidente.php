<div class="row-fluid">	
	<div class="span2">
		<div class="box span12">
			<div class="box-header well" data-original-title>
				
			</div>
			<div>
			<ul class="nav nav-tabs nav-stacked">
				<li>
					<a href="#dadosregac">Dados Registo Acidente</a>
				</li>
				<li>
					<a href="#ptrabalho">Posto de Trabalho</a>
				</li>
				<li>
					<a href="#maquinas">Máquinas</a>
				</li>
				<li>
					<a href="#trabalhadores">Trabalhadores</a>
				</li>
				
			</ul>
			</div>
		</div>
	</div>
	<div class="span10">	
		<div class="row-fluid">	<!--/ inicio box dados posto trabalho-->
		<div class="box span12">
			<div class="box-header well" data-original-title>
				<a id="dadospt"></a><h2><i class="icon-list"></i> Dados Posto Trabalho</h2>
				<div class="box-icon">
					<a href="posto_trabalho.php?tid=<?=$mid?>&action=edit" title="Editar Posto Trabalho" class="btn btn-setting btn-round"><i class="icon-pencil"></i></a>
					<a href="posto_trabalho.php?tid=<?=$mid?>&action=delete" title="Remover Posto Trabalho" class="btn btn-minimize btn-round"><i class="icon-remove"></i></a>
					<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">
				<table class="table table-striped">


					<thead>
						<tr>
							<th colspan='2'><p class="lead"><img style="height:50px" src="<?=$ptrabalho['foto']?>"> <?=$ptrabalho['nome']?></p></th>
						</tr>
					</thead>

					
				</table>
			</div>
		</div><!--/ fim box dados posto trabalho-->
		</div>
		
		<div class="row-fluid"> <!--/ inicio box dados trabalhadores-->	
		
		<div class="box span12">
			<div class="box-header well" data-original-title>
				<a id="trabalhadores"></a><h2><i class="icon icon-user icon-black"></i> Trabalhadores</h2>
				<div class="box-icon">
					<a href="adicionar_trabalhador.php?tid=<?=$tid?>" title="Adicionar Trabalhador" class="btn btn-round"><i class="icon-plus"></i></a>
					<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>
								Nome
							</th>
							<th>
								Número Mecanográfico
							</th>
							<th>
								Opção
							</th>
						</tr>
					</thead>
					<tbody>				
					<?php if (empty($trabalhadores)) {?>
						<tr>
							<td colspan="3">
								Não há trabalhadores associados
							</td>
						</tr>
					<?php } else {?>
						<?php foreach($trabalhadores as $trabalhador) { ?>
							<tr>
								<td>
									<?=$trabalhador["nome"]?>
								</td>
								<td>
									<?=$trabalhador["num_mecanografico"]?>
								</td>
								<td>
									<a class="btn" href="index.php?tid=<?= $trabalhador["tid"]?>"><i class="icon-eye-open" title="Ver detalhes do trabalhador"></i></a> 
								</td>
							</tr>
						<?php }?>
					<?php }?>
					</tbody>
				</table>
			</div>
		</div><!--/ fim box dados trabalhadores-->	

		<div class="row-fluid"><!--/ inicio box dados maquinas-->	
		
		<div class="box span12">
			<div class="box-header well" data-original-title>
				<a id="maquinas"></a><h2><i class="icon icon-wrench icon-black"></i> Máquinas</h2>
				<div class="box-icon">
					<a href="adicionar_maquina.php?tid=<?=$tid?>" title="Adicionar Máquina" class="btn btn-round"><i class="icon-plus"></i></a>
					<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>
								Nome
							</th>
							<th>
								Núm. Série
							</th>
							<th>
								Opção
							</th>
						</tr>
					</thead>
					<tbody>				
					<?php if (empty($maquinas)) {?>
						<tr>
							<td colspan="3">
								Não há máquinas associadas
							</td>
						</tr>
					<?php } else {?>
						<?php foreach($maquinas as $maquina) { ?>
							<tr>
								<td>
									<?=$maquina["nome"]?>
								</td>
								<td>
									<?=$maquina["num_serie"]?>
								</td>
								<td>
									<a class="btn" href="index.php?mid=<?=$maquina["mid"]?>"><i class="icon-eye-open" title="Ver detalhes da máquina"></i></a> 
								</td>
							</tr>
						<?php }?>
					<?php }?>
					</tbody>
				</table>
			</div>
		</div><!--/box-->
		
		<div class="row-fluid"><!--/ inicio box dados avaliacoes fisico ruido-->	
		
		<div class="box span12">
			<div class="box-header well" data-original-title>
				<a id="afruido"></a><h2><i class="icon icon-profile icon-black"></i> Avaliação Agente Físico: Ruído</h2>
				<div class="box-icon">
					<a href="adicionar_ag_fisico_ruido.php?tid=<?=$tid?>" title="Adicionar Avaliação Agente Físico: Ruído" class="btn btn-round"><i class="icon-plus"></i></a>
					<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>
								Data
							</th>
							<th>
								Valor Registado
							</th>
						</tr>
					</thead>
					<tbody>				
					<?php if (empty($afruidos)) {?>
						<tr>
							<td colspan="2">
								Não há avaliações de agentes físicos ruído associadas
							</td>
						</tr>
					<?php } else {?>
						<?php foreach($afruidos as $afruido) { ?>
							<tr>
								<td>
									<?=$afruido["data"]?>
								</td>
								<td>
									<?=$afruido["valor"]?>
								</td>
							</tr>
						<?php }?>
					<?php }?>
					</tbody>
				</table>
			</div>
		</div><!--/box avaliação ruido-->
		
		<div class="row-fluid"><!--/ inicio box dados avaliacoes fisico iluminancia-->	
		
		<div class="box span12">
			<div class="box-header well" data-original-title>
				<a id="afiluminancia"></a><h2><i class="icon icon-profile icon-black"></i> Avaliação Agente Físico: Iluminancia</h2>
				<div class="box-icon">
					<a href="adicionar_ag_fisico_iluminancia.php?tid=<?=$tid?>" title="Adicionar Avaliação Agente Físico: Iluminancia" class="btn btn-round"><i class="icon-plus"></i></a>
					<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>
								Data
							</th>
							<th>
								Valor Registado
							</th>
						</tr>
					</thead>
					<tbody>				
					<?php if (empty($afiluminancias)) {?>
						<tr>
							<td colspan="2">
								Não há avaliações de agentes físicos iluminância associadas
							</td>
						</tr>
					<?php } else {?>
						<?php foreach($afiluminancias as $afilumin) { ?>
							<tr>
								<td>
									<?=$afilumin["data"]?>
								</td>
								<td>
									<?=$afilumin["valor"]?>
								</td>
							</tr>
						<?php }?>
					<?php }?>
					</tbody>
				</table>
			</div>
		</div><!--/box avaliação risco iluminancia-->
		
		<div class="row-fluid"><!--/ inicio box dados avaliacoes fisico ambtermico-->	
		
		<div class="box span12">
			<div class="box-header well" data-original-title>
				<a id="afambterm"></a><h2><i class="icon icon-profile icon-black"></i> Avaliação Agente Físico: Ambiente Térmico</h2>
				<div class="box-icon">
					<a href="adicionar_ag_fisico_ambterm.php?tid=<?=$tid?>" title="Adicionar Avaliação Agente Físico: Ambiente Térmico" class="btn btn-round"><i class="icon-plus"></i></a>
					<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>
								Data
							</th>
							<th>
								Valor Registado
							</th>
						</tr>
					</thead>
					<tbody>				
					<?php if (empty($afambterms)) {?>
						<tr>
							<td colspan="2">
								Não há avaliações de agentes físicos ambiente térmico associadas
							</td>
						</tr>
					<?php } else {?>
						<?php foreach($afambterms as $afambterm) { ?>
							<tr>
								<td>
									<?=$afambterm["data"]?>
								</td>
								<td>
									<?=$afambterm["valor"]?>
								</td>
							</tr>
						<?php }?>
					<?php }?>
					</tbody>
				</table>
			</div>
		</div><!--/box avaliação risco ambiente térmico-->
		
		<div class="row-fluid"><!--/ inicio box dados avaliacoes fisico vibracoes-->	
		
		<div class="box span12">
			<div class="box-header well" data-original-title>
				<a id="afvibracoes"></a><h2><i class="icon icon-profile icon-black"></i> Avaliação Agente Físico: Vibrações</h2>
				<div class="box-icon">
					<a href="adicionar_ag_fisico_vibracoes.php?tid=<?=$tid?>" title="Adicionar Avaliação Agente Físico: Vibrações" class="btn btn-round"><i class="icon-plus"></i></a>
					<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>
								Data
							</th>
							<th>
								Valor Registado
							</th>
						</tr>
					</thead>
					<tbody>				
					<?php if (empty($afvibracoes)) {?>
						<tr>
							<td colspan="2">
								Não há avaliações de agentes físicos ambiente vibrações associadas
							</td>
						</tr>
					<?php } else {?>
						<?php foreach($afvibracoes as $afvibracao) { ?>
							<tr>
								<td>
									<?=$afvibracao["data"]?>
								</td>
								<td>
									<?=$afvibracao["valor"]?>
								</td>
							</tr>
						<?php }?>
					<?php }?>
					</tbody>
				</table>
			</div>
		</div><!--/box avaliação risco ambiente vibracoes-->
		
		<div class="row-fluid"><!--/ inicio box dados avaliacoes biologico-->	
		
		<div class="box span12">
			<div class="box-header well" data-original-title>
				<a id="agbiologico"></a><h2><i class="icon icon-profile icon-black"></i> Avaliação Agentes Biologicos</h2>
				<div class="box-icon">
					<a href="adicionar_ag_biologico.php?tid=<?=$tid?>" title="Adicionar Avaliação Agente Biológicos" class="btn btn-round"><i class="icon-plus"></i></a>
					<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>
								Data
							</th>
							<th>
								Valor Registado
							</th>
						</tr>
					</thead>
					<tbody>				
					<?php if (empty($agbiologicos)) {?>
						<tr>
							<td colspan="2">
								Não há avaliações de agentes biológicos associadas
							</td>
						</tr>
					<?php } else {?>
						<?php foreach($agbiologicos as $agbiologico) { ?>
							<tr>
								<td>
									<?=$agbiologico["data"]?>
								</td>
								<td>
									<?=$agbiologico["valor"]?>
								</td>
							</tr>
						<?php }?>
					<?php }?>
					</tbody>
				</table>
			</div>
		</div><!--/box avaliação risco agentes biologicos-->
		
		<div class="row-fluid"><!--/ inicio box dados avaliacoes quimicos-->	
		
		<div class="box span12">
			<div class="box-header well" data-original-title>
				<a id="agquimicos"></a><h2><i class="icon icon-profile icon-black"></i> Avaliação Agentes Químicos</h2>
				<div class="box-icon">
					<a href="adicionar_ag_quimico.php?tid=<?=$tid?>" title="Adicionar Avaliação Agente Químicos" class="btn btn-round"><i class="icon-plus"></i></a>
					<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>
								Data
							</th>
							<th>
								Agente
							</th>
							<th>
								Valor Registado
							</th>
						</tr>
					</thead>
					<tbody>				
					<?php if (empty($agquimicos)) {?>
						<tr>
							<td colspan="3">
								Não há avaliações de agentes químicos associadas
							</td>
						</tr>
					<?php } else {?>
						<?php foreach($agquimicos as $agquimico) { ?>
							<tr>
								<td>
									<?=$agquimico["data"]?>
								</td>
								<td>
									<?=$agquimico["agente"]?>
								</td>
								<td>
									<?=$agquimico["dose"]?>
								</td>
							</tr>
						<?php }?>
					<?php }?>
					</tbody>
				</table>
			</div>
		</div><!--/box avaliação risco agentes quimicos-->
		
		<div class="row-fluid"><!--/ inicio box dados avaliacoes risco-->	
		
		<div class="box span12">
			<div class="box-header well" data-original-title>
				<a id="avalrisco"></a><h2><i class="icon icon-profile icon-black"></i> Avaliação Riscos</h2>
				<div class="box-icon">
					<a href="adicionar_avaliacao_risco.php?tid=<?=$tid?>" title="Adicionar Avaliação Risco" class="btn btn-round"><i class="icon-plus"></i></a>
					<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>
								Data
							</th>
							<th>
								Tarefa
							</th>
							<th>
								Perigo
							</th>
							<th>
								Risco
							</th>
						</tr>
					</thead>
					<tbody>				
					<?php if (empty($avalriscos)) {?>
						<tr>
							<td colspan="4">
								Não há avaliações de risco associadas
							</td>
						</tr>
					<?php } else {?>
						<?php foreach($avalriscos as $avalrisco) { ?>
							<tr>
								<td>
									<?=$avalrisco["data"]?>
								</td>
								<td>
									<?=$avalrisco["tarefa"]?>
								</td>
								<td>
									<?=$avalrisco["perigo"]?>
								</td>
								<td>
									<?=$avalrisco["risco"]?>
								</td>
							</tr>
						<?php }?>
					<?php }?>
					</tbody>
				</table>
			</div>
		</div><!--/box avaliação risco agentes quimicos-->
		
		<div class="row-fluid"><!--/box formação requerida-->
		
		<div class="box span12">
			<div class="box-header well" data-original-title>
				<a id="forma2"></a><h2><i class="icon icon-wrench icon-black"></i> Formação Requerida</h2>
				<div class="box-icon">
					<a href="adicionar_formacao.php?tid=<?=$tid?>" title="Adicionar Formação" class="btn btn-round"><i class="icon-plus"></i></a>
					<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>
								Tema
							</th>
							<th>
								Núm. Horas
							</th>
							<th>
								Máquina Associada
							</th>
							<th>
								Opção
							</th>
						</tr>
					</thead>
					<tbody>				
					<?php if (empty($forrequeridas)) {?>
						<tr>
							<td colspan="4">
								Não há formações associadas
							</td>
						</tr>
					<?php } else {?>
						<?php foreach($forrequeridas as $forrequerida) { ?>
							<tr>
								<td>
									<?=$forrequerida["tema"]?>
								</td>
								<td>
									<?=$forrequerida["duracao"]?>
								</td>
								<td>
									<?=$forrequerida["maq_nome"]?>
								</td>
								<td>
									<a class="btn" href="index.php?mid=<?=$forrequerida["for_id"]?>"><i class="icon-eye-open" title="Ver detalhes da formação"></i></a> 
								</td>
							</tr>
						<?php }?>
					<?php }?>
					</tbody>
				</table>
			</div>
		</div><!--/box formação detida-->
		
		</div>
	</div><!--/span-->
</div><!--/row-->
