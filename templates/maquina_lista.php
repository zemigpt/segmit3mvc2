﻿<div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><a href="#" class="btn-minimize"><i class="icon-chevron-up"></i></a> <i class="icon-tasks"></i> Lista de Máquinas</h2>
			<div class="box-icon">
				<a href="maquina.php?action=add" title="Adicionar Máquina" class="btn  btn-round"><i class="icon-plus"></i></a>
			 
			</div>
		</div>
		<div class="box-content">
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>Foto</th>
						<th>Nome Máquina</th>
						<th>Acções</th>
					</tr>
				</thead>

				<tbody>
					<?php if (empty($maquinas)) echo '<tr><td colspan="8">Esta empresa não tem máquinas associadas</td></tr>'; ?>
						<?php foreach ($maquinas as $maquina): ?>				
						<tr>
							<td><img src="<?= $maquina["foto"]?>" width="75px"/></td>
							<td><?= $maquina["nome"]?></td>
							<td><a class="btn" href="maquina.php?id=<?= $maquina["mid"]?>"><i class="icon-eye-open" title="Ver detalhes da máquina"></i></a> <a class="btn" href="maquina.php?action=edit&id=<?=$maquina['mid']?>"  title="Editar máquina"><i class="icon-pencil"></i></a> <a class="btn" href="maquina.php?action=delete&id=<?=$maquina['mid']?>"  title="Remover máquina"><i class="icon-trash"></i></a></td>
						</tr>
						<? endforeach ?>
				</tbody>
			</table>
		</div>
	</div><!--/span-->
</div><!--/row-->

