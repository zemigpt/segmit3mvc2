﻿<div class="row-fluid">
	<div class="span6 offset3">
		<form action="check50.php" method="post" id="remover-check50" class="form well">
			<fieldset>
				<legend><?=$title?></legend>
				<div class="center">
					<br/>
					<h4>Tem a certeza que deseja remover a checklist</h4>
					<br/>
					<p><?=$checklist['mid']?> ?</p>
				</div>

				<input type="hidden" name="check50_eid" value="<?=$eid?>">
				<input type="hidden" name="check50_mid" value="<?=$checklist['mid']?>">
				<input type="hidden" name="action" value="delete">
				
				<div class="form-actions center">
					<button type="submit" class="btn btn-danger">Sim, remover</button>
					<a href="#" class="btn" onclick="history.go(-1);">Cancelar</a>
				</div>					
			</fieldset>
		</form>
	</div>
</div>
