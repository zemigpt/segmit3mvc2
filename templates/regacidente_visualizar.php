﻿<div class="row-fluid">	
	<div class="span2">
		<div class="box span12">
			<div class="box-header well" data-original-title>
				
			</div>
			<div>
			<ul class="nav nav-tabs nav-stacked">
				<li>
					<a href="#dadosregac">Dados Registo Acidente</a>
				</li>
				<li>
					<a href="#ptrabalho">Posto de Trabalho</a>
				</li>
				<li>
					<a href="#maquinas">Máquinas</a>
				</li>
				<li>
					<a href="#trabalhadores">Trabalhadores</a>
				</li>
			</ul>
			</div>
		</div>
	</div>
	<div class="span10">	
		<div class="row-fluid">	
		<div class="box span12">
			<div class="box-header well" data-original-title>
				<a id="dadosregac"></a><h2><i class="icon-list"></i> Dados Registo Acidente</h2>
				<div class="box-icon">
					<a href="regacidente.php?id=<?=$ra_id?>&action=edit" title="Editar Registo de Acidente" class="btn btn-round"><i class="icon-pencil"></i></a>
					<a href="regacidente.php?id=<?=$ra_id?>&action=delete" title="Remover Registo de Acidente" class="btn btn-minimize btn-round"><i class="icon-remove"></i></a>
					<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">
				<table class="table table-striped">
					<tbody>					
							<tr>
								<th width="33%">Registo de Acidente</th>
								<td width="67%"><?=$regacidente['ra_id']?></td>
							</tr>
							<tr>
								<th width="33%">Data e Hora</th>
								<td width="67%"><?=$regacidente['data']?> <?=$regacidente['hora']?> </td>
							</tr>
							<tr>
								<th width="33%">Local</th>
								<td width="67%"><?=$regacidente['local']?></td>
							</tr>
							<tr>
								<th width="33%">Como ocorreu</th>
								<td width="67%"><?=$regacidente['como']?></td>
							</tr>
							<tr>
								<th width="33%">Regiões do corpo atingidas</th>
								<td width="67%"><?=$regacidente['regiao']?></td>
							</tr>
							<tr>
								<th width="33%">Lesões visíveis</th>
								<td width="67%"><?=$regacidente['lesao']?></td>
							</tr>
							<tr>
								<th width="33%">Outras possíveis lesões</th>
								<td width="67%"><?=$regacidente['outras_lesoes']?></td>
							</tr>
							<tr>
								<th width="33%">Testemunhas oculares</th>
								<td width="67%"><?=$regacidente['testemunhas']?></td>
							</tr>
							<tr>
								<th width="33%">Assistência</th>
								<td width="67%"><?=$regacidente['assistencia']?></td>
							</tr>
							<tr>
								<th width="33%">Tipo de Lesão</th>
								<td width="67%"><?=$regacidente['tipo_lesao']?></td>
							</tr>
							<tr>
								<th width="33%">Participação à companhia de seguros</th>
								<td width="67%"><?=$regacidente['participacao']?></td>
							</tr>
							<tr>
								<th width="33%">Com incapacidade temporária</th>
								<td width="67%"><?=$regacidente['incapacidade']?></td>
							</tr>
							<tr>
								<th width="33%">Alta provisória</th>
								<td width="67%"><?=$regacidente['alta_provisoria']?></td>
							</tr>
							<tr>
								<th width="33%">Alta definitiva </th>
								<td width="67%"><?=$regacidente['alta_definitiva']?></td>
							</tr>
							<tr>
								<th width="33%">Causa da Lesão</th>
								<td width="67%"><?=$regacidente['causa']?></td>
							</tr>
							<tr>
								<th width="33%">Localização da Lesão</th>
								<td width="67%">
									<?php $lesoes =""; ?> 
									<?php foreach ($localizacoes_lesoes_lista as $lesao) 
											{
												$lesoes .= $lesao . ", ";
											} 
											$lesoes = rtrim($lesoes, ", ");
											echo $lesoes;
									?>
								</td>
							</tr>
							<tr>
								<th width="33%">Agente do Acidente</th>
								<td width="67%"><?=$regacidente['agente']?></td>
							</tr>
							<tr>
								<th width="33%">Acto Inseguro</th>
								<td width="67%"><?=$regacidente['acto']?></td>
							</tr>
							<tr>
								<th width="33%">Descrição da Lesão</th>
								<td width="67%"><?=$regacidente['descricao']?></td>
							</tr>
							<tr>
								<th width="33%">Condição Perigosa</th>
								<td width="67%"><?=$regacidente['condicao']?></td>
							</tr>
													
							<tr>
								<th width="33%">Relatório</th>
								<td width="67%"><?=$regacidente['relatorio']?></td>
							</tr>
							<tr>
								<th width="33%">Estado</th>
								<td width="67%"><?=$regacidente['estado']?></td>
							</tr>
						</tbody>	
				</table>
			</div>
		</div><!--/box dados trabalhador-->
		</div>

				<div class="row-fluid"> <!--/ inicio box dados posto trabalho-->
		<div class="box span12">
			<div class="box-header well" data-original-title>
				<a id="postotrab"></a><h2><i class="icon-briefcase"></i> Posto de Trabalho</h2>
				<div class="box-icon">
					<a href="#myModalAdicionarPtrabalho" role="button" data-toggle="modal" title="Adicionar Posto Trabalho" class="btn btn-round"><i class="icon-plus"></i></a>
					<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>
								Nome
							</th>
							<th>
								Foto
							</th>
							<th>
								Opção
							</th>							
						</tr>
					</thead>
					<tbody>					
						<?php if (empty($postos_trabalho)) {?>
							<tr>
								<td colspan="3">
									Não há posto de trabalho associado ao registo de acidente
								</td>
							</tr>
						<?php } else {?>
							<?php foreach($postos_trabalho as $posto_trabalho) { ?>
								<tr>
									<td>
										<?=$posto_trabalho["nome"]?>
									</td>
									<td>
										<img style="height:50px" src="<?=$posto_trabalho['foto']?>">
									</td>
									<td>
										<a class="btn" href="ptrabalho.php?id=<?= $posto_trabalho["ptid"]?>"><i class="icon-eye-open" title="Ver detalhes do posto de trabalho"></i></a> 
									</td>
								</tr>
							<?php }?>
						<?php }?>
					</tbody>
				</table>
			</div>
		</div><!--/ fim box dados posto trabalho-->	
		</div>
		
				<div class="row-fluid"> <!--/ inicio box dados trabalhadores-->	
		
		<div class="box span12">
			<div class="box-header well" data-original-title>
				<a id="trabalhadores"></a><h2><i class="icon icon-user icon-black"></i> Trabalhadores</h2>
				<div class="box-icon">
					<a href="#myModalAdicionarTrabalhador" role="button" data-toggle="modal" title="Adicionar Trabalhador" class="btn btn-round"><i class="icon-plus"></i></a>
					
					<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>
								Nome
							</th>
							<th>
								Número Mecanográfico
							</th>
							<th>
								Opção
							</th>
						</tr>
					</thead>
					<tbody>				
					<?php if (empty($trabalhadores)) {?>
						<tr>
							<td colspan="3">
								Não há trabalhadores associados
							</td>
						</tr>
					<?php } else {?>
						<?php foreach($trabalhadores as $trabalhador) { ?>
							<tr>
								<td>
									<?=$trabalhador["nome"]?>
								</td>
								<td>
									<?=$trabalhador["num_mecanografico"]?>
								</td>
								<td>
									<a class="btn" href="trabalhador.php?id=<?= $trabalhador["tid"]?>"><i class="icon-eye-open" title="Ver detalhes do trabalhador"></i></a> 
								</td>
							</tr>
						<?php }?>
					<?php }?>
					</tbody>
				</table>
			</div>
		</div><!--/ fim box dados trabalhadores-->	

		<div class="row-fluid"><!--/ inicio box dados maquinas-->	
		
		<div class="box span12">
			<div class="box-header well" data-original-title>
				<a id="maquinas"></a><h2><i class="icon icon-wrench icon-black"></i> Máquinas</h2>
				<div class="box-icon">
					<a href="#myModalAdicionarMaquina" role="button" data-toggle="modal" title="Adicionar Máquina" class="btn btn-round"><i class="icon-plus"></i></a>
					<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>
								Nome
							</th>
							<th>
								Núm. Série
							</th>
							<th>
								Opção
							</th>
						</tr>
					</thead>
					<tbody>				
					<?php if (empty($maquinas)) {?>
						<tr>
							<td colspan="3">
								Não há máquinas associadas
							</td>
						</tr>
					<?php } else {?>
						<?php foreach($maquinas as $maquina) { ?>
							<tr>
								<td>
									<?=$maquina["nome"]?>
								</td>
								<td>
									<?=$maquina["num_serie"]?>
								</td>
								<td>
									<a class="btn" href="maquina.php?id=<?=$maquina["mid"]?>"><i class="icon-eye-open" title="Ver detalhes da máquina"></i></a> 
								</td>
							</tr>
						<?php }?>
					<?php }?>
					</tbody>
				</table>
			</div>
		</div><!--/box-->
				
		</div>
	</div><!--/span-->
</div><!--/row-->

<!-- Modal Seleccionar Trabalhador -->
<div id="myModalAdicionarTrabalhador" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">Adicionar Trabalhador</h3>
	</div>
	<div class="modal-body">
		<p>&nbsp;</p>
		<form action="associar_trabalhador_ao_regacidente.php" method="post" id="associar_trabalhador_ao_regacidente" class="form-horizontal" >
			<fieldset>
				<div class="control-group"> 
					<label class="control-label" for="tid">Seleccionar Trabalhador</label> 
					<div class="controls"> 
						<div class="pull-left">
								<?php if (empty($todos_trabalhadores)) { ?>
									 <input type="text" class="input-xlarge uneditable-input info" placeholder="Já todos trabalhadores pertencem a este registo acidente">
									 <input type="hidden" name="tid" value="0">
								<?php } else { ?>
									<select name="tid" id="sel_trabalhador" >
										<option value="0">- Trabalhadores -</option>
										<?php foreach ($todos_trabalhadores as $trabalhador) { ?>
											<option value="<?= $trabalhador['tid'] ?>"><?= $trabalhador['nome'] ?></option>
										<?php } ?>
									</select>
								<?php } ?>
						</div>
					</div>
				</div>
				<input type="hidden" name="ra_id" value="<?=$ra_id?>">
				<input type="hidden" name="ajax" value="true">
			</fieldset>
		</form>
	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
		<button id="associar_trabalhador_ao_regacidente-submit" type="submit" class="btn btn-primary" <?php if (empty($todos_trabalhadores)) echo "disabled" ?>>Adicionar</button>
	</div>
</div>

<!-- Modal Seleccionar Posto Trabalho -->
<div id="myModalAdicionarPtrabalho" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">Adicionar Posto Trabalho</h3>
	</div>
	<div class="modal-body">
		<p>&nbsp;</p>
		<form action="associar_ptrabalho_ao_regacidente.php" method="post" id="associar_ptrabalho_ao_regacidente" class="form-horizontal" >
			<fieldset>
				<div class="control-group"> 
					<label class="control-label" for="ptid">Seleccionar Posto Trabalho</label> 
					<div class="controls"> 
						<div class="pull-left">
								<?php if (empty($todos_ptrabalhos)) { ?>
									 <input type="text" class="input-xlarge uneditable-input info" placeholder="Já todos os postos de trabalho pertencem a este registo acidente">
									 <input type="hidden" name="ptid" value="0">
								<?php } else { ?>
									<select name="ptid" id="sel_ptrabalho" >
										<option value="0">- Postos Trabalho -</option>
										<?php foreach ($todos_ptrabalhos as $ptrabalho) { ?>
											<option value="<?= $ptrabalho['ptid'] ?>"><?= $ptrabalho['nome'] ?></option>
										<?php } ?>
									</select>
								<?php } ?>
						</div>
					</div>
				</div>
				<input type="hidden" name="ra_id" value="<?=$ra_id?>">
				<input type="hidden" name="ajax" value="true">
			</fieldset>
		</form>
	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
		<button id="associar_ptrabalho_ao_regacidente-submit" type="submit" class="btn btn-primary" <?php if (empty($todos_ptrabalhos)) echo "disabled" ?>>Adicionar</button>
	</div>
</div>

<!-- Modal Seleccionar Máquina -->
<div id="myModalAdicionarMaquina" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">Adicionar Máquina</h3>
	</div>
	<div class="modal-body">
		<p>&nbsp;</p>
		<form action="associar_maquina_ao_regacidente.php" method="post" id="associar_maquina_ao_regacidente" class="form-horizontal" >
			<fieldset>
				<div class="control-group"> 
					<label class="control-label" for="mid">Seleccionar Máquina </label> 
					<div class="controls"> 
						<div class="pull-left">
								<?php if (empty($todos_ptrabalhos)) { ?>
									 <input type="text" class="input-xlarge uneditable-input info" placeholder="Já todos as máquinas pertencem a este registo acidente">
									 <input type="hidden" name="mid" value="0">
								<?php } else { ?>
									<select name="mid" id="sel_ptrabalho" >
										<option value="0">- Máquinas -</option>
										<?php foreach ($todas_maquinas as $maquina) { ?>
											<option value="<?= $maquina['mid'] ?>"><?= $maquina['nome'] ?></option>
										<?php } ?>
									</select>
								<?php } ?>
						</div>
					</div>
				</div>
				<input type="hidden" name="ra_id" value="<?=$ra_id?>">
				<input type="hidden" name="ajax" value="true">
			</fieldset>
		</form>
	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
		<button id="associar_maquina_ao_regacidente-submit" type="submit" class="btn btn-primary" <?php if (empty($todos_ptrabalhos)) echo "disabled" ?>>Adicionar</button>
	</div>
</div>

