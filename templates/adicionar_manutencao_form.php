﻿<script src="js/jquery-ui-1.10.2.custom.min.js"></script>
<link rel="stylesheet" href="css/jquery-ui/jquery-ui-1.10.2.custom.css" />

  <script>
  $(function() {
    $( "#datepicker" ).datepicker({ dateFormat: "yy-mm-dd" });
  });
  </script>

<div class="row-fluid">
	<div class="span6 offset3">
		<form action="adicionar_manutencao.php" method="post" enctype="multipart/form-data" id="adicionar-manutencao" class="form-horizontal well" >
			<fieldset>
				<legend><?=$title?></legend>
								
				<div class="control-group"> 
					<label class="control-label" for="maq_manutencao_data_manutencao">Data Manutenção</label> 
					<div class="controls"> 
						<input autofocus type="date" name="maq_manutencao_data_manutencao"/> 
					</div>
				</div>

				<div class="control-group"> 
					<label class="control-label" for="maq_manutencao_relatorio_manutencao">Relatorio Manutenção</label>
					<div class="controls"> 
						<input type="file" name="maq_manutencao_relatorio_manutencao"/> 
					</div> 
				</div>

				<div class="control-group">
					<label class="control-label" for="maq_manutencao_data_prox_manutencao">Data Próxima Manutenção</label>
					<div class="controls"> 
						<input type="date" name="maq_manutencao_data_prox_manutencao"/> 
					</div> 
				</div>

				
				<input type="hidden" name="maquina-manutencao_eid" value="<?=$eid?>">
				
				<div class="form-actions">
					<button type="submit" class="btn btn-primary">Adicionar Manutenção de Máquina</button>
				</div>
								
			</fieldset>
		</form>
	</div>
</div>
