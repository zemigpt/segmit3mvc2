﻿<div class="row-fluid">	
	<div class="span2">
		<div class="box span12">
			<div class="box-header well" data-original-title>
				
			</div>
			<div>
			<ul class="nav nav-tabs nav-stacked">
				<li>
					<a href="#dadostrab">Dados do trabalhador</a>
				</li>
				<li>
					<a href="#forma1">Formação Detida</a>
				</li>
				<li>
					<a href="#postotrab">Posto de Trabalho</a>
				</li>
				<li>
					<a href="#maquinas">Máquinas associadas</a>
				</li>
				<li>
					<a href="#epi">EPI's</a>
				</li>
				<li>
					<a href="#forma2">Formação Requerida</a>
				</li>
				<li>
					<a href="#regacidente">Registo Acidentes</a>
				</li>
				<li>
					<a href="#riscos">Riscos Associados</a>
				</li>
			</ul>
			</div>
		</div>
	</div>
	<div class="span10">	
		<div class="row-fluid">	
		<div class="box span12">
			<div class="box-header well" data-original-title>
				<a id="dadostrab"></a><h2><i class="icon-user"></i> Dados do trabalhador</h2>
				<div class="box-icon">
					<a href="trabalhador.php?id=<?=$tid?>&action=edit" title="Editar Trabalhador" class="btn btn-round"><i class="icon-pencil"></i></a>
					<a href="trabalhador.php?id=<?=$tid?>&action=delete" title="Remover Trabalhador" class="btn btn-round"><i class="icon-remove"></i></a>
					<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">
				<table class="table table-striped">


					<thead>
						<tr>
							<th colspan='2'><p class="lead"><img style="height:50px" src="<?=$trabalhador['foto']?>"> <?=$trabalhador['nome']?></p></th>
						</tr>
					</thead>

					<tbody>
						<tr>
							<th width="33%">Num. Mecanográfico</th>
							<td width="67%"><?=$trabalhador['num_mecanografico']?></td>
						</tr>
						<tr>
							<th width="33%">Morada</th>
							<td width="67%"><?=$trabalhador['morada']?></td>
						</tr>
						<tr>
							<th>Cód. Postal</th>
							<td><?=$trabalhador['codpostal']?></td>
						</tr>
						<tr>
							<th>Localidade</th>
							<td><?=$trabalhador['localidade']?></td>
						</tr>
						<tr>
							<th>Telefone</th>
							<td><?=$trabalhador['telefone']?></td>
						</tr>
						<tr>
							<th>Email</th>
							<td><?=$trabalhador['email']?></td>
						</tr>
						<tr>
							<th>Nome Contacto de Emergência</th>
							<td><?=$trabalhador['contacto_sos_nome']?></td>
						</tr>
						<tr>
							<th>Tel. Contacto de Emergência</th>
							<td><?=$trabalhador['contacto_sos_telefone']?></td>
						</tr>
						<tr>
							<th>Documento de Identificação</th>
							<td><?=$trabalhador['bi']?></td>
						</tr>
						<tr>
							<th>Validade Doc. Ident.</th>
							<td><?=$trabalhador['bi_validade']?></td>
						</tr>
						<tr>
							<th>Num. Seg. Social</th>
							<td><?=$trabalhador['niss']?></td>
						</tr>
						<tr>
							<th>Núm Contribuinte</th>
							<td><?=$trabalhador['nif']?></td>
						</tr>
						<tr>
							<th>Apólice Seguro</th>
							<td><?=$trabalhador['apolice_seguro']?></td>
						</tr>
						<tr>
							<th>Tamanho Sapato</th>
							<td><?=$trabalhador['tamanho_sapato']?></td>
						</tr>
						<tr>
							<th>Tamanho Roupa</th>
							<td><?=$trabalhador['tamanho_roupa']?></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div><!--/box dados trabalhador-->
		</div>

		<div class="row-fluid"><!--/box formação detida-->
		
		<div class="box span12">
			<div class="box-header well" data-original-title>
				<a id="forma1"></a><h2><i class="icon icon-wrench icon-black"></i> Formação Detida</h2>
				<div class="box-icon">
					<a href="#myModalAdicionarFormacao" role="button" data-toggle="modal" title="Adicionar Formação" class="btn btn-round"><i class="icon-plus"></i></a>
					<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>
								Tema
							</th>
							<th>
								Núm. Horas
							</th>
							<th>
								Opção
							</th>
						</tr>
					</thead>
					<tbody>				
					<?php if (empty($formacaodetidas)) {?>
						<tr>
							<td colspan="3">
								Não há formações associadas
							</td>
						</tr>
					<?php } else {?>
						<?php foreach($formacaodetidas as $formacaodetida) { ?>
							<tr>
								<td>
									<?=$formacaodetida["tema"]?>
								</td>
								<td>
									<?=$formacaodetida["duracao"]?>
								</td>
								<td>
									<a class="btn" href="index.php?mid=<?=$formacaodetida["for_id"]?>"><i class="icon-eye-open" title="Ver detalhes da formação"></i></a> 
								</td>
							</tr>
						<?php }?>
					<?php }?>
					</tbody>
				</table>
			</div>
		</div><!--/box formação detida-->
		
		<div class="row-fluid">
		<div class="box span12">
			<div class="box-header well" data-original-title>
				<a id="postotrab"></a><h2><i class="icon-briefcase"></i> Posto de Trabalho</h2>
				<div class="box-icon">
					<a href="#myModalAdicionarPtrabalho" role="button" data-toggle="modal" title="Adicionar Posto de Trabalho" class="btn  btn-round"><i class="icon-plus"></i></a>
					<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>
								Nome
							</th>
							<th>
								Foto
							</th>
							<th>
								Opção
							</th>
						</tr>
					</thead>
					<tbody>					
						<?php if (empty($ptrabalhos)) {?>
							<tr>
								<td colspan="3">
									Não há posto de trabalho associado ao trabalhador
								</td>
							</tr>
						<?php } else {?>
							<?php foreach($ptrabalhos as $ptrabalho) { ?>
								<tr>
									<td>
										<?=$ptrabalho["nome"]?>
									</td>
									<td>
										<img style="height:50px" src="<?=$ptrabalho['foto']?>">
									</td>
									<td>
										<a class="btn" href="index.php?ptid=<?= $ptrabalho["ptid"]?>"><i class="icon-eye-open" title="Ver detalhes do posto de trabalho"></i></a> 
									</td>
								</tr>
							<?php }?>
						<?php }?>
					</tbody>
				</table>
			</div>
		</div><!--/box listagem de máquinas associadas ao trabalhador-->	
		</div>
		<div class="row-fluid">
		
		<div class="box span12">
			<div class="box-header well" data-original-title>
				<a id="maquinas"></a><h2><i class="icon icon-wrench icon-black"></i> Maquinas</h2>
				<div class="box-icon">
					<a href="#myModalAdicionarMaquina" role="button" data-toggle="modal" title="Adicionar Máquina" class="btn  btn-round"><i class="icon-plus"></i></a>
					<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>
								Nome
							</th>
							<th>
								Núm. Série
							</th>
							<th>
								Opção
							</th>
						</tr>
					</thead>
					<tbody>				
					<?php if (empty($maquinas)) {?>
						<tr>
							<td colspan="3">
								Não há máquinas associadas
							</td>
						</tr>
					<?php } else {?>
						<?php foreach($maquinas as $maquina) { ?>
							<tr>
								<td>
									<?=$maquina["nome"]?>
								</td>
								<td>
									<?=$maquina["num_serie"]?>
								</td>
								<td>
									<a class="btn" href="index.php?mid=<?=$maquina["mid"]?>"><i class="icon-eye-open" title="Ver detalhes da máquina"></i></a> 
								</td>
							</tr>
						<?php }?>
					<?php }?>
					</tbody>
				</table>
			</div>
		</div><!--/box-->
		
		<div class="row-fluid"><!--/ inicio box dados EPIs-->	
		
		<div class="box span12">
			<div class="box-header well" data-original-title>
				<a id="epi"></a><h2><i class="icon icon-th icon-black"></i> Equipamento Protecção Individual</h2>
				<div class="box-icon">
					<!-- não há adição de EPI's ao trabalhador, mas sim ao posto de trabalho -->
					<!-- <a href="#myModalAdicionarEPI" role="button" data-toggle="modal"  title="Adicionar EPI" class="btn btn-round"><i class="icon-plus"></i></a> -->
					<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>
								Nome
							</th>
						</tr>
					</thead>
					<tbody>				
					<?php if (empty($epis)) {?>
						<tr>
							<td colspan="1">
								Não há EPI's associados a este trabalhador
							</td>
						</tr>
					<?php } else {?>
						<?php foreach($epis as $epi) { ?>
							<tr>
								<td>
									<?=$epi["nome"]?>
								</td>
							</tr>
						<?php }?>
					<?php }?>
					</tbody>
				</table>
			</div>
		</div><!--/box final EPI's-->
		
		<div class="row-fluid"><!--/box formação requerida-->
		
		<div class="box span12">
			<div class="box-header well" data-original-title>
				<a id="forma2"></a><h2><i class="icon icon-wrench icon-black"></i> Formação Requerida</h2>
				<div class="box-icon">
					<!-- retirei a linha seguinte por achar que esta formação não deve ser adicionada neste campo, é apenas informativa -->
					<!-- <a href="adicionar_formacao.php?tid=<?=$tid?>" title="Adicionar Formação" class="btn btn-round"><i class="icon-plus"></i></a> --> 
					<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>
								Tema
							</th>
							<th>
								Núm. Horas
							</th>
							<th>
								Máquina Associada
							</th>
							<th>
								Opção
							</th>
						</tr>
					</thead>
					<tbody>				
					<?php if (empty($forrequeridas)) {?>
						<tr>
							<td colspan="4">
								Não há formações associadas
							</td>
						</tr>
					<?php } else {?>
						<?php foreach($forrequeridas as $forrequerida) { ?>
							<tr>
								<td>
									<?=$forrequerida["tema"]?>
								</td>
								<td>
									<?=$forrequerida["duracao"]?>
								</td>
								<td>
									<?=$forrequerida["maq_nome"]?>
								</td>
								<td>
									<a class="btn" href="index.php?for_id=<?=$forrequerida["for_id"]?>"><i class="icon-eye-open" title="Ver detalhes da formação"></i></a> 
								</td>
							</tr>
						<?php }?>
					<?php }?>
					</tbody>
				</table>
			</div>
		</div><!--/box formação detida-->
		
		<div class="row-fluid"><!--/box registo acidentes-->
		
		<div class="box span12">
			<div class="box-header well" data-original-title>
				<a id="regacidente"></a><h2><i class="icon icon-wrench icon-black"></i> Registo de Acidentes</h2>
				<div class="box-icon">
					<!-- anular a adição de registo de acidentes a partir do trabalhador!!! 
					<a href="adicionar_regacidente.php?tid=<?=$tid?>" title="Adicionar Registo Acidente" class="btn btn-round"><i class="icon-plus"></i></a> -->
					<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>
								Data
							</th>
							<th>
								Estado
							</th>
							<th>
								Opção
							</th>
						</tr>
					</thead>
					<tbody>				
					<?php if (empty($regacidentes)) {?>
						<tr>
							<td colspan="3">
								Não há registo de acidentes associados
							</td>
						</tr>
					<?php } else {?>
						<?php foreach($regacidentes as $regacidente) { ?>
							<tr>
								<td>
									<?=$regacidente["data"]?>
								</td>
								<td>
									<?=$regacidente["estado"]?>
								</td>
								
								<td>
									<a class="btn" href="index.php?ra_id=<?=$regacidente["ra_id"]?>"><i class="icon-eye-open" title="Ver detalhes da formação"></i></a> 
								</td>
							</tr>
						<?php }?>
					<?php }?>
					</tbody>
				</table>
			</div>
		</div><!--/box registo acidentes-->
		
		<div class="row-fluid"><!--/box riscos-->
		
		<div class="box span12">
			<div class="box-header well" data-original-title>
				<a id="riscos"></a><h2><i class="icon icon-wrench icon-black"></i> Riscos Associados ao Trabalhador</h2>
				<div class="box-icon">
					<!-- anular a adição de registo de acidentes a partir do trabalhador!!! 
					<a href="adicionar_regacidente.php?tid=<?=$tid?>" title="Adicionar Registo Acidente" class="btn btn-round"><i class="icon-plus"></i></a> -->
					<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>
								Perigo
							</th>
							<th>
								Risco
							</th>
							<th>
								Grau Perigosidade
							</th>
							<th>
								Medidas
							</th>
							<th>
								Opção
							</th>
						</tr>
					</thead>
					<tbody>				
					<?php if (empty($riscos)) {?>
						<tr>
							<td colspan="5">
								Não há registo de riscos associados
							</td>
						</tr>
					<?php } else {?>
						<?php foreach($riscos as $risco) { ?>
							<tr>
								<td>
									<?=$risco["perigo"]?>
								</td>
								<td>
									<?=$risco["risco"]?>
								</td>
								<td>
									<?=$risco["grau_perigosidade"]?>
								</td>
								<td>
									<?=$risco["medidas"]?>
								</td>
								<td>
									<a class="btn" href="avalrisco.php?id=<?=$risco["pta_id"]?>"><i class="icon-eye-open" title="Ver detalhes do risco"></i></a> 
								</td>
							</tr>
						<?php }?>
					<?php }?>
					</tbody>
				</table>
			</div>
		</div><!--/box riscos-->
		
		</div>
	</div><!--/span-->
</div><!--/row-->

<!-- Modal Seleccionar Formação -->
<div id="myModalAdicionarFormacao" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">Adicionar Formação</h3>
	</div>
	<div class="modal-body">
		<p>&nbsp;</p>
		<form action="associar_formacao_ao_trabalhador.php" method="post" id="associar_formacao_ao_trabalhador" class="form-horizontal" >
			<fieldset>
				<div class="control-group"> 
					<label class="control-label" for="for_id">Seleccionar Formação</label> 
					<div class="controls"> 
						<div class="pull-left">
								<?php if (empty($todas_formacoes)) { ?>
									 <input type="text" class="input-xlarge uneditable-input info" placeholder="Já todas formações pertencem a este trabalhador">
									 <input type="hidden" name="for_id" value="0">
								<?php } else { ?>
									<select name="for_id" id="sel_formacao" >
										<option value="0">- Formações -</option>
										<?php foreach ($todas_formacoes as $form) { ?>
											<option value="<?= $form['for_id'] ?>"><?= $form['tema'] ?></option>
										<?php } ?>
									</select>
								<?php } ?>
						</div>
					</div>
				</div>
				<input type="hidden" name="tid" value="<?=$tid?>">
				<input type="hidden" name="ajax" value="true">
			</fieldset>
		</form>
	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
		<button id="associar_formacao_ao_trabalhador-submit" type="submit" class="btn btn-primary" <?php if (empty($todas_formacoes)) echo "disabled" ?>>Adicionar</button>
	</div>
</div>

<!-- Modal Seleccionar Posto Trabalho -->
<div id="myModalAdicionarPtrabalho" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">Adicionar Posto Trabalho</h3>
	</div>
	<div class="modal-body">
		<p>&nbsp;</p>
		<form action="associar_ptrabalho_ao_trabalhador.php" method="post" id="associar_formacao_ao_trabalhador" class="form-horizontal" >
			<fieldset>
				<div class="control-group"> 
					<label class="control-label" for="for_id">Seleccionar Posto Trabalho</label> 
					<div class="controls"> 
						<div class="pull-left">
								<?php if (empty($todos_ptrabalhos)) { ?>
									 <input type="text" class="input-xlarge uneditable-input info" placeholder="Já todos os postos de trabalho pertencem a este trabalhador">
									 <input type="hidden" name="ptid" value="0">
								<?php } else { ?>
									<select name="ptid" id="sel_ptrabalho" >
										<option value="0">- Postos de Trabalho -</option>
										<?php foreach ($todos_ptrabalhos as $ptrabalho) { ?>
											<option value="<?= $ptrabalho['ptid'] ?>"><?= $ptrabalho['nome'] ?></option>
										<?php } ?>
									</select>
								<?php } ?>
						</div>
					</div>
				</div>
				<input type="hidden" name="tid" value="<?=$tid?>">
				<input type="hidden" name="ajax" value="true">
			</fieldset>
		</form>
	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
		<button id="associar_ptrabalho_ao_trabalhador-submit" type="submit" class="btn btn-primary" <?php if (empty($todos_ptrabalhos)) echo "disabled" ?>>Adicionar</button>
	</div>
</div>

<!-- Modal Seleccionar Máquina -->
<div id="myModalAdicionarMaquina" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">Adicionar Máquina</h3>
	</div>
	<div class="modal-body">
		<p>&nbsp;</p>
		<form action="associar_maquina_ao_trabalhador.php" method="post" id="associar_maquina_ao_trabalhador" class="form-horizontal" >
			<fieldset>
				<div class="control-group"> 
					<label class="control-label" for="mid">Seleccionar Máquina</label> 
					<div class="controls"> 
						<div class="pull-left">
								<?php if (empty($todas_maquinas)) { ?>
									 <input type="text" class="input-xlarge uneditable-input info" placeholder="Já todos as máquinas pertencem a este trabalhador">
									 <input type="hidden" name="mid" value="0">
								<?php } else { ?>
									<select name="mid" id="sel_maquina" >
										<option value="0">- Máquinas -</option>
										<?php foreach ($todas_maquinas as $maquina) { ?>
											<option value="<?= $maquina['mid'] ?>"><?= $maquina['nome'] ?></option>
										<?php } ?>
									</select>
								<?php } ?>
						</div>
					</div>
				</div>
				<input type="hidden" name="tid" value="<?=$tid?>">
				<input type="hidden" name="ajax" value="true">
			</fieldset>
		</form>
	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
		<button id="associar_maquina_ao_trabalhador-submit" type="submit" class="btn btn-primary" <?php if (empty($todas_maquinas)) echo "disabled" ?>>Adicionar</button>
	</div>
</div>



</div>

