﻿<div class="row-fluid">
	<div class="span6 offset3">
		<form action="ptrabalho.php" method="post" id="remover-ptrabalho" class="form well">
			<fieldset>
				<legend><?=$title?></legend>
				<div class="center">
					<br/>
					<h4>Tem a certeza que deseja remover o posto de trabalho</h4>
					<br/>
					<p><?=$ptrabalho['nome']?> ?</p>
				</div>

				<input type="hidden" name="posto_ptid" value="<?=$ptrabalho['ptid']?>">
				<input type="hidden" name="action" value="delete">
				
				<div class="form-actions center">
					<button type="submit" class="btn btn-danger">Sim, remover</button>
					<a href="#" class="btn" onclick="history.go(-1);">Cancelar</a>
				</div>					
			</fieldset>
		</form>
	</div>
</div>
