﻿<div class="row-fluid">
	<div class="span6 offset3">
		<form action="ptrabalho.php" method="post" enctype="multipart/form-data" id="editar-ptrabalho" class="form-horizontal well" >
			<fieldset>
				<legend><?=$title?></legend>
				
				<div class="control-group"> 
					<label class="control-label" for="posto_nome">Nome do Posto de Trabalho</label> 
					<div class="controls"> 
						<input autofocus type="text" name="posto_nome" value="<?=$ptrabalho['nome']?>"/>  
					</div> 
				</div>
				<div class="control-group"> 
					<label class="control-label" for="posto_foto">Fotografia</label> 
					<div class="controls"> 
						<img src="<?=$ptrabalho['foto']?>" height="100px"><br/>
						<input type="hidden" name="posto_foto_existente" value="<?=$ptrabalho['foto']?>">
						<input type="file" name="posto_foto"/> 
					</div> 
				</div>
				
				<div class="control-group"> 
					<label class="control-label" for="posto_descricao">Descrição</label> 
					<div class="controls"> 
						<input type="text" name="posto_descricao" value="<?=$ptrabalho['descricao']?>" />  
					</div> 
				</div>

				
				<input type="hidden" name="posto-trabalho_eid" value=" - <?=$_SESSION['cur_eid']?>">
				<input type="hidden" name="posto_ptid" value="<?=$ptrabalho['ptid']?>">
				<input type="hidden" name="action" value="edit">
				<div class="form-actions">
					<button type="submit" class="btn btn-primary">Gravar alterações</button>
					<a href="#" class="btn" onclick="history.go(-1);">Cancelar</a>
				</div>
								
			</fieldset>
		</form>
	</div>
</div>
