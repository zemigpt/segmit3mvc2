﻿<div class="row-fluid">
	<div class="span6 offset3">
		<form action="trabalhador.php" method="post" id="remover-trabalhador" class="form well">
			<fieldset>
				<legend><?=$title?></legend>
				<div class="center">
					<br/>
					<h4>Tem a certeza que deseja remover o trabalhador</h4>
					<br/>
					<p><?=$trabalhador['nome']?> ?</p>
				</div>

				<input type="hidden" name="trab_eid" value="<?=$eid?>">
				<input type="hidden" name="trab_id" value="<?=$trabalhador['tid']?>">
				<input type="hidden" name="action" value="delete">
				
				<div class="form-actions center">
					<button type="submit" class="btn btn-danger">Sim, remover</button>
					<a href="#" class="btn" onclick="history.go(-1);">Cancelar</a>
				</div>					
			</fieldset>
		</form>
	</div>
</div>
