﻿<div class="row-fluid">
	<div class="span6 offset3">
		<form action="avalrisco.php" method="post" id="remover-avaliacao risco" class="form well">
			<fieldset>
				<legend><?=$title?></legend>
				<div class="center">
					<br/>
					<h4>Tem a certeza que deseja remover a avaliação de risco?</h4>
					<br/>
					<p><strong>Risco:</strong> <?=$avalrisco['risco']?></p>
					<p><strong>Tarefa:</strong> <?=$avalrisco['tarefa']?></p>
					<p><strong>Perigo:</strong> <?=$avalrisco['perigo']?></p>
				</div>

				<input type="hidden" name="avalrisco_eid" value="<?=$eid?>">
				<input type="hidden" name="avalrisco_pta_id" value="<?=$avalrisco['pta_id']?>">
				<input type="hidden" name="action" value="delete">
				
				<div class="form-actions center">
					<button type="submit" class="btn btn-danger">Sim, remover</button>
					<a href="#" class="btn" onclick="history.go(-1);">Cancelar</a>
				</div>					
			</fieldset>
		</form>
	</div>
</div>
