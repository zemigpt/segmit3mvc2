<!-- jquery script adapted from Lecture 9m -->
<script src="/js/jquery-1.8.2.js"></script>
<script src="/js/jquery-ui-1.10.2.custom.min.js"></script>
<link rel="stylesheet" href="css/jquery-ui/jquery-ui-1.10.2.custom.css" />
<link rel="stylesheet" href="/js/tinyeditor/tinyeditor.css">
<script src="/js/tinyeditor/tiny.editor.packed.js"></script>

        <script>

            // onready
            $(document).ready(function() {
                              
                var tinyeditor = new TINY.editor.edit('editor',{
                id:'event-description', // (required) ID of the textarea
                width:550, // (optional) width of the editor
                height: 175,
	            cssclass: 'tinyeditor',
	            controlclass: 'tinyeditor-control',
	            rowclass: 'tinyeditor-header',
	            dividerclass: 'tinyeditor-divider',
	            controls: ['bold', 'italic', 'underline', 'strikethrough', '|',
		            'orderedlist', 'unorderedlist', '|', 'outdent', 'indent', '|', 'leftalign',
		            'centeralign', 'rightalign', 'blockjustify', '|', 'unformat', '|', 'undo', 'redo', 'n',
		            'font', 'size', 'style', '|', 'image', 'hr', 'link', 'unlink'],
	            footer: true,
	            fonts: ['Verdana','Arial','Georgia','Trebuchet MS'],
	            xhtml: true,
	            cssfile: 'custom.css',
	            bodyid: 'editor',
	            footerclass: 'tinyeditor-footer',
	            toggle: {text: 'source', activetext: 'wysiwyg', cssclass: 'toggle'},
	            resize: {cssclass: 'resize'}
            });

                             
                 

                          
                   
                     
                
                // onsubmit
                $('#addevent').submit(function() {

                    tinyeditor.post();

                    // validate form
                    if ($("#eventtypeselect").children("option").filter(":selected").val() == '')
                    {
                        alert('You must choose a type of event to log!');
                        return false;
                    }    
                    if ($('#addevent input[name=event_title]').val() == '' && $('#addevent textarea[name=event_description]').val() =='' )
                    {
                        alert("Event title and event description can't be both blank");
                        return false;
                    }
                   
                    
                    // valid!
                    return true;

                });

            });
            
            $(function() {
                $( "#datepicker" ).datepicker({ dateFormat: "yy-mm-dd" });
              });



            // var for add file function
            var fileid = 1;

            // add more pictures    
            $(function() {      
                $('#add-image').on('click', function(e){
                    fileid++;
                    var input = $('#event_image');
                    $(this).before('<input type="file" name="event_image[]" /><br/>');
                    
                    e.preventDefault();
                    
                });
            });

        </script>

<h2>Add Post</h2>
<br/>
<form action="add_event.php" method="post" id="addevent" class="form-horizontal" enctype="multipart/form-data">
    <fieldset>
        <div class="control-group">
            <label class="control-label" for="eventtypeselect">Category</label>
            <div class="controls">
                <select name="event_type" id="eventtypeselect">
                    <option value="" selected> - Select category - </option>
                    <?php for($i = 0; $i < count($eventtypes); $i++) { ?>
                        <option value="<?= $eventtypes[$i]['event_type'] ?>"><?= $eventtypes[$i]['event_type_description'] ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="datepicker">Date</label>
            <div class="controls">
                <input id="datepicker" name="event_date" placeholder="Date" type="text" value="<?= date('Y-m-d') ?>"/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="event-title">Title</label>
            <div class="controls">
                <input type="text" id="event-title" name="event_title" placeholder="Title">
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="event-description">Description</label>
            <div class="controls">
                <textarea id="event-description" name="event_description" rows="5" class="input-large"></textarea>
            </div>
        </div>
        
        <div class="control-group">
            <label class="control-label" for="event-title">Pictures</label>
            <div class="controls">
                <input type="file" name="event_image[]"/><br/>
                <a href="#" id="add-image"">Add another Picture</a>
            </div>
        </div>
        <br/>
        <div class="control-group">
            <button type="submit" class="btn btn-primary" onclick='tinyeditor.post()'>Add Post</button>
        </div>
    </fieldset>
</form>

