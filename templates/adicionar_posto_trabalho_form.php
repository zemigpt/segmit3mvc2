﻿
<div class="row-fluid">
	<div class="span6">
		<form action="adicionar_posto_trabalho.php" method="post" enctype="multipart/form-data" id="adicionar-trabalhador" class="form-horizontal well" >
			<fieldset>
				<legend><?=$title?></legend>
				
				<div class="control-group"> 
					<label class="control-label" for="posto_nome">Nome do Posto de Trabalho</label> 
					<div class="controls"> 
						<input autofocus type="text" name="posto_nome"/>  
					</div> 
				</div>
				<div class="control-group"> 
					<label class="control-label" for="posto_foto">Fotografia</label> 
					<div class="controls"> 
						<input type="file" name="posto_foto"/> 
					</div> 
				</div>
				
				<input type="hidden" name="posto-trabalho_eid" value="<?=$eid?>">
				<input type="hidden" name="posto-trabalho_tid" value="<?=$tid?>">
				<input type="hidden" name="posto-trabalho_mid" value="<?=$mid?>">
				<div class="form-actions">
					<button type="cancel" class="btn">Cancelar</button>
					<button type="submit" class="btn btn-primary">Adicionar Posto de Trabalho</button>					
				</div>
								
			</fieldset>
		</form>
	</div>
</div>


