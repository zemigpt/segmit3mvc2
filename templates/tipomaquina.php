<div class="row-fluid">	
	<div class="span2">
		<div class="box span12">
			<div class="box-header well" data-original-title>
				
			</div>
			<div>
			<ul class="nav nav-tabs nav-stacked">
				<li>
					<a href="#dadostm">Dados Tipo Máquina</a>
				</li>
				<li>
					<a href="#forma">Formação Específica</a>
				</li>
				
				
			</ul>
			</div>
		</div>
	</div>
	<div class="span10">	
		<div class="row-fluid">	<!--/ inicio box dados tipo máquina-->
		<div class="box span12">
			<div class="box-header well" data-original-title>
				<a id="dadostm"></a><h2><i class="icon-list"></i> Dados Tipo Máquina</h2>
				<div class="box-icon">
					<a href="tipomaquina.php?tm_id=<?=$tm_id?>&action=edit" title="Editar Tipo Máquina" class="btn btn-setting btn-round"><i class="icon-pencil"></i></a>
					<a href="tipomaquina.php?tm_id=<?=$tm_id?>&action=delete" title="Remover Tipo Máquina" class="btn btn-minimize btn-round"><i class="icon-remove"></i></a>
					<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">
				<table class="table table-striped">
					<tbody>					
							<tr>
								<th width="33%">Nome</th>
								<td width="67%"><?=$tipo_maquina['nome']?></td>
							</tr>
						</tbody>		

					
				</table>
			</div>
		</div><!--/ fim box dados posto trabalho-->
		</div>
		
		<div class="row-fluid"> <!--/ inicio box dados formação-->
		<div class="box span12">
			<div class="box-header well" data-original-title>
				<a id="forma"></a><h2><i class="icon-briefcase"></i> Formação Específica</h2>
				<div class="box-icon">
					<a href="#myModalAdicionarFormacao" role="button" data-toggle="modal" title="Adicionar Formação" class="btn btn-round"><i class="icon-plus"></i></a>
					<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>
								Tema
							</th>
							<th>
								Duração
							</th>
							<th>
								Opção
							</th>							
						</tr>
					</thead>
					<tbody>					
						<?php if (empty($formacoes)) {?>
							<tr>
								<td colspan="3">
									Não há formações associadas ao tipo de máquina
								</td>
							</tr>
						<?php } else {?>
							<?php foreach($formacoes as $formacao) { ?>
								<tr>
									<td>
										<?=$formacao["tema"]?>
									</td>
									<td>
										<?=$formacao["duracao"]?>
									</td>
									<td>
										<a class="btn" href="index.php?for_id=<?= $formacao["for_id"]?>"><i class="icon-eye-open" title="Ver detalhes da formação"></i></a> 
									</td>
								</tr>
							<?php }?>
						<?php }?>
					</tbody>
				</table>
			</div>
		</div><!--/ fim box dados posto trabalho-->	
		</div>
		
		
		</div>
	</div><!--/span-->
</div><!--/row-->

<!-- Modal Seleccionar Formação -->
<div id="myModalAdicionarFormacao" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">Adicionar Formação</h3>
	</div>
	<div class="modal-body">
		<p>&nbsp;</p>
		<form action="associar_formacao_ao_tipomaquina.php" method="post" id="associar_formacao_ao_tipomaquina" class="form-horizontal" >
			<fieldset>
				<div class="control-group"> 
					<label class="control-label" for="for_id">Seleccionar Formação</label> 
					<div class="controls"> 
						<div class="pull-left">
								<?php if (empty($todas_formacoes)) { ?>
									 <input type="text" class="input-xlarge uneditable-input info" placeholder="Já todas formações pertencem a este tipo de máquina">
									 <input type="hidden" name="for_id" value="0">
								<?php } else { ?>
									<select name="for_id" id="sel_formacao" >
										<option value="0">- Formações -</option>
										<?php foreach ($todas_formacoes as $form) { ?>
											<option value="<?= $form['for_id'] ?>"><?= $form['tema'] ?></option>
										<?php } ?>
									</select>
								<?php } ?>
						</div>
					</div>
				</div>
				<input type="hidden" name="tm_id" value="<?=$tm_id?>">
				<input type="hidden" name="ajax" value="true">
			</fieldset>
		</form>
	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
		<button id="associar_formacao_ao_tipomaquina-submit" type="submit" class="btn btn-primary" <?php if (empty($todas_formacoes)) echo "disabled" ?>>Adicionar</button>
	</div>
</div>

