﻿<div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><a href="#" class="btn-minimize"><i class="icon-chevron-up"></i></a> <i class="icon-user"></i> Tipo Máquina</h2>
			<div class="box-icon">
				<a href="adicionar_tipo_maquina.php?eid=<?=$eid?>" title="Adicionar Tipo Máquina" class="btn btn-setting btn-round"><i class="icon-plus"></i></a>
			 
			</div>
		</div>
		<div class="box-content">
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>Nome</th>
						
						<th>Opções</th>
					</tr>
				</thead>

				<tbody>
					<?php if (empty($tipo_maquinas)) echo '<tr><td colspan="3">Esta empresa não tem possui tipo de máquinas associadas</td></tr>'; ?>
						<?php foreach ($tipo_maquinas as $tipomaquina): ?>				
						<tr>
							<td><?= $tipomaquina['nome'] ?></td>
							
							<td><a class="btn" href="index.php?tm_id=<?= $tipomaquina["tm_id"]?>"><i class="icon-eye-open" title="Ver detalhes do tipo de máquina"></i></a> <a class="btn" href="#"  title="Editar tipo de máquina"><i class="icon-pencil"></i></a></td>
						</tr>
						<? endforeach ?>
				</tbody>
			</table>
		</div>
	</div><!--/span-->
</div><!--/row-->

