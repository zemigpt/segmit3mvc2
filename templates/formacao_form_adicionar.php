﻿<div class="row-fluid">
	<div class="span6 offset3">
		<form action="formacao.php" method="post" enctype="multipart/form-data" id="adicionar-formacao" class="form-horizontal well" >
			<fieldset>
				<legend><?=$title?></legend>
				
				<div class="control-group"> 
					<label class="control-label" for="formacao_tema">Tema</label> 
					<div class="controls"> 
						<input type="text" name="formacao_tema"/>  
					</div> 
				</div>

				<div class="control-group"> 
					<label class="control-label" for="formacao_duracao">Duracao</label>
					<div class="controls"> 
						<input type="text" name="formacao_duracao"/>  
					</div> 
				</div>

				<div class="control-group"> 
					<label class="control-label" for="formacao_entidade_formadora">Entidade Formadora</label>
					<div class="controls"> 
						<input type="text" name="formacao_entidade_formadora"/> 
					</div> 
				</div>
				
				<div class="control-group"> 
					<label class="control-label" for="formacao_relatorio">Relatorio</label> 
					<div class="controls"> 
						<input type="file" name="formacao_relatorio"/>  
					</div> 
				</div>

				<input type="hidden" name="formacao_eid" value="<?=$eid?>">
				<input type="hidden" name="action" value="add">
				
				<div class="form-actions">
					<button type="submit" class="btn btn-primary">Adicionar formação</button>
					<a href="#" class="btn" onclick="history.go(-1);">Cancelar</a>
				</div>
								
			</fieldset>
		</form>
	</div>
</div>
