﻿<div class="row-fluid">
	<div class="span6 offset3">
		<form action="maquina.php" method="post" id="remover-maquina" class="form well">
			<fieldset>
				<legend><?=$title?></legend>
				<div class="center">
					<br/>
					<h4>Tem a certeza que deseja remover a máquina</h4>
					<br/>
					<p><?=$maquina['nome']?> ?</p>
				</div>

				<input type="hidden" name="maq_eid" value="<?=$eid?>">
				<input type="hidden" name="maq_mid" value="<?=$maquina['mid']?>">
				<input type="hidden" name="action" value="delete">
				
				<div class="form-actions center">
					<button type="submit" class="btn btn-danger">Sim, remover</button>
					<a href="#" class="btn" onclick="history.go(-1);">Cancelar</a>
				</div>					
			</fieldset>
		</form>
	</div>
</div>
