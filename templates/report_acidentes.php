<div class="row-fluid">	
		<div class="box span3">
			<div class="box-header well" data-original-title>
				<h2><i class="icon-fire"></i> Acidentes por ano</h2>
				<div class="box-icon">
					<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>Ano</th>
							<th>Num. Acidentes</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($acidentesporano as $linha) { ?>
						<tr>
							<td><?= $linha['ano'] ?></td>
							<td><?= $linha['acidentes'] ?></td>
						</tr>
						<?php }?>
					</tbody>
				</table>
			</div>
		</div>

		<div class="box span4">
			<div class="box-header well" data-original-title>
				<h2><i class="icon-fire"></i> Acidentes por Posto de Trabalho</h2>
				<div class="box-icon">
					<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>Posto Trabalho</th>
							<th>Num. Acidentes</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($acidentesporpostotrabalho as $linha) { ?>
						<tr>
							<td><?= $linha['posto'] ?></td>
							<td><?= $linha['acidentes'] ?></td>
						</tr>
						<?php }?>
					</tbody>
				</table>
			</div>
		</div>
		
		<div class="box span5">
			<div class="box-header well" data-original-title>
				<h2><i class="icon-fire"></i> Acidentes por Trabalhador</h2>
				<div class="box-icon">
					<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				</div>
			</div>
			<div class="box-content">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>Trabalhador</th>
							<th>Num. Acidentes</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($acidentesportrabalhador as $linha) { ?>
						<tr>
							<td><?= $linha['trabalhador'] ?></td>
							<td><?= $linha['acidentes'] ?></td>
						</tr>
						<?php }?>
					</tbody>
				</table>
			</div>
		</div>
</div>