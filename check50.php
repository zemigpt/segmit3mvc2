<?php

    // configuration
    require_once("includes/config.php");

	
	
	/************************************************************** 
	 *
	 * Trata dos casos em que o ficheiro é usado com $_POST
	 *
	 *************************************************************/
	 
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        
		// verifica se estamos a editar um trabalhador existente ou a adicionar um novo
		if ($_POST['action']=="edit")
		{
			if (isset($_POST['check50_id']))
			{
				
				/*
				$upload_result = UploadFile($_FILES["formacao_relatorio"], "pdf");
				if ($upload_result != "ERR")
				{
					$relatorio_url = $upload_result;
				}
				else
				{
					$relatorio_url = "";
				}			
				
				// se o upload de foto nova falhou, continua a usar o existente
				if  (!empty($_FILES["formacao_relatorio"]) && $relatorio_url=="" && !empty($_POST['formacao_relatorio']))
					$relatorio_url = $_POST['formacao_relatorio'];
				*/
				
				$sql = "UPDATE check50
						SET check50_1 =?, check50_1_obs =?, check50_2 =?, check50_2_obs =?, check50_3 =?, check50_3_obs =?,  check50_4 =?, check50_4_obs =?, check50_5 =?, check50_5_obs =?, check50_6 =?, check50_6_obs =?, check50_7 =?, check50_7_obs =?, check50_8 =?, check50_8_obs =?, check50_9 =?, check50_9_obs =?, check50_10 =?, check50_10_obs =?, check50_11 =?, check50_11_obs =?, check50_12 =?, check50_12_obs =?, check50_13 =?, check50_13_obs =?, check50_14 =?, check50_14_obs =?, check50_15 =?, check50_15_obs =?, check50_16 =?, check50_16_obs =?, check50_17 =?, check50_17_obs =?, check50_18 =?, check50_18_obs =?, check50_19 =?, check50_19_obs =?, check50_20 =?, check50_20_obs =?, check50_21 =?, check50_21_obs =?, check50_22 =?, check50_22_obs =?, check50_23 =?, check50_23_obs =?, check50_24 =?, check50_24_obs =?, check50_25 =?, check50_25_obs =?, check50_26 =?, check50_26_obs =?, check50_27 =?, check50_27_obs =?, check50_28 =?, check50_28_obs =?, check50_29 =?, check50_29_obs =?, check50_30 =?, check50_30_obs =?, check50_31 =?, check50_31_obs =?, check50_33 =?, check50_33_obs =?, check50_34 =?, check50_34_obs =?, check50_35 =?, check50_35_obs =?, check50_36 =?, check50_36_obs =?, check50_37 =?, check50_37_obs =?, check50_38 =?, check50_38_obs =?, check50_39 =?, check50_39_obs =?, check50_40 =?, check50_40_obs =?, check50_41 =?, check50_41_obs =?, check50_42 =?, check50_42_obs =?, check50_43 =?, check50_43_obs =?, check50_44 =?, check50_44_obs =?, check50_45 =?, check50_45_obs =?, check50_46 =?, check50_46_obs =?, check50_47 =?, check50_47_obs =?, check50_48 =?, check50_48_obs =?, check50_49 =?, check50_49_obs =?, check50_50 =?, check50_50_obs =?, check50_51 =?, check50_51_obs =?, check50_52 =?, check50_52_obs =?, check50_53 =?, check50_53_obs =?, check50_54 =?, check50_54_obs =?, check50_55 =?, check50_55_obs =?, check50_56 =?, check50_56_obs =?, check50_57 =?, check50_57_obs =?, check50_58 =?, check50_58_obs =?, check50_59 =?, check50_59_obs =?, check50_60 =?, check50_60_obs =?, check50_61 =?, check50_61_obs =?, check50_62 =?, check50_62_obs =?, check50_63 =?, check50_63_obs =?, check50_64 =?, check50_64_obs =?, check50_65 =?, check50_65_obs =?, check50_66 =?, check50_66_obs =?, check50_67 =?, check50_67_obs =?, check50_68 =?, check50_68_obs =?, check50_69 =?, check50_69_obs =?, check50_70 =?, check50_70_obs =?, check50_71 =?, check50_71_obs =?, check50_72 =?, check50_72_obs =?, check50_73 =?, check50_73_obs =?, check50_74 =?, check50_74_obs =?, check50_75 =?, check50_75_obs =?, check50_76 =?, check50_76_obs =?, check50_77 =?, check50_77_obs =?, check50_78 =?, check50_78_obs =?, check50_79 =?, check50_79_obs =?, check50_80 =?, check50_80_obs =?, check50_81 =?, check50_81_obs =?, check50_82 =?, check50_82_obs =?, check50_83 =?, check50_83_obs =?, check50_84 =?, check50_84_obs =?, check50_85 =?, check50_85_obs =?, check50_86 =?, check50_86_obs =?, check50_87 =?, check50_87_obs =?, check50_88 =?, check50_88_obs =?, check50_89 =?, check50_89_obs =?, check50_90 =?, check50_90_obs =?, check50_91 =?, check50_91_obs =?, check50_92 =?, check50_92_obs =?, check50_93 =?, check50_93_obs =?, check50_94 =?, check50_94_obs =?, check50_95 =?, check50_95_obs =?, check50_96 =?, check50_96_obs =?, check50_97 =?, check50_97_obs =?, check50_98 =?, check50_98_obs =?, check50_99 =?, check50_99_obs =?, check50_100 =?, check50_100_obs =?, check50_101 =?, check50_101_obs =?, check50_102 =?, check50_102_obs =?, check50_103 =?, check50_103_obs =?, check50_104 =?, check50_104_obs =?, check50_105 =?, check50_105_obs =?
						WHERE check50_id = ? AND eid = ?";
				
					
				if (query($sql, $_POST['check50_1'] , $_POST['check50_1_obs'], $_POST['check50_2'] , $_POST['check50_2_obs'], $_POST['check50_3'] , $_POST['check50_3_obs'], $_POST['check50_4'] , $_POST['check50_4_obs'], $_POST['check50_5'] , $_POST['check50_5_obs'], $_POST['check50_6'] , $_POST['check50_6_obs'], $_POST['check50_7'] , $_POST['check50_7_obs'], $_POST['check50_8'] , $_POST['check50_8_obs'], $_POST['check50_9'] , $_POST['check50_9_obs'], $_POST['check50_10'] , $_POST['check50_10_obs'], $_POST['check50_11'] , $_POST['check50_11_obs'], $_POST['check50_12'] , $_POST['check50_12_obs'], $_POST['check50_13'] , $_POST['check50_13_obs'], $_POST['check50_14'] , $_POST['check50_14_obs'], $_POST['check50_15'] , $_POST['check50_15_obs'], $_POST['check50_16'] , $_POST['check50_16_obs'], $_POST['check50_17'] , $_POST['check50_17_obs'], $_POST['check50_18'] , $_POST['check50_18_obs'], $_POST['check50_19'] , $_POST['check50_19_obs'], $_POST['check50_20'] , $_POST['check50_20_obs'], $_POST['check50_21'] , $_POST['check50_21_obs'], $_POST['check50_22'] , $_POST['check50_22_obs'], $_POST['check50_23'] , $_POST['check50_23_obs'], $_POST['check50_24'] , $_POST['check50_24_obs'], $_POST['check50_25'] , $_POST['check50_25_obs'], $_POST['check50_26'] , $_POST['check50_26_obs'], $_POST['check50_27'] , $_POST['check50_27_obs'], $_POST['check50_28'] , $_POST['check50_28_obs'], $_POST['check50_29'] , $_POST['check50_29_obs'], $_POST['check50_30'] , $_POST['check50_30_obs'], $_POST['check50_31'] , $_POST['check50_31_obs'], $_POST['check50_33'] , $_POST['check50_33_obs'], $_POST['check50_34'] , $_POST['check50_34_obs'], $_POST['check50_35'] , $_POST['check50_35_obs'], $_POST['check50_36'] , $_POST['check50_36_obs'], $_POST['check50_37'] , $_POST['check50_37_obs'], $_POST['check50_38'] , $_POST['check50_38_obs'], $_POST['check50_39'] , $_POST['check50_39_obs'], $_POST['check50_40'] , $_POST['check50_40_obs'], $_POST['check50_41'] , $_POST['check50_41_obs'], $_POST['check50_42'] , $_POST['check50_42_obs'], $_POST['check50_43'] , $_POST['check50_43_obs'], $_POST['check50_44'] , $_POST['check50_44_obs'], $_POST['check50_45'] , $_POST['check50_45_obs'], $_POST['check50_46'] , $_POST['check50_46_obs'], $_POST['check50_47'] , $_POST['check50_47_obs'], $_POST['check50_48'] , $_POST['check50_48_obs'], $_POST['check50_49'] , $_POST['check50_49_obs'], $_POST['check50_50'] , $_POST['check50_50_obs'], $_POST['check50_51'] , $_POST['check50_51_obs'], $_POST['check50_52'] , $_POST['check50_52_obs'], $_POST['check50_53'] , $_POST['check50_53_obs'], $_POST['check50_54'] , $_POST['check50_54_obs'], $_POST['check50_55'] , $_POST['check50_55_obs'], $_POST['check50_56'] , $_POST['check50_56_obs'], $_POST['check50_57'] , $_POST['check50_57_obs'], $_POST['check50_58'] , $_POST['check50_58_obs'], $_POST['check50_59'] , $_POST['check50_59_obs'], $_POST['check50_60'] , $_POST['check50_60_obs'], $_POST['check50_61'] , $_POST['check50_61_obs'], $_POST['check50_62'] , $_POST['check50_62_obs'], $_POST['check50_63'] , $_POST['check50_63_obs'], $_POST['check50_64'] , $_POST['check50_64_obs'], $_POST['check50_65'] , $_POST['check50_65_obs'], $_POST['check50_66'] , $_POST['check50_66_obs'], $_POST['check50_67'] , $_POST['check50_67_obs'], $_POST['check50_68'] , $_POST['check50_68_obs'], $_POST['check50_69'] , $_POST['check50_69_obs'], $_POST['check50_70'] , $_POST['check50_70_obs'], $_POST['check50_71'] , $_POST['check50_71_obs'], $_POST['check50_72'] , $_POST['check50_72_obs'], $_POST['check50_73'] , $_POST['check50_73_obs'], $_POST['check50_74'] , $_POST['check50_74_obs'], $_POST['check50_75'] , $_POST['check50_75_obs'], $_POST['check50_76'] , $_POST['check50_76_obs'], $_POST['check50_77'] , $_POST['check50_77_obs'], $_POST['check50_78'] , $_POST['check50_78_obs'], $_POST['check50_79'] , $_POST['check50_79_obs'], $_POST['check50_80'] , $_POST['check50_80_obs'], $_POST['check50_81'] , $_POST['check50_81_obs'], $_POST['check50_82'] , $_POST['check50_82_obs'], $_POST['check50_83'] , $_POST['check50_83_obs'], $_POST['check50_84'] , $_POST['check50_84_obs'], $_POST['check50_85'] , $_POST['check50_85_obs'], $_POST['check50_86'] , $_POST['check50_86_obs'], $_POST['check50_87'] , $_POST['check50_87_obs'], $_POST['check50_88'] , $_POST['check50_88_obs'], $_POST['check50_89'] , $_POST['check50_89_obs'], $_POST['check50_90'] , $_POST['check50_90_obs'], $_POST['check50_91'] , $_POST['check50_91_obs'], $_POST['check50_92'] , $_POST['check50_92_obs'], $_POST['check50_93'] , $_POST['check50_93_obs'], $_POST['check50_94'] , $_POST['check50_94_obs'], $_POST['check50_95'] , $_POST['check50_95_obs'], $_POST['check50_96'] , $_POST['check50_96_obs'], $_POST['check50_97'] , $_POST['check50_97_obs'], $_POST['check50_98'] , $_POST['check50_98_obs'], $_POST['check50_99'] , $_POST['check50_99_obs'], $_POST['check50_100'] , $_POST['check50_100_obs'], $_POST['check50_101'] , $_POST['check50_101_obs'], $_POST['check50_102'] , $_POST['check50_102_obs'], $_POST['check50_103'] , $_POST['check50_103_obs'], $_POST['check50_104'] , $_POST['check50_104_obs'], $_POST['check50_105'] , $_POST['check50_105_obs'], $_POST['check50_id'], $_SESSION['cur_eid'] ) === false)
				{
					apologize("Algo correu mal ao editar a checklist DL50 na base de dados.");
				}
				else
				{

									
					// redirect to homepage
					redirect("check50.php?id=" . $_POST['check50_id']);
					
				}
			}
			// não temos for_id para identificar o trabalhador - aborta
			else
				apologize("Checklist não identificada - impossível editar");
			
		
		}
		elseif 	($_POST['action']=="add")
		{
			/*
			$upload_result = UploadFile($_FILES["formacao_relatorio"], "pdf");
			if ($upload_result != "ERR")
			{
				$relatorio_url = $upload_result;
			}
			else
			{
				$relatorio_url = "";
			}				
            */
			
            $mid = $_POST['mid'];		
			
            $sql = "INSERT INTO check50 
					(check50_1, check50_1_obs, check50_2, check50_2_obs, check50_3, check50_3_obs, check50_4, check50_4_obs, check50_5, check50_5_obs, check50_6, check50_6_obs, check50_7, check50_7_obs, check50_8, check50_8_obs, check50_9, check50_9_obs, check50_10, check50_10_obs, check50_11, check50_11_obs, check50_12, check50_12_obs, check50_13, check50_13_obs, check50_14, check50_14_obs, check50_15, check50_15_obs, check50_16, check50_16_obs, check50_17, check50_17_obs, check50_18, check50_18_obs, check50_19, check50_19_obs, check50_20, check50_20_obs, check50_21, check50_21_obs, check50_22, check50_22_obs, check50_23, check50_23_obs, check50_24, check50_24_obs, check50_25, check50_25_obs, check50_26, check50_26_obs, check50_27, check50_27_obs, check50_28, check50_28_obs, check50_29, check50_29_obs, check50_30, check50_30_obs, check50_31, check50_31_obs, check50_33, check50_33_obs, check50_34, check50_34_obs, check50_35, check50_35_obs, check50_36, check50_36_obs, check50_37, check50_37_obs, check50_38, check50_38_obs, check50_39, check50_39_obs, check50_40, check50_40_obs, check50_41, check50_41_obs, check50_42, check50_42_obs, check50_43, check50_43_obs, check50_44, check50_44_obs, check50_45, check50_45_obs, check50_46, check50_46_obs, check50_47, check50_47_obs, check50_48, check50_48_obs, check50_49, check50_49_obs, check50_50, check50_50_obs, check50_51, check50_51_obs, check50_52, check50_52_obs, check50_53, check50_53_obs, check50_54, check50_54_obs, check50_55, check50_55_obs, check50_56, check50_56_obs, check50_57, check50_57_obs, check50_58, check50_58_obs, check50_59, check50_59_obs, check50_60, check50_60_obs, check50_61, check50_61_obs, check50_62, check50_62_obs, check50_63, check50_63_obs, check50_64, check50_64_obs, check50_65, check50_65_obs, check50_66, check50_66_obs, check50_67, check50_67_obs, check50_68, check50_68_obs, check50_69, check50_69_obs, check50_70, check50_70_obs, check50_71, check50_71_obs, check50_72, check50_72_obs, check50_73, check50_73_obs, check50_74, check50_74_obs, check50_75, check50_75_obs, check50_76, check50_76_obs, check50_77, check50_77_obs, check50_78, check50_78_obs, check50_79, check50_79_obs, check50_80, check50_80_obs, check50_81, check50_81_obs, check50_82, check50_82_obs, check50_83, check50_83_obs, check50_84, check50_84_obs, check50_85, check50_85_obs, check50_86, check50_86_obs, check50_87, check50_87_obs, check50_88, check50_88_obs, check50_89, check50_89_obs, check50_90, check50_90_obs, check50_91, check50_91_obs, check50_92, check50_92_obs, check50_93, check50_93_obs, check50_94, check50_94_obs, check50_95, check50_95_obs, check50_96, check50_96_obs, check50_97, check50_97_obs, check50_98, check50_98_obs, check50_99, check50_99_obs, check50_100, check50_100_obs, check50_101, check50_101_obs, check50_102, check50_102_obs, check50_103, check50_103_obs, check50_104, check50_104_obs, check50_105, check50_105_obs, mid, eid)
					VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";           
            
          	if (query($sql, $_POST['check50_1'] , $_POST['check50_1_obs'], $_POST['check50_2'] , $_POST['check50_2_obs'], $_POST['check50_3'] , $_POST['check50_3_obs'], $_POST['check50_4'] , $_POST['check50_4_obs'], $_POST['check50_5'] , $_POST['check50_5_obs'], $_POST['check50_6'] , $_POST['check50_6_obs'], $_POST['check50_7'] , $_POST['check50_7_obs'], $_POST['check50_8'] , $_POST['check50_8_obs'], $_POST['check50_9'] , $_POST['check50_9_obs'], $_POST['check50_10'] , $_POST['check50_10_obs'], $_POST['check50_11'] , $_POST['check50_11_obs'], $_POST['check50_12'] , $_POST['check50_12_obs'], $_POST['check50_13'] , $_POST['check50_13_obs'], $_POST['check50_14'] , $_POST['check50_14_obs'], $_POST['check50_15'] , $_POST['check50_15_obs'], $_POST['check50_16'] , $_POST['check50_16_obs'], $_POST['check50_17'] , $_POST['check50_17_obs'], $_POST['check50_18'] , $_POST['check50_18_obs'], $_POST['check50_19'] , $_POST['check50_19_obs'], $_POST['check50_20'] , $_POST['check50_20_obs'], $_POST['check50_21'] , $_POST['check50_21_obs'], $_POST['check50_22'] , $_POST['check50_22_obs'], $_POST['check50_23'] , $_POST['check50_23_obs'], $_POST['check50_24'] , $_POST['check50_24_obs'], $_POST['check50_25'] , $_POST['check50_25_obs'], $_POST['check50_26'] , $_POST['check50_26_obs'], $_POST['check50_27'] , $_POST['check50_27_obs'], $_POST['check50_28'] , $_POST['check50_28_obs'], $_POST['check50_29'] , $_POST['check50_29_obs'], $_POST['check50_30'] , $_POST['check50_30_obs'], $_POST['check50_31'] , $_POST['check50_31_obs'], $_POST['check50_33'] , $_POST['check50_33_obs'], $_POST['check50_34'] , $_POST['check50_34_obs'], $_POST['check50_35'] , $_POST['check50_35_obs'], $_POST['check50_36'] , $_POST['check50_36_obs'], $_POST['check50_37'] , $_POST['check50_37_obs'], $_POST['check50_38'] , $_POST['check50_38_obs'], $_POST['check50_39'] , $_POST['check50_39_obs'], $_POST['check50_40'] , $_POST['check50_40_obs'], $_POST['check50_41'] , $_POST['check50_41_obs'], $_POST['check50_42'] , $_POST['check50_42_obs'], $_POST['check50_43'] , $_POST['check50_43_obs'], $_POST['check50_44'] , $_POST['check50_44_obs'], $_POST['check50_45'] , $_POST['check50_45_obs'], $_POST['check50_46'] , $_POST['check50_46_obs'], $_POST['check50_47'] , $_POST['check50_47_obs'], $_POST['check50_48'] , $_POST['check50_48_obs'], $_POST['check50_49'] , $_POST['check50_49_obs'], $_POST['check50_50'] , $_POST['check50_50_obs'], $_POST['check50_51'] , $_POST['check50_51_obs'], $_POST['check50_52'] , $_POST['check50_52_obs'], $_POST['check50_53'] , $_POST['check50_53_obs'], $_POST['check50_54'] , $_POST['check50_54_obs'], $_POST['check50_55'] , $_POST['check50_55_obs'], $_POST['check50_56'] , $_POST['check50_56_obs'], $_POST['check50_57'] , $_POST['check50_57_obs'], $_POST['check50_58'] , $_POST['check50_58_obs'], $_POST['check50_59'] , $_POST['check50_59_obs'], $_POST['check50_60'] , $_POST['check50_60_obs'], $_POST['check50_61'] , $_POST['check50_61_obs'], $_POST['check50_62'] , $_POST['check50_62_obs'], $_POST['check50_63'] , $_POST['check50_63_obs'], $_POST['check50_64'] , $_POST['check50_64_obs'], $_POST['check50_65'] , $_POST['check50_65_obs'], $_POST['check50_66'] , $_POST['check50_66_obs'], $_POST['check50_67'] , $_POST['check50_67_obs'], $_POST['check50_68'] , $_POST['check50_68_obs'], $_POST['check50_69'] , $_POST['check50_69_obs'], $_POST['check50_70'] , $_POST['check50_70_obs'], $_POST['check50_71'] , $_POST['check50_71_obs'], $_POST['check50_72'] , $_POST['check50_72_obs'], $_POST['check50_73'] , $_POST['check50_73_obs'], $_POST['check50_74'] , $_POST['check50_74_obs'], $_POST['check50_75'] , $_POST['check50_75_obs'], $_POST['check50_76'] , $_POST['check50_76_obs'], $_POST['check50_77'] , $_POST['check50_77_obs'], $_POST['check50_78'] , $_POST['check50_78_obs'], $_POST['check50_79'] , $_POST['check50_79_obs'], $_POST['check50_80'] , $_POST['check50_80_obs'], $_POST['check50_81'] , $_POST['check50_81_obs'], $_POST['check50_82'] , $_POST['check50_82_obs'], $_POST['check50_83'] , $_POST['check50_83_obs'], $_POST['check50_84'] , $_POST['check50_84_obs'], $_POST['check50_85'] , $_POST['check50_85_obs'], $_POST['check50_86'] , $_POST['check50_86_obs'], $_POST['check50_87'] , $_POST['check50_87_obs'], $_POST['check50_88'] , $_POST['check50_88_obs'], $_POST['check50_89'] , $_POST['check50_89_obs'], $_POST['check50_90'] , $_POST['check50_90_obs'], $_POST['check50_91'] , $_POST['check50_91_obs'], $_POST['check50_92'] , $_POST['check50_92_obs'], $_POST['check50_93'] , $_POST['check50_93_obs'], $_POST['check50_94'] , $_POST['check50_94_obs'], $_POST['check50_95'] , $_POST['check50_95_obs'], $_POST['check50_96'] , $_POST['check50_96_obs'], $_POST['check50_97'] , $_POST['check50_97_obs'], $_POST['check50_98'] , $_POST['check50_98_obs'], $_POST['check50_99'] , $_POST['check50_99_obs'], $_POST['check50_100'] , $_POST['check50_100_obs'], $_POST['check50_101'] , $_POST['check50_101_obs'], $_POST['check50_102'] , $_POST['check50_102_obs'], $_POST['check50_103'] , $_POST['check50_103_obs'], $_POST['check50_104'] , $_POST['check50_104_obs'], $_POST['check50_105'] , $_POST['check50_105_obs'], $mid, $_SESSION['cur_eid'] ) === false)
            {
                apologize("Algo correu mal ao inserir a nova formação na base de dados.");
            }
            else
            {

				
				$rows = query("SELECT LAST_INSERT_ID() AS check50_id");
				
				$check50_id = $rows[0]["check50_id"];
								
				// redirect to homepage
                redirect("check50.php?id=" . $check50_id);
				
			}
        }  
        elseif 	($_POST['action']=="delete")
		{
			$sql ="DELETE FROM check50 WHERE check50_id = ? AND eid = ?";
			query($sql, $_POST['check50_id'], $_SESSION['cur_eid']);
			// redirect to lista de trabalhadores
            redirect("check50.php");
		}
		// não foi especificada acção no formulário - aborta
		else
			apologize("Não foi especificada acção no formulário...");
    }
	
	
	
	
	/************************************************************** 
	 *
	 * Trata dos casos em que o ficheiro é usado com $_GET
	 *
	 *************************************************************/
	 
    else
    {
		//se for especificada uma acção no URL
		if (isset($_GET['action']))
		{
			// caso o utilizador queira editar um trabalhador
			if ($_GET['action'] == 'edit')
			{
				if (isset($_GET['id']))
				{
					$check50_id = $_GET['id']; 
					
					$checklists  = query("SELECT * FROM check50 WHERE check50_id = ? AND eid = ?", $check50_id, $_SESSION['cur_eid']);
					
					// se existe trabalhador com este tid
					if (!empty($checklists[0])) 
					{
						$checklist = $checklists[0];
						//constroi as várias queries necessárias
						
					
					}
					
					// a query devolveu uma lista vazia
					else 
						apologize("A checklist especificada não existe...");
				
					render("check50_form_editar.php", array("title" => "Editar Checklist DL 50/2005", "checklist" => $checklist));
				}
				
				// o utilizador quer editar, mas falta o tid no URL, logo não sabemos qual é o trabalhador a editar
				else 
					apologize("Não foi especificado qual a checklist a editar.");
			}
			
			// caso o utilizador queira adicionar um 
			elseif ($_GET['action'] == 'add')
			{
				$mid = $_GET['id']; 
				
				render("check50_form_adicionar.php", array("title" => "Adicionar Checklist DL 50/2005", "mid" => $mid));
			}
			
			// caso o utilizador queira apagar um trabalhador
			elseif ($_GET['action'] == 'delete')
			{
				if (isset($_GET['id']))
				{
					$check50_id = $_GET['id']; 
					
					$checklists  = query("SELECT * FROM check50 WHERE check50_id = ? AND eid = ?", $check50_id, $_SESSION['cur_eid']);
					
					// se existe trabalhador com este tid
					if (!empty($checklists[0])) 
					{
						$checklist = $checklists[0];
					}
					
					// a query devolveu uma lista vazia
					else 
						apologize("Remover checklist - a checklist especificada não existe...");
				
					render("check50_form_remover.php", array("title" => "Remover Checklist DL 50/2005", "checklist" => $checklist));
				}
				
				// o utilizador quer editar, mas falta o tid no URL, logo não sabemos qual é o trabalhador a editar
				else 
					apologize("Não foi especificado qual a checklist a remover.");
			}
			
			else
				apologize("Operação não suportada.");
		
		}		
		
		
		//se não houver acção especificada no URL, mas houver um tid
		elseif (isset($_GET['id'])) 
		{
			$check50_id = $_GET['id']; 
			
			//constroi as várias queries necessárias
			$checklists  = query("SELECT * FROM check50 WHERE check50_id = ? AND eid = ?", $check50_id, $_SESSION['cur_eid']);
									
			if (!empty($checklists[0])) // se há trabalhadores com este tid
			{
				// o trabalhador que queremos está na linha 0
				$checklist = $checklists[0];
				
				// dado que o trabalhador existe podemos executar as outras queries
				//$maquinas = query($sql_maquinas, $tid);
				
				// mostra o trabalhador
				render("check50_visualizar.php", array("title" => "Checklist DL 50/2005", "checklist" => $checklist, "check50_id" => $check50_id));
			}
			else // a query devolveu uma lista vazia
				apologize("A checklist especificada não existe...");

		
		}
		
		
		
		// se não for especificado o tid do trabalhador no URL, mostra lista de trabalhadores
		else
		{
			$checklists = query("SELECT * FROM check50 WHERE eid = ?", $_SESSION['cur_eid']);
			render("check50_lista.php", array("title" => "Lista de Checklists", "checklists" => $checklists));
		}
		
    }

?>

