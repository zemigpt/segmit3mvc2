<?php

    // configuration
    require("includes/config.php");

    // if form was submitted
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {	
		if ($_POST['ptid'] == 0)
			apologize("Tem de escolher um posto de trabalho da lista!");
		
		// verifica se o formulário não foi adulterado com SQL injections
		$sql_ptid = "SELECT * FROM `posto-trabalho` WHERE ptid = ? AND eid = ?";
		$sql_raid = "SELECT * FROM registo_acidente WHERE ra_id = ? AND eid = ?";
				
		$rows_ptid = query($sql_ptid, $_POST['ptid'], $_SESSION['cur_eid']);
		$rows_raid = query($sql_raid, $_POST['ra_id'], $_SESSION['cur_eid']);
				
		if ((($rows_ptid === false) || empty($rows_ptid)) || (($rows_raid === false) || empty($rows_raid)))
			apologize("Os dados para associar o posto de trabalho ao registo foram corrompidos.");
		else
		{
			$sql = "INSERT INTO `posto-to-reg_acidente`
			(ptid, ra_id) 
			VALUES (?, ?)";
			
			if (query($sql, $_POST['ptid'], $_POST['ra_id']) === false)
				apologize("Algo correu mal ao associar o posto de trabalho ao registo de acidente na base de dados.");
			else
				redirect("regacidente.php?id=" . $_POST['ra_id']);
		}
	}
	else
		apologize("Algo correu mal ao associar o posto de trabalho ao registo de acidente");
?>