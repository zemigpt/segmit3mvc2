<?php

    // configuration
    require("includes/config.php");

    // if form was submitted
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        /**************
		*
		* Validation
		*
		*****************/
		
		/*if (empty($_POST["field"]))
        {
            apologize("O campo 'field' � obrigat�rio!");
        }
        
        else
        {*/
            // if none of the above occurred, odds are everything is ok
            // so insert new record in database
                   
           
		    // grava o ficheiro
            // code adapted from http://www.w3schools.com/php/php_file_upload.asp            
            
            $sql = "INSERT INTO ag_fisico_aval_ruido
					(registo, data, intervalo, eid) 
					VALUES(?,?,?,?)";
            
            
            
       
            if (query($sql, $_POST['ag_fisico_aval_ruido_registo'], $_POST['ag_fisico_aval_ruido_data'], $_POST['ag_fisico_aval_ruido_intervalo'], $_SESSION['cur_eid'] ) === false)
            {
                apologize("Algo correu mal ao inserir o registo de avalia��o de agentes f�sicos ru�do na base de dados.");
            }
            else
            {

				
				$rows = query("SELECT LAST_INSERT_ID() AS afar_id");
				
				$afar_id = $rows[0]["afar_id"];
				
				
				if (!isset($_POST["ajax"]) || $_POST["ajax"] != "true") {
					// redirect to homepage
					redirect("index.php?eid=" . $_SESSION['cur_eid']);
				}
				else
				{
					echo json_encode(array("afar_id" => $afar_id, "ag_fisico_aval_ruido_registo" => $_POST['ag_fisico_aval_ruido_registo']));
				}
			}
            
            
            
        /*}
		
		**********************/
    }
    else
    {
        // else render form 
        render("adicionar_ag_fisico_aval_ruido_form.php", array("title" => "Adicionar Avalia��o Agente F�sico: Ruido", "eid" => $_SESSION['cur_eid']));
    }

?>

