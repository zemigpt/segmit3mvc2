<?php

    // configuration
    require("includes/config.php");

    // if form was submitted
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {	
		if ($_POST['mid'] == 0)
			apologize("Tem de escolher uma máquina da lista!");
		
		// verifica se o formulário não foi adulterado com SQL injections
		$sql_mid = "SELECT * FROM maquina WHERE mid = ? AND eid = ?";
		$sql_ptid = "SELECT * FROM `posto-trabalho` WHERE ptid = ? AND eid = ?";
		
		$rows_mid = query($sql_mid, $_POST['mid'], $_SESSION['cur_eid']);
		$rows_ptid = query($sql_ptid, $_POST['ptid'], $_SESSION['cur_eid']);
		
		if ((($rows_mid === false) || empty($rows_mid)) || (($rows_ptid === false) || empty($rows_ptid)))
			apologize("Os dados para associar a máquina ao posto de trabalho foram corrompidos.");
		else
		{
			$sql = "INSERT INTO `maquina-to-posto_trabalho`
			(ptid, mid) 
			VALUES (?, ?)";
			
			if (query($sql, $_POST['ptid'], $_POST['mid']) === false)
				apologize("Algo correu mal ao associar o trabalhador ao posto de trabalho na base de dados.");
			else
				redirect("ptrabalho.php?id=" . $_POST['ptid']);
		}
	}
	else
		apologize("Algo correu mal ao associar máquina ao posto de trabalho");
?>