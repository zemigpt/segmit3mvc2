<?php
	
    // configuration
    require_once("includes/config.php");

	
	
	/************************************************************** 
	 *
	 * Trata dos casos em que o ficheiro é usado com $_POST
	 *
	 *************************************************************/
	 
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        
		// verifica se estamos a editar um trabalhador existente ou a adicionar um novo
		if ($_POST['action']=="edit")
		{
			if (isset($_POST['avalrisco_pta_id']))
			{
				//não há colocação de fotos na avaliação de risco - para já
				/*$upload_result = UploadFile($_FILES["trab_foto"], "image");
				if ($upload_result != "ERR")
				{
					$foto_url = $upload_result;
				}
				else
				{
					$foto_url = "";
				}			
				
				// se o upload de foto nova falhou, continua a usar o existente
				if  (!empty($_FILES["trab_foto"]) && $foto_url=="" && !empty($_POST['trab_foto_existente']))
					$foto_url = $_POST['trab_foto_existente'];
				*/
				
				$sql_1 = "SELECT * FROM posto_avaliacao WHERE pta_id = ? AND eid = ?";
				$pta_edited = query ($sql_1, $_POST['avalrisco_pta_id'], $_SESSION['cur_eid'] );
				$pta_edited = $pta_edited[0];
				
				$sql_2 = "INSERT INTO posto_avaliacao_revisao
						(data, ptid,  tarefa, perigo, probabilidade, exposicao, consequencia, grau_perigosidade, factor_custo, grau_correccao, indice_justificacao, risco, medidas, responsavel, data_prevista_resolucao, custo_previsto_resolucao, data_efectiva_resolucao, custo_efectivo_resolucao, user, motivo_alteracao, eid, pta_id)
						VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";           
							
				if (query ($sql_2, $pta_edited['data'], $pta_edited['ptid'], $pta_edited['tarefa'], $pta_edited['perigo'], $pta_edited['probabilidade'], $pta_edited['exposicao'], $pta_edited['consequencia'], $pta_edited['grau_perigosidade'], $pta_edited['factor_custo'], $pta_edited['grau_correccao'], $pta_edited['indice_justificacao'], $pta_edited['risco'], $pta_edited['medidas'], $pta_edited['responsavel'], $pta_edited['data_prevista_resolucao'], $pta_edited['custo_previsto_resolucao'], $pta_edited['data_efectiva_resolucao'], $pta_edited['custo_efectivo_resolucao'], $_SESSION['id'], $_POST['avalrisco_motivo_revisao'], $_SESSION['cur_eid'], $pta_edited['pta_id'] ) === false) 
				{
					apologize ("Algo correu mal ao gravar a revisão da avaliação de risco na base de dados");
				}
				
				$gp = round($_POST['avalrisco_consequencia'] * $_POST['avalrisco_exposicao'] * $_POST['avalrisco_probabilidade']);
				$ji = round($gp/($_POST['avalrisco_factor_custo'] * $_POST['avalrisco_grau_correccao']));
				
				$sql_3 = "UPDATE posto_avaliacao 
						SET data = ?, ptid = ?, tarefa = ?, perigo = ?, probabilidade = ?, exposicao = ?, consequencia = ?, grau_perigosidade = ?, factor_custo = ?, grau_correccao = ?, indice_justificacao = ?, risco = ?, medidas = ?, responsavel = ?, data_prevista_resolucao = ?, custo_previsto_resolucao = ?, data_efectiva_resolucao = ?, custo_efectivo_resolucao = ? 
						WHERE pta_id = ? AND eid = ?";
				
					
				if (query($sql_3, $_POST['avalrisco_data'], $_POST['avalrisco_ptid'], $_POST['avalrisco_tarefa'], $_POST['avalrisco_perigo'], $_POST['avalrisco_probabilidade'], $_POST['avalrisco_exposicao'], $_POST['avalrisco_consequencia'], $gp, $_POST['avalrisco_factor_custo'], $_POST['avalrisco_grau_correccao'], $ji, $_POST['avalrisco_risco'], $_POST['avalrisco_medidas'], $_POST['avalrisco_responsavel'], $_POST['avalrisco_data_prevista_resolucao'], $_POST['avalrisco_custo_previsto_resolucao'], $_POST['avalrisco_data_efectiva_resolucao'], $_POST['avalrisco_custo_efectivo_resolucao'], $_POST['avalrisco_pta_id'], $_SESSION['cur_eid'] ) === false)
				{
					apologize("Algo correu mal ao editar a avaliação de risco na base de dados.");
				}
				else
				{

									
					// redirect to homepage
					redirect("avalrisco.php?id=" . $_POST['avalrisco_pta_id']);
					
				}
			}
			// não temos tid para identificar o trabalhador - aborta
			else
				apologize("Avaliação de risco não identificada - impossível editar");
			
		
		}
		elseif 	($_POST['action']=="add")
		{
			//não há introdução de fotos - para já
			/*
			$upload_result = UploadFile($_FILES["trab_foto"], "image");
			if ($upload_result != "ERR")
			{
				$foto_url = $upload_result;
			}
			else
			{
				$foto_url = "";
			}			
            */
			$gp = round($_POST['avalrisco_consequencia'] * $_POST['avalrisco_exposicao'] * $_POST['avalrisco_probabilidade']);
			$ji = round($gp/($_POST['avalrisco_factor_custo'] * $_POST['avalrisco_grau_correccao']));
			
            $sql = "INSERT INTO posto_avaliacao 
					(data, ptid, tarefa, perigo, probabilidade, exposicao, consequencia, grau_perigosidade, factor_custo, grau_correccao, indice_justificacao, risco, medidas, responsavel, data_prevista_resolucao, custo_previsto_resolucao, data_efectiva_resolucao, custo_efectivo_resolucao, eid) 
					VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";           
            
            if (query($sql, $_POST['avalrisco_data'], $_POST['avalrisco_ptid'], $_POST['avalrisco_tarefa'], $_POST['avalrisco_perigo'], $_POST['avalrisco_probabilidade'], $_POST['avalrisco_exposicao'], $_POST['avalrisco_consequencia'], $gp, $_POST['avalrisco_factor_custo'], $_POST['avalrisco_grau_correccao'], $ji, $_POST['avalrisco_risco'], $_POST['avalrisco_medidas'], $_POST['avalrisco_responsavel'], $_POST['avalrisco_data_prevista_resolucao'], $_POST['avalrisco_custo_previsto_resolucao'], $_POST['avalrisco_data_efectiva_resolucao'], $_POST['avalrisco_custo_efectivo_resolucao'], $_SESSION['cur_eid'] ) === false)
            {
                apologize("Algo correu mal ao inserir  a nova avaliacao de riscos na base de dados.");
            }
            else
            {

				
				$rows = query("SELECT LAST_INSERT_ID() AS pta_id");
				
				$pta_id = $rows[0]["pta_id"];
								
				// redirect to homepage
                redirect("avalrisco.php?id=" . $pta_id);
				
			}
        }  
        elseif 	($_POST['action']=="delete")
		{
			$sql ="DELETE FROM posto_avaliacao WHERE pta_id = ? AND eid = ?";
			query($sql, $_POST['avalrisco_pta_id'], $_SESSION['cur_eid']);
			// redirect to lista de avaliações de risco
            redirect("avalrisco.php");
		}
		// não foi especificada acção no formulário - aborta
		else
			apologize("Não foi especificada acção no formulário...");
    }
	
	
	
	
	/************************************************************** 
	 *
	 * Trata dos casos em que o ficheiro é usado com $_GET
	 *
	 *************************************************************/
	 
    else
    {
		//se for especificada uma acção no URL
		if (isset($_GET['action']))
		{
			// caso o utilizador queira editar um avalrisco
			if ($_GET['action'] == 'edit')
			{
				if (isset($_GET['id']))
				{
					$pta_id = $_GET['id']; 
					
					$avalriscos  = query("SELECT * FROM posto_avaliacao WHERE pta_id = ? AND eid = ?", $pta_id, $_SESSION['cur_eid']);
					
					
					
					// se existe avalrisco com este tid
					if (!empty($avalriscos[0])) 
					{
						$avalrisco = $avalriscos[0];
						
						//constroi as várias variáveis necessárias
						$gp = round($avalrisco['consequencia'] * $avalrisco['exposicao'] * $avalrisco['probabilidade']);
						if ($gp >= 400) 
							$gp_texto = "Grave e iminente";
						elseif ($gp < 400 && $gp >= 200) 
							$gp_texto = "Elevado";
						elseif ($gp < 200 && $gp >= 70) 
							$gp_texto = "Notável";
						elseif ($gp < 70 && $gp >= 20) 
							$gp_texto = "Moderado";
						elseif ($gp < 20) 
							$gp_texto = "Aceitável";
									
						$ji = round($gp/($avalrisco['factor_custo'] * $avalrisco['grau_correccao']));
						if ($ji >= 20)
							$ji_texto = "Muito justificado";
						elseif ($ji >= 10 && $ji < 20)
							$ji_texto = "Provável justificação";
						elseif ($ji < 10)
							$ji_texto = "Não justificado";
							
						//faz lista dos postos de trabalho
						$sql="SELECT * FROM `posto-trabalho` WHERE eid = ?";
						$ptrabalhos = query($sql, $_SESSION['cur_eid']);
						
						render("avalrisco_form_editar.php", array("title" => "Editar Avaliação Risco - Método William Fine", "avalrisco" => $avalrisco, "gp" => $gp, "gp_texto" => $gp_texto, "ji" => $ji, "ji_texto" => $ji_texto, "ptrabalhos" => $ptrabalhos));
					
					}
					
					// a query devolveu uma lista vazia
					else 
						apologize("A avaliação de risco especificada não existe...");
				

				}
				
				// o utilizador quer editar, mas falta o pta_id no URL, logo não sabemos qual é o avalrisco a editar
				else 
					apologize("Não foi especificado qual a avaliação de risco a editar.");
			}
			
			// caso o utilizador queira adicionar uma avaliação de risco
			elseif ($_GET['action'] == 'add')
			{
				//faz lista dos postos de trabalho
				$sql="SELECT * FROM `posto-trabalho` WHERE eid = ?";
				$ptrabalhos = query($sql, $_SESSION['cur_eid']);
			
				 render("avalrisco_form_adicionar.php", array("title" => "Adicionar Avaliação de Risco - Método William Fine", "ptrabalhos" => $ptrabalhos));
			}
			
			// caso o utilizador queira apagar um avaliação risco
			elseif ($_GET['action'] == 'delete')
			{
				if (isset($_GET['id']))
				{
					$pta_id = $_GET['id']; 
					
					$avalriscos  = query("SELECT * FROM posto_avaliacao WHERE pta_id = ? AND eid = ?", $pta_id, $_SESSION['cur_eid']);
					
					// se existe trabalhador com este tid
					if (!empty($avalriscos[0])) 
					{
						$avalrisco = $avalriscos[0];
					}
					
					// a query devolveu uma lista vazia
					else 
						apologize("Remover avaliação de risco - A avaliação de risco especificada não existe...");
				
					render("avalrisco_form_remover.php", array("title" => "Remover Avaliação Risco", "avalrisco" => $avalrisco));
				}
				
				// o utilizador quer editar, mas falta o tid no URL, logo não sabemos qual é o trabalhador a editar
				else 
					apologize("Não foi especificado qual a avaliação de risco a remover.");
			}
			
			else
				apologize("Operação não suportada.");
		
		}		
		
		
		//se não houver acção especificada no URL, mas houver um id
		elseif (isset($_GET['id'])) 
		{
			$pta_id = $_GET['id']; 
			
			//constroi as várias queries necessárias
			$sql = "Select
				  `posto-trabalho`.nome As nome,
				  posto_avaliacao.pta_id As pta_id,
				  posto_avaliacao.data As data,
				  posto_avaliacao.tarefa As tarefa,
				  posto_avaliacao.perigo As perigo,
				  posto_avaliacao.probabilidade As probabilidade,
				  posto_avaliacao.exposicao As exposicao,
				  posto_avaliacao.consequencia As consequencia,
				  posto_avaliacao.grau_perigosidade As grau_perigosidade,
				  posto_avaliacao.factor_custo As factor_custo,
				  posto_avaliacao.grau_correccao As grau_correccao,
				  posto_avaliacao.indice_justificacao As indice_justificacao,
				  posto_avaliacao.risco As risco,
				  posto_avaliacao.medidas As medidas,
				  posto_avaliacao.responsavel As responsavel,
				  posto_avaliacao.data_prevista_resolucao As data_prevista_resolucao,
				  posto_avaliacao.custo_previsto_resolucao As custo_previsto_resolucao,
				  posto_avaliacao.data_efectiva_resolucao As data_efectiva_resolucao,
				  posto_avaliacao.custo_efectivo_resolucao As custo_efectivo_resolucao,
				  posto_avaliacao.eid As eid
				From
				  posto_avaliacao Inner Join
				  `posto-trabalho` On posto_avaliacao.ptid = `posto-trabalho`.ptid
				Where
				  posto_avaliacao.pta_id = ? AND posto_avaliacao.eid = ?";
			$avalriscos  = query($sql, $pta_id, $_SESSION['cur_eid']);
			//dump($avalriscos);
						
			if (!empty($avalriscos[0])) // se há avaliações de risco com este pta_id
			{
				// a avaliação que queremos está na linha 0
				$avalrisco = $avalriscos[0];
				
				$sql_revisoes = " Select
				  posto_avaliacao.pta_id As pta_id,
				  posto_avaliacao_revisao.timestamp As timestamp,
				  posto_avaliacao_revisao.revpta_id As id_revisao,
				  posto_avaliacao_revisao.motivo_alteracao As motivo,
				  posto_avaliacao_revisao.risco As risco_revisao,
				  posto_avaliacao.ptid As ptid,
				  posto_avaliacao_revisao.grau_perigosidade As grau_perigosidade
				From
				  posto_avaliacao Inner Join
				  posto_avaliacao_revisao On posto_avaliacao_revisao.pta_id =
					posto_avaliacao.pta_id
				Where
				  posto_avaliacao.pta_id = ? AND posto_avaliacao.eid = ?";
				  
				$revisoes = query ($sql_revisoes, $pta_id, $_SESSION['cur_eid']);
				
				//constroi as várias variáveis necessárias
				$gp = round($avalrisco['consequencia'] * $avalrisco['exposicao'] * $avalrisco['probabilidade']);
				if ($gp >= 400) {
					$gp_texto = "Grave e iminente";
					$gp_class = "grave";
				}
				elseif ($gp < 400 && $gp >= 200) {
					$gp_texto = "Elevado";
					$gp_class = "elevado";
				}
				elseif ($gp < 200 && $gp >= 70) {
					$gp_texto = "Notável";
					$gp_class = "notavel";
				}
				elseif ($gp < 70 && $gp >= 20) {
					$gp_texto = "Moderado";
					$gp_class = "moderado";
				}
				elseif ($gp < 20) {
					$gp_texto = "Aceitável";
					$gp_class = "aceitavel";
				}
							
				$ji = round($gp/($avalrisco['factor_custo'] * $avalrisco['grau_correccao']));
				if ($ji >= 20) {
					$ji_texto =  "Muito justificado";
				}
				elseif ($ji >= 10 && $ji < 20) {
					$ji_texto =  "Provável justificação";
				}
				elseif ($ji < 10) {
					$ji_texto =  "Não justificado";
				}
				
								
				// mostra a avaliação de risco
				render("avalrisco_visualizar.php", array("title" => "Avaliação Riscos", "avalrisco" => $avalrisco, "pta_id" => $pta_id, "gp_texto" => $gp_texto, "gp_class" => $gp_class, "ji_texto" => $ji_texto, "revisoes" => $revisoes));
			}
			else // a query devolveu uma lista vazia
				apologize("A avaliação de riscos especificada não existe...");

		
		}
		
		
		
		// se não for especificado o pta_id do avalrisco no URL, mostra lista de avaliacoes
		else
		{
			$sql_avalriscos = "Select
			  `posto-trabalho`.nome As ptnome,
			  posto_avaliacao.pta_id,
			  posto_avaliacao.data,
			  posto_avaliacao.tarefa,
			  posto_avaliacao.perigo,
			  posto_avaliacao.risco,
			  posto_avaliacao.grau_perigosidade,
			  posto_avaliacao.indice_justificacao,
			  posto_avaliacao.responsavel,
			  posto_avaliacao.data_prevista_resolucao,
			  posto_avaliacao.medidas,
			  posto_avaliacao.exposicao,
			  posto_avaliacao.consequencia,
			  posto_avaliacao.probabilidade,
			  posto_avaliacao.factor_custo,
			  posto_avaliacao.grau_correccao
			  
			From
			  posto_avaliacao Inner Join
			  `posto-trabalho` On posto_avaliacao.ptid = `posto-trabalho`.ptid
			Where posto_avaliacao.eid = ? ";
			
			$avalriscos = query($sql_avalriscos, $_SESSION['cur_eid']);
			
			if (!empty($avalriscos))    
			{
				foreach ($avalriscos as $avalrisco)
				{
					
					$gp = round($avalrisco['consequencia'] * $avalrisco['exposicao'] * $avalrisco['probabilidade']);
					
					if ($gp >= 400) {
						$avalrisco['gp_texto'] = "Grave e iminente";
						$avalrisco['gp_class'] = "grave";
					}
					elseif ($gp < 400 && $gp >= 200) {
						$avalrisco['gp_texto'] = "Elevado";
						$avalrisco['gp_class'] = "elevado";
					}
					elseif ($gp < 200 && $gp >= 70) {
						$avalrisco['gp_texto'] = "Notável";
						$avalrisco['gp_class'] = "notavel";
					}
					elseif ($gp < 70 && $gp >= 20) {
						$avalrisco['gp_texto'] = "Moderado";
						$avalrisco['gp_class'] = "moderado";
					}
					elseif ($gp < 20) {
						$avalrisco['gp_texto'] = "Aceitável";
						$avalrisco['gp_class'] = "aceitavel";
					}
								
					$ji = round($gp/($avalrisco['factor_custo'] * $avalrisco['grau_correccao']));
					if ($ji >= 20) {
						$avalrisco['ji_texto'] =  "Muito justificado";
					}
					elseif ($ji >= 10 && $ji < 20) {
						$avalrisco['ji_texto'] =  "Provável justificação";
					}
					elseif ($ji < 10) {
						$avalrisco['ji_texto'] =  "Não justificado";
					}
						
					$lista_avalriscos[] = $avalrisco;
				}
			
			
			//dump($lista_avalriscos);
			render("avalrisco_lista.php", array("title" => "Lista de Avaliações de Risco", "avalriscos" => $lista_avalriscos));
			
			}
			else{
			//caso a lista de riscos esteja vazia 
			render("avalrisco_lista.php", array("title" => "Lista de Avaliações de Risco", "avalriscos" => $avalriscos));
			}
		}
		
    }

?>

