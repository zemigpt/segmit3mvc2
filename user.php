<?php

    // configuration
    require("includes/config.php");

    // get user data from DB
    $sql = "SELECT * FROM users WHERE user_id = ?";
    $rows = query($sql, $_SESSION["id"]);
    $user = $rows[0];

    // get user's babies from DB
    $babies = query("SELECT * FROM baby WHERE father_id = ? OR mother_id = ?", $_SESSION["id"], $_SESSION["id"]);
        
    // render info
    render("user_info.php", array("title" => "User info", "user" => $user, "babies" => $babies));

?>
