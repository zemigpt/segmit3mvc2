<?php

    // configuration
    require("includes/config.php");

    // if form was submitted
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        if (empty($_POST["username"]))
        {
            apologize("You must provide your username.");
        }
        else if  (empty($_POST["password"]))
        {
            apologize("You must provide a password.");
        }
        
        else if ($_POST["password"] != $_POST["confirmation"])
        {
            apologize("Passwords do not match!");
        }
        else if  (empty($_POST["email"]))
        {
            apologize("You must provide an email.");
        }
        else
        {
            // if none of the above occurred, odds are everything is ok
            // so insert new user in database
            if (query("INSERT INTO users (username, password, email) VALUES(?, ?, ?)", $_POST["username"], crypt($_POST["password"]), $_POST["email"]) === false)
            {
                apologize("Something went wrong... Perhaps that username already exists.");
            }
            else
            {
				// if query returns true then new user registration was successull. 
				// login user 
				
				$rows = query("SELECT LAST_INSERT_ID() AS id");
				$id = $rows[0]["id"];
				
				// remember that user's now logged in by storing user's ID in session
				$_SESSION["id"] = $id;

                // redirect to homepage
                redirect("/");
				
			}
            
            
            
        }
    }
    else
    {
        // else render form
        render("register_form.php", array("title" => "Register"));
    }

?>

