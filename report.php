<?php

    // configuration
    require_once("includes/config.php");

	
	/************************************************************** 
	 *
	 * Trata dos casos em que o ficheiro é usado com $_GET
	 *
	 *************************************************************/
	 

		//se for especificada uma acção no URL
		if (isset($_GET['type']))
		{
			// caso o utilizador queira editar um trabalhador
			if ($_GET['type'] == 'compatibilidade-trab-maq')
			{
				$sql_tipomaq = "Select
					  tipo_maquina.nome,
					  tipo_maquina.tm_id,
					  formacao.tema,
					  formacao.for_id
					From
					  tipo_maquina Inner Join
					  `formacao-to-tipomaquina`
						On tipo_maquina.tm_id = `formacao-to-tipomaquina`.tm_id Inner Join
					  formacao On `formacao-to-tipomaquina`.for_id = formacao.for_id";
				$tipos_maquina = query($sql_tipomaq);
				
				$sql_trab = "Select
						  trabalhador.tid,
						  trabalhador.nome,
						  formacao.for_id,
						  formacao.tema
						From
						  trabalhador Inner Join
						  `formacao-to-trabalhador` On trabalhador.tid = `formacao-to-trabalhador`.tid
						  Inner Join
						  formacao On `formacao-to-trabalhador`.for_id = formacao.for_id";
				
				$trabalhadores = query($sql_trab);				
						  
						  
						  
						  
						  
						  
						  
						  
				
			}
			elseif ($_GET['type'] == 'acidentes') {
				$sql_acidentesporano = "SELECT Year( `registo_acidente`.data ) AS ano, Count( * ) AS acidentes
										FROM registo_acidente
										GROUP BY Ano";
				$acidentesporano = query($sql_acidentesporano);
				
				$sql_acidentesporpostotrabalho = "SELECT `posto-trabalho`.nome as posto, Count( * ) AS acidentes
										FROM `posto-trabalho` 
										LEFT JOIN `posto-to-reg_acidente` ON `posto-trabalho`.ptid = `posto-to-reg_acidente`.ptid
										INNER JOIN registo_acidente ON `posto-to-reg_acidente`.ra_id = registo_acidente.ra_id
										GROUP BY `posto-trabalho`.nome";
				$acidentesporpostotrabalho = query($sql_acidentesporpostotrabalho);
				
				$sql_acidentesportrabalhador = "SELECT trabalhador.nome as trabalhador, Count( * ) AS acidentes
										FROM trabalhador
										INNER JOIN `trabalhador-to-reg_acidente` ON trabalhador.tid = `trabalhador-to-reg_acidente`.tid
										INNER JOIN registo_acidente ON `trabalhador-to-reg_acidente`.ra_id = registo_acidente.ra_id
										GROUP BY trabalhador.nome";
				$acidentesportrabalhador = query($sql_acidentesportrabalhador);
			
				render("report_acidentes.php", array("title" => "Relatório - Acidentes", "acidentesporano" => $acidentesporano, "acidentesporpostotrabalho" => $acidentesporpostotrabalho, "acidentesportrabalhador" => $acidentesportrabalhador));
			
			}
			
			
			
			
			else
			{
				apologize("Tipo de operação não suportado.");
			}

		
		}		
		
		

		
		// se não for especificado o tid do trabalhador no URL, mostra lista de trabalhadores
		else
		{
			apologize("Nenhuma operação indicada.");
		}
		

?>

