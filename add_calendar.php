<?php

    // configuration
    require("includes/config.php");

    // if form was submitted
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        
        $user_id = $_SESSION["id"];
        $baby_id = $_SESSION["baby_id"];

        if (empty($_POST["calendar_start_date"]))
        {
            apologize("The event calendar must have a start date. None was specified.");
            exit();
        }
        elseif (empty($_POST["calendar_description"]))
        {
            apologize("Calendar event description can't be blank!");
            exit();       
        } 
        else
        {
            // if none of the above occurred, odds are everything is ok
            // so try to insert new event in database
            
            $query_string = "INSERT INTO calendar (baby_id, start_date_time, calendar_description) VALUES(?, ?, ?)";
           
            if (query($query_string, $baby_id, $_POST["calendar_start_date"], $_POST["calendar_description"]) === false)
            {
                apologize("Something went wrong...");
            }
            else
            {
				// if query returns true then new event insert was successull. 
				
                // redirect to homepage
                redirect("/");
				
			}
            
        }
    }
    else
    {
                
        // else render form
        render("add_calendar_form.php", array("title" => "Add Calendar event"));
    }

?>

