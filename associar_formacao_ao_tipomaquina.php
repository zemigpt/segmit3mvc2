<?php

    // configuration
    require("includes/config.php");

    // if form was submitted
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {	
		if ($_POST['for_id'] == 0)
			apologize("Tem de escolher uma formação da lista!");
		
		// verifica se o formulário não foi adulterado com SQL injections
		$sql_form = "SELECT * FROM formacao WHERE for_id = ? AND eid = ?";
		$sql_tm_id = "SELECT * FROM tipo_maquina WHERE tm_id = ? AND eid = ?";
		
		$rows_form = query($sql_form, $_POST['for_id'], $_SESSION['cur_eid']);
		$rows_tm_id = query($sql_tm_id, $_POST['tm_id'], $_SESSION['cur_eid']);
		
		if ((($rows_form === false) || empty($rows_form)) || (($rows_tm_id === false) || empty($rows_tm_id)))
			apologize("Os dados para associar a formação ao tipo de máquina foram corrompidos.");
		else
		{
			$sql = "INSERT INTO `formacao-to-tipomaquina`
			(for_id, tm_id) 
			VALUES (?, ?)";
			
			if (query($sql, $_POST['for_id'], $_POST['tm_id']) === false)
				apologize("Algo correu mal ao associar a formação ao tipo de máquina na base de dados.");
			else
				redirect("tmaquina.php?id=" . $_POST['tm_id']);
		}
	}
	else
		apologize("Algo correu mal ao associar a formação ao tipo de máquina");
?>