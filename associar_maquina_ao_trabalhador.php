<?php

    // configuration
    require("includes/config.php");

    // if form was submitted
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {	
		if ($_POST['mid'] == 0)
			apologize("Tem de escolher uma maquina da lista!");
		
		// verifica se o formulário não foi adulterado com SQL injections
		$sql_mid = "SELECT * FROM maquina WHERE mid = ? AND eid = ?";
		$sql_tid = "SELECT * FROM trabalhador WHERE tid = ? AND eid = ?";
		
		$rows_mid = query($sql_mid, $_POST['mid'], $_SESSION['cur_eid']);
		$rows_tid = query($sql_tid, $_POST['tid'], $_SESSION['cur_eid']);
		
		if ((($rows_mid === false) || empty($rows_mid)) || (($rows_tid === false) || empty($rows_tid)))
			apologize("Os dados para associar a maquina ao trabalhador foram corrompidos.");			
		else
		{
			$sql = "INSERT INTO `maquina-to-trabalhador`
			(mid, tid) 
			VALUES (?, ?)";
			
			if (query($sql, $_POST['mid'], $_POST['tid']) === false)
				apologize("Algo correu mal ao associar a maquina ao trabalhador na base de dados.");
			else
				redirect("trabalhador.php?id=" . $_POST['tid']);
		}
	}
	else
		apologize("Algo correu mal ao associar a maquina ao trabalhador");
?>