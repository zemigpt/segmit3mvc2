<?php

    // configuration
    require("includes/config.php");


    if (isset($_POST["calendar_id"]))
        $calendar_id = $_POST["calendar_id"];
        
    // if form was submitted
    if ($_SERVER["REQUEST_METHOD"] == "POST" && !isset($_POST["formname"]))
    {
        
        $user_id = $_SESSION["id"];
        $baby_id = $_SESSION["baby_id"];
        

        if (empty($_POST["calendar_start_date"]))
        {
            apologize("The event calendar must have a start date. None was specified.");
            exit();
        }
        elseif (empty($_POST["calendar_description"]))
        {
            apologize("Calendar event description can't be blank!");
            exit();       
        } 
        else
        {
            // if none of the above occurred, odds are everything is ok
            // so try to insert new event in database
            
            $query_string = "UPDATE calendar SET start_date_time = ?, calendar_description = ? WHERE calendar_id = ?";
           
            if (query($query_string, $_POST["calendar_start_date"], $_POST["calendar_description"], $calendar_id) === false)
            {
                apologize("Something went wrong...");
            }
            else
            {
				// redirect to previous page if set
				
                if (isset($_POST["referrer"]))
                    redirect($_POST["referrer"]);
                else
                    redirect("/");
				
			}
            
        }
    }
    else
    {
        $rows = query("SELECT * FROM calendar WHERE calendar_id = ?", $calendar_id);
        $calendar = $rows[0];
              
        // else render form
        render("edit_calendar_form.php", array("title" => "Edit Calendar event", "calendar" => $calendar, "referrer" => $_POST["referrer"]));
    }

?>

