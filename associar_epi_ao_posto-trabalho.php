<?php

    // configuration
    require("includes/config.php");

    // if form was submitted
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {	
		if ($_POST['epi_id'] == 0)
			apologize("Tem de escolher um EPI da lista!");
		
		// verifica se o formulário não foi adulterado com SQL injections
		$sql_epi = "SELECT * FROM epi WHERE epi_id = ?";
		$sql_ptid = "SELECT * FROM `posto-trabalho` WHERE ptid = ? AND eid = ?";
		
		$rows_epi = query($sql_epi, $_POST['epi_id']);
		$rows_ptid = query($sql_ptid, $_POST['ptid'], $_SESSION['cur_eid']);
		
		if ((($rows_epi === false) || empty($rows_epi)) || (($rows_ptid === false) || empty($rows_ptid)))
			apologize("Os dados para associar o EPI ao posto de trabalho foram corrompidos.");
		else
		{
			$sql = "INSERT INTO `posto-to-epi`
			(ptid, epi_id) 
			VALUES (?, ?)";
			
			if (query($sql, $_POST['ptid'], $_POST['epi_id']) === false)
				apologize("Algo correu mal ao associar o epi ao posto de trabalho na base de dados.");
			else
				redirect("ptrabalho.php?id=" . $_POST['ptid']);
		}
	}
	else
		apologize("Algo correu mal ao associar epi ao posto de trabalho");
?>