<?php

    // configuration
    require("includes/config.php");
	
	// if form was submitted
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
		if (isset($_POST['ag_type']) && isset($_POST['ptid'])) 
		{
			$type = $_POST['ag_type'];
			$ptid = $_POST['ptid'];
			
			// trata do ficheiro pdf anexado
			if (isset($_FILES["relatorio"]))
			{
				$upload_result = UploadFile($_FILES["relatorio"], "pdf");
				if ($upload_result != "ERR")
				{
					$relatorio_url = $upload_result;
				}
				else
				{
					$relatorio_url = "";
				}
			}
			else
			{
				$relatorio_url = "";
			}
			
			// inicialização de variáveis genéricas para os agentes físicos.
			// abaixo faz-se override para os agentes biológicos e quimicos
			if (isset($_POST['valor_registado']))
			{
				$fields = "(valor_registado, data_execucao, data_validade, entidade_executante, relatorio_avaliacao, eid)";
				$vars = "VALUES ('" . $_POST['valor_registado'] . "', '" . $_POST['data_execucao'] . "', '" . $_POST['data_validade'] . "', '" . $_POST['entidade_executante'] . "', '" . $relatorio_url . "', '" . $_SESSION['cur_eid'] . "')";
			}
					
					
			switch ($type)
			{
				case "afar":
					$table = "ag_fisico_aval_ruido";
					$id = "afar_id";
					$rel_table = "`posto-to-ag_fisico_aval_ruido`";
					break;
					
				case "afai":
					$table = "ag_fisico_aval_ilum";
					$id = "afai_id";
					$rel_table = "`posto-to-ag_fisico_aval_ilum`";
					break;
					
				case "afat":
					$table = "ag_fisico_aval_termico";
					$id = "afat_id";
					$rel_table = "`posto-to-ag_fisico_aval_termico`";
					break;
					
				case "afav":
					$table = "ag_fisico_aval_vibracoes";
					$id = "afav_id";
					$rel_table = "`posto-to-ag_fisico_aval_vibracoes`";
					break;
					
				case "aquim":
					$table = "ag_quimico";
					$fields = "(dose, agente, data_execucao, entidade_executante, relatorio_avaliacao, eid)";
					$vars = "VALUES ('" . $_POST['dose'] . "', '" . $_POST['agente_quimico'] . "', '" . $_POST['data_execucao'] . "', '" . $_POST['entidade_executante'] . "', '" . $relatorio_url . "', '" . $_SESSION['cur_eid'] . "')";
					$id = "aq_id";
					$rel_table = "`posto-to-ag_quimico`";
					break;
				
				case "abio":
					$table = "ag_biologico";
					$id = "ab_id";
					$rel_table = "`posto-to-ag_biologico`";
					break;
			
			}

			$sql = "INSERT INTO " . $table . " " . $fields . " " . $vars;

			if (query($sql) === false)
			{
				apologize("Algo correu mal ao inserir a avaliação na base de dados");			
			}
			else
			{
				$rows = query("SELECT LAST_INSERT_ID() AS " . $id . " ");
				$insert_id = $rows[0][$id];
				
				$sql = "INSERT INTO " . $rel_table . " (ptid, " . $id . ") VALUES (?, ?)";
				if (query($sql, $ptid, $insert_id) === false)
				{
					apologize("Algo correu mal ao associar o agente ao posto de trabalho.");
				}
				else
				{
					redirect("ptrabalho.php?id=" . $ptid);
				}
			
			}
		}
		else
		{
			apologize("Erro - parâmetros inválidos");
		}
	}
?>