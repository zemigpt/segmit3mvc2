<?php

    // configuration
    require("includes/config.php");

    // if form was submitted
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        /**************
		*
		* Validation
		*
		*****************/
		
		/*if (empty($_POST["field"]))
        {
            apologize("O campo 'field' � obrigat�rio!");
        }
        
        else
        {*/
            // if none of the above occurred, odds are everything is ok
            // so insert new record in database
                   
           
		    // grava o ficheiro
            // code adapted from http://www.w3schools.com/php/php_file_upload.asp            
            
            $sql = "INSERT INTO ag_biologico
					(avaliacao, eid) 
					VALUES(?,?)";
            
            
            
       
            if (query($sql, $_POST['ag_biologico_avaliacao'], $_SESSION['cur_eid'] ) === false)
            {
                apologize("Algo correu mal ao inserir o novo tipo de m�quina na base de dados.");
            }
            else
            {

				
				$rows = query("SELECT LAST_INSERT_ID() AS ab_id");
				
				$ab_id = $rows[0]["ab_id"];
				
				
				if (!isset($_POST["ajax"]) || $_POST["ajax"] != "true") {
					// redirect to homepage
					redirect("index.php?eid=" . $_SESSION['cur_eid']);
				}
				else
				{
					echo json_encode(array("ab_id" => $ab_id, "ag_biologico_avaliacao" => $_POST['ag_biologico_avaliacao']));
				}
			}
            
            
            
        /*}
		
		**********************/
    }
    else
    {
        // else render form
        render("adicionar_ag_biologico_form.php", array("title" => "Adicionar Avalia��o Agente Biol�gico", "eid" => $_SESSION['cur_eid']));
    }

?>

