<?php

    // configuration
    require_once("includes/config.php");

	
	
	/************************************************************** 
	 *
	 * Trata dos casos em que o ficheiro é usado com $_POST
	 *
	 *************************************************************/
	 
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
		if (!isset($_POST['action']))
			apologize("Não foi especificada acção no formulário...");
		
		// verifica se estamos a editar um trabalhador existente ou a adicionar um novo
		if ($_POST['action']=="edit")
		{
			if (isset($_POST['maq_id']))
			{
				// regista o upload de nova foto
				$upload_result = UploadFile($_FILES["maq_foto"], "image");
				
				if ($upload_result != "ERR")
					$foto_maq = $upload_result;
				else
					$foto_maq = "";			
			   
			   
				// se o upload de foto nova falhou, continua a usar o existente
				if  (!empty($_FILES["maq_foto"]) && $foto_maq=="" && !empty($_POST['maq_foto_existente']))
					$foto_maq = $_POST['maq_foto_existente'];	
				
				
				// regista o upload de novo manual
				$upload_result = UploadFile($_FILES["maq_manual_instrucao"], "pdf");
				
				if ($upload_result != "ERR")
					$manual_instrucao_url = $upload_result;
				else
					$manual_instrucao_url = "";
				
				// se o upload de novo manual falhou, continua a usar o existente
				if  (!empty($_FILES["maq_manual_instrucao"]) && $manual_instrucao_url=="" && !empty($_POST['maq_manual_instrucao_existente']))
					$manual_instrucao_url = $_POST['maq_manual_instrucao_existente'];
				
				// regista o upload de nova instrução de trabalho
				$upload_result = UploadFile($_FILES["maq_instrucao_trabalho"], "pdf");
				
				if ($upload_result != "ERR")
					$instrucao_trabalho_url = $upload_result;
				else
					$instrucao_trabalho_url = "";
				
				// se o upload de nova instrução de trabalho falhou, continua a usar a existente
				if  (!empty($_FILES["maq_instrucao_trabalho"]) && $instrucao_trabalho_url=="" && !empty($_POST['maq_instrucao_trabalho_existente']))
					$instrucao_trabalho_url = $_POST['maq_instrucao_trabalho_existente'];
				
				// regista o upload de novo declaracao CE
				$upload_result = UploadFile($_FILES["maq_declaracao_CE"], "pdf");
				
				if ($upload_result != "ERR")
					$declaracao_CE_url = $upload_result;
				else
					$declaracao_CE_url = "";
				
				// se o upload de novo declaracao CE falhou, continua a usar o existente
				if  (!empty($_FILES["maq_declaracao_CE"]) && $declaracao_CE_url=="" && !empty($_POST['maq_declaracao_CE_existente']))
					$declaracao_CE_url = $_POST['maq_declaracao_CE_existente'];
					
				// regista o upload de novo declaracao DL50
				$upload_result = UploadFile($_FILES["maq_declaracao_DL50"], "pdf");
				
				if ($upload_result != "ERR")
					$declaracao_DL50_url = $upload_result;
				else
					$declaracao_DL50_url = "";
				
				// se o upload de novo declaracao DL50 falhou, continua a usar o existente
				if  (!empty($_FILES["maq_declaracao_DL50"]) && $declaracao_DL50_url=="" && !empty($_POST['maq_declaracao_DL50_existente']))
					$declaracao_DL50_url = $_POST['maq_declaracao_DL50_existente'];
				
				//constroi a query sql
				$sql = "UPDATE maquina
					SET nome = ? , tm_id = ? , fid = ? , num_serie = ? , foto = ? , data_compra = ? , data_aprov_seguranca = ? , manual_instrucao = ? , instrucao_trabalho = ?, declaracao_CE = ?, declaracao_DL50 = ?
					WHERE mid = ? AND eid = ?";
				
				// corre a query sql e verifica se deu erro
				if (query($sql, $_POST['maq_nome'], $_POST['maq_tipo_maquina'],$_POST['maq_fabricante'], $_POST['maq_num_serie'], $foto_maq, $_POST['maq_data_compra'], $_POST['maq_data_aprov_seguranca'], $manual_instrucao_url, $instrucao_trabalho_url, $declaracao_CE_url, $declaracao_DL50_url, $_POST['maq_id'], $_SESSION['cur_eid'] ) === false)
				{
					apologize("Algo correu mal a inserir a máquina nova na base de dados.");
				}
				else
				{
					// redirect to homepage
					redirect("maquina.php?id=" . $_POST['maq_id']);
					
				}
			}		
			
			// não temos mid para identificar a máquina - aborta
			else
				apologize("Máquina não identificada - impossível editar");		
		}

		elseif 	($_POST['action']=="add")
		{
			// regista o upload de nova foto
			$upload_result = UploadFile($_FILES["maq_foto"], "image");
			
			if ($upload_result != "ERR")
				$foto_maq = $upload_result;
			else
				$foto_maq = "";			
		   
		   
			// regista o upload de novo manual
			$upload_result = UploadFile($_FILES["maq_manual_instrucao"], "pdf");
			
			if ($upload_result != "ERR")
				$manual_instrucao_url = $upload_result;
			else
				$manual_instrucao_url = "";

			
			// regista o upload de nova instrução de trabalho
			$upload_result = UploadFile($_FILES["maq_instrucao_trabalho"], "pdf");
			
			if ($upload_result != "ERR")
				$instrucao_trabalho_url = $upload_result;
			else
				$instrucao_trabalho_url = "";
				
			// regista o upload de novo declaracao CE
			$upload_result = UploadFile($_FILES["maq_declaracao_CE"], "pdf");
			
			if ($upload_result != "ERR")
				$declaracao_CE_url = $upload_result;
			else
				$declaracao_CE_url = "";
				
			// regista o upload de novo declaracao DL50
			$upload_result = UploadFile($_FILES["maq_declaracao_DL50"], "pdf");
			
			if ($upload_result != "ERR")
				$declaracao_DL50_url = $upload_result;
			else
				$declaracao_DL50_url = "";
					
					
			$sql = "INSERT INTO maquina 
					(nome, tm_id, fid, num_serie, foto, data_compra, data_aprov_seguranca, manual_instrucao, instrucao_trabalho, declaracao_CE, declaracao_DL50, eid) 
					VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

			// corre a query sql e verifica se deu erro
			if (query($sql, $_POST['maq_nome'], $_POST['maq_tipo_maquina'],$_POST['maq_fabricante'], $_POST['maq_num_serie'], $foto_maq, $_POST['maq_data_compra'], $_POST['maq_data_aprov_seguranca'], $manual_instrucao_url, $instrucao_trabalho_url, $declaracao_CE_url, $declaracao_DL50_url, $_SESSION['cur_eid'] ) === false)
			{
				apologize("Algo correu mal a inserir a máquina nova na base de dados.");
			}
			
			// se não tiver dado erro
			else 
			{
				$rows = query("SELECT LAST_INSERT_ID() AS mid");
				$mid = $rows[0]["mid"];
				
				redirect("maquina.php?id=" . $mid);	
			}	
		}	
		
		elseif ($_POST['action'] == "delete")
		{
			
			$sql = "SELECT * FROM maquina WHERE mid = ? AND eid = ?";
			$maquinas = query($sql, $_POST['maq_id'], $_SESSION['cur_eid']);

			$maquina = $maquinas[0];
			$fabricante = $maquina['fid'];
			
			// apaga a maquina da tabela 'maquina'
			$sql = "DELETE FROM maquina WHERE mid = ? AND eid = ?";
			query($sql, $_POST['maq_mid'], $_SESSION['cur_eid']);
			
			// verifica se ainda há alguma máquina com este fabricante.
			$sql = "SELECT * FROM maquina WHERE fid = ? AND eid = ?";
			$maquinas = query($sql, $fabricante, $_SESSION['cur_eid']);
			
			if (empty($maquinas)) // Caso não haja, apaga o fabricante da respectiva tabela
			{
				$sql = "DELETE FROM fabricante WHERE fid = ? AND eid = ?";
				query($sql, $fabricante, $_SESSION['cur_eid']);
			}
			
			// apaga as referencias a esta máquina nas outras tabelas relacionadas
			$sql = "DELETE FROM `maquina-to-posto_trabalho` WHERE mid = ?";
			query($sql, $_POST['maq_id']);
			
			$sql = "DELETE FROM `maquina-to-reg_acidente` WHERE mid = ?";
			query($sql, $_POST['maq_id']);
			
			$sql = "DELETE FROM `maquina-to-trabalhador` WHERE mid = ?";
			query($sql, $_POST['maq_id']);
			
			// redirect to lista de maquinas
            redirect("maquina.php");
		}
		// não foi especificada acção no formulário - aborta
		else
			apologize("Não foi especificada acção no formulário...");
		
    }
	
	
		
	
	/************************************************************** 
	 *
	 * Trata dos casos em que o ficheiro é usado com $_GET
	 *
	 *************************************************************/
	 
    else
    {		
		//se for especificada uma acção no URL
		if (isset($_GET['action']))
		{
			// caso o utilizador queira editar uma máquina
			if ($_GET['action'] == 'edit')
			{
				if (isset($_GET['id']))
				{
					$mid = $_GET['id']; 
					
					$maquinas = query("SELECT * FROM maquina WHERE mid = ? AND eid = ?", $mid, $_SESSION['cur_eid']);
					
					// se existe máquina com este mid
					if (!empty($maquinas[0]))
					{
						$maquina = $maquinas[0];
					
						//constroi as várias queries necessárias
						
						// faz lista de tipos de maquina
						$sql="SELECT * FROM tipo_maquina WHERE eid = ?";
						$tipo_maquinas = query($sql, $_SESSION['cur_eid']);
						
						/// faz lista de fabricantes
						$sql="SELECT * FROM fabricante WHERE eid = ?";
						$fabricantes = query($sql, $_SESSION['cur_eid']);
					}
					
					// a query devolveu uma lista vazia
					else 
						apologize("O trabalhador especificado não existe...");
		
					render("maquina_form_editar.php", array("title" => "Editar Máquina", "maquina" => $maquina, "tipo_maquinas" => $tipo_maquinas, "fabricantes" => $fabricantes, "mid" => $mid));
				}
				
				// o utilizador quer editar, mas falta o tid no URL, logo não sabemos qual é o trabalhador a editar
				else 
					apologize("Não foi especificado qual a máquina a editar.");
			}
			
			// caso o utilizador queira adicionar um trabalhador
			elseif ($_GET['action'] == 'add')
			{
				// faz lista de tipos de maquina
				$sql="SELECT * FROM tipo_maquina WHERE eid = ?";
				$tipo_maquinas = query($sql, $_SESSION['cur_eid']);
				
				/// faz lista de fabricantes
				$sql="SELECT * FROM fabricante WHERE eid = ?";
				$fabricantes = query($sql, $_SESSION['cur_eid']);
				
				// verifica se esta máquina está a ser adicionada através do trabalhador
				if (isset($_GET['tid']))
					$tid=$_GET['tid'];
				else
					$tid="";
					
				// verifica se esta máquina está a ser adicionada através do posto de trabalho
				if (isset($_GET['ptid']))
					$ptid=$_GET['ptid'];
				else
					$ptid="";
					
				render("maquina_form_adicionar.php", array("title" => "Adicionar Maquina", "fabricantes" => $fabricantes, "tipo_maquinas" => $tipo_maquinas, "eid" => $_SESSION['cur_eid'], "tid" => $tid, "ptid" => $ptid ));
			}
			
			// caso o utilizador queira apagar uma máquina
			elseif ($_GET['action'] == 'delete')
			{
				if (isset($_GET['id']))
				{
					$mid = $_GET['id']; 
					
					$maquinas  = query("SELECT * FROM maquina WHERE mid = ? AND eid = ?", $mid, $_SESSION['cur_eid']);
					
					// se existe maquina com este mid
					if (!empty($maquinas[0])) 
					{
						$maquina = $maquinas[0];
					}
					
					// a query devolveu uma lista vazia
					else 
						apologize("Remover máquina - A máquina especificada não existe...");
				
					render("maquina_form_remover.php", array("title" => "Remover máquina", "maquina" => $maquina));
				}
				
				// o utilizador quer editar, mas falta o mid no URL, logo não sabemos qual é o maquina a editar
				else 
					apologize("Não foi especificado qual o maquina a remover.");
			}
			
			else
				apologize("Operação não suportada.");
		

    }
	
	//se não houver acção especificada no URL, mas houver um mid
		elseif (isset($_GET['id'])) 
		{
			$mid = $_GET['id']; 
			
			$sql_maquinas = "Select
				  fabricante.nome As fabricante,
				  maquina.mid,
				  maquina.nome As maquina,
				  maquina.num_serie As num_serie,
				  maquina.foto As foto,
				  maquina.data_compra As data_compra,
				  maquina.data_aprov_seguranca As data_aprov_seguranca,
				  maquina.manual_instrucao As manual_instrucao,
				  maquina.instrucao_trabalho As instrucao_trabalho,
				  maquina.declaracao_CE As declaracao_CE,
				  maquina.declaracao_DL50 As declaracao_DL50,
				  tipo_maquina.nome As tipo_maquina,
				  maquina_manutencao.data_manutencao As data_manutencao,
				  maquina_manutencao.relatorio_manutencao As relatorio_manutencao,
				  maquina_manutencao.data_prox_manutencao As data_prox_manutencao,
				  maquina_revisao.data_revisao As data_revisao,
				  maquina_revisao.relatorio_revisao As relatorio_revisao,
				  maquina_revisao.data_prox_revisao As data_prox_revisao
			From
				  maquina Left Join
				  fabricante On fabricante.fid = maquina.fid
				  Left Join
				  tipo_maquina On tipo_maquina.tm_id =
					maquina.tm_id Left Join
				  maquina_manutencao On maquina_manutencao.mid =
					maquina.mid Left Join
				  maquina_revisao On maquina_revisao.mid =
					maquina.mid
			Where
				maquina.mid = ? AND `maquina`.eid = ?";
								
			
			
			//constroi as várias queries necessárias					
			$sql_trabalhadores = "Select
				  maquina.mid,
				  trabalhador.tid As tid,
				  trabalhador.nome As nome,
				  trabalhador.num_mecanografico As num_mecanografico
			From
				maquina Inner Join
				  `maquina-to-trabalhador` On maquina.mid = `maquina-to-trabalhador`.mid
				  Inner Join
				  trabalhador On `maquina-to-trabalhador`.tid = trabalhador.tid
			Where
			  maquina.mid = ? AND `maquina`.eid = ?";
		  
			$sql_manutencoes = "Select
			  maquina.nome As nome,
			  maquina_manutencao.data_manutencao As data_manutencao,
			  maquina_manutencao.relatorio_manutencao As relatorio_manutencao,
			  maquina_manutencao.data_prox_manutencao As data_prox_manutencao,
			  
			  maquina.mid
			From
			  maquina Left Join
			  maquina_manutencao On maquina_manutencao.mid =
				maquina.mid 
			Where
			  maquina.mid = ? AND `maquina`.eid = ?";
	  
			$sql_revisoes = "Select
			  maquina.nome As nome,
			  maquina_revisao.data_revisao As data_revisao,
			  maquina_revisao.relatorio_revisao As relatorio_revisao,
			  maquina_revisao.data_prox_revisao As data_prox_revisao,
			  maquina.mid
			From
			  maquina Left Join
			  maquina_revisao On maquina_revisao.mid =
				maquina.mid
			Where
			  maquina.mid = ? AND `maquina`.eid = ?";
			
			
			$sql_postos_trabalho = "SELECT 
			  `posto-trabalho`.ptid AS ptid,
			  `posto-trabalho`.nome AS nome,
			  `posto-trabalho`.foto AS foto
			FROM maquina
			  LEFT JOIN `maquina-to-posto_trabalho` ON `maquina-to-posto_trabalho`.mid = maquina.mid
			  LEFT JOIN `posto-trabalho` ON `posto-trabalho`.ptid = `maquina-to-posto_trabalho`.ptid
			WHERE  
			  maquina.mid = ? AND `maquina`.eid = ?";
			  
			$sql_forrequeridas = "Select
			  formacao.tema As tema,
			  formacao.duracao As duracao,
			  formacao.for_id As for_id,
			  maquina.nome As maq_nome,
			  maquina.mid As mid,
			  maquina.eid
			From
			  formacao Inner Join
			  `formacao-to-tipomaquina`
				On formacao.for_id = `formacao-to-tipomaquina`.for_id Inner Join
			  tipo_maquina On `formacao-to-tipomaquina`.tm_id = tipo_maquina.tm_id
			  Inner Join
			  maquina On tipo_maquina.tm_id = maquina.tm_id
			Where
			  maquina.mid = ? And
			  maquina.eid = ?";
			  
			$sql_regacidentes = "Select
			  registo_acidente.ra_id,
			  registo_acidente.eid,
			  registo_acidente.estado As estado,
			  registo_acidente.data As data,
			  maquina.eid As eid1,
			  maquina.mid As mid
			From
			  registo_acidente Inner Join
			  `maquina-to-reg_acidente` On registo_acidente.ra_id =
				`maquina-to-reg_acidente`.ra_id Inner Join
			  maquina On maquina.mid = `maquina-to-reg_acidente`.mid
			Where
			  maquina.eid = ? And
			  maquina.mid = ?";
			  
			$sql_todos_ptrabalhos = "Select
			  maquina.mid As mid,
			  `posto-trabalho`.ptid As ptid,
			  `posto-trabalho`.eid As eid,
			  `posto-trabalho`.nome As nome,
			  `posto-trabalho`.foto As foto		  
			From
				`posto-trabalho` Left Join
					(SELECT * FROM `maquina-to-posto_trabalho` WHERE `maquina-to-posto_trabalho`.mid = ?) maquina ON `posto-trabalho`.ptid = maquina.ptid
				WHERE maquina.mid IS NULL
				AND eid = ?
				ORDER BY nome ASC";
				
			$sql_todos_trabalhadores = "Select
				maquina.mid As mid,
				trabalhador.tid As tid, 
				trabalhador.eid As eid,
				trabalhador.nome As nome,
				trabalhador.num_mecanografico As num_mecanografico,
				trabalhador.foto As foto
			From
				trabalhador Left Join (Select * FROM `maquina-to-trabalhador` WHERE `maquina-to-trabalhador`.mid = ?) maquina ON trabalhador.tid = maquina.tid
			WHERE maquina.mid IS NULL
			AND eid = ?
			ORDER BY nome ASC";
			
			$sql_check50s = "Select * from check50 where mid = ? AND eid = ?";
			

			  
			$maquinas = query($sql_maquinas, $mid, $_SESSION['cur_eid']);
			
			if (!empty($maquinas[0])) // se há máquinas com este id
			{
				// a máquina que queremos está na linha 0
				$maquina = $maquinas[0];
			
				// dado que a máquina existe, podemos executar as outras queries
				$trabalhadores = query($sql_trabalhadores, $mid, $_SESSION['cur_eid']);
				$manutencoes = query($sql_manutencoes, $mid, $_SESSION['cur_eid']);
				$revisoes = query($sql_revisoes, $mid, $_SESSION['cur_eid']);
				$postos_trabalho =  query($sql_postos_trabalho, $mid, $_SESSION['cur_eid']);
				$forrequeridas =  query($sql_forrequeridas, $mid, $_SESSION['cur_eid']);
				$regacidentes =  query($sql_regacidentes, $mid, $_SESSION['cur_eid']);
				$todos_ptrabalhos =  query($sql_todos_ptrabalhos, $mid, $_SESSION['cur_eid']);
				$todos_trabalhadores =  query($sql_todos_trabalhadores, $mid, $_SESSION['cur_eid']);
				$todos_trabalhadores =  query($sql_todos_trabalhadores, $mid, $_SESSION['cur_eid']);
				$check50s = query($sql_check50s, $mid, $_SESSION['cur_eid']);

				// mostra os detalhes da máquina
				render("maquina_visualizar.php", array("title" => "Máquina - " . $maquina['maquina'], "mid" => $mid, "maquina" => $maquina, "trabalhadores" => $trabalhadores, "manutencoes" => $manutencoes, "revisoes" => $revisoes, "postos_trabalho" => $postos_trabalho, "forrequeridas" => $forrequeridas, "regacidentes" => $regacidentes, "todos_ptrabalhos" => $todos_ptrabalhos, "todos_trabalhadores" => $todos_trabalhadores, "check50s" => $check50s));
			}
				
			else
				apologize("A máquina especificada não existe...");
				
		}
		
		// se não for especificado o tid do trabalhador no URL, mostra lista de trabalhadores
		else
		{
			$maquinas = query("SELECT * FROM maquina WHERE eid = ?", $_SESSION['cur_eid']);
			if (!empty($maquinas[0]))
				$maquina = $maquinas[0];
			
			// mostra lista de maquinas
			render("maquina_lista.php", array("title" => "Lista de Máquinas", "maquinas" => $maquinas));
		}
	}
?>

