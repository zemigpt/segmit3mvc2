<?php

    // configuration
    require_once("includes/config.php");

	
	
	/************************************************************** 
	 *
	 * Trata dos casos em que o ficheiro é usado com $_POST
	 *
	 *************************************************************/
	 
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        
		// verifica se estamos a editar um trabalhador existente ou a adicionar um novo
		if ($_POST['action']=="edit")
		{
			if (isset($_POST['trab_id']))
			{
				$upload_result = UploadFile($_FILES["trab_foto"], "image");
				if ($upload_result != "ERR")
				{
					$foto_url = $upload_result;
				}
				else
				{
					$foto_url = "";
				}			
				
				// se o upload de foto nova falhou, continua a usar o existente
				if  (!empty($_FILES["trab_foto"]) && $foto_url=="" && !empty($_POST['trab_foto_existente']))
					$foto_url = $_POST['trab_foto_existente'];
				
				$sql = "UPDATE trabalhador 
						SET nome = ?, foto = ?, num_mecanografico = ?, morada = ?, codpostal = ?, localidade = ?, telefone = ?, email = ?, contacto_sos_nome = ?, contacto_sos_telefone = ?, bi = ?, bi_validade = ?, niss = ?, nif = ?, apolice_seguro = ?, tamanho_sapato = ?, tamanho_roupa = ? 
						WHERE tid = ? AND eid = ?";
				
					
				if (query($sql, $_POST['trab_nome'], $foto_url, $_POST['trab_num_mecanografico'], $_POST['trab_morada'], $_POST['trab_codpostal'], $_POST['trab_localidade'], $_POST['trab_telefone'], $_POST['trab_email'], $_POST['trab_contacto_sos_nome'], $_POST['trab_contacto_sos_telefone'], $_POST['trab_bi'], $_POST['trab_bi_validade'], $_POST['trab_niss'], $_POST['trab_nif'], $_POST['trab_apolice_seguro'], $_POST['trab_tamanho_sapato'], $_POST['trab_tamanho_roupa'], $_POST['trab_id'], $_SESSION['cur_eid'] ) === false)
				{
					apologize("Algo correu mal ao editar o trabalhador na base de dados.");
				}
				else
				{

									
					// redirect to homepage
					redirect("trabalhador.php?id=" . $_POST['trab_id']);
					
				}
			}
			// não temos tid para identificar o trabalhador - aborta
			else
				apologize("Trabalhador não identificado - impossível editar");
			
		
		}
		elseif 	($_POST['action']=="add")
		{
			$upload_result = UploadFile($_FILES["trab_foto"], "image");
			if ($upload_result != "ERR")
			{
				$foto_url = $upload_result;
			}
			else
			{
				$foto_url = "";
			}			
            
            
            $sql = "INSERT INTO trabalhador 
					(nome, foto, num_mecanografico, morada, codpostal, localidade, telefone, email, contacto_sos_nome, contacto_sos_telefone, bi, bi_validade, niss, nif, apolice_seguro, eid) 
					VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";           
            
            if (query($sql, $_POST['trab_nome'], $foto_url, $_POST['trab_num_mecanografico'], $_POST['trab_morada'], $_POST['trab_codpostal'], $_POST['trab_localidade'], $_POST['trab_telefone'], $_POST['trab_email'], $_POST['trab_contacto_sos_nome'], $_POST['trab_contacto_sos_telefone'], $_POST['trab_bi'], $_POST['trab_bi_validade'], $_POST['trab_niss'], $_POST['trab_nif'], $_POST['trab_apolice_seguro'], $_SESSION['cur_eid'] ) === false)
            {
                apologize("Algo correu mal ao inserir o novo trabalhador na base de dados.");
            }
            else
            {

				
				$rows = query("SELECT LAST_INSERT_ID() AS tid");
				
				$tid = $rows[0]["tid"];
								
				// redirect to homepage
                redirect("trabalhador.php?id=" . $tid);
				
			}
        }  
        elseif 	($_POST['action']=="delete")
		{
			$sql ="DELETE FROM trabalhador WHERE tid = ? AND eid = ?";
			query($sql, $_POST['trab_id'], $_SESSION['cur_eid']);
			// redirect to lista de trabalhadores
            redirect("trabalhador.php");
		}
		// não foi especificada acção no formulário - aborta
		else
			apologize("Não foi especificada acção no formulário...");
    }
	
	
	
	
	/************************************************************** 
	 *
	 * Trata dos casos em que o ficheiro é usado com $_GET
	 *
	 *************************************************************/
	 
    else
    {
		//se for especificada uma acção no URL
		if (isset($_GET['action']))
		{
			// caso o utilizador queira editar um trabalhador
			if ($_GET['action'] == 'edit')
			{
				if (isset($_GET['id']))
				{
					$tid = $_GET['id']; 
					
					$trabalhadores  = query("SELECT * FROM trabalhador WHERE tid = ? AND eid = ?", $tid, $_SESSION['cur_eid']);
					
					// se existe trabalhador com este tid
					if (!empty($trabalhadores[0])) 
					{
						$trabalhador = $trabalhadores[0];
						//constroi as várias queries necessárias
						
					
					}
					
					// a query devolveu uma lista vazia
					else 
						apologize("O trabalhador especificado não existe...");
				
					render("trabalhador_form_editar.php", array("title" => "Editar Trabalhador", "trabalhador" => $trabalhador));
				}
				
				// o utilizador quer editar, mas falta o tid no URL, logo não sabemos qual é o trabalhador a editar
				else 
					apologize("Não foi especificado qual o trabalhador a editar.");
			}
			
			// caso o utilizador queira adicionar um trabalhador
			elseif ($_GET['action'] == 'add')
			{
				 render("trabalhador_form_adicionar.php", array("title" => "Adicionar Trabalhador"));
			}
			
			// caso o utilizador queira apagar um trabalhador
			elseif ($_GET['action'] == 'delete')
			{
				if (isset($_GET['id']))
				{
					$tid = $_GET['id']; 
					
					$trabalhadores  = query("SELECT * FROM trabalhador WHERE tid = ? AND eid = ?", $tid, $_SESSION['cur_eid']);
					
					// se existe trabalhador com este tid
					if (!empty($trabalhadores[0])) 
					{
						$trabalhador = $trabalhadores[0];
					}
					
					// a query devolveu uma lista vazia
					else 
						apologize("Remover trabalhador - O trabalhador especificado não existe...");
				
					render("trabalhador_form_remover.php", array("title" => "Remover Trabalhador", "trabalhador" => $trabalhador));
				}
				
				// o utilizador quer editar, mas falta o tid no URL, logo não sabemos qual é o trabalhador a editar
				else 
					apologize("Não foi especificado qual o trabalhador a remover.");
			}
			
			else
				apologize("Operação não suportada.");
		
		}		
		
		
		//se não houver acção especificada no URL, mas houver um tid
		elseif (isset($_GET['id'])) 
		{
			$tid = $_GET['id']; 
			
			//constroi as várias queries necessárias
			$trabalhadores  = query("SELECT * FROM trabalhador WHERE tid = ? AND eid = ?", $tid, $_SESSION['cur_eid']);
			$sql_maquinas = "SELECT * FROM maquina 
								INNER JOIN `maquina-to-trabalhador` ON maquina.mid = `maquina-to-trabalhador`.mid
								WHERE `maquina-to-trabalhador`.tid = ?";
								
			$sql_ptrabalhos = "Select
			  `posto-trabalho`.nome As nome,
			  `posto-trabalho`.foto As foto,
			  `posto-trabalho`.ptid,
			  trabalhador.tid
			From
			  `posto-trabalho` Inner Join
			  `posto-to-trabalhador` On `posto-trabalho`.ptid = `posto-to-trabalhador`.ptid
			  Inner Join
			  trabalhador On `posto-to-trabalhador`.tid = trabalhador.tid
			Where
			  trabalhador.tid = ?";
			  
			$sql_formacaodetidas = "Select
			  formacao.for_id As for_id,
			  `formacao-to-trabalhador`.tid,
			  formacao.tema As tema,
			  formacao.duracao As duracao,
			  trabalhador.eid
			From
			  formacao Inner Join
			  `formacao-to-trabalhador`
				On formacao.for_id = `formacao-to-trabalhador`.for_id Inner Join
			  trabalhador On `formacao-to-trabalhador`.tid = trabalhador.tid
			Where
			  `formacao-to-trabalhador`.tid = ? And trabalhador.eid = ?";
			  
			$sql_forrequeridas = "Select
			  formacao.tema As tema,
			  formacao.duracao As duracao,
			  formacao.for_id As for_id,
			  maquina.nome As maq_nome,
			  trabalhador.eid As eid,
			  trabalhador.tid As tid,
			  maquina.mid As mid
			From
			  formacao Inner Join
			  `formacao-to-tipomaquina`
				On formacao.for_id = `formacao-to-tipomaquina`.for_id Inner Join
			  tipo_maquina On `formacao-to-tipomaquina`.tm_id = tipo_maquina.tm_id
			  Inner Join
			  maquina On tipo_maquina.tm_id = maquina.tm_id Inner Join
			  `maquina-to-trabalhador` On maquina.mid = `maquina-to-trabalhador`.mid
			  Inner Join
			  trabalhador On trabalhador.tid = `maquina-to-trabalhador`.tid
			Where
			  trabalhador.tid = ? And
			  trabalhador.eid = ? ";
			  
			$sql_regacidentes = "Select
			  registo_acidente.ra_id,
			  registo_acidente.eid,
			  trabalhador.nome,
			  trabalhador.tid,
			  trabalhador.eid,
			  registo_acidente.estado As estado,
			  registo_acidente.data As data
			From
			  registo_acidente Inner Join
			  `trabalhador-to-reg_acidente` On registo_acidente.ra_id =
				`trabalhador-to-reg_acidente`.ra_id Inner Join
			  trabalhador On `trabalhador-to-reg_acidente`.tid = trabalhador.tid
			Where
			  trabalhador.tid = ? AND trabalhador.eid = ?";
			  
			$sql_todas_formacoes ="Select
			  formacao.for_id As for_id,
			  formacao.eid As eid,
			  formacao.tema As tema,
			  trabalhador.tid As tid
			From
			  formacao Left Join
			  (Select * From `formacao-to-trabalhador` Where `formacao-to-trabalhador`.tid = ?) trabalhador
				On formacao.for_id = trabalhador.for_id 
			Where
			  (trabalhador.tid IS NULL) 
			   And formacao.eid = ?
			Order By formacao.for_id ASC";
			
			$sql_todos_ptrabalhos = " Select
			  `posto-trabalho`.ptid As ptid,
			  `posto-trabalho`.eid As eid,
			  `posto-trabalho`.foto As foto,
			  `posto-trabalho`.nome As nome,
			  trabalhador.tid As tid
			From
			  `posto-trabalho` Left Join
				(SELECT * FROM `posto-to-trabalhador` WHERE `posto-to-trabalhador`.tid = ?) trabalhador ON `posto-trabalho`.ptid = trabalhador.ptid 
			WHERE trabalhador.tid IS NULL
			AND eid = ?
			ORDER BY nome ASC";
			
			$sql_todas_maquinas = "Select
			  trabalhador.tid As tid,
			  maquina.nome As nome,
			  maquina.num_serie As num_serie,
			  maquina.foto As foto,
			  maquina.mid As mid,
			  maquina.eid As eid
			From
			  maquina Left Join 
				(SELECT * FROM `maquina-to-trabalhador` WHERE `maquina-to-trabalhador`.tid = ?) trabalhador ON maquina.mid = trabalhador.mid
			WHERE trabalhador.tid IS NULL
			AND EID = ?
			ORDER BY nome ASC";
			
			$sql_epis = "Select
			  epi.nome As nome,
			  `posto-trabalho`.ptid As ptid,
			  epi.epi_id As epi_id,
			  trabalhador.tid As tid,
			  trabalhador.eid As eid
			From
			  epi Inner Join
			  `posto-to-epi` On `posto-to-epi`.epi_id = epi.epi_id Inner Join
			  `posto-trabalho` On `posto-trabalho`.ptid = `posto-to-epi`.ptid Inner Join
			  `posto-to-trabalhador` On `posto-trabalho`.ptid = `posto-to-trabalhador`.ptid
			  Inner Join
			  trabalhador On `posto-to-trabalhador`.tid = trabalhador.tid
			Where
			  trabalhador.tid = ? And trabalhador.eid = ?
			  ORDER BY nome ASC";
			  
			$sql_riscos = "Select
			  posto_avaliacao.pta_id As pta_id,
			  posto_avaliacao.ptid As ptid,
			  posto_avaliacao.perigo As perigo,
			  posto_avaliacao.grau_perigosidade As grau_perigosidade,
			  posto_avaliacao.risco As risco,
			  posto_avaliacao.medidas As medidas,
			  trabalhador.tid As tid,
			  trabalhador.eid
			From
			  posto_avaliacao Inner Join
			  `posto-to-trabalhador` On posto_avaliacao.ptid = `posto-to-trabalhador`.ptid
			  Inner Join
			  trabalhador On trabalhador.tid = `posto-to-trabalhador`.tid
			Where
			  trabalhador.tid = ? AND trabalhador.eid = ?
			  ORDER BY posto_avaliacao.grau_perigosidade DESC";
		

						
			if (!empty($trabalhadores[0])) // se há trabalhadores com este tid
			{
				// o trabalhador que queremos está na linha 0
				$trabalhador = $trabalhadores[0];
				
				// dado que o trabalhador existe podemos executar as outras queries
				$maquinas = query($sql_maquinas, $tid);
				$ptrabalhos = query($sql_ptrabalhos, $tid);
				$formacaodetidas = query($sql_formacaodetidas, $tid, $_SESSION['cur_eid']);
				$forrequeridas = query($sql_forrequeridas, $tid, $_SESSION['cur_eid']);
				$regacidentes = query($sql_regacidentes, $tid, $_SESSION['cur_eid']);
				$todas_formacoes = query($sql_todas_formacoes, $tid, $_SESSION['cur_eid']);
				$todos_ptrabalhos = query($sql_todos_ptrabalhos, $tid, $_SESSION['cur_eid']);
				$todas_maquinas = query($sql_todas_maquinas, $tid, $_SESSION['cur_eid']);
				$epis = query($sql_epis, $tid, $_SESSION['cur_eid']);
				$riscos = query($sql_riscos, $tid, $_SESSION['cur_eid']);
				
				// mostra o trabalhador
				render("trabalhador_visualizar.php", array("title" => "Trabalhador - " . $trabalhador['nome'], "trabalhador" => $trabalhador, "tid" => $tid, "maquinas" => $maquinas, "ptrabalhos" => $ptrabalhos, "formacaodetidas" => $formacaodetidas, "forrequeridas" => $forrequeridas, "regacidentes" => $regacidentes, "todas_formacoes" => $todas_formacoes, "todos_ptrabalhos" => $todos_ptrabalhos, "todas_maquinas" => $todas_maquinas, "epis" => $epis, "riscos" => $riscos));
			}
			else // a query devolveu uma lista vazia
				apologize("O trabalhador especificado não existe...");

		
		}
		
		
		
		// se não for especificado o tid do trabalhador no URL, mostra lista de trabalhadores
		else
		{
			$trabalhadores = query("SELECT * FROM trabalhador WHERE eid = ?", $_SESSION['cur_eid']);
			render("trabalhador_lista.php", array("title" => "Lista de Trabalhadores", "trabalhadores" => $trabalhadores));
		}
		
    }

?>

