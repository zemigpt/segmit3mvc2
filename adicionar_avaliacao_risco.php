<?php

    // configuration
    require_once("includes/config.php");
	
    // if form was submitted
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        /**************
		*
		* Validation
		*
		*****************/
		
		/*if (empty($_POST["field"]))
        {
            apologize("O campo 'field' � obrigat�rio!");
        }
        
        else
        {*/
            // if none of the above occurred, odds are everything is ok
            // so insert new record in database
                   
           
		    // grava o ficheiro
            // code adapted from http://www.w3schools.com/php/php_file_upload.asp
			
			
			/*$upload_result = UploadFile($_FILES["trab_foto"]);
			if ($upload_result != "ERR")
			{
				$foto_url = $upload_result;
			}
			else
			{
				$foto_url = "";
			}			
            */
 			

			
            $sql = "INSERT INTO posto_avaliacao 
					(data, tarefa, perigo, probabilidade, exposicao, consequencia, grau_perigosidade, factor_custo, grau_correccao, indice_justificacao, risco, medidas, ptid, eid) 
					VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            
            
            
			
            if (query($sql, 
			$_POST['posto_avaliacao_data'],
			$_POST['posto_avaliacao_tarefa'],
			$_POST['posto_avaliacao_perigo'], 
			$_POST['posto_avaliacao_probabilidade'],
			$_POST['posto_avaliacao_exposicao'], 
			$_POST['posto_avaliacao_consequencia'], 
			$_POST['posto_avaliacao_grau_perigosidade_valor'],
			$_POST['posto_avaliacao_factor_custo'], 
			$_POST['posto_avaliacao_grau_correccao'], 
			$_POST['posto_avaliacao_indice_justificacao_valor'], 
			$_POST['posto_avaliacao_risco'],
			$_POST['posto_avaliacao_medidas'],
			$_POST['posto-trabalho_ptid'],
			$_SESSION['cur_eid'] ) === false)
            {
                
				apologize("Algo correu mal ao inserir a nova an�lise de risco na base de dados.");
            }
            else
            {

				
				$rows = query("SELECT LAST_INSERT_ID() AS risco_id");
				$risco_id = $rows[0]["risco_id"];
								
				// redirect to homepage
                redirect("index.php?ptid=" . $_POST['posto-trabalho_ptid']);
				
			}
            
            
            
        /*}
		
		**********************/
    }
    else
    {				
        // else render form
        render("adicionar_avaliacao_risco_form.php", array("title" => "Adicionar Avaliacao de Risco", "ptid" => $_GET['ptid'], "eid" => $_SESSION['cur_eid']));
    }

?>