<?php

    // configuration
    require_once("includes/config.php");

    // if form was submitted
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        /**************
		*
		* Validation
		*
		*****************/
		
		/*if (empty($_POST["field"]))
        {
            apologize("O campo 'field' é obrigatório!");
        }
        
        else
        {*/
            // if none of the above occurred, odds are everything is ok
            // so insert new record in database
                   
           
		    // grava o ficheiro
            // code adapted from http://www.w3schools.com/php/php_file_upload.asp
			
			$upload_result = UploadFile($_FILES["formacao_relatorio"], "pdf");
			if ($upload_result != "ERR")
			{
				$relatorio_url = $upload_result;
			}
			else
			{
				$relatorio_url = "";
			}			
			
			
			$sql = "INSERT INTO formacao 
					(tema, duracao, entidade_formadora, relatorio, eid) 
					VALUES(?, ?, ?, ?, ?)";
            
            
            
            
            if (query($sql, $_POST['formacao_tema'], $_POST['formacao_duracao'], $_POST['formacao_entidade_formadora'], $relatorio_url, $_SESSION['cur_eid'] ) === false)
            {
                apologize("Algo correu mal ao inserir a nova formação na base de dados.");
            }
            else
            {

				
				$rows = query("SELECT LAST_INSERT_ID() AS for_id");
				
				$for_id = $rows[0]["for_id"];
								
				// redirect to homepage
                redirect("index.php?eid=" . $_SESSION['cur_eid']);
				
			}
            
            
            
        /*}
		
		**********************/
    }
    else
    {
        // else render form
        render("adicionar_formacao_form.php", array("title" => "Adicionar Formação", "eid" => $_SESSION['cur_eid']));
    }

?>

