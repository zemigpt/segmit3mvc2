<?php

    // configuration
    require_once("includes/config.php"); 

	
	if (isset($_GET['eid'])){
		$eid = $_GET['eid'];
		if (eid_autorizado($eid, $_SESSION['id']))
			$_SESSION['cur_eid']=$eid;
		else {
			apologize("A empresa escolhida não está na lista de empresas do utilizador!");
			exit();
		}
		$trabalhadores = query("SELECT * FROM trabalhador WHERE eid = ?", $eid);
		
		// $trabalhadores = array();
		// dump($rows);
		// foreach ($rows as $row)
		// {
			// $trabalhadores[] = array(
									// 'nome' => $row['nome'],
									// 'foto' => $row['foto'],
									
			
			// );
		// }
		
		// render dashboard empresa
		// render("dashboard_empresa.php", array("title" => "Dashboard", "trabalhadores" => $trabalhadores, "eid" => $eid));
		redirect("trabalhador.php");
	}
	
	elseif (isset($_GET['tid']))
	{
		apologize("Estás a usar a versão antiga, actualiza os links para usarem a página 'trabalhador.php'");
	}
	
	elseif (isset($_GET['mid'])){
		
		apologize("Estás a usar a versão antiga, actualiza os links para usarem a página 'maquina.php'");
	}
	
	elseif (isset($_GET['ptid'])){
	
		apologize("Estás a usar a versão antiga, actualiza os links para usarem a página 'ptrabalho.php'");
			
	}
	
	elseif (isset($_GET['for_id']))  //início de dados sobre formacoes
	{
		apologize("Estás a usar a versão antiga, actualiza os links para usarem a página 'formacao.php'");
	}
	
	elseif (isset($_GET['ra_id'])) //início de dados sobre registo acidentes
	{
		apologize("Estás a usar a versão antiga, actualiza os links para usarem a página 'regacidente.php'");
	}
	
	elseif (isset($_GET['tm_id'])){
	
		apologize("Estás a usar a versão antiga, actualiza os links para usarem a página 'tmaquina.php'");
	}
	
	else {
		
		// retrieve the groups the user belongs to
		$empresas = $_SESSION["eid"];
		
		//dump($empresas);
		/*dump($groups);
		foreach ($groups as $group)
		{
			$empresa = query("SELECT * FROM groups WHERE eid = ?", $group);
			
			$empresas[] = array(
				"nome" => $empresa[0]["nome"],
				"eid" => $empresa[0]["eid"],
			);
		}*/
		
		// render dashboard
		render("dashboard.php", array("title" => "Dashboard", "empresas" => $empresas));
	
	}
	

?>
