<?php

    // configuration
    require_once("includes/config.php");

    // if form was submitted
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        /**************
		*
		* Validation
		*
		*****************/
		
		/*if (empty($_POST["field"]))
        {
            apologize("O campo 'field' � obrigat�rio!");
        }
        
        else
        {*/
            // if none of the above occurred, odds are everything is ok
            // so insert new record in database
                   
           
		    // grava o ficheiro
            // code adapted from http://www.w3schools.com/php/php_file_upload.asp            
            
            $sql = "INSERT INTO fabricante
					(nome, eid) 
					VALUES(?,?)";
            
            
            
       
            if (query($sql, $_POST['fabricante_nome'], $_SESSION['cur_eid'] ) === false)
            {
                apologize("Algo correu mal ao inserir o novo fabricante na base de dados.");
            }
            else
            {

				
				$rows = query("SELECT LAST_INSERT_ID() AS fid");
				
				$fid = $rows[0]["fid"];
				
				
				if (!isset($_POST["ajax"]) || $_POST["ajax"] != "true") {
					// redirect to homepage
					redirect("index.php?eid=" . $_SESSION['cur_eid']);
				}
				else
				{
					echo json_encode(array("fid" => $fid, "fabricante_nome" => $_POST['fabricante_nome']));
				}
			}
            
            
            
        /*}
		
		**********************/
    }
    else
    {
        // else render form
        render("adicionar_fabricante.php", array("title" => "Adicionar Fabricante", "eid" => $_SESSION['cur_eid']));
    }

?>

