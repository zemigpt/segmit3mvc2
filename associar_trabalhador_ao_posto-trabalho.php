<?php

    // configuration
    require("includes/config.php");

    // if form was submitted
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {	
		if ($_POST['tid'] == 0)
			apologize("Tem de escolher um trabalhador da lista!");
		
		// verifica se o formulário não foi adulterado com SQL injections
		$sql_tid = "SELECT * FROM trabalhador WHERE tid = ? AND eid = ?";
		$sql_ptid = "SELECT * FROM `posto-trabalho` WHERE ptid = ? AND eid = ?";
		
		$rows_tid = query($sql_tid, $_POST['tid'], $_SESSION['cur_eid']);
		$rows_ptid = query($sql_ptid, $_POST['ptid'], $_SESSION['cur_eid']);
		
		if ((($rows_tid === false) || empty($rows_tid)) || (($rows_ptid === false) || empty($rows_ptid)))
			apologize("Os dados para associar o trabalhador ao posto de trabalho foram corrompidos.");
		else
		{
			$sql = "INSERT INTO `posto-to-trabalhador`
			(ptid, tid) 
			VALUES (?, ?)";
			
			if (query($sql, $_POST['ptid'], $_POST['tid']) === false)
				apologize("Algo correu mal ao associar o trabalhador ao posto de trabalho na base de dados.");
			else
				redirect("ptrabalho.php?id=" . $_POST['ptid']);
		}
	}
	else
		apologize("Algo correu mal ao associar trabalhador ao posto de trabalho");
?>