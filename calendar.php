<?php
    
    // configuration
    require("includes/config.php");
    
    if (isset($_SESSION["baby_id"]))
    {
        $baby_id = $_SESSION["baby_id"];
    
        // Get values from query string
        $day = (isset($_GET["day"])) ? $_GET['day'] : "";
        $month = (isset($_GET["month"])) ? $_GET['month'] : "";
        $year = (isset($_GET["year"])) ? $_GET['year'] : "";
        
        //comparaters for today's date
        if(empty($day)){ $day = date("d"); }
        if(empty($month)){ $month = date("m"); }
        if(empty($year)){ $year = date("Y"); } 

        //set up vars for calendar etc
        $currentTimeStamp = strtotime("$year-$month-$day");
        $monthName = date("F", $currentTimeStamp);
        $numDays = date("t", $currentTimeStamp);
        $counter = 0;

        
        $calendar_data = array();
        
        for ($i=1; $i<$numDays+1; $i++)
        {
            $sql="SELECT * FROM calendar WHERE baby_id = ? AND start_date_time LIKE '%" . str_pad($year, 2, "0", STR_PAD_LEFT) . '-' . str_pad($month, 2, "0", STR_PAD_LEFT) . '-' . str_pad($i, 2, "0", STR_PAD_LEFT) . "%'";
                
            $result = query($sql, $baby_id);
            $calendar_data[$i]=array();
            
            foreach($result as $row)
            {
                $calendar_data[$i]["events"][] = array(
                                                  "start_date_time" => $row["start_date_time"],
                                                  "calendar_description" => $row["calendar_description"],
                                                  "calendar_id" => $row["calendar_id"]
                                                  );
                                                                        
            }
            
            $dateday = strtotime($year . '-' . $month . '-' . str_pad($i, 2, "0", STR_PAD_LEFT));
            $today = strtotime(date("Y") . '-' . date("m") . '-' . date("d")); 
            
            if ($dateday == $today)
            {
                $calendar_data[$i]["class"] = "today";
            }
             
        
        }
        
        //dump($calendar_data);
           
        $data = array(
            "month" => $month,
            "day" => $day,
            "year" => $year,
            "monthName" => $monthName,
            "numDays" => $numDays,
            "counter" => $counter
        );
        
        render("calendar_table.php", array("title" => "Calendar", "data" => $data, "calendar_data" => $calendar_data));
        
    }
    else
    {
        apologize("There are no babies selected yet, so there's no calendar to show.");
    }
    
?>
